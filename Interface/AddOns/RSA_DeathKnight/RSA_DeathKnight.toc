## Interface: 70300
## Title: RSA [|c50C41F3BDeath Knight|r]
## Notes: RSA Death Knight Module. This is needed to announce Death Knight Spells.
## Author: Raeli, Jenneth, Xadurk
## Dependencies: RSA
## LoadOnDemand: 1

## Title-zhTW: RSA [|c50C41F3B死亡騎士|r]
## Notes-zhTW: RSA 死亡騎士模組。通報死亡騎士的技能。


DeathKnight.lua
