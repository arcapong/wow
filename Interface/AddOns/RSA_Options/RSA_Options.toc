## Interface: 70300
## Title: RSA [|c11FF75B3Options|r]
## Notes: Load on Demand configuration options for Raeli's Spell Announcer.
## Author: Raeli, Jenneth, Xadurk
## Dependencies: RSA
## LoadOnDemand: 1

## Title-zhTW: RSA [|c11FF75B3選項|r]
## Notes-zhTW: 載入RSA需求配置選項。


Options.lua
