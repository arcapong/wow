## Interface: 70300
## Title: RSA [|c5500DBBDMonk|r]
## Notes: RSA Monk Module. This is needed to announce Monk Spells.
## Author: Raeli, Jenneth, Xadurk
## Dependencies: RSA
## LoadOnDemand: 1

## Title-zhTW: RSA [|c5700DBBD武僧|r]
## Notes-zhTW: RSA 武僧模組。通報武僧的技能。


Monk.lua
