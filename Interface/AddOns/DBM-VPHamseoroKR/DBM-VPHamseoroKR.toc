﻿## Interface: 70100
## X-Min-Interface: 70100
## Title:|cffffe00a<|r|cffff7d0aDBM Voicepack|r|cffffe00a>|r |cff69ccf0Hamseoro (Korean Female)|r
## Title-koKR:|cffffe00a<|r|cffff7d0aDBM 음성팩|r|cffffe00a>|r |cff69ccf0함서로 (한국어 여성)|r
## DefaultState: enabled
## RequiredDeps: DBM-Core
## Author: Elnarfim
## Version: 2.5
## X-DBM-Voice: 1
## X-DBM-Voice-Name: 함서로 (한국어 여성)
## X-DBM-Voice-ShortName: HamseoroKR
## X-DBM-Voice-Version: 6
## X-DBM-Voice-HasCount: 1