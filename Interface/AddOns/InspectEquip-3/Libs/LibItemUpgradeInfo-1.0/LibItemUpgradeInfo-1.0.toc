## Interface: 70300
## Title: Lib: ItemUpgradeInfo-1.0
## Notes: Database of item upgrade IDs
## Author: eridius
## Version: Release-70300-29-1-g9f23d3f 70200
## X-Revision: 9f23d3f
## X-Category: Library

LibItemUpgradeInfo-1.0.xml
