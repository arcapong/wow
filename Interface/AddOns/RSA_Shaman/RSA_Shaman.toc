## Interface: 70300
## Title: RSA [|c590055FFShaman|r]
## Notes: RSA Shaman Module. This is needed to announce Shaman Spells.
## Author: Raeli, Jenneth, Xadurk
## Dependencies: RSA
## LoadOnDemand: 1

## Title-zhTW: RSA [|cff0055FF薩滿|r]
## Notes-zhTW: RSA 薩滿模組。通報薩滿的技能。


Shaman.lua
