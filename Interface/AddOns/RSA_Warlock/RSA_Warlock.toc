## Interface: 70300
## Title: RSA [|c608245abWarlock|r]
## Notes: RSA Warlock Module. This is needed to announce Warlock Spells.
## Author: Raeli, Jenneth, Xadurk
## Dependencies: RSA
## LoadOnDemand: 1

## Title-zhTW: RSA [|c588245ab術士|r]
## Notes-zhTW: RSA 術士模組。通報術士的技能。


Warlock.lua
