
Blizzard_Console_SavedVars = {
	["version"] = 3,
	["messageHistory"] = {
		{
			"LightBuffer mode changed to 2", -- [1]
			0, -- [2]
		}, -- [1]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [2]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [3]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [4]
		{
			"Proficiency in item class 2 set to 0x0000000080", -- [1]
			0, -- [2]
		}, -- [5]
		{
			"Proficiency in item class 2 set to 0x0000000081", -- [1]
			0, -- [2]
		}, -- [6]
		{
			"Proficiency in item class 2 set to 0x0000000091", -- [1]
			0, -- [2]
		}, -- [7]
		{
			"Proficiency in item class 2 set to 0x0000000491", -- [1]
			0, -- [2]
		}, -- [8]
		{
			"Proficiency in item class 4 set to 0x0000000021", -- [1]
			0, -- [2]
		}, -- [9]
		{
			"Proficiency in item class 2 set to 0x0000004491", -- [1]
			0, -- [2]
		}, -- [10]
		{
			"Proficiency in item class 2 set to 0x00000044d1", -- [1]
			0, -- [2]
		}, -- [11]
		{
			"Proficiency in item class 4 set to 0x0000000025", -- [1]
			0, -- [2]
		}, -- [12]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [13]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [14]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [15]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [16]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [17]
		{
			"Time set to 12/15/2017 (Fri) 1:10", -- [1]
			0, -- [2]
		}, -- [18]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [19]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [20]
		{
			"Total: 0d 0h 8m 20s", -- [1]
			0, -- [2]
		}, -- [21]
		{
			"Level: 0d 0h 8m 20s", -- [1]
			0, -- [2]
		}, -- [22]
		{
			"Skill 183 increased from 5 to 10", -- [1]
			0, -- [2]
		}, -- [23]
		{
			"Skill 829 increased from 5 to 10", -- [1]
			0, -- [2]
		}, -- [24]
		{
			"Skill 899 increased from 5 to 10", -- [1]
			0, -- [2]
		}, -- [25]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [26]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [27]
		{
			"Detail doodad instancing enabled.", -- [1]
			0, -- [2]
		}, -- [28]
		{
			"Water detail changed to 0", -- [1]
			0, -- [2]
		}, -- [29]
		{
			"Ripple detail changed to 2", -- [1]
			0, -- [2]
		}, -- [30]
		{
			"Reflection mode changed to 3", -- [1]
			0, -- [2]
		}, -- [31]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [32]
		{
			"Sunshafts quality changed to 0", -- [1]
			0, -- [2]
		}, -- [33]
		{
			"Refraction mode changed to 0", -- [1]
			0, -- [2]
		}, -- [34]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [35]
		{
			"WorldPoolUsage must be stream, dynamic or static.", -- [1]
			0, -- [2]
		}, -- [36]
		{
			"CVar 'worldPoolUsage' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [37]
		{
			"Alpha map bit depth set to 8bit on restart.", -- [1]
			0, -- [2]
		}, -- [38]
		{
			"Hardware PCF enabled.", -- [1]
			0, -- [2]
		}, -- [39]
		{
			"Projected textures disabled.", -- [1]
			0, -- [2]
		}, -- [40]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [41]
		{
			"Shadow mode changed to 0 - Precomputed terrain shadows, blob shadows.", -- [1]
			0, -- [2]
		}, -- [42]
		{
			"Shadow texture size changed to 1024.", -- [1]
			0, -- [2]
		}, -- [43]
		{
			"Soft shadows changed to 0.", -- [1]
			0, -- [2]
		}, -- [44]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [45]
		{
			"set pending client restart", -- [1]
			0, -- [2]
		}, -- [46]
		{
			"set pending gxRestart", -- [1]
			0, -- [2]
		}, -- [47]
		{
			"set pending gxRestart", -- [1]
			0, -- [2]
		}, -- [48]
		{
			"set pending gxRestart", -- [1]
			0, -- [2]
		}, -- [49]
		{
			"set pending gxRestart", -- [1]
			0, -- [2]
		}, -- [50]
		{
			"set pending gxRestart", -- [1]
			0, -- [2]
		}, -- [51]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [52]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [53]
		{
			"Detail doodad instancing enabled.", -- [1]
			0, -- [2]
		}, -- [54]
		{
			"Water detail changed to 1", -- [1]
			0, -- [2]
		}, -- [55]
		{
			"Ripple detail changed to 0", -- [1]
			0, -- [2]
		}, -- [56]
		{
			"Reflection mode changed to 0", -- [1]
			0, -- [2]
		}, -- [57]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [58]
		{
			"Sunshafts quality changed to 0", -- [1]
			0, -- [2]
		}, -- [59]
		{
			"Refraction mode changed to 0", -- [1]
			0, -- [2]
		}, -- [60]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [61]
		{
			"WorldPoolUsage must be stream, dynamic or static.", -- [1]
			0, -- [2]
		}, -- [62]
		{
			"CVar 'worldPoolUsage' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [63]
		{
			"Alpha map bit depth set to 8bit on restart.", -- [1]
			0, -- [2]
		}, -- [64]
		{
			"Hardware PCF enabled.", -- [1]
			0, -- [2]
		}, -- [65]
		{
			"Projected textures disabled.", -- [1]
			0, -- [2]
		}, -- [66]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [67]
		{
			"Shadow mode changed to 0 - Precomputed terrain shadows, blob shadows.", -- [1]
			0, -- [2]
		}, -- [68]
		{
			"Shadow texture size changed to 1024.", -- [1]
			0, -- [2]
		}, -- [69]
		{
			"Soft shadows changed to 0.", -- [1]
			0, -- [2]
		}, -- [70]
		{
			"SSAO mode set to 0", -- [1]
			0, -- [2]
		}, -- [71]
		{
			"SSAO distance value set to 100.000000", -- [1]
			0, -- [2]
		}, -- [72]
		{
			"SSAO blur set to 2", -- [1]
			0, -- [2]
		}, -- [73]
		{
			"Depth Based Opacity Disabled", -- [1]
			0, -- [2]
		}, -- [74]
		{
			"SkyCloudLOD set to 0", -- [1]
			0, -- [2]
		}, -- [75]
		{
			"Terrain mip level changed to 1.", -- [1]
			0, -- [2]
		}, -- [76]
		{
			"Outline mode changed to 0", -- [1]
			0, -- [2]
		}, -- [77]
		{
			"LightBuffer mode changed to 2", -- [1]
			0, -- [2]
		}, -- [78]
		{
			"Physics interaction level changed to 1", -- [1]
			0, -- [2]
		}, -- [79]
		{
			"Render scale changed to 0.5", -- [1]
			0, -- [2]
		}, -- [80]
		{
			"Resample quality changed to 0", -- [1]
			0, -- [2]
		}, -- [81]
		{
			"MSAA disabled", -- [1]
			0, -- [2]
		}, -- [82]
		{
			"MSAA for alpha-test disabled.", -- [1]
			0, -- [2]
		}, -- [83]
		{
			"Component texture lod changed to 1", -- [1]
			0, -- [2]
		}, -- [84]
		{
			"Component texture lod changed to 1", -- [1]
			0, -- [2]
		}, -- [85]
		{
			"World preload object sort enabled.", -- [1]
			0, -- [2]
		}, -- [86]
		{
			"World load object sort enabled.", -- [1]
			0, -- [2]
		}, -- [87]
		{
			"World preload non critical enabled.", -- [1]
			0, -- [2]
		}, -- [88]
		{
			"World preload high res textures enabled.", -- [1]
			0, -- [2]
		}, -- [89]
		{
			"CVar 'fullDump' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [90]
		{
			"Error display disabled", -- [1]
			0, -- [2]
		}, -- [91]
		{
			"Error display shown", -- [1]
			0, -- [2]
		}, -- [92]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [93]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [94]
		{
			"Now filtering: all messages", -- [1]
			0, -- [2]
		}, -- [95]
		{
			"FFX: Anti Aliasing Mode disabled", -- [1]
			0, -- [2]
		}, -- [96]
		{
			"FFX: Color Blind Test Mode Disabled", -- [1]
			0, -- [2]
		}, -- [97]
		{
			"[GlueLogin] Starting loginlauncherPortal=\"kr.actual.battle.net\" loginPortal=\"kr.actual.battle.net:1119\"", -- [1]
			0, -- [2]
		}, -- [98]
		{
			"[GlueLogin] Resetting", -- [1]
			0, -- [2]
		}, -- [99]
		{
			"[IBN_Login] Initializing", -- [1]
			0, -- [2]
		}, -- [100]
		{
			"[IBN_Login] Attempting logonhost=\"kr.actual.battle.net\" port=\"1119\"", -- [1]
			0, -- [2]
		}, -- [101]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [102]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [103]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [104]
		{
			"[GlueLogin] Logon complete.", -- [1]
			0, -- [2]
		}, -- [105]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [106]
		{
			"[IBN_Login] Requesting realm list ticket", -- [1]
			0, -- [2]
		}, -- [107]
		{
			"[IBN_Login] Received realm list ticketcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [108]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [109]
		{
			"[IBN_Login] Received sub region listcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [110]
		{
			"[IBN_Login] Requesting last played charsnumSubRegions=\"4\"", -- [1]
			0, -- [2]
		}, -- [111]
		{
			"[GlueLogin] Realm list ready.", -- [1]
			0, -- [2]
		}, -- [112]
		{
			"[IBN_Login] Joining realmsubRegion=\"2-101-89\" realmAddress=\"2-1-29\"", -- [1]
			0, -- [2]
		}, -- [113]
		{
			"[IBN_Login] OnRealmJoincode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [114]
		{
			"[GlueLogin] Received AuthedToWoWresult=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [115]
		{
			"Got new connection 2", -- [1]
			0, -- [2]
		}, -- [116]
		{
			"[IBN_Login] Front disconnectingconnectionId=\"1\"", -- [1]
			0, -- [2]
		}, -- [117]
		{
			"[GlueLogin] Disconnecting from authentication server.", -- [1]
			0, -- [2]
		}, -- [118]
		{
			"[IBN_BackInterface] Session with Battle.net established.", -- [1]
			0, -- [2]
		}, -- [119]
		{
			"[IBN_Login] Front disconnectedconnectionId=\"1\" result=\"( code=\"ERROR_OK (0)\" localizedMessage=\"\" debugMessage=\"\")\"", -- [1]
			0, -- [2]
		}, -- [120]
		{
			"[GlueLogin] Disconnected from authentication server.", -- [1]
			0, -- [2]
		}, -- [121]
		{
			"[IBN_Login] Requesting change realm list", -- [1]
			0, -- [2]
		}, -- [122]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [123]
		{
			"[IBN_Login] Received realm list ticketcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [124]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [125]
		{
			"[IBN_Login] Received sub region listcode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [126]
		{
			"[IBN_Login] Requesting realm listsnumSubRegions=\"4\"", -- [1]
			0, -- [2]
		}, -- [127]
		{
			"[GlueLogin] Realm list ready.", -- [1]
			0, -- [2]
		}, -- [128]
		{
			"[IBN_Login] Joining realmsubRegion=\"2-101-89\" realmAddress=\"2-1-47\"", -- [1]
			0, -- [2]
		}, -- [129]
		{
			"[IBN_Login] OnRealmJoincode=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [130]
		{
			"NetClient::HandleDisconnect()", -- [1]
			0, -- [2]
		}, -- [131]
		{
			"[IBN_BackInterface] Session with Battle.net destroyed.", -- [1]
			0, -- [2]
		}, -- [132]
		{
			"[GlueLogin] Received AuthedToWoWresult=\"ERROR_OK (0)\"", -- [1]
			0, -- [2]
		}, -- [133]
		{
			"Got new connection 2", -- [1]
			0, -- [2]
		}, -- [134]
		{
			"[IBN_BackInterface] Session with Battle.net established.", -- [1]
			0, -- [2]
		}, -- [135]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [136]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [137]
		{
			"Detail doodad instancing enabled.", -- [1]
			0, -- [2]
		}, -- [138]
		{
			"Water detail changed to 1", -- [1]
			0, -- [2]
		}, -- [139]
		{
			"Ripple detail changed to 0", -- [1]
			0, -- [2]
		}, -- [140]
		{
			"Reflection mode changed to 0", -- [1]
			0, -- [2]
		}, -- [141]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [142]
		{
			"Sunshafts quality changed to 0", -- [1]
			0, -- [2]
		}, -- [143]
		{
			"Refraction mode changed to 0", -- [1]
			0, -- [2]
		}, -- [144]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [145]
		{
			"WorldPoolUsage must be stream, dynamic or static.", -- [1]
			0, -- [2]
		}, -- [146]
		{
			"CVar 'worldPoolUsage' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [147]
		{
			"Alpha map bit depth set to 8bit on restart.", -- [1]
			0, -- [2]
		}, -- [148]
		{
			"Hardware PCF enabled.", -- [1]
			0, -- [2]
		}, -- [149]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [150]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [151]
		{
			"Detail doodad instancing enabled.", -- [1]
			0, -- [2]
		}, -- [152]
		{
			"Water detail changed to 1", -- [1]
			0, -- [2]
		}, -- [153]
		{
			"Ripple detail changed to 0", -- [1]
			0, -- [2]
		}, -- [154]
		{
			"Reflection mode changed to 0", -- [1]
			0, -- [2]
		}, -- [155]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [156]
		{
			"Sunshafts quality changed to 0", -- [1]
			0, -- [2]
		}, -- [157]
		{
			"Refraction mode changed to 0", -- [1]
			0, -- [2]
		}, -- [158]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [159]
		{
			"WorldPoolUsage must be stream, dynamic or static.", -- [1]
			0, -- [2]
		}, -- [160]
		{
			"CVar 'worldPoolUsage' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [161]
		{
			"Alpha map bit depth set to 8bit on restart.", -- [1]
			0, -- [2]
		}, -- [162]
		{
			"Hardware PCF enabled.", -- [1]
			0, -- [2]
		}, -- [163]
		{
			"Projected textures disabled.", -- [1]
			0, -- [2]
		}, -- [164]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [165]
		{
			"Shadow mode changed to 0 - Precomputed terrain shadows, blob shadows.", -- [1]
			0, -- [2]
		}, -- [166]
		{
			"Shadow texture size changed to 1024.", -- [1]
			0, -- [2]
		}, -- [167]
		{
			"Soft shadows changed to 0.", -- [1]
			0, -- [2]
		}, -- [168]
		{
			"SSAO mode set to 0", -- [1]
			0, -- [2]
		}, -- [169]
		{
			"SSAO distance value set to 100.000000", -- [1]
			0, -- [2]
		}, -- [170]
		{
			"SSAO blur set to 2", -- [1]
			0, -- [2]
		}, -- [171]
		{
			"Depth Based Opacity Disabled", -- [1]
			0, -- [2]
		}, -- [172]
		{
			"SkyCloudLOD set to 0", -- [1]
			0, -- [2]
		}, -- [173]
		{
			"Terrain mip level changed to 1.", -- [1]
			0, -- [2]
		}, -- [174]
		{
			"Outline mode changed to 0", -- [1]
			0, -- [2]
		}, -- [175]
		{
			"LightBuffer mode changed to 2", -- [1]
			0, -- [2]
		}, -- [176]
		{
			"Physics interaction level changed to 1", -- [1]
			0, -- [2]
		}, -- [177]
		{
			"Render scale changed to 0.5", -- [1]
			0, -- [2]
		}, -- [178]
		{
			"Resample quality changed to 0", -- [1]
			0, -- [2]
		}, -- [179]
		{
			"MSAA disabled", -- [1]
			0, -- [2]
		}, -- [180]
		{
			"MSAA for alpha-test disabled.", -- [1]
			0, -- [2]
		}, -- [181]
		{
			"Component texture lod changed to 1", -- [1]
			0, -- [2]
		}, -- [182]
		{
			"Component texture lod changed to 1", -- [1]
			0, -- [2]
		}, -- [183]
		{
			"World preload object sort enabled.", -- [1]
			0, -- [2]
		}, -- [184]
		{
			"World load object sort enabled.", -- [1]
			0, -- [2]
		}, -- [185]
		{
			"World preload non critical enabled.", -- [1]
			0, -- [2]
		}, -- [186]
		{
			"World preload high res textures enabled.", -- [1]
			0, -- [2]
		}, -- [187]
		{
			"CVar 'fullDump' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [188]
		{
			"Error display disabled", -- [1]
			0, -- [2]
		}, -- [189]
		{
			"Error display shown", -- [1]
			0, -- [2]
		}, -- [190]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [191]
		{
			"Displaying errors through fatal errors", -- [1]
			0, -- [2]
		}, -- [192]
		{
			"Now filtering: all messages", -- [1]
			0, -- [2]
		}, -- [193]
		{
			"FFX: Anti Aliasing Mode disabled", -- [1]
			0, -- [2]
		}, -- [194]
		{
			"FFX: Color Blind Test Mode Disabled", -- [1]
			0, -- [2]
		}, -- [195]
		{
			"[GlueLogin] Starting loginlauncherPortal=\"kr.actual.battle.net\" loginPortal=\"kr.actual.battle.net:1119\"", -- [1]
			0, -- [2]
		}, -- [196]
		{
			"[GlueLogin] Resetting", -- [1]
			0, -- [2]
		}, -- [197]
		{
			"[IBN_Login] Initializing", -- [1]
			0, -- [2]
		}, -- [198]
		{
			"[IBN_Login] Attempting logonhost=\"kr.actual.battle.net\" port=\"1119\"", -- [1]
			0, -- [2]
		}, -- [199]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [200]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [201]
		{
			"[GlueLogin] Waiting for server response.", -- [1]
			0, -- [2]
		}, -- [202]
		{
			"[GlueLogin] Logon complete.", -- [1]
			0, -- [2]
		}, -- [203]
		{
			"[GlueLogin] Waiting for realm list.", -- [1]
			0, -- [2]
		}, -- [204]
		{
			"[IBN_Login] Requesting realm list ticket", -- [1]
			0, -- [2]
		}, -- [205]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [206]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [207]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [208]
		{
			"Proficiency in item class 2 set to 0x0000000080", -- [1]
			0, -- [2]
		}, -- [209]
		{
			"Proficiency in item class 2 set to 0x0000000081", -- [1]
			0, -- [2]
		}, -- [210]
		{
			"Proficiency in item class 2 set to 0x0000000091", -- [1]
			0, -- [2]
		}, -- [211]
		{
			"Proficiency in item class 2 set to 0x0000000491", -- [1]
			0, -- [2]
		}, -- [212]
		{
			"Proficiency in item class 4 set to 0x0000000021", -- [1]
			0, -- [2]
		}, -- [213]
		{
			"Proficiency in item class 2 set to 0x0000004491", -- [1]
			0, -- [2]
		}, -- [214]
		{
			"Proficiency in item class 2 set to 0x00000044d1", -- [1]
			0, -- [2]
		}, -- [215]
		{
			"Proficiency in item class 4 set to 0x0000000025", -- [1]
			0, -- [2]
		}, -- [216]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [217]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [218]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [219]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [220]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [221]
		{
			"Attempted to register existing command: ShowObjUsage\n", -- [1]
			0, -- [2]
		}, -- [222]
		{
			"Attempted to register existing command: SetDifficulty\n", -- [1]
			0, -- [2]
		}, -- [223]
		{
			"Failed to set SpellClutter callback on graphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [224]
		{
			"Failed to set SpellClutter callback on RAIDgraphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [225]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [226]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [227]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [228]
		{
			"Detail doodad instancing enabled.", -- [1]
			0, -- [2]
		}, -- [229]
		{
			"Water detail changed to 0", -- [1]
			0, -- [2]
		}, -- [230]
		{
			"Ripple detail changed to 0", -- [1]
			0, -- [2]
		}, -- [231]
		{
			"Reflection mode changed to 0", -- [1]
			0, -- [2]
		}, -- [232]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [233]
		{
			"Sunshafts quality changed to 0", -- [1]
			0, -- [2]
		}, -- [234]
		{
			"Refraction mode changed to 0", -- [1]
			0, -- [2]
		}, -- [235]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [236]
		{
			"WorldPoolUsage must be stream, dynamic or static.", -- [1]
			0, -- [2]
		}, -- [237]
		{
			"CVar 'worldPoolUsage' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [238]
		{
			"Alpha map bit depth set to 8bit on restart.", -- [1]
			0, -- [2]
		}, -- [239]
		{
			"Hardware PCF enabled.", -- [1]
			0, -- [2]
		}, -- [240]
		{
			"Projected textures disabled.", -- [1]
			0, -- [2]
		}, -- [241]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [242]
		{
			"Shadow mode changed to 0 - Precomputed terrain shadows, blob shadows.", -- [1]
			0, -- [2]
		}, -- [243]
		{
			"Shadow texture size changed to 1024.", -- [1]
			0, -- [2]
		}, -- [244]
		{
			"Soft shadows changed to 0.", -- [1]
			0, -- [2]
		}, -- [245]
		{
			"SSAO mode set to 0", -- [1]
			0, -- [2]
		}, -- [246]
		{
			"SSAO distance value set to 100.000000", -- [1]
			0, -- [2]
		}, -- [247]
		{
			"SSAO blur set to 2", -- [1]
			0, -- [2]
		}, -- [248]
		{
			"Depth Based Opacity Disabled", -- [1]
			0, -- [2]
		}, -- [249]
		{
			"SkyCloudLOD set to 0", -- [1]
			0, -- [2]
		}, -- [250]
		{
			"Terrain mip level changed to 1.", -- [1]
			0, -- [2]
		}, -- [251]
		{
			"Outline mode changed to 0", -- [1]
			0, -- [2]
		}, -- [252]
		{
			"LightBuffer mode changed to 2", -- [1]
			0, -- [2]
		}, -- [253]
		{
			"Physics interaction level changed to 1", -- [1]
			0, -- [2]
		}, -- [254]
		{
			"Render scale changed to 0.5", -- [1]
			0, -- [2]
		}, -- [255]
		{
			"Resample quality changed to 0", -- [1]
			0, -- [2]
		}, -- [256]
		{
			"MSAA disabled", -- [1]
			0, -- [2]
		}, -- [257]
		{
			"MSAA for alpha-test disabled.", -- [1]
			0, -- [2]
		}, -- [258]
		{
			"Component texture lod changed to 1", -- [1]
			0, -- [2]
		}, -- [259]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [260]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [261]
		{
			"Detail doodad instancing enabled.", -- [1]
			0, -- [2]
		}, -- [262]
		{
			"Water detail changed to 0", -- [1]
			0, -- [2]
		}, -- [263]
		{
			"Ripple detail changed to 0", -- [1]
			0, -- [2]
		}, -- [264]
		{
			"Reflection mode changed to 0", -- [1]
			0, -- [2]
		}, -- [265]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [266]
		{
			"Sunshafts quality changed to 0", -- [1]
			0, -- [2]
		}, -- [267]
		{
			"Refraction mode changed to 0", -- [1]
			0, -- [2]
		}, -- [268]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [269]
		{
			"WorldPoolUsage must be stream, dynamic or static.", -- [1]
			0, -- [2]
		}, -- [270]
		{
			"CVar 'worldPoolUsage' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [271]
		{
			"Alpha map bit depth set to 8bit on restart.", -- [1]
			0, -- [2]
		}, -- [272]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [273]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [274]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [275]
		{
			"Proficiency in item class 2 set to 0x0000000080", -- [1]
			0, -- [2]
		}, -- [276]
		{
			"Proficiency in item class 2 set to 0x0000000081", -- [1]
			0, -- [2]
		}, -- [277]
		{
			"Proficiency in item class 2 set to 0x0000000085", -- [1]
			0, -- [2]
		}, -- [278]
		{
			"Proficiency in item class 2 set to 0x000000008d", -- [1]
			0, -- [2]
		}, -- [279]
		{
			"Proficiency in item class 2 set to 0x000000009d", -- [1]
			0, -- [2]
		}, -- [280]
		{
			"Proficiency in item class 2 set to 0x000000019d", -- [1]
			0, -- [2]
		}, -- [281]
		{
			"Proficiency in item class 2 set to 0x000000059d", -- [1]
			0, -- [2]
		}, -- [282]
		{
			"Proficiency in item class 2 set to 0x00000005bd", -- [1]
			0, -- [2]
		}, -- [283]
		{
			"Proficiency in item class 2 set to 0x00000005bf", -- [1]
			0, -- [2]
		}, -- [284]
		{
			"Proficiency in item class 2 set to 0x00000085bf", -- [1]
			0, -- [2]
		}, -- [285]
		{
			"Proficiency in item class 4 set to 0x0000000021", -- [1]
			0, -- [2]
		}, -- [286]
		{
			"Proficiency in item class 2 set to 0x000000c5bf", -- [1]
			0, -- [2]
		}, -- [287]
		{
			"Proficiency in item class 2 set to 0x000004c5bf", -- [1]
			0, -- [2]
		}, -- [288]
		{
			"Proficiency in item class 2 set to 0x000004c5ff", -- [1]
			0, -- [2]
		}, -- [289]
		{
			"Proficiency in item class 4 set to 0x0000000031", -- [1]
			0, -- [2]
		}, -- [290]
		{
			"Proficiency in item class 2 set to 0x000014c5ff", -- [1]
			0, -- [2]
		}, -- [291]
		{
			"Proficiency in item class 4 set to 0x0000000039", -- [1]
			0, -- [2]
		}, -- [292]
		{
			"Proficiency in item class 4 set to 0x000000003d", -- [1]
			0, -- [2]
		}, -- [293]
		{
			"Proficiency in item class 4 set to 0x000000003f", -- [1]
			0, -- [2]
		}, -- [294]
		{
			"Proficiency in item class 4 set to 0x000000007f", -- [1]
			0, -- [2]
		}, -- [295]
		{
			"Proficiency in item class 2 set to 0x000014e5ff", -- [1]
			0, -- [2]
		}, -- [296]
		{
			"Proficiency in item class 2 set to 0x000014e5ff", -- [1]
			0, -- [2]
		}, -- [297]
		{
			"Proficiency in item class 4 set to 0x000000007f", -- [1]
			0, -- [2]
		}, -- [298]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [299]
		{
			"Time set to 12/15/2017 (Fri) 12:49", -- [1]
			0, -- [2]
		}, -- [300]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [301]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [302]
		{
			"Total: 10d 17h 59m 17s", -- [1]
			0, -- [2]
		}, -- [303]
		{
			"Level: 0d 15h 12m 57s", -- [1]
			0, -- [2]
		}, -- [304]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [305]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [306]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [307]
		{
			"Detail doodad instancing enabled.", -- [1]
			0, -- [2]
		}, -- [308]
		{
			"Water detail changed to 0", -- [1]
			0, -- [2]
		}, -- [309]
		{
			"Ripple detail changed to 0", -- [1]
			0, -- [2]
		}, -- [310]
		{
			"Reflection mode changed to 0", -- [1]
			0, -- [2]
		}, -- [311]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [312]
		{
			"Sunshafts quality changed to 0", -- [1]
			0, -- [2]
		}, -- [313]
		{
			"Refraction mode changed to 0", -- [1]
			0, -- [2]
		}, -- [314]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [315]
		{
			"WorldPoolUsage must be stream, dynamic or static.", -- [1]
			0, -- [2]
		}, -- [316]
		{
			"CVar 'worldPoolUsage' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [317]
		{
			"Alpha map bit depth set to 8bit on restart.", -- [1]
			0, -- [2]
		}, -- [318]
		{
			"Hardware PCF enabled.", -- [1]
			0, -- [2]
		}, -- [319]
		{
			"Projected textures disabled.", -- [1]
			0, -- [2]
		}, -- [320]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [321]
		{
			"Shadow mode changed to 0 - Precomputed terrain shadows, blob shadows.", -- [1]
			0, -- [2]
		}, -- [322]
		{
			"Shadow texture size changed to 1024.", -- [1]
			0, -- [2]
		}, -- [323]
		{
			"Soft shadows changed to 0.", -- [1]
			0, -- [2]
		}, -- [324]
		{
			"SSAO mode set to 0", -- [1]
			0, -- [2]
		}, -- [325]
		{
			"SSAO distance value set to 100.000000", -- [1]
			0, -- [2]
		}, -- [326]
		{
			"SSAO blur set to 2", -- [1]
			0, -- [2]
		}, -- [327]
		{
			"Depth Based Opacity Disabled", -- [1]
			0, -- [2]
		}, -- [328]
		{
			"SkyCloudLOD set to 0", -- [1]
			0, -- [2]
		}, -- [329]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [330]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [331]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [332]
		{
			"Proficiency in item class 2 set to 0x0000000080", -- [1]
			0, -- [2]
		}, -- [333]
		{
			"Proficiency in item class 2 set to 0x0000000081", -- [1]
			0, -- [2]
		}, -- [334]
		{
			"Proficiency in item class 2 set to 0x0000000091", -- [1]
			0, -- [2]
		}, -- [335]
		{
			"Proficiency in item class 2 set to 0x0000000491", -- [1]
			0, -- [2]
		}, -- [336]
		{
			"Proficiency in item class 4 set to 0x0000000021", -- [1]
			0, -- [2]
		}, -- [337]
		{
			"Proficiency in item class 2 set to 0x0000004491", -- [1]
			0, -- [2]
		}, -- [338]
		{
			"Proficiency in item class 2 set to 0x00000044d1", -- [1]
			0, -- [2]
		}, -- [339]
		{
			"Proficiency in item class 4 set to 0x0000000025", -- [1]
			0, -- [2]
		}, -- [340]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [341]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [342]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [343]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [344]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [345]
		{
			"Time set to 12/15/2017 (Fri) 12:52", -- [1]
			0, -- [2]
		}, -- [346]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [347]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [348]
		{
			"Total: 0d 0h 4m 26s", -- [1]
			0, -- [2]
		}, -- [349]
		{
			"Level: 0d 0h 4m 26s", -- [1]
			0, -- [2]
		}, -- [350]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [351]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [352]
		{
			"Attempted to register existing command: ShowObjUsage\n", -- [1]
			0, -- [2]
		}, -- [353]
		{
			"Attempted to register existing command: SetDifficulty\n", -- [1]
			0, -- [2]
		}, -- [354]
		{
			"Failed to set SpellClutter callback on graphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [355]
		{
			"Failed to set SpellClutter callback on RAIDgraphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [356]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [357]
		{
			"Proficiency in item class 2 set to 0x0000000080", -- [1]
			0, -- [2]
		}, -- [358]
		{
			"Proficiency in item class 2 set to 0x0000000081", -- [1]
			0, -- [2]
		}, -- [359]
		{
			"Proficiency in item class 2 set to 0x0000000091", -- [1]
			0, -- [2]
		}, -- [360]
		{
			"Proficiency in item class 2 set to 0x0000000491", -- [1]
			0, -- [2]
		}, -- [361]
		{
			"Proficiency in item class 4 set to 0x0000000021", -- [1]
			0, -- [2]
		}, -- [362]
		{
			"Proficiency in item class 2 set to 0x0000004491", -- [1]
			0, -- [2]
		}, -- [363]
		{
			"Proficiency in item class 2 set to 0x00000044d1", -- [1]
			0, -- [2]
		}, -- [364]
		{
			"Proficiency in item class 4 set to 0x0000000025", -- [1]
			0, -- [2]
		}, -- [365]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [366]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [367]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [368]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [369]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [370]
		{
			"Time set to 12/15/2017 (Fri) 12:55", -- [1]
			0, -- [2]
		}, -- [371]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [372]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [373]
		{
			"Total: 0d 0h 7m 11s", -- [1]
			0, -- [2]
		}, -- [374]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [375]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [376]
		{
			"Attempted to register existing command: ShowObjUsage\n", -- [1]
			0, -- [2]
		}, -- [377]
		{
			"Attempted to register existing command: SetDifficulty\n", -- [1]
			0, -- [2]
		}, -- [378]
		{
			"Failed to set SpellClutter callback on graphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [379]
		{
			"Failed to set SpellClutter callback on RAIDgraphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [380]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [381]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [382]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [383]
		{
			"Attempted to register existing command: ShowObjUsage\n", -- [1]
			0, -- [2]
		}, -- [384]
		{
			"Attempted to register existing command: SetDifficulty\n", -- [1]
			0, -- [2]
		}, -- [385]
		{
			"Failed to set SpellClutter callback on graphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [386]
		{
			"Failed to set SpellClutter callback on RAIDgraphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [387]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [388]
		{
			"Proficiency in item class 2 set to 0x0000000080", -- [1]
			0, -- [2]
		}, -- [389]
		{
			"Proficiency in item class 2 set to 0x0000000081", -- [1]
			0, -- [2]
		}, -- [390]
		{
			"Proficiency in item class 2 set to 0x0000000091", -- [1]
			0, -- [2]
		}, -- [391]
		{
			"Proficiency in item class 2 set to 0x0000000491", -- [1]
			0, -- [2]
		}, -- [392]
		{
			"Proficiency in item class 4 set to 0x0000000021", -- [1]
			0, -- [2]
		}, -- [393]
		{
			"Proficiency in item class 2 set to 0x0000004491", -- [1]
			0, -- [2]
		}, -- [394]
		{
			"Proficiency in item class 2 set to 0x00000044d1", -- [1]
			0, -- [2]
		}, -- [395]
		{
			"Proficiency in item class 4 set to 0x0000000025", -- [1]
			0, -- [2]
		}, -- [396]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [397]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [398]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [399]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [400]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [401]
		{
			"Time set to 12/15/2017 (Fri) 13:05", -- [1]
			0, -- [2]
		}, -- [402]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [403]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [404]
		{
			"Attempted to register existing command: ShowObjUsage\n", -- [1]
			0, -- [2]
		}, -- [405]
		{
			"Attempted to register existing command: SetDifficulty\n", -- [1]
			0, -- [2]
		}, -- [406]
		{
			"Failed to set SpellClutter callback on graphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [407]
		{
			"Failed to set SpellClutter callback on RAIDgraphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [408]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [409]
		{
			"Proficiency in item class 2 set to 0x0000000080", -- [1]
			0, -- [2]
		}, -- [410]
		{
			"Proficiency in item class 2 set to 0x0000000081", -- [1]
			0, -- [2]
		}, -- [411]
		{
			"Proficiency in item class 2 set to 0x0000000091", -- [1]
			0, -- [2]
		}, -- [412]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [413]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [414]
		{
			"Detail doodad instancing enabled.", -- [1]
			0, -- [2]
		}, -- [415]
		{
			"Water detail changed to 3", -- [1]
			0, -- [2]
		}, -- [416]
		{
			"Ripple detail changed to 2", -- [1]
			0, -- [2]
		}, -- [417]
		{
			"Reflection mode changed to 3", -- [1]
			0, -- [2]
		}, -- [418]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [419]
		{
			"Sunshafts quality changed to 2", -- [1]
			0, -- [2]
		}, -- [420]
		{
			"Refraction mode changed to 2", -- [1]
			0, -- [2]
		}, -- [421]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [422]
		{
			"WorldPoolUsage must be stream, dynamic or static.", -- [1]
			0, -- [2]
		}, -- [423]
		{
			"CVar 'worldPoolUsage' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [424]
		{
			"Alpha map bit depth set to 8bit on restart.", -- [1]
			0, -- [2]
		}, -- [425]
		{
			"Hardware PCF enabled.", -- [1]
			0, -- [2]
		}, -- [426]
		{
			"Projected textures enabled.", -- [1]
			0, -- [2]
		}, -- [427]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [428]
		{
			"Shadow mode changed to 3 - 4 band dynamic shadows, 2048", -- [1]
			0, -- [2]
		}, -- [429]
		{
			"Shadow texture size changed to 2048.", -- [1]
			0, -- [2]
		}, -- [430]
		{
			"Soft shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [431]
		{
			"SSAO mode set to 3", -- [1]
			0, -- [2]
		}, -- [432]
		{
			"SSAO distance value set to 100.000000", -- [1]
			0, -- [2]
		}, -- [433]
		{
			"SSAO blur set to 2", -- [1]
			0, -- [2]
		}, -- [434]
		{
			"Depth Based Opacity Enabled", -- [1]
			0, -- [2]
		}, -- [435]
		{
			"SkyCloudLOD set to 0", -- [1]
			0, -- [2]
		}, -- [436]
		{
			"Terrain mip level changed to 0.", -- [1]
			0, -- [2]
		}, -- [437]
		{
			"Outline mode changed to 2", -- [1]
			0, -- [2]
		}, -- [438]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [439]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [440]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [441]
		{
			"Proficiency in item class 2 set to 0x0000000080", -- [1]
			0, -- [2]
		}, -- [442]
		{
			"Proficiency in item class 2 set to 0x0000000081", -- [1]
			0, -- [2]
		}, -- [443]
		{
			"Proficiency in item class 2 set to 0x0000000091", -- [1]
			0, -- [2]
		}, -- [444]
		{
			"Proficiency in item class 2 set to 0x0000000491", -- [1]
			0, -- [2]
		}, -- [445]
		{
			"Proficiency in item class 4 set to 0x0000000021", -- [1]
			0, -- [2]
		}, -- [446]
		{
			"Proficiency in item class 2 set to 0x0000004491", -- [1]
			0, -- [2]
		}, -- [447]
		{
			"Proficiency in item class 2 set to 0x00000044d1", -- [1]
			0, -- [2]
		}, -- [448]
		{
			"Proficiency in item class 4 set to 0x0000000025", -- [1]
			0, -- [2]
		}, -- [449]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [450]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [451]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [452]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [453]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [454]
		{
			"Time set to 12/16/2017 (Sat) 20:41", -- [1]
			0, -- [2]
		}, -- [455]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [456]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [457]
		{
			"Total: 0d 0h 16m 14s", -- [1]
			0, -- [2]
		}, -- [458]
		{
			"Level: 0d 0h 3m 41s", -- [1]
			0, -- [2]
		}, -- [459]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [460]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [461]
		{
			"Detail doodad instancing enabled.", -- [1]
			0, -- [2]
		}, -- [462]
		{
			"Water detail changed to 3", -- [1]
			0, -- [2]
		}, -- [463]
		{
			"Ripple detail changed to 2", -- [1]
			0, -- [2]
		}, -- [464]
		{
			"Reflection mode changed to 3", -- [1]
			0, -- [2]
		}, -- [465]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [466]
		{
			"Sunshafts quality changed to 2", -- [1]
			0, -- [2]
		}, -- [467]
		{
			"Refraction mode changed to 2", -- [1]
			0, -- [2]
		}, -- [468]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [469]
		{
			"WorldPoolUsage must be stream, dynamic or static.", -- [1]
			0, -- [2]
		}, -- [470]
		{
			"CVar 'worldPoolUsage' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [471]
		{
			"Alpha map bit depth set to 8bit on restart.", -- [1]
			0, -- [2]
		}, -- [472]
		{
			"Hardware PCF enabled.", -- [1]
			0, -- [2]
		}, -- [473]
		{
			"Projected textures enabled.", -- [1]
			0, -- [2]
		}, -- [474]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [475]
		{
			"Shadow mode changed to 3 - 4 band dynamic shadows, 2048", -- [1]
			0, -- [2]
		}, -- [476]
		{
			"Shadow texture size changed to 2048.", -- [1]
			0, -- [2]
		}, -- [477]
		{
			"Soft shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [478]
		{
			"SSAO mode set to 3", -- [1]
			0, -- [2]
		}, -- [479]
		{
			"SSAO distance value set to 100.000000", -- [1]
			0, -- [2]
		}, -- [480]
		{
			"SSAO blur set to 2", -- [1]
			0, -- [2]
		}, -- [481]
		{
			"Depth Based Opacity Enabled", -- [1]
			0, -- [2]
		}, -- [482]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [483]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [484]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [485]
		{
			"Proficiency in item class 2 set to 0x0000000080", -- [1]
			0, -- [2]
		}, -- [486]
		{
			"Proficiency in item class 2 set to 0x0000000081", -- [1]
			0, -- [2]
		}, -- [487]
		{
			"Proficiency in item class 2 set to 0x0000000091", -- [1]
			0, -- [2]
		}, -- [488]
		{
			"Proficiency in item class 2 set to 0x0000000491", -- [1]
			0, -- [2]
		}, -- [489]
		{
			"Proficiency in item class 4 set to 0x0000000021", -- [1]
			0, -- [2]
		}, -- [490]
		{
			"Proficiency in item class 2 set to 0x0000004491", -- [1]
			0, -- [2]
		}, -- [491]
		{
			"Proficiency in item class 2 set to 0x00000044d1", -- [1]
			0, -- [2]
		}, -- [492]
		{
			"Proficiency in item class 4 set to 0x0000000025", -- [1]
			0, -- [2]
		}, -- [493]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [494]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [495]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [496]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [497]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [498]
		{
			"Time set to 12/17/2017 (Sun) 8:14", -- [1]
			0, -- [2]
		}, -- [499]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [500]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [501]
		{
			"Total: 0d 0h 0m 1s", -- [1]
			0, -- [2]
		}, -- [502]
		{
			"Level: 0d 0h 0m 1s", -- [1]
			0, -- [2]
		}, -- [503]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [504]
		{
			"Failed to set SpellClutter callback on graphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [505]
		{
			"Failed to set SpellClutter callback on RAIDgraphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [506]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [507]
		{
			"Total: 0d 0h 0m 15s", -- [1]
			0, -- [2]
		}, -- [508]
		{
			"Level: 0d 0h 0m 15s", -- [1]
			0, -- [2]
		}, -- [509]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [510]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [511]
		{
			"Detail doodad instancing enabled.", -- [1]
			0, -- [2]
		}, -- [512]
		{
			"Water detail changed to 3", -- [1]
			0, -- [2]
		}, -- [513]
		{
			"Ripple detail changed to 2", -- [1]
			0, -- [2]
		}, -- [514]
		{
			"Reflection mode changed to 3", -- [1]
			0, -- [2]
		}, -- [515]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [516]
		{
			"Sunshafts quality changed to 2", -- [1]
			0, -- [2]
		}, -- [517]
		{
			"Refraction mode changed to 2", -- [1]
			0, -- [2]
		}, -- [518]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [519]
		{
			"WorldPoolUsage must be stream, dynamic or static.", -- [1]
			0, -- [2]
		}, -- [520]
		{
			"CVar 'worldPoolUsage' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [521]
		{
			"Alpha map bit depth set to 8bit on restart.", -- [1]
			0, -- [2]
		}, -- [522]
		{
			"Hardware PCF enabled.", -- [1]
			0, -- [2]
		}, -- [523]
		{
			"Projected textures enabled.", -- [1]
			0, -- [2]
		}, -- [524]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [525]
		{
			"Shadow mode changed to 3 - 4 band dynamic shadows, 2048", -- [1]
			0, -- [2]
		}, -- [526]
		{
			"Shadow texture size changed to 2048.", -- [1]
			0, -- [2]
		}, -- [527]
		{
			"Soft shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [528]
		{
			"SSAO mode set to 3", -- [1]
			0, -- [2]
		}, -- [529]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [530]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [531]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [532]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [533]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [534]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [535]
		{
			"Time set to 12/17/2017 (Sun) 8:18", -- [1]
			0, -- [2]
		}, -- [536]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [537]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [538]
		{
			"Total: 0d 0h 3m 48s", -- [1]
			0, -- [2]
		}, -- [539]
		{
			"Level: 0d 0h 3m 48s", -- [1]
			0, -- [2]
		}, -- [540]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [541]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [542]
		{
			"Attempted to register existing command: ShowObjUsage\n", -- [1]
			0, -- [2]
		}, -- [543]
		{
			"Attempted to register existing command: SetDifficulty\n", -- [1]
			0, -- [2]
		}, -- [544]
		{
			"Failed to set SpellClutter callback on graphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [545]
		{
			"Failed to set SpellClutter callback on RAIDgraphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [546]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [547]
		{
			"Proficiency in item class 2 set to 0x0000000080", -- [1]
			0, -- [2]
		}, -- [548]
		{
			"Proficiency in item class 2 set to 0x0000000081", -- [1]
			0, -- [2]
		}, -- [549]
		{
			"Proficiency in item class 2 set to 0x0000000091", -- [1]
			0, -- [2]
		}, -- [550]
		{
			"Proficiency in item class 2 set to 0x0000000491", -- [1]
			0, -- [2]
		}, -- [551]
		{
			"Proficiency in item class 4 set to 0x0000000021", -- [1]
			0, -- [2]
		}, -- [552]
		{
			"Proficiency in item class 2 set to 0x0000004491", -- [1]
			0, -- [2]
		}, -- [553]
		{
			"Proficiency in item class 2 set to 0x00000044d1", -- [1]
			0, -- [2]
		}, -- [554]
		{
			"Proficiency in item class 4 set to 0x0000000025", -- [1]
			0, -- [2]
		}, -- [555]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [556]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [557]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [558]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [559]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [560]
		{
			"Time set to 12/17/2017 (Sun) 8:18", -- [1]
			0, -- [2]
		}, -- [561]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [562]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [563]
		{
			"Total: 0d 0h 4m 22s", -- [1]
			0, -- [2]
		}, -- [564]
		{
			"Level: 0d 0h 4m 22s", -- [1]
			0, -- [2]
		}, -- [565]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [566]
		{
			"Failed to set SpellClutter callback on graphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [567]
		{
			"Failed to set SpellClutter callback on RAIDgraphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [568]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [569]
		{
			"Total: 0d 0h 4m 30s", -- [1]
			0, -- [2]
		}, -- [570]
		{
			"Level: 0d 0h 4m 30s", -- [1]
			0, -- [2]
		}, -- [571]
		{
			"Skill 183 increased from 5 to 10", -- [1]
			0, -- [2]
		}, -- [572]
		{
			"Skill 829 increased from 5 to 10", -- [1]
			0, -- [2]
		}, -- [573]
		{
			"Skill 899 increased from 5 to 10", -- [1]
			0, -- [2]
		}, -- [574]
		{
			"Skill 183 increased from 10 to 15", -- [1]
			0, -- [2]
		}, -- [575]
		{
			"Skill 829 increased from 10 to 15", -- [1]
			0, -- [2]
		}, -- [576]
		{
			"Skill 899 increased from 10 to 15", -- [1]
			0, -- [2]
		}, -- [577]
		{
			"Skill 183 increased from 15 to 20", -- [1]
			0, -- [2]
		}, -- [578]
		{
			"Skill 829 increased from 15 to 20", -- [1]
			0, -- [2]
		}, -- [579]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [580]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [581]
		{
			"Detail doodad instancing enabled.", -- [1]
			0, -- [2]
		}, -- [582]
		{
			"Water detail changed to 3", -- [1]
			0, -- [2]
		}, -- [583]
		{
			"Ripple detail changed to 2", -- [1]
			0, -- [2]
		}, -- [584]
		{
			"Reflection mode changed to 3", -- [1]
			0, -- [2]
		}, -- [585]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [586]
		{
			"Sunshafts quality changed to 2", -- [1]
			0, -- [2]
		}, -- [587]
		{
			"Refraction mode changed to 2", -- [1]
			0, -- [2]
		}, -- [588]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [589]
		{
			"WorldPoolUsage must be stream, dynamic or static.", -- [1]
			0, -- [2]
		}, -- [590]
		{
			"CVar 'worldPoolUsage' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [591]
		{
			"Alpha map bit depth set to 8bit on restart.", -- [1]
			0, -- [2]
		}, -- [592]
		{
			"Hardware PCF enabled.", -- [1]
			0, -- [2]
		}, -- [593]
		{
			"Projected textures enabled.", -- [1]
			0, -- [2]
		}, -- [594]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [595]
		{
			"Shadow mode changed to 3 - 4 band dynamic shadows, 2048", -- [1]
			0, -- [2]
		}, -- [596]
		{
			"Shadow texture size changed to 2048.", -- [1]
			0, -- [2]
		}, -- [597]
		{
			"Soft shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [598]
		{
			"SSAO mode set to 3", -- [1]
			0, -- [2]
		}, -- [599]
		{
			"SSAO distance value set to 100.000000", -- [1]
			0, -- [2]
		}, -- [600]
		{
			"SSAO blur set to 2", -- [1]
			0, -- [2]
		}, -- [601]
		{
			"Depth Based Opacity Enabled", -- [1]
			0, -- [2]
		}, -- [602]
		{
			"SkyCloudLOD set to 0", -- [1]
			0, -- [2]
		}, -- [603]
		{
			"Terrain mip level changed to 0.", -- [1]
			0, -- [2]
		}, -- [604]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [605]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [606]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [607]
		{
			"Proficiency in item class 2 set to 0x0000000080", -- [1]
			0, -- [2]
		}, -- [608]
		{
			"Proficiency in item class 2 set to 0x0000000081", -- [1]
			0, -- [2]
		}, -- [609]
		{
			"Proficiency in item class 2 set to 0x0000000091", -- [1]
			0, -- [2]
		}, -- [610]
		{
			"Proficiency in item class 2 set to 0x0000000491", -- [1]
			0, -- [2]
		}, -- [611]
		{
			"Proficiency in item class 4 set to 0x0000000021", -- [1]
			0, -- [2]
		}, -- [612]
		{
			"Proficiency in item class 2 set to 0x0000004491", -- [1]
			0, -- [2]
		}, -- [613]
		{
			"Proficiency in item class 2 set to 0x00000044d1", -- [1]
			0, -- [2]
		}, -- [614]
		{
			"Proficiency in item class 4 set to 0x0000000025", -- [1]
			0, -- [2]
		}, -- [615]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [616]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [617]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [618]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [619]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [620]
		{
			"Time set to 12/17/2017 (Sun) 16:36", -- [1]
			0, -- [2]
		}, -- [621]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [622]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [623]
		{
			"Total: 0d 1h 17m 26s", -- [1]
			0, -- [2]
		}, -- [624]
		{
			"Level: 0d 0h 1m 21s", -- [1]
			0, -- [2]
		}, -- [625]
		{
			"Skill 183 increased from 60 to 65", -- [1]
			0, -- [2]
		}, -- [626]
		{
			"Skill 829 increased from 60 to 65", -- [1]
			0, -- [2]
		}, -- [627]
		{
			"Skill 899 increased from 60 to 65", -- [1]
			0, -- [2]
		}, -- [628]
		{
			"Skill 183 increased from 65 to 70", -- [1]
			0, -- [2]
		}, -- [629]
		{
			"Skill 829 increased from 65 to 70", -- [1]
			0, -- [2]
		}, -- [630]
		{
			"Skill 899 increased from 65 to 70", -- [1]
			0, -- [2]
		}, -- [631]
		{
			"Skill 109 increased from 0 to 300", -- [1]
			0, -- [2]
		}, -- [632]
		{
			"Skill 136 increased from 0 to 1", -- [1]
			0, -- [2]
		}, -- [633]
		{
			"Skill 137 increased from 1 to 0", -- [1]
			0, -- [2]
		}, -- [634]
		{
			"Skill 162 increased from 0 to 1", -- [1]
			0, -- [2]
		}, -- [635]
		{
			"Skill 183 increased from 0 to 70", -- [1]
			0, -- [2]
		}, -- [636]
		{
			"Skill 229 increased from 0 to 1", -- [1]
			0, -- [2]
		}, -- [637]
		{
			"Skill 393 increased from 1 to 0", -- [1]
			0, -- [2]
		}, -- [638]
		{
			"Skill 415 increased from 0 to 1", -- [1]
			0, -- [2]
		}, -- [639]
		{
			"Skill 473 increased from 0 to 1", -- [1]
			0, -- [2]
		}, -- [640]
		{
			"Skill 777 increased from 0 to 1", -- [1]
			0, -- [2]
		}, -- [641]
		{
			"Skill 810 increased from 70 to 1", -- [1]
			0, -- [2]
		}, -- [642]
		{
			"Skill 821 increased from 70 to 0", -- [1]
			0, -- [2]
		}, -- [643]
		{
			"Skill 829 increased from 300 to 70", -- [1]
			0, -- [2]
		}, -- [644]
		{
			"Skill 899 increased from 1 to 70", -- [1]
			0, -- [2]
		}, -- [645]
		{
			"Skill 905 increased from 0 to 315", -- [1]
			0, -- [2]
		}, -- [646]
		{
			"Skill 934 increased from 0 to 1", -- [1]
			0, -- [2]
		}, -- [647]
		{
			"Skill 1830 increased from 0 to 1", -- [1]
			0, -- [2]
		}, -- [648]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [649]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [650]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [651]
		{
			"Attempted to register existing command: ShowObjUsage\n", -- [1]
			0, -- [2]
		}, -- [652]
		{
			"Attempted to register existing command: SetDifficulty\n", -- [1]
			0, -- [2]
		}, -- [653]
		{
			"Failed to set SpellClutter callback on graphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [654]
		{
			"Failed to set SpellClutter callback on RAIDgraphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [655]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [656]
		{
			"Proficiency in item class 2 set to 0x0000000080", -- [1]
			0, -- [2]
		}, -- [657]
		{
			"Proficiency in item class 2 set to 0x0000000081", -- [1]
			0, -- [2]
		}, -- [658]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [659]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [660]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [661]
		{
			"Detail doodad instancing enabled.", -- [1]
			0, -- [2]
		}, -- [662]
		{
			"Water detail changed to 3", -- [1]
			0, -- [2]
		}, -- [663]
		{
			"Ripple detail changed to 2", -- [1]
			0, -- [2]
		}, -- [664]
		{
			"Reflection mode changed to 3", -- [1]
			0, -- [2]
		}, -- [665]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [666]
		{
			"Sunshafts quality changed to 2", -- [1]
			0, -- [2]
		}, -- [667]
		{
			"Refraction mode changed to 2", -- [1]
			0, -- [2]
		}, -- [668]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [669]
		{
			"WorldPoolUsage must be stream, dynamic or static.", -- [1]
			0, -- [2]
		}, -- [670]
		{
			"CVar 'worldPoolUsage' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [671]
		{
			"Alpha map bit depth set to 8bit on restart.", -- [1]
			0, -- [2]
		}, -- [672]
		{
			"Hardware PCF enabled.", -- [1]
			0, -- [2]
		}, -- [673]
		{
			"Projected textures enabled.", -- [1]
			0, -- [2]
		}, -- [674]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [675]
		{
			"Shadow mode changed to 3 - 4 band dynamic shadows, 2048", -- [1]
			0, -- [2]
		}, -- [676]
		{
			"Shadow texture size changed to 2048.", -- [1]
			0, -- [2]
		}, -- [677]
		{
			"Soft shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [678]
		{
			"SSAO mode set to 3", -- [1]
			0, -- [2]
		}, -- [679]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [680]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [681]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [682]
		{
			"Proficiency in item class 2 set to 0x0000000010", -- [1]
			0, -- [2]
		}, -- [683]
		{
			"Proficiency in item class 2 set to 0x0000000410", -- [1]
			0, -- [2]
		}, -- [684]
		{
			"Proficiency in item class 2 set to 0x0000008410", -- [1]
			0, -- [2]
		}, -- [685]
		{
			"Proficiency in item class 4 set to 0x0000000021", -- [1]
			0, -- [2]
		}, -- [686]
		{
			"Proficiency in item class 2 set to 0x000000c410", -- [1]
			0, -- [2]
		}, -- [687]
		{
			"Proficiency in item class 2 set to 0x000008c410", -- [1]
			0, -- [2]
		}, -- [688]
		{
			"Proficiency in item class 2 set to 0x000018c410", -- [1]
			0, -- [2]
		}, -- [689]
		{
			"Proficiency in item class 4 set to 0x0000000023", -- [1]
			0, -- [2]
		}, -- [690]
		{
			"Proficiency in item class 2 set to 0x000018c410", -- [1]
			0, -- [2]
		}, -- [691]
		{
			"Proficiency in item class 4 set to 0x0000000023", -- [1]
			0, -- [2]
		}, -- [692]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [693]
		{
			"Time set to 12/17/2017 (Sun) 17:04", -- [1]
			0, -- [2]
		}, -- [694]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [695]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [696]
		{
			"Total: 10d 5h 37m 38s", -- [1]
			0, -- [2]
		}, -- [697]
		{
			"Level: 8d 18h 11m 49s", -- [1]
			0, -- [2]
		}, -- [698]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [699]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [700]
		{
			"Attempted to register existing command: ShowObjUsage\n", -- [1]
			0, -- [2]
		}, -- [701]
		{
			"Attempted to register existing command: SetDifficulty\n", -- [1]
			0, -- [2]
		}, -- [702]
		{
			"Failed to set SpellClutter callback on graphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [703]
		{
			"Failed to set SpellClutter callback on RAIDgraphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [704]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [705]
		{
			"Proficiency in item class 2 set to 0x0000000080", -- [1]
			0, -- [2]
		}, -- [706]
		{
			"Proficiency in item class 2 set to 0x0000000081", -- [1]
			0, -- [2]
		}, -- [707]
		{
			"Proficiency in item class 2 set to 0x0000000091", -- [1]
			0, -- [2]
		}, -- [708]
		{
			"Proficiency in item class 2 set to 0x0000000491", -- [1]
			0, -- [2]
		}, -- [709]
		{
			"Proficiency in item class 4 set to 0x0000000021", -- [1]
			0, -- [2]
		}, -- [710]
		{
			"Proficiency in item class 2 set to 0x0000004491", -- [1]
			0, -- [2]
		}, -- [711]
		{
			"Proficiency in item class 2 set to 0x00000044d1", -- [1]
			0, -- [2]
		}, -- [712]
		{
			"Proficiency in item class 4 set to 0x0000000025", -- [1]
			0, -- [2]
		}, -- [713]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [714]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [715]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [716]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [717]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [718]
		{
			"Time set to 12/17/2017 (Sun) 17:03", -- [1]
			0, -- [2]
		}, -- [719]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [720]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [721]
		{
			"Total: 0d 1h 42m 47s", -- [1]
			0, -- [2]
		}, -- [722]
		{
			"Level: 0d 0h 6m 30s", -- [1]
			0, -- [2]
		}, -- [723]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [724]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [725]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [726]
		{
			"Skill 821 increased from 0 to 1", -- [1]
			0, -- [2]
		}, -- [727]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [728]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [729]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [730]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [731]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [732]
		{
			"Detail doodad instancing enabled.", -- [1]
			0, -- [2]
		}, -- [733]
		{
			"Water detail changed to 3", -- [1]
			0, -- [2]
		}, -- [734]
		{
			"Ripple detail changed to 2", -- [1]
			0, -- [2]
		}, -- [735]
		{
			"Reflection mode changed to 3", -- [1]
			0, -- [2]
		}, -- [736]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [737]
		{
			"Sunshafts quality changed to 2", -- [1]
			0, -- [2]
		}, -- [738]
		{
			"Refraction mode changed to 2", -- [1]
			0, -- [2]
		}, -- [739]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [740]
		{
			"WorldPoolUsage must be stream, dynamic or static.", -- [1]
			0, -- [2]
		}, -- [741]
		{
			"CVar 'worldPoolUsage' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [742]
		{
			"Alpha map bit depth set to 8bit on restart.", -- [1]
			0, -- [2]
		}, -- [743]
		{
			"Hardware PCF enabled.", -- [1]
			0, -- [2]
		}, -- [744]
		{
			"Projected textures enabled.", -- [1]
			0, -- [2]
		}, -- [745]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [746]
		{
			"Shadow mode changed to 3 - 4 band dynamic shadows, 2048", -- [1]
			0, -- [2]
		}, -- [747]
		{
			"Shadow texture size changed to 2048.", -- [1]
			0, -- [2]
		}, -- [748]
		{
			"Soft shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [749]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [750]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [751]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [752]
		{
			"Proficiency in item class 2 set to 0x0000000010", -- [1]
			0, -- [2]
		}, -- [753]
		{
			"Proficiency in item class 2 set to 0x0000000410", -- [1]
			0, -- [2]
		}, -- [754]
		{
			"Proficiency in item class 2 set to 0x0000008410", -- [1]
			0, -- [2]
		}, -- [755]
		{
			"Proficiency in item class 4 set to 0x0000000021", -- [1]
			0, -- [2]
		}, -- [756]
		{
			"Proficiency in item class 2 set to 0x000000c410", -- [1]
			0, -- [2]
		}, -- [757]
		{
			"Proficiency in item class 2 set to 0x000008c410", -- [1]
			0, -- [2]
		}, -- [758]
		{
			"Proficiency in item class 2 set to 0x000018c410", -- [1]
			0, -- [2]
		}, -- [759]
		{
			"Proficiency in item class 4 set to 0x0000000023", -- [1]
			0, -- [2]
		}, -- [760]
		{
			"Proficiency in item class 2 set to 0x000018c410", -- [1]
			0, -- [2]
		}, -- [761]
		{
			"Proficiency in item class 4 set to 0x0000000023", -- [1]
			0, -- [2]
		}, -- [762]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [763]
		{
			"Time set to 12/17/2017 (Sun) 17:56", -- [1]
			0, -- [2]
		}, -- [764]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [765]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [766]
		{
			"Total: 10d 5h 37m 59s", -- [1]
			0, -- [2]
		}, -- [767]
		{
			"Level: 8d 18h 12m 10s", -- [1]
			0, -- [2]
		}, -- [768]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [769]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [770]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [771]
		{
			"Detail doodad instancing enabled.", -- [1]
			0, -- [2]
		}, -- [772]
		{
			"Water detail changed to 3", -- [1]
			0, -- [2]
		}, -- [773]
		{
			"Ripple detail changed to 2", -- [1]
			0, -- [2]
		}, -- [774]
		{
			"Reflection mode changed to 3", -- [1]
			0, -- [2]
		}, -- [775]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [776]
		{
			"Sunshafts quality changed to 2", -- [1]
			0, -- [2]
		}, -- [777]
		{
			"Refraction mode changed to 2", -- [1]
			0, -- [2]
		}, -- [778]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [779]
		{
			"WorldPoolUsage must be stream, dynamic or static.", -- [1]
			0, -- [2]
		}, -- [780]
		{
			"CVar 'worldPoolUsage' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [781]
		{
			"Alpha map bit depth set to 8bit on restart.", -- [1]
			0, -- [2]
		}, -- [782]
		{
			"Hardware PCF enabled.", -- [1]
			0, -- [2]
		}, -- [783]
		{
			"Projected textures enabled.", -- [1]
			0, -- [2]
		}, -- [784]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [785]
		{
			"Shadow mode changed to 3 - 4 band dynamic shadows, 2048", -- [1]
			0, -- [2]
		}, -- [786]
		{
			"Shadow texture size changed to 2048.", -- [1]
			0, -- [2]
		}, -- [787]
		{
			"Soft shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [788]
		{
			"SSAO mode set to 3", -- [1]
			0, -- [2]
		}, -- [789]
		{
			"SSAO distance value set to 100.000000", -- [1]
			0, -- [2]
		}, -- [790]
		{
			"SSAO blur set to 2", -- [1]
			0, -- [2]
		}, -- [791]
		{
			"Depth Based Opacity Enabled", -- [1]
			0, -- [2]
		}, -- [792]
		{
			"SkyCloudLOD set to 0", -- [1]
			0, -- [2]
		}, -- [793]
		{
			"Terrain mip level changed to 0.", -- [1]
			0, -- [2]
		}, -- [794]
		{
			"Outline mode changed to 2", -- [1]
			0, -- [2]
		}, -- [795]
		{
			"LightBuffer mode changed to 2", -- [1]
			0, -- [2]
		}, -- [796]
		{
			"Physics interaction level changed to 1", -- [1]
			0, -- [2]
		}, -- [797]
		{
			"Render scale changed to 1", -- [1]
			0, -- [2]
		}, -- [798]
		{
			"Resample quality changed to 0", -- [1]
			0, -- [2]
		}, -- [799]
		{
			"MSAA disabled", -- [1]
			0, -- [2]
		}, -- [800]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [801]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [802]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [803]
		{
			"Proficiency in item class 2 set to 0x0000000010", -- [1]
			0, -- [2]
		}, -- [804]
		{
			"Proficiency in item class 2 set to 0x0000000410", -- [1]
			0, -- [2]
		}, -- [805]
		{
			"Proficiency in item class 2 set to 0x0000008410", -- [1]
			0, -- [2]
		}, -- [806]
		{
			"Proficiency in item class 4 set to 0x0000000021", -- [1]
			0, -- [2]
		}, -- [807]
		{
			"Proficiency in item class 2 set to 0x000000c410", -- [1]
			0, -- [2]
		}, -- [808]
		{
			"Proficiency in item class 2 set to 0x000008c410", -- [1]
			0, -- [2]
		}, -- [809]
		{
			"Proficiency in item class 2 set to 0x000018c410", -- [1]
			0, -- [2]
		}, -- [810]
		{
			"Proficiency in item class 4 set to 0x0000000023", -- [1]
			0, -- [2]
		}, -- [811]
		{
			"Proficiency in item class 2 set to 0x000018c410", -- [1]
			0, -- [2]
		}, -- [812]
		{
			"Proficiency in item class 4 set to 0x0000000023", -- [1]
			0, -- [2]
		}, -- [813]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [814]
		{
			"Time set to 12/17/2017 (Sun) 21:00", -- [1]
			0, -- [2]
		}, -- [815]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [816]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [817]
		{
			"Total: 10d 5h 38m 31s", -- [1]
			0, -- [2]
		}, -- [818]
		{
			"Level: 8d 18h 12m 42s", -- [1]
			0, -- [2]
		}, -- [819]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [820]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [821]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [822]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [823]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [824]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [825]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [826]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [827]
		{
			"Detail doodad instancing enabled.", -- [1]
			0, -- [2]
		}, -- [828]
		{
			"Water detail changed to 0", -- [1]
			0, -- [2]
		}, -- [829]
		{
			"Ripple detail changed to 0", -- [1]
			0, -- [2]
		}, -- [830]
		{
			"Reflection mode changed to 0", -- [1]
			0, -- [2]
		}, -- [831]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [832]
		{
			"Sunshafts quality changed to 0", -- [1]
			0, -- [2]
		}, -- [833]
		{
			"Refraction mode changed to 0", -- [1]
			0, -- [2]
		}, -- [834]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [835]
		{
			"WorldPoolUsage must be stream, dynamic or static.", -- [1]
			0, -- [2]
		}, -- [836]
		{
			"CVar 'worldPoolUsage' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [837]
		{
			"Alpha map bit depth set to 8bit on restart.", -- [1]
			0, -- [2]
		}, -- [838]
		{
			"Hardware PCF enabled.", -- [1]
			0, -- [2]
		}, -- [839]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [840]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [841]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [842]
		{
			"Proficiency in item class 2 set to 0x0000000080", -- [1]
			0, -- [2]
		}, -- [843]
		{
			"Proficiency in item class 2 set to 0x0000000081", -- [1]
			0, -- [2]
		}, -- [844]
		{
			"Proficiency in item class 2 set to 0x0000000091", -- [1]
			0, -- [2]
		}, -- [845]
		{
			"Proficiency in item class 2 set to 0x0000000491", -- [1]
			0, -- [2]
		}, -- [846]
		{
			"Proficiency in item class 4 set to 0x0000000021", -- [1]
			0, -- [2]
		}, -- [847]
		{
			"Proficiency in item class 2 set to 0x0000004491", -- [1]
			0, -- [2]
		}, -- [848]
		{
			"Proficiency in item class 2 set to 0x00000044d1", -- [1]
			0, -- [2]
		}, -- [849]
		{
			"Proficiency in item class 4 set to 0x0000000025", -- [1]
			0, -- [2]
		}, -- [850]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [851]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [852]
		{
			"Proficiency in item class 2 set to 0x00000064d1", -- [1]
			0, -- [2]
		}, -- [853]
		{
			"Proficiency in item class 4 set to 0x0000000027", -- [1]
			0, -- [2]
		}, -- [854]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [855]
		{
			"Time set to 12/18/2017 (Mon) 12:54", -- [1]
			0, -- [2]
		}, -- [856]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [857]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [858]
		{
			"Total: 0d 2h 34m 32s", -- [1]
			0, -- [2]
		}, -- [859]
		{
			"Level: 0d 0h 1m 18s", -- [1]
			0, -- [2]
		}, -- [860]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [861]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [862]
		{
			"Attempted to register existing command: ShowObjUsage\n", -- [1]
			0, -- [2]
		}, -- [863]
		{
			"Attempted to register existing command: SetDifficulty\n", -- [1]
			0, -- [2]
		}, -- [864]
		{
			"Failed to set SpellClutter callback on graphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [865]
		{
			"Failed to set SpellClutter callback on RAIDgraphicsQuality CVar - using default of 30.0", -- [1]
			0, -- [2]
		}, -- [866]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [867]
		{
			"Proficiency in item class 2 set to 0x0000000010", -- [1]
			0, -- [2]
		}, -- [868]
		{
			"Proficiency in item class 2 set to 0x0000000410", -- [1]
			0, -- [2]
		}, -- [869]
		{
			"Proficiency in item class 2 set to 0x0000008410", -- [1]
			0, -- [2]
		}, -- [870]
		{
			"Proficiency in item class 4 set to 0x0000000021", -- [1]
			0, -- [2]
		}, -- [871]
		{
			"Proficiency in item class 2 set to 0x000000c410", -- [1]
			0, -- [2]
		}, -- [872]
		{
			"Proficiency in item class 2 set to 0x000008c410", -- [1]
			0, -- [2]
		}, -- [873]
		{
			"Proficiency in item class 2 set to 0x000018c410", -- [1]
			0, -- [2]
		}, -- [874]
		{
			"Proficiency in item class 4 set to 0x0000000023", -- [1]
			0, -- [2]
		}, -- [875]
		{
			"Proficiency in item class 2 set to 0x000018c410", -- [1]
			0, -- [2]
		}, -- [876]
		{
			"Proficiency in item class 4 set to 0x0000000023", -- [1]
			0, -- [2]
		}, -- [877]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [878]
		{
			"Time set to 12/18/2017 (Mon) 12:55", -- [1]
			0, -- [2]
		}, -- [879]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [880]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [881]
		{
			"Total: 10d 5h 53m 2s", -- [1]
			0, -- [2]
		}, -- [882]
		{
			"Level: 8d 18h 27m 13s", -- [1]
			0, -- [2]
		}, -- [883]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [884]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [885]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [886]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [887]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [888]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [889]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [890]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [891]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [892]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [893]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [894]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [895]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [896]
		{
			"Sorting particles normally.", -- [1]
			0, -- [2]
		}, -- [897]
		{
			"Detail doodad instancing enabled.", -- [1]
			0, -- [2]
		}, -- [898]
		{
			"Water detail changed to 3", -- [1]
			0, -- [2]
		}, -- [899]
		{
			"Ripple detail changed to 2", -- [1]
			0, -- [2]
		}, -- [900]
		{
			"Reflection mode changed to 3", -- [1]
			0, -- [2]
		}, -- [901]
		{
			"Reflection downscale changed to 0", -- [1]
			0, -- [2]
		}, -- [902]
		{
			"Sunshafts quality changed to 2", -- [1]
			0, -- [2]
		}, -- [903]
		{
			"Refraction mode changed to 2", -- [1]
			0, -- [2]
		}, -- [904]
		{
			"Enabling BSP node cache (first time - starting up)", -- [1]
			0, -- [2]
		}, -- [905]
		{
			"WorldPoolUsage must be stream, dynamic or static.", -- [1]
			0, -- [2]
		}, -- [906]
		{
			"CVar 'worldPoolUsage' failed validation for its initial value.", -- [1]
			0, -- [2]
		}, -- [907]
		{
			"Alpha map bit depth set to 8bit on restart.", -- [1]
			0, -- [2]
		}, -- [908]
		{
			"Hardware PCF enabled.", -- [1]
			0, -- [2]
		}, -- [909]
		{
			"Projected textures enabled.", -- [1]
			0, -- [2]
		}, -- [910]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [911]
		{
			"Shadow mode changed to 3 - 4 band dynamic shadows, 2048", -- [1]
			0, -- [2]
		}, -- [912]
		{
			"Shadow texture size changed to 2048.", -- [1]
			0, -- [2]
		}, -- [913]
		{
			"Soft shadows changed to 1.", -- [1]
			0, -- [2]
		}, -- [914]
		{
			"SSAO mode set to 3", -- [1]
			0, -- [2]
		}, -- [915]
		{
			"SSAO distance value set to 100.000000", -- [1]
			0, -- [2]
		}, -- [916]
		{
			"SSAO blur set to 2", -- [1]
			0, -- [2]
		}, -- [917]
		{
			"Depth Based Opacity Enabled", -- [1]
			0, -- [2]
		}, -- [918]
		{
			"SkyCloudLOD set to 0", -- [1]
			0, -- [2]
		}, -- [919]
		{
			"Terrain mip level changed to 0.", -- [1]
			0, -- [2]
		}, -- [920]
		{
			"Outline mode changed to 2", -- [1]
			0, -- [2]
		}, -- [921]
		{
			"LightBuffer mode changed to 2", -- [1]
			0, -- [2]
		}, -- [922]
		{
			"Physics interaction level changed to 1", -- [1]
			0, -- [2]
		}, -- [923]
		{
			"Render scale changed to 1", -- [1]
			0, -- [2]
		}, -- [924]
		{
			"Resample quality changed to 0", -- [1]
			0, -- [2]
		}, -- [925]
		{
			"MSAA disabled", -- [1]
			0, -- [2]
		}, -- [926]
		{
			"MSAA for alpha-test enabled.", -- [1]
			0, -- [2]
		}, -- [927]
		{
			"Component texture lod changed to 0", -- [1]
			0, -- [2]
		}, -- [928]
		{
			"Component texture lod changed to 0", -- [1]
			0, -- [2]
		}, -- [929]
		{
			"World preload object sort enabled.", -- [1]
			0, -- [2]
		}, -- [930]
		{
			"World load object sort enabled.", -- [1]
			0, -- [2]
		}, -- [931]
		{
			"World preload non critical enabled.", -- [1]
			0, -- [2]
		}, -- [932]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [933]
		{
			"Texture cache size set to 512MB.", -- [1]
			0, -- [2]
		}, -- [934]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [935]
		{
			"Proficiency in item class 2 set to 0x0000000080", -- [1]
			0, -- [2]
		}, -- [936]
		{
			"Proficiency in item class 2 set to 0x0000000480", -- [1]
			0, -- [2]
		}, -- [937]
		{
			"Proficiency in item class 2 set to 0x0000008480", -- [1]
			0, -- [2]
		}, -- [938]
		{
			"Proficiency in item class 4 set to 0x0000000021", -- [1]
			0, -- [2]
		}, -- [939]
		{
			"Proficiency in item class 2 set to 0x000000c480", -- [1]
			0, -- [2]
		}, -- [940]
		{
			"Proficiency in item class 2 set to 0x000008c480", -- [1]
			0, -- [2]
		}, -- [941]
		{
			"Proficiency in item class 4 set to 0x0000000023", -- [1]
			0, -- [2]
		}, -- [942]
		{
			"Proficiency in item class 2 set to 0x000008c480", -- [1]
			0, -- [2]
		}, -- [943]
		{
			"Proficiency in item class 4 set to 0x0000000023", -- [1]
			0, -- [2]
		}, -- [944]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [945]
		{
			"Time set to 12/18/2017 (Mon) 22:49", -- [1]
			0, -- [2]
		}, -- [946]
		{
			"Gamespeed set from 0.017 to 0.017", -- [1]
			0, -- [2]
		}, -- [947]
		{
			"Time played:", -- [1]
			0, -- [2]
		}, -- [948]
		{
			"Total: 9d 8h 37m 23s", -- [1]
			0, -- [2]
		}, -- [949]
		{
			"Level: 8d 11h 55m 44s", -- [1]
			0, -- [2]
		}, -- [950]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [951]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [952]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [953]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [954]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [955]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [956]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [957]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [958]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [959]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [960]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [961]
		{
			"Got new connection 3", -- [1]
			0, -- [2]
		}, -- [962]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [963]
		{
			"World transfer pending...", -- [1]
			0, -- [2]
		}, -- [964]
		{
			"Weather changed to 1, intensity 0.000000\n", -- [1]
			0, -- [2]
		}, -- [965]
		{
			"-------------------------------------------------- Previous Session --------------------------------------------------", -- [1]
			0, -- [2]
		}, -- [966]
	},
	["height"] = 300,
	["fontHeight"] = 14,
	["isShown"] = false,
	["commandHistory"] = {
	},
}
