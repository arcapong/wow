
DBMPartyWoD_SavedStats = {
	["1234"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1210"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1225"] = {
		["normalPulls"] = 2,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 29.0680000000284,
		["normalKills"] = 2,
		["mythicPulls"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["normal25Kills"] = 0,
		["heroicPulls"] = 0,
		["timewalkerKills"] = 0,
		["normalBestTime"] = 29.0680000000284,
		["challengePulls"] = 0,
	},
	["968"] = {
		["normalPulls"] = 2,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 99.1689999999981,
		["normalKills"] = 2,
		["mythicPulls"] = 0,
		["normalBestTime"] = 62.7689999999711,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["normal25Kills"] = 0,
		["heroicPulls"] = 0,
		["timewalkerKills"] = 0,
		["heroic25Kills"] = 0,
		["challengePulls"] = 0,
	},
	["1139"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["GRDTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1238"] = {
		["normalPulls"] = 1,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 74.6540000000096,
		["normalKills"] = 1,
		["heroic25Kills"] = 0,
		["normalBestTime"] = 74.6540000000096,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1140"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1214"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1216"] = {
		["normalPulls"] = 2,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 24.0020000000368,
		["normalKills"] = 2,
		["mythicPulls"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["normal25Kills"] = 0,
		["heroicPulls"] = 0,
		["timewalkerKills"] = 0,
		["normalBestTime"] = 24.0020000000368,
		["challengePulls"] = 0,
	},
	["1207"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1209"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1186"] = {
		["normalPulls"] = 2,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 25.5109999999986,
		["normalKills"] = 2,
		["mythicPulls"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["normal25Kills"] = 0,
		["heroicPulls"] = 0,
		["timewalkerKills"] = 0,
		["normalBestTime"] = 25.5109999999986,
		["challengePulls"] = 0,
	},
	["1226"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1237"] = {
		["normalPulls"] = 1,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 41.8579999999784,
		["normalKills"] = 1,
		["heroic25Kills"] = 0,
		["normalBestTime"] = 41.8579999999784,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["SkyreachTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1160"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1236"] = {
		["normalPulls"] = 1,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 57.6069999999891,
		["normalKills"] = 1,
		["heroic25Kills"] = 0,
		["normalBestTime"] = 57.6069999999891,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1185"] = {
		["normalPulls"] = 2,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 41.7989999999991,
		["normalKills"] = 2,
		["mythicPulls"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["normal25Kills"] = 0,
		["heroicPulls"] = 0,
		["timewalkerKills"] = 0,
		["normalBestTime"] = 33.4049999999988,
		["challengePulls"] = 0,
	},
	["1168"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["BSMTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1138"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1227"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["AuchTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1235"] = {
		["normalPulls"] = 1,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 62.4539999999979,
		["normalKills"] = 1,
		["heroic25Kills"] = 0,
		["normalBestTime"] = 62.4539999999979,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["UBRSTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["965"] = {
		["normalPulls"] = 2,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 84.2900000000009,
		["normalKills"] = 2,
		["mythicPulls"] = 0,
		["normalBestTime"] = 55.1319999999832,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["normal25Kills"] = 0,
		["heroicPulls"] = 0,
		["timewalkerKills"] = 0,
		["heroic25Kills"] = 0,
		["challengePulls"] = 0,
	},
	["1228"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1133"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["893"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["888"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1163"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["966"] = {
		["normalPulls"] = 2,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 77.732,
		["normalKills"] = 2,
		["mythicPulls"] = 0,
		["normalBestTime"] = 69.8709999999846,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["normal25Kills"] = 0,
		["heroicPulls"] = 0,
		["timewalkerKills"] = 0,
		["heroic25Kills"] = 0,
		["challengePulls"] = 0,
	},
	["967"] = {
		["normalPulls"] = 2,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 72.8760000000002,
		["normalKills"] = 2,
		["mythicPulls"] = 0,
		["normalBestTime"] = 61.3549999999814,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["normal25Kills"] = 0,
		["heroicPulls"] = 0,
		["timewalkerKills"] = 0,
		["heroic25Kills"] = 0,
		["challengePulls"] = 0,
	},
	["1229"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["889"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1208"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["887"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
}
