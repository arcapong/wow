
InvenRaidFrames3CharDB = {
	["clickCasting"] = {
		{
		}, -- [1]
		{
			["2"] = "spell__해독",
		}, -- [2]
		{
			["2"] = "spell__해독",
		}, -- [3]
		{
		}, -- [4]
	},
	["classBuff2"] = {
	},
	["class"] = "MONK",
	["spellTimer"] = {
		{
			["name"] = "포용의 안개",
			["scale"] = 1,
			["pos"] = "BOTTOMLEFT",
			["display"] = 1,
			["use"] = 1,
		}, -- [1]
		{
			["name"] = "소생의 안개",
			["scale"] = 1,
			["pos"] = "BOTTOM",
			["display"] = 1,
			["use"] = 1,
		}, -- [2]
		{
			["display"] = 1,
			["pos"] = "BOTTOMRIGHT",
			["scale"] = 1,
			["use"] = 0,
		}, -- [3]
		{
			["display"] = 1,
			["pos"] = "LEFT",
			["scale"] = 1,
			["use"] = 0,
		}, -- [4]
		{
			["display"] = 1,
			["pos"] = "RIGHT",
			["scale"] = 1,
			["use"] = 0,
		}, -- [5]
		{
			["display"] = 1,
			["pos"] = "TOPLEFT",
			["scale"] = 1,
			["use"] = 0,
		}, -- [6]
		{
			["display"] = 1,
			["pos"] = "TOP",
			["scale"] = 1,
			["use"] = 0,
		}, -- [7]
		{
			["display"] = 1,
			["pos"] = "TOPRIGHT",
			["scale"] = 1,
			["use"] = 0,
		}, -- [8]
	},
}
