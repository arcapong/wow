
TidyPlatesOptions = {
	["PrimaryProfile"] = "Damage",
	["SecondSpecProfile"] = "Damage",
	["FriendlyAutomation"] = "No Automation",
	["EnemyAutomation"] = "No Automation",
	["SecondaryTheme"] = "Neon",
	["CompatibilityMode"] = false,
	["PrimaryTheme"] = "Neon",
	["FourthSpecProfile"] = "Damage",
	["SecondaryProfile"] = "Damage",
	["ThirdSpecProfile"] = "Damage",
	["DisableCastBars"] = false,
	["ActiveTheme"] = "Neon",
	["ForceBlizzardFont"] = false,
	["WelcomeShown"] = true,
	["FirstSpecProfile"] = "Damage",
}
