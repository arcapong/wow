
EnemyGridDBChr = {
	["KeyBinds"] = {
		[64] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
		},
		[63] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
		},
		[62] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
		},
	},
	["spellRangeCheck"] = {
		[64] = "얼음 화살",
		[63] = "화염구",
		[62] = "비전 작렬",
	},
	["debuffsBanned"] = {
	},
	["specEnabled"] = {
		[64] = true,
		[63] = true,
		[62] = true,
	},
}
