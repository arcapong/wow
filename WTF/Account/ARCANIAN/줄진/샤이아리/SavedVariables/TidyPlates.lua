
TidyPlatesOptions = {
	["PrimaryProfile"] = "Damage",
	["ForceBlizzardFont"] = false,
	["SecondSpecProfile"] = "Damage",
	["FriendlyAutomation"] = "No Automation",
	["DisableCastBars"] = false,
	["SecondaryTheme"] = "Neon",
	["CompatibilityMode"] = false,
	["PrimaryTheme"] = "Neon",
	["FourthSpecProfile"] = "Damage",
	["SecondaryProfile"] = "Damage",
	["ThirdSpecProfile"] = "Damage",
	["ActiveTheme"] = "Neon",
	["ActiveProfile"] = "Damage",
	["EnemyAutomation"] = "No Automation",
	["WelcomeShown"] = true,
	["FirstSpecProfile"] = "Damage",
}
