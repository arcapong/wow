
SkadaPerCharDB = {
	["sets"] = {
		{
			["healingabsorbed"] = 0,
			["alertDamage"] = 0,
			["mobdone"] = 0,
			["ccbreaks"] = 0,
			["time"] = 6,
			["interrupts"] = 0,
			["dispells"] = 0,
			["damagetaken"] = 0,
			["power"] = {
			},
			["damage"] = 1024853,
			["players"] = {
				{
					["last"] = 1471192720,
					["healingabsorbed"] = 0,
					["class"] = "MAGE",
					["damaged"] = {
						["갈퀴송곳니 기술병"] = 423977,
						["갈퀴송곳니 공모자"] = 417588,
						["배반자 멘누"] = 183288,
					},
					["alertDamage"] = 0,
					["ccbreaks"] = 0,
					["time"] = 4,
					["interrupts"] = 0,
					["dispells"] = 0,
					["role"] = "NONE",
					["auras"] = {
						["울부짖는 영혼"] = {
							["name"] = "울부짖는 영혼",
							["active"] = 0,
							["id"] = 177046,
							["uptime"] = 6,
							["auratype"] = "BUFF",
						},
						["흐르는 생각"] = {
							["name"] = "흐르는 생각",
							["active"] = 0,
							["id"] = 156150,
							["uptime"] = 4,
							["auratype"] = "BUFF",
						},
						["대마법사의 상급 염화"] = {
							["name"] = "대마법사의 상급 염화",
							["active"] = 0,
							["id"] = 177176,
							["uptime"] = 6,
							["auratype"] = "BUFF",
						},
						["피눈물의 징표"] = {
							["name"] = "피눈물의 징표",
							["active"] = 0,
							["id"] = 173322,
							["uptime"] = 5,
							["auratype"] = "BUFF",
						},
					},
					["damage"] = 1024853,
					["damagespells"] = {
						["얼음창"] = {
							["min"] = 183288,
							["critical"] = 2,
							["hit"] = 2,
							["totalhits"] = 4,
							["id"] = 228598,
							["max"] = 423977,
							["damage"] = 1024853,
						},
					},
					["first"] = 1471192716,
					["damagetaken"] = 0,
					["shielding"] = 0,
					["id"] = "Player-2116-04D356F9",
					["maxhp"] = 313740,
					["healing"] = 0,
					["damagetakenspells"] = {
					},
					["healed"] = {
					},
					["overhealing"] = 0,
					["name"] = "샤이아리",
					["alertCount"] = 0,
					["power"] = {
					},
					["healingspells"] = {
					},
					["alert"] = {
					},
					["multistrikes"] = 0,
				}, -- [1]
			},
			["deaths"] = 0,
			["mobs"] = {
				["갈퀴송곳니 기술병"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 423977,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 423977,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
				["갈퀴송곳니 공모자"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 417588,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 417588,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
				["배반자 멘누"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 183288,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 183288,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
			},
			["gotboss"] = true,
			["alertCount"] = 0,
			["healing"] = 0,
			["multistrikes"] = 0,
			["starttime"] = 1471192715,
			["overhealing"] = 0,
			["shielding"] = 0,
			["name"] = "배반자 멘누 (3)",
			["mobname"] = "배반자 멘누",
			["mobtaken"] = 1024853,
			["mobhdone"] = 0,
			["last_action"] = 1471192715,
			["endtime"] = 1471192721,
		}, -- [1]
		{
			["healingabsorbed"] = 0,
			["alertDamage"] = 0,
			["mobdone"] = 0,
			["ccbreaks"] = 0,
			["time"] = 6,
			["interrupts"] = 0,
			["dispells"] = 0,
			["damagetaken"] = 0,
			["power"] = {
			},
			["damage"] = 714621,
			["players"] = {
				{
					["last"] = 1471192474,
					["healingabsorbed"] = 0,
					["class"] = "MAGE",
					["damaged"] = {
						["갈퀴송곳니 공모자"] = 314981,
						["갈퀴송곳니 기술병"] = 199426,
						["배반자 멘누"] = 200214,
					},
					["alertDamage"] = 0,
					["ccbreaks"] = 0,
					["time"] = 5,
					["interrupts"] = 0,
					["dispells"] = 0,
					["role"] = "NONE",
					["auras"] = {
						["흐르는 생각"] = {
							["name"] = "흐르는 생각",
							["active"] = 0,
							["id"] = 156150,
							["uptime"] = 4,
							["auratype"] = "BUFF",
						},
						["대마법사의 상급 염화"] = {
							["name"] = "대마법사의 상급 염화",
							["active"] = 0,
							["id"] = 177176,
							["uptime"] = 6,
							["auratype"] = "BUFF",
						},
						["피눈물의 징표"] = {
							["name"] = "피눈물의 징표",
							["active"] = 0,
							["id"] = 173322,
							["uptime"] = 5,
							["auratype"] = "BUFF",
						},
					},
					["damage"] = 714621,
					["damagespells"] = {
						["얼음창"] = {
							["min"] = 103951,
							["hit"] = 4,
							["totalhits"] = 4,
							["id"] = 228598,
							["max"] = 211030,
							["damage"] = 714621,
						},
					},
					["first"] = 1471192469,
					["damagetaken"] = 0,
					["shielding"] = 0,
					["id"] = "Player-2116-04D356F9",
					["maxhp"] = 313740,
					["healing"] = 0,
					["damagetakenspells"] = {
					},
					["healed"] = {
					},
					["overhealing"] = 0,
					["name"] = "샤이아리",
					["alertCount"] = 0,
					["power"] = {
					},
					["healingspells"] = {
					},
					["alert"] = {
					},
					["multistrikes"] = 0,
				}, -- [1]
			},
			["deaths"] = 0,
			["mobs"] = {
				["갈퀴송곳니 공모자"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 314981,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 314981,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
				["갈퀴송곳니 기술병"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 199426,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 199426,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
				["배반자 멘누"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 200214,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 200214,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
			},
			["gotboss"] = true,
			["alertCount"] = 0,
			["healing"] = 0,
			["multistrikes"] = 0,
			["starttime"] = 1471192469,
			["overhealing"] = 0,
			["shielding"] = 0,
			["name"] = "배반자 멘누 (2)",
			["mobname"] = "배반자 멘누",
			["mobtaken"] = 714621,
			["mobhdone"] = 0,
			["last_action"] = 1471192469,
			["endtime"] = 1471192475,
		}, -- [2]
		{
			["healingabsorbed"] = 0,
			["alertDamage"] = 0,
			["ccbreaks"] = 0,
			["time"] = 13,
			["interrupts"] = 0,
			["mobdone"] = 20,
			["dispells"] = 0,
			["mobtaken"] = 1040309,
			["damage"] = 1040309,
			["players"] = {
				{
					["last"] = 1471192325,
					["healingabsorbed"] = 0,
					["class"] = "MAGE",
					["damaged"] = {
						["갈퀴송곳니 요술사"] = 321065,
						["갈퀴송곳니 노예감독"] = 207056,
						["갈퀴송곳니 용사"] = 105275,
						["거친황야 노예"] = 406913,
					},
					["alertDamage"] = 0,
					["ccbreaks"] = 0,
					["time"] = 12,
					["interrupts"] = 0,
					["dispells"] = 0,
					["role"] = "NONE",
					["auras"] = {
						["흐르는 생각"] = {
							["name"] = "흐르는 생각",
							["active"] = 0,
							["id"] = 156150,
							["uptime"] = 6,
							["auratype"] = "BUFF",
						},
						["대마법사의 상급 염화"] = {
							["name"] = "대마법사의 상급 염화",
							["active"] = 0,
							["id"] = 177176,
							["uptime"] = 12,
							["auratype"] = "BUFF",
						},
						["퍼붓는 잿가루"] = {
							["name"] = "퍼붓는 잿가루",
							["active"] = 0,
							["id"] = 177083,
							["uptime"] = 4,
							["auratype"] = "BUFF",
						},
						["용해된 금속"] = {
							["name"] = "용해된 금속",
							["active"] = 0,
							["id"] = 177081,
							["uptime"] = 4,
							["auratype"] = "BUFF",
						},
						["울부짖는 영혼"] = {
							["name"] = "울부짖는 영혼",
							["active"] = 0,
							["id"] = 177046,
							["uptime"] = 4,
							["auratype"] = "BUFF",
						},
					},
					["damage"] = 1040309,
					["damagespells"] = {
						["얼음창"] = {
							["min"] = 105275,
							["critical"] = 1,
							["hit"] = 4,
							["totalhits"] = 5,
							["id"] = 228598,
							["max"] = 406913,
							["damage"] = 1040309,
						},
					},
					["first"] = 1471192313,
					["damagetaken"] = 20,
					["shielding"] = 0,
					["id"] = "Player-2116-04D356F9",
					["maxhp"] = 313740,
					["healing"] = 0,
					["damagetakenspells"] = {
						["얼음 화살"] = {
							["crushing"] = 0,
							["id"] = 15497,
							["damage"] = 20,
							["max"] = 20,
							["name"] = "얼음 화살",
							["glancing"] = 0,
							["multistrike"] = 0,
							["critical"] = 0,
							["blocked"] = 0,
							["totalhits"] = 1,
							["resisted"] = 0,
							["min"] = 20,
							["absorbed"] = 0,
						},
					},
					["healed"] = {
					},
					["overhealing"] = 0,
					["name"] = "샤이아리",
					["alertCount"] = 0,
					["power"] = {
					},
					["healingspells"] = {
					},
					["alert"] = {
					},
					["multistrikes"] = 0,
				}, -- [1]
			},
			["deaths"] = 0,
			["mobs"] = {
				["갈퀴송곳니 요술사"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 321065,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 321065,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
				["갈퀴송곳니 노예감독"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 207056,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 207056,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
				["갈퀴송곳니 용사"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 105275,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 105275,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
				["거친황야 노예"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 406913,
							["done"] = 20,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 406913,
					["done"] = 20,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
			},
			["power"] = {
			},
			["damagetaken"] = 20,
			["healing"] = 0,
			["alertCount"] = 0,
			["shielding"] = 0,
			["starttime"] = 1471192313,
			["overhealing"] = 0,
			["name"] = "갈퀴송곳니 요술사",
			["mobname"] = "갈퀴송곳니 요술사",
			["multistrikes"] = 0,
			["mobhdone"] = 0,
			["last_action"] = 1471192313,
			["endtime"] = 1471192326,
		}, -- [3]
		{
			["healingabsorbed"] = 0,
			["alertDamage"] = 0,
			["mobdone"] = 0,
			["ccbreaks"] = 0,
			["time"] = 6,
			["interrupts"] = 0,
			["dispells"] = 0,
			["damagetaken"] = 0,
			["power"] = {
			},
			["damage"] = 819296,
			["players"] = {
				{
					["last"] = 1471192274,
					["healingabsorbed"] = 0,
					["class"] = "MAGE",
					["damaged"] = {
						["갈퀴송곳니 공모자"] = 474561,
						["갈퀴송곳니 기술병"] = 179229,
						["배반자 멘누"] = 165506,
					},
					["alertDamage"] = 0,
					["ccbreaks"] = 0,
					["time"] = 4,
					["interrupts"] = 0,
					["dispells"] = 0,
					["role"] = "NONE",
					["auras"] = {
						["피눈물의 징표"] = {
							["name"] = "피눈물의 징표",
							["active"] = 0,
							["id"] = 173322,
							["uptime"] = 5,
							["auratype"] = "BUFF",
						},
						["흐르는 생각"] = {
							["name"] = "흐르는 생각",
							["active"] = 0,
							["id"] = 156150,
							["uptime"] = 3,
							["auratype"] = "BUFF",
						},
					},
					["damage"] = 819296,
					["damagespells"] = {
						["얼음창"] = {
							["min"] = 91996,
							["critical"] = 1,
							["hit"] = 3,
							["totalhits"] = 4,
							["id"] = 228598,
							["max"] = 382565,
							["damage"] = 819296,
						},
					},
					["first"] = 1471192270,
					["damagetaken"] = 0,
					["shielding"] = 0,
					["id"] = "Player-2116-04D356F9",
					["maxhp"] = 313740,
					["healing"] = 0,
					["damagetakenspells"] = {
					},
					["healed"] = {
					},
					["overhealing"] = 0,
					["name"] = "샤이아리",
					["alertCount"] = 0,
					["power"] = {
					},
					["healingspells"] = {
					},
					["alert"] = {
					},
					["multistrikes"] = 0,
				}, -- [1]
			},
			["deaths"] = 0,
			["mobs"] = {
				["갈퀴송곳니 공모자"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 474561,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 474561,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
				["갈퀴송곳니 기술병"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 179229,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 179229,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
				["배반자 멘누"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 165506,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 165506,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
			},
			["gotboss"] = true,
			["alertCount"] = 0,
			["healing"] = 0,
			["multistrikes"] = 0,
			["starttime"] = 1471192269,
			["overhealing"] = 0,
			["shielding"] = 0,
			["name"] = "배반자 멘누",
			["mobname"] = "배반자 멘누",
			["mobtaken"] = 819296,
			["mobhdone"] = 0,
			["last_action"] = 1471192269,
			["endtime"] = 1471192275,
		}, -- [4]
		{
			["healingabsorbed"] = 0,
			["alertDamage"] = 0,
			["ccbreaks"] = 0,
			["time"] = 6,
			["interrupts"] = 0,
			["mobdone"] = 0,
			["dispells"] = 0,
			["mobtaken"] = 799383,
			["damage"] = 799383,
			["players"] = {
				{
					["last"] = 1471192166,
					["healingabsorbed"] = 0,
					["class"] = "MAGE",
					["damaged"] = {
						["상급 수렁마크루라"] = 427242,
						["수렁마크루라"] = 372141,
					},
					["alertDamage"] = 0,
					["ccbreaks"] = 0,
					["time"] = 4,
					["interrupts"] = 0,
					["dispells"] = 0,
					["role"] = "NONE",
					["auras"] = {
						["흐르는 생각"] = {
							["name"] = "흐르는 생각",
							["active"] = 0,
							["id"] = 156150,
							["uptime"] = 5,
							["auratype"] = "BUFF",
						},
						["용해된 금속"] = {
							["name"] = "용해된 금속",
							["active"] = 0,
							["id"] = 177081,
							["uptime"] = 6,
							["auratype"] = "BUFF",
						},
						["퍼붓는 잿가루"] = {
							["name"] = "퍼붓는 잿가루",
							["active"] = 0,
							["id"] = 177083,
							["uptime"] = 5,
							["auratype"] = "BUFF",
						},
					},
					["damage"] = 799383,
					["damagespells"] = {
						["얼음창"] = {
							["min"] = 82541,
							["critical"] = 2,
							["hit"] = 1,
							["totalhits"] = 3,
							["id"] = 228598,
							["max"] = 372141,
							["damage"] = 799383,
						},
					},
					["first"] = 1471192162,
					["damagetaken"] = 0,
					["shielding"] = 0,
					["id"] = "Player-2116-04D356F9",
					["maxhp"] = 313740,
					["healing"] = 0,
					["damagetakenspells"] = {
					},
					["healed"] = {
					},
					["overhealing"] = 0,
					["name"] = "샤이아리",
					["alertCount"] = 0,
					["power"] = {
					},
					["healingspells"] = {
					},
					["alert"] = {
					},
					["multistrikes"] = 0,
				}, -- [1]
			},
			["deaths"] = 0,
			["mobs"] = {
				["상급 수렁마크루라"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 427242,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 427242,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
				["수렁마크루라"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 372141,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 372141,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
			},
			["power"] = {
			},
			["damagetaken"] = 0,
			["healing"] = 0,
			["alertCount"] = 0,
			["shielding"] = 0,
			["starttime"] = 1471192161,
			["overhealing"] = 0,
			["name"] = "상급 수렁마크루라",
			["mobname"] = "상급 수렁마크루라",
			["multistrikes"] = 0,
			["mobhdone"] = 0,
			["last_action"] = 1471192161,
			["endtime"] = 1471192167,
		}, -- [5]
		{
			["healingabsorbed"] = 0,
			["alertDamage"] = 0,
			["ccbreaks"] = 0,
			["time"] = 7,
			["interrupts"] = 0,
			["mobdone"] = 0,
			["dispells"] = 0,
			["mobtaken"] = 0,
			["damage"] = 0,
			["players"] = {
				{
					["last"] = 1471192152,
					["healingabsorbed"] = 0,
					["class"] = "MAGE",
					["damaged"] = {
					},
					["alertDamage"] = 0,
					["ccbreaks"] = 0,
					["time"] = 2,
					["interrupts"] = 0,
					["dispells"] = 0,
					["role"] = "NONE",
					["auras"] = {
					},
					["damage"] = 0,
					["damagespells"] = {
						["얼음창"] = {
							["totalhits"] = 2,
							["EVADE"] = 2,
							["max"] = 0,
							["id"] = 228598,
							["damage"] = 0,
						},
					},
					["first"] = 1471192150,
					["damagetaken"] = 0,
					["shielding"] = 0,
					["id"] = "Player-2116-04D356F9",
					["maxhp"] = 313740,
					["healing"] = 0,
					["damagetakenspells"] = {
					},
					["healed"] = {
					},
					["overhealing"] = 0,
					["name"] = "샤이아리",
					["alertCount"] = 0,
					["power"] = {
					},
					["healingspells"] = {
					},
					["alert"] = {
					},
					["multistrikes"] = 0,
				}, -- [1]
			},
			["deaths"] = 0,
			["mobs"] = {
			},
			["power"] = {
			},
			["damagetaken"] = 0,
			["healing"] = 0,
			["alertCount"] = 0,
			["shielding"] = 0,
			["starttime"] = 1471192150,
			["overhealing"] = 0,
			["name"] = "거친황야 일꾼 (2)",
			["mobname"] = "거친황야 일꾼",
			["multistrikes"] = 0,
			["mobhdone"] = 0,
			["last_action"] = 1471192150,
			["endtime"] = 1471192157,
		}, -- [6]
		{
			["healingabsorbed"] = 0,
			["alertDamage"] = 0,
			["ccbreaks"] = 0,
			["time"] = 10,
			["interrupts"] = 0,
			["mobdone"] = 0,
			["dispells"] = 0,
			["mobtaken"] = 423287,
			["damage"] = 423287,
			["players"] = {
				{
					["last"] = 1471192148,
					["healingabsorbed"] = 0,
					["class"] = "MAGE",
					["damaged"] = {
						["상급 수렁마크루라"] = 263283,
						["수렁마크루라"] = 160004,
					},
					["alertDamage"] = 0,
					["ccbreaks"] = 0,
					["time"] = 8,
					["interrupts"] = 0,
					["dispells"] = 0,
					["role"] = "NONE",
					["auras"] = {
						["점멸"] = {
							["name"] = "점멸",
							["active"] = 0,
							["id"] = 1953,
							["uptime"] = 1,
							["auratype"] = "BUFF",
						},
						["흐르는 생각"] = {
							["name"] = "흐르는 생각",
							["active"] = 0,
							["id"] = 156150,
							["uptime"] = 2,
							["auratype"] = "BUFF",
						},
					},
					["damage"] = 423287,
					["damagespells"] = {
						["얼음창"] = {
							["min"] = 80789,
							["totalhits"] = 5,
							["hit"] = 3,
							["EVADE"] = 2,
							["max"] = 182494,
							["id"] = 228598,
							["damage"] = 423287,
						},
					},
					["first"] = 1471192140,
					["damagetaken"] = 0,
					["shielding"] = 0,
					["id"] = "Player-2116-04D356F9",
					["maxhp"] = 313740,
					["healing"] = 0,
					["damagetakenspells"] = {
					},
					["healed"] = {
					},
					["overhealing"] = 0,
					["name"] = "샤이아리",
					["alertCount"] = 0,
					["power"] = {
					},
					["healingspells"] = {
					},
					["alert"] = {
					},
					["multistrikes"] = 0,
				}, -- [1]
			},
			["deaths"] = 0,
			["mobs"] = {
				["상급 수렁마크루라"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 263283,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 263283,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
				["수렁마크루라"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 160004,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 160004,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
			},
			["power"] = {
			},
			["damagetaken"] = 0,
			["healing"] = 0,
			["alertCount"] = 0,
			["shielding"] = 0,
			["starttime"] = 1471192139,
			["overhealing"] = 0,
			["name"] = "거친황야 일꾼",
			["mobname"] = "거친황야 일꾼",
			["multistrikes"] = 0,
			["mobhdone"] = 0,
			["last_action"] = 1471192139,
			["endtime"] = 1471192149,
		}, -- [7]
		{
			["healingabsorbed"] = 0,
			["alertDamage"] = 0,
			["ccbreaks"] = 0,
			["time"] = 75,
			["interrupts"] = 0,
			["mobdone"] = 0,
			["dispells"] = 0,
			["mobtaken"] = 19612044,
			["damage"] = 19612044,
			["players"] = {
				{
					["last"] = 1471191496,
					["healingabsorbed"] = 0,
					["class"] = "MAGE",
					["damaged"] = {
						["토끼"] = 58221,
						["다람쥐"] = 58220,
						["훈련용 허수아비"] = 19444717,
						["공격대원의 훈련용 허수아비"] = 50886,
					},
					["alertDamage"] = 0,
					["ccbreaks"] = 0,
					["time"] = 72,
					["interrupts"] = 0,
					["dispells"] = 0,
					["role"] = "NONE",
					["auras"] = {
						["흐르는 생각"] = {
							["name"] = "흐르는 생각",
							["active"] = 0,
							["id"] = 156150,
							["uptime"] = 31,
							["auratype"] = "BUFF",
						},
						["대마법사의 상급 염화"] = {
							["name"] = "대마법사의 상급 염화",
							["active"] = 0,
							["id"] = 177176,
							["uptime"] = 10,
							["auratype"] = "BUFF",
						},
						["용해된 금속"] = {
							["name"] = "용해된 금속",
							["active"] = 0,
							["id"] = 177081,
							["uptime"] = 30,
							["auratype"] = "BUFF",
						},
						["고드름"] = {
							["name"] = "고드름",
							["active"] = 0,
							["id"] = 205473,
							["uptime"] = 20,
							["auratype"] = "BUFF",
						},
						["두뇌 빙결"] = {
							["name"] = "두뇌 빙결",
							["active"] = 0,
							["id"] = 190446,
							["uptime"] = 5,
							["auratype"] = "BUFF",
						},
						["서리의 손가락"] = {
							["name"] = "서리의 손가락",
							["active"] = 0,
							["id"] = 44544,
							["uptime"] = 19,
							["auratype"] = "BUFF",
						},
						["질풍"] = {
							["name"] = "질풍",
							["active"] = 0,
							["id"] = 228354,
							["uptime"] = 5,
							["auratype"] = "DEBUFF",
						},
						["퍼붓는 잿가루"] = {
							["name"] = "퍼붓는 잿가루",
							["active"] = 0,
							["id"] = 177083,
							["uptime"] = 30,
							["auratype"] = "BUFF",
						},
						["빙결"] = {
							["name"] = "빙결",
							["active"] = 0,
							["id"] = 205708,
							["uptime"] = 20,
							["auratype"] = "DEBUFF",
						},
						["울부짖는 영혼"] = {
							["name"] = "울부짖는 영혼",
							["active"] = 0,
							["id"] = 177046,
							["uptime"] = 24,
							["auratype"] = "BUFF",
						},
						["얼음 파편"] = {
							["name"] = "얼음 파편",
							["active"] = 0,
							["id"] = 166869,
							["uptime"] = 17,
							["auratype"] = "BUFF",
						},
						["피눈물의 징표"] = {
							["name"] = "피눈물의 징표",
							["active"] = 0,
							["id"] = 173322,
							["uptime"] = 11,
							["auratype"] = "BUFF",
						},
					},
					["damage"] = 19612044,
					["damagespells"] = {
						["얼음 화살"] = {
							["min"] = 183122,
							["critical"] = 6,
							["hit"] = 18,
							["totalhits"] = 24,
							["id"] = 228597,
							["max"] = 422591,
							["damage"] = 6080085,
						},
						["얼어붙은 구슬"] = {
							["min"] = 2018,
							["critical"] = 4,
							["hit"] = 42,
							["totalhits"] = 46,
							["id"] = 84721,
							["max"] = 71499,
							["damage"] = 1064638,
						},
						["얼음창"] = {
							["min"] = 53419,
							["critical"] = 20,
							["hit"] = 10,
							["totalhits"] = 30,
							["id"] = 228598,
							["max"] = 669430,
							["damage"] = 10399442,
						},
						["질풍"] = {
							["min"] = 96938,
							["critical"] = 3,
							["hit"] = 9,
							["totalhits"] = 12,
							["id"] = 228354,
							["max"] = 223743,
							["damage"] = 1603383,
						},
						["고드름"] = {
							["min"] = 4032,
							["hit"] = 48,
							["totalhits"] = 48,
							["id"] = 148022,
							["max"] = 22075,
							["damage"] = 464496,
						},
					},
					["first"] = 1471191424,
					["damagetaken"] = 0,
					["shielding"] = 0,
					["id"] = "Player-2116-04D356F9",
					["maxhp"] = 313740,
					["healing"] = 0,
					["damagetakenspells"] = {
					},
					["healed"] = {
					},
					["overhealing"] = 0,
					["name"] = "샤이아리",
					["alertCount"] = 0,
					["power"] = {
					},
					["healingspells"] = {
					},
					["alert"] = {
					},
					["multistrikes"] = 0,
				}, -- [1]
			},
			["deaths"] = 0,
			["mobs"] = {
				["다람쥐"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 58220,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 58220,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
				["공격대원의 훈련용 허수아비"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 50886,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 50886,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
				["토끼"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 58221,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 58221,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
				["훈련용 허수아비"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 19444717,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 19444717,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
				["루네코-헬스크림"] = {
					["players"] = {
					},
					["hdone"] = 0,
					["hdonespell"] = {
						["생기흡수"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 6353,
							["hits"] = 23,
						},
					},
					["taken"] = 0,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
						["생기흡수"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 6353,
							["hits"] = 23,
						},
					},
				},
			},
			["power"] = {
			},
			["damagetaken"] = 0,
			["healing"] = 0,
			["alertCount"] = 0,
			["shielding"] = 0,
			["starttime"] = 1471191423,
			["overhealing"] = 0,
			["name"] = "훈련용 허수아비 (3)",
			["mobname"] = "훈련용 허수아비",
			["multistrikes"] = 0,
			["mobhdone"] = 0,
			["last_action"] = 1471191423,
			["endtime"] = 1471191498,
		}, -- [8]
		{
			["healingabsorbed"] = 0,
			["alertDamage"] = 0,
			["ccbreaks"] = 0,
			["time"] = 39,
			["interrupts"] = 0,
			["mobdone"] = 0,
			["dispells"] = 0,
			["mobtaken"] = 5248306,
			["damage"] = 5248306,
			["players"] = {
				{
					["last"] = 1471191418,
					["healingabsorbed"] = 0,
					["class"] = "MAGE",
					["damaged"] = {
						["훈련용 허수아비"] = 5248306,
					},
					["alertDamage"] = 0,
					["ccbreaks"] = 0,
					["time"] = 34,
					["interrupts"] = 0,
					["dispells"] = 0,
					["role"] = "NONE",
					["auras"] = {
						["두뇌 빙결"] = {
							["name"] = "두뇌 빙결",
							["active"] = 0,
							["id"] = 190446,
							["uptime"] = 10,
							["auratype"] = "BUFF",
						},
						["질풍"] = {
							["name"] = "질풍",
							["active"] = 0,
							["id"] = 228354,
							["uptime"] = 6,
							["auratype"] = "DEBUFF",
						},
						["대마법사의 상급 염화"] = {
							["name"] = "대마법사의 상급 염화",
							["active"] = 0,
							["id"] = 177176,
							["uptime"] = 19,
							["auratype"] = "BUFF",
						},
						["고드름"] = {
							["name"] = "고드름",
							["active"] = 0,
							["id"] = 205473,
							["uptime"] = 11,
							["auratype"] = "BUFF",
						},
						["빙결"] = {
							["name"] = "빙결",
							["active"] = 0,
							["id"] = 205708,
							["uptime"] = 33,
							["auratype"] = "DEBUFF",
						},
						["서리의 손가락"] = {
							["name"] = "서리의 손가락",
							["active"] = 0,
							["id"] = 44544,
							["uptime"] = 3,
							["auratype"] = "BUFF",
						},
						["흐르는 생각"] = {
							["name"] = "흐르는 생각",
							["active"] = 0,
							["id"] = 156150,
							["uptime"] = 16,
							["auratype"] = "BUFF",
						},
						["피눈물의 징표"] = {
							["name"] = "피눈물의 징표",
							["active"] = 0,
							["id"] = 173322,
							["uptime"] = 7,
							["auratype"] = "BUFF",
						},
					},
					["damage"] = 5248306,
					["damagespells"] = {
						["질풍"] = {
							["min"] = 104414,
							["critical"] = 1,
							["hit"] = 11,
							["totalhits"] = 12,
							["id"] = 228354,
							["max"] = 208864,
							["damage"] = 1437839,
						},
						["얼음창"] = {
							["min"] = 310835,
							["critical"] = 1,
							["hit"] = 1,
							["totalhits"] = 2,
							["id"] = 228598,
							["max"] = 317835,
							["damage"] = 628670,
						},
						["얼음 화살"] = {
							["min"] = 183122,
							["critical"] = 3,
							["hit"] = 8,
							["totalhits"] = 11,
							["id"] = 228597,
							["max"] = 485964,
							["damage"] = 2983438,
						},
						["고드름"] = {
							["min"] = 4032,
							["hit"] = 20,
							["totalhits"] = 20,
							["id"] = 148022,
							["max"] = 21398,
							["damage"] = 198359,
						},
					},
					["first"] = 1471191384,
					["damagetaken"] = 0,
					["shielding"] = 0,
					["id"] = "Player-2116-04D356F9",
					["maxhp"] = 313740,
					["healing"] = 0,
					["damagetakenspells"] = {
					},
					["healed"] = {
					},
					["overhealing"] = 0,
					["name"] = "샤이아리",
					["alertCount"] = 0,
					["power"] = {
					},
					["healingspells"] = {
					},
					["alert"] = {
					},
					["multistrikes"] = 0,
				}, -- [1]
			},
			["deaths"] = 0,
			["mobs"] = {
				["사제옥선-세나리우스"] = {
					["players"] = {
					},
					["hdone"] = 0,
					["hdonespell"] = {
						["탄원"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 14108,
							["hits"] = 2,
						},
						["속죄"] = {
							["min"] = 0,
							["crits"] = 1,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 15131,
							["hits"] = 23,
						},
						["어둠의 치유"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 23509,
							["hits"] = 1,
						},
					},
					["taken"] = 0,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
						["탄원"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 14108,
							["hits"] = 2,
						},
						["속죄"] = {
							["min"] = 0,
							["crits"] = 1,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 8434,
							["hits"] = 13,
						},
						["어둠의 치유"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 23509,
							["hits"] = 1,
						},
					},
				},
				["모두까기-듀로탄"] = {
					["players"] = {
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 0,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
						["속죄"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 3348,
							["hits"] = 5,
						},
					},
				},
				["훈련용 허수아비"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 5248306,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 5248306,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
				["Jakart-듀로탄"] = {
					["players"] = {
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 0,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
						["속죄"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 3349,
							["hits"] = 5,
						},
					},
				},
			},
			["power"] = {
			},
			["damagetaken"] = 0,
			["healing"] = 0,
			["alertCount"] = 0,
			["shielding"] = 0,
			["starttime"] = 1471191384,
			["overhealing"] = 0,
			["name"] = "훈련용 허수아비 (2)",
			["mobname"] = "훈련용 허수아비",
			["multistrikes"] = 0,
			["mobhdone"] = 0,
			["last_action"] = 1471191384,
			["endtime"] = 1471191423,
		}, -- [9]
		{
			["healingabsorbed"] = 0,
			["alertDamage"] = 0,
			["ccbreaks"] = 0,
			["time"] = 12,
			["interrupts"] = 0,
			["mobdone"] = 0,
			["dispells"] = 0,
			["mobtaken"] = 950028,
			["damage"] = 950028,
			["players"] = {
				{
					["last"] = 1471191378,
					["healingabsorbed"] = 0,
					["class"] = "MAGE",
					["damaged"] = {
						["훈련용 허수아비"] = 950028,
					},
					["alertDamage"] = 0,
					["ccbreaks"] = 0,
					["time"] = 10,
					["interrupts"] = 0,
					["dispells"] = 0,
					["role"] = "NONE",
					["auras"] = {
						["흐르는 생각"] = {
							["name"] = "흐르는 생각",
							["active"] = 0,
							["id"] = 156150,
							["uptime"] = 4,
							["auratype"] = "BUFF",
						},
						["대마법사의 상급 염화"] = {
							["name"] = "대마법사의 상급 염화",
							["active"] = 0,
							["id"] = 177176,
							["uptime"] = 10,
							["auratype"] = "BUFF",
						},
						["용해된 금속"] = {
							["name"] = "용해된 금속",
							["active"] = 0,
							["id"] = 177081,
							["uptime"] = 10,
							["auratype"] = "BUFF",
						},
						["고드름"] = {
							["name"] = "고드름",
							["active"] = 0,
							["id"] = 205473,
							["uptime"] = 10,
							["auratype"] = "BUFF",
						},
						["질풍"] = {
							["name"] = "질풍",
							["active"] = 0,
							["id"] = 228354,
							["uptime"] = 2,
							["auratype"] = "DEBUFF",
						},
						["퍼붓는 잿가루"] = {
							["name"] = "퍼붓는 잿가루",
							["active"] = 0,
							["id"] = 177083,
							["uptime"] = 10,
							["auratype"] = "BUFF",
						},
						["빙결"] = {
							["name"] = "빙결",
							["active"] = 0,
							["id"] = 205708,
							["uptime"] = 9,
							["auratype"] = "DEBUFF",
						},
						["울부짖는 영혼"] = {
							["name"] = "울부짖는 영혼",
							["active"] = 0,
							["id"] = 177046,
							["uptime"] = 10,
							["auratype"] = "BUFF",
						},
						["피눈물의 징표"] = {
							["name"] = "피눈물의 징표",
							["active"] = 0,
							["id"] = 173322,
							["uptime"] = 11,
							["auratype"] = "BUFF",
						},
					},
					["damage"] = 950028,
					["damagespells"] = {
						["질풍"] = {
							["min"] = 120070,
							["critical"] = 1,
							["hit"] = 2,
							["totalhits"] = 3,
							["id"] = 228354,
							["max"] = 240104,
							["damage"] = 480263,
						},
						["얼음 화살"] = {
							["min"] = 234882,
							["hit"] = 2,
							["totalhits"] = 2,
							["id"] = 228597,
							["max"] = 234883,
							["damage"] = 469765,
						},
					},
					["first"] = 1471191368,
					["damagetaken"] = 0,
					["shielding"] = 0,
					["id"] = "Player-2116-04D356F9",
					["maxhp"] = 313740,
					["healing"] = 0,
					["damagetakenspells"] = {
					},
					["healed"] = {
					},
					["overhealing"] = 0,
					["name"] = "샤이아리",
					["alertCount"] = 0,
					["power"] = {
					},
					["healingspells"] = {
					},
					["alert"] = {
					},
					["multistrikes"] = 0,
				}, -- [1]
			},
			["deaths"] = 0,
			["mobs"] = {
				["사제옥선-세나리우스"] = {
					["players"] = {
					},
					["hdone"] = 0,
					["hdonespell"] = {
						["어둠의 치유"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 20041,
							["hits"] = 1,
						},
						["속죄"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 3279,
							["hits"] = 6,
						},
						["신의 권능: 광휘"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 20040,
							["hits"] = 3,
						},
					},
					["taken"] = 0,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
						["어둠의 치유"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 20041,
							["hits"] = 1,
						},
						["속죄"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 1093,
							["hits"] = 2,
						},
						["신의 권능: 광휘"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 6680,
							["hits"] = 1,
						},
					},
				},
				["Jakart-듀로탄"] = {
					["players"] = {
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 0,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
						["속죄"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 1093,
							["hits"] = 2,
						},
						["신의 권능: 광휘"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 6680,
							["hits"] = 1,
						},
					},
				},
				["훈련용 허수아비"] = {
					["players"] = {
						["샤이아리"] = {
							["taken"] = 950028,
							["done"] = 0,
							["class"] = "MAGE",
							["role"] = "NONE",
						},
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 950028,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
					},
				},
				["모두까기-듀로탄"] = {
					["players"] = {
					},
					["hdone"] = 0,
					["hdonespell"] = {
					},
					["taken"] = 0,
					["done"] = 0,
					["htaken"] = 0,
					["htakenspell"] = {
						["속죄"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 1093,
							["hits"] = 2,
						},
						["신의 권능: 광휘"] = {
							["min"] = 0,
							["crits"] = 0,
							["max"] = 0,
							["healing"] = 0,
							["overhealing"] = 6680,
							["hits"] = 1,
						},
					},
				},
			},
			["power"] = {
			},
			["damagetaken"] = 0,
			["healing"] = 0,
			["alertCount"] = 0,
			["shielding"] = 0,
			["starttime"] = 1471191368,
			["overhealing"] = 0,
			["name"] = "훈련용 허수아비",
			["mobname"] = "훈련용 허수아비",
			["multistrikes"] = 0,
			["mobhdone"] = 0,
			["last_action"] = 1471191368,
			["endtime"] = 1471191380,
		}, -- [10]
	},
	["total"] = {
		["healingabsorbed"] = 0,
		["alertDamage"] = 0,
		["ccbreaks"] = 0,
		["time"] = 192,
		["interrupts"] = 0,
		["mobdone"] = 20,
		["damage"] = 34247489,
		["players"] = {
			{
				["healingabsorbed"] = 0,
				["class"] = "MAGE",
				["damaged"] = {
				},
				["alertDamage"] = 0,
				["ccbreaks"] = 0,
				["time"] = 174,
				["interrupts"] = 0,
				["dispells"] = 0,
				["role"] = "NONE",
				["auras"] = {
					["흐르는 생각"] = {
						["name"] = "흐르는 생각",
						["active"] = 0,
						["id"] = 156150,
						["uptime"] = 136,
						["auratype"] = "BUFF",
					},
					["대마법사의 상급 염화"] = {
						["name"] = "대마법사의 상급 염화",
						["active"] = 0,
						["id"] = 177176,
						["uptime"] = 122,
						["auratype"] = "BUFF",
					},
					["울부짖는 영혼"] = {
						["name"] = "울부짖는 영혼",
						["active"] = 0,
						["id"] = 177046,
						["uptime"] = 111,
						["auratype"] = "BUFF",
					},
					["용해된 금속"] = {
						["name"] = "용해된 금속",
						["active"] = 0,
						["id"] = 177081,
						["uptime"] = 113,
						["auratype"] = "BUFF",
					},
					["고드름"] = {
						["name"] = "고드름",
						["active"] = 0,
						["id"] = 205473,
						["uptime"] = 177,
						["auratype"] = "BUFF",
					},
					["두뇌 빙결"] = {
						["name"] = "두뇌 빙결",
						["active"] = 0,
						["id"] = 190446,
						["uptime"] = 162,
						["auratype"] = "BUFF",
					},
					["서리의 손가락"] = {
						["name"] = "서리의 손가락",
						["active"] = 0,
						["id"] = 44544,
						["uptime"] = 22,
						["auratype"] = "BUFF",
					},
					["질풍"] = {
						["name"] = "질풍",
						["active"] = 0,
						["id"] = 228354,
						["uptime"] = 13,
						["auratype"] = "DEBUFF",
					},
					["퍼붓는 잿가루"] = {
						["name"] = "퍼붓는 잿가루",
						["active"] = 0,
						["id"] = 177083,
						["uptime"] = 112,
						["auratype"] = "BUFF",
					},
					["빙결"] = {
						["name"] = "빙결",
						["active"] = 0,
						["id"] = 205708,
						["uptime"] = 198,
						["auratype"] = "DEBUFF",
					},
					["점멸"] = {
						["name"] = "점멸",
						["active"] = 0,
						["id"] = 1953,
						["uptime"] = 1,
						["auratype"] = "BUFF",
					},
					["얼음 파편"] = {
						["name"] = "얼음 파편",
						["active"] = 0,
						["id"] = 166869,
						["uptime"] = 17,
						["auratype"] = "BUFF",
					},
					["피눈물의 징표"] = {
						["name"] = "피눈물의 징표",
						["active"] = 0,
						["id"] = 173322,
						["uptime"] = 215,
						["auratype"] = "BUFF",
					},
				},
				["damage"] = 34247489,
				["damagespells"] = {
					["얼음 화살"] = {
						["min"] = 183122,
						["critical"] = 9,
						["hit"] = 28,
						["totalhits"] = 37,
						["id"] = 228597,
						["max"] = 485964,
						["damage"] = 9533288,
					},
					["얼어붙은 구슬"] = {
						["min"] = 2018,
						["critical"] = 4,
						["hit"] = 42,
						["totalhits"] = 46,
						["id"] = 84721,
						["max"] = 71499,
						["damage"] = 1064638,
					},
					["얼음창"] = {
						["min"] = 53419,
						["totalhits"] = 80,
						["critical"] = 32,
						["hit"] = 42,
						["EVADE"] = 6,
						["max"] = 669430,
						["id"] = 228598,
						["damage"] = 19341618,
					},
					["눈보라"] = {
						["min"] = 3320,
						["critical"] = 2,
						["hit"] = 6,
						["totalhits"] = 8,
						["id"] = 190357,
						["max"] = 92576,
						["damage"] = 123605,
					},
					["질풍"] = {
						["min"] = 96938,
						["critical"] = 5,
						["hit"] = 22,
						["totalhits"] = 27,
						["id"] = 228354,
						["max"] = 240104,
						["damage"] = 3521485,
					},
					["고드름"] = {
						["min"] = 4032,
						["hit"] = 68,
						["totalhits"] = 68,
						["id"] = 148022,
						["max"] = 22075,
						["damage"] = 662855,
					},
				},
				["healing"] = 0,
				["damagetaken"] = 20,
				["shielding"] = 0,
				["id"] = "Player-2116-04D356F9",
				["maxhp"] = 313740,
				["deathlog"] = {
					{
						["ts"] = 1471192318.411,
						["amount"] = -20,
						["srcname"] = "거친황야 노예",
						["hp"] = 313720,
						["spellid"] = 15497,
					}, -- [1]
					["pos"] = 2,
				},
				["damagetakenspells"] = {
					["얼음 화살"] = {
						["crushing"] = 0,
						["id"] = 15497,
						["damage"] = 20,
						["max"] = 20,
						["name"] = "얼음 화살",
						["glancing"] = 0,
						["multistrike"] = 0,
						["critical"] = 0,
						["blocked"] = 0,
						["totalhits"] = 1,
						["resisted"] = 0,
						["min"] = 20,
						["absorbed"] = 0,
					},
				},
				["healed"] = {
				},
				["overhealing"] = 0,
				["name"] = "샤이아리",
				["alertCount"] = 0,
				["power"] = {
				},
				["healingspells"] = {
				},
				["alert"] = {
				},
				["multistrikes"] = 0,
			}, -- [1]
		},
		["deaths"] = 0,
		["mobs"] = {
			["수렁마크루라"] = {
				["players"] = {
					["샤이아리"] = {
						["taken"] = 697040,
						["done"] = 0,
						["class"] = "MAGE",
						["role"] = "NONE",
					},
				},
				["hdone"] = 0,
				["hdonespell"] = {
				},
				["taken"] = 697040,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
				},
			},
			["배반자 멘누"] = {
				["players"] = {
					["샤이아리"] = {
						["taken"] = 926649,
						["done"] = 0,
						["class"] = "MAGE",
						["role"] = "NONE",
					},
				},
				["hdone"] = 0,
				["hdonespell"] = {
				},
				["taken"] = 926649,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
				},
			},
			["외무부장관-세나리우스"] = {
				["players"] = {
				},
				["hdone"] = 0,
				["hdonespell"] = {
					["피의 갈증 치유"] = {
						["min"] = 0,
						["crits"] = 0,
						["max"] = 0,
						["healing"] = 0,
						["overhealing"] = 2022,
						["hits"] = 1,
					},
				},
				["taken"] = 0,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
					["피의 갈증 치유"] = {
						["min"] = 0,
						["crits"] = 0,
						["max"] = 0,
						["healing"] = 0,
						["overhealing"] = 2022,
						["hits"] = 1,
					},
				},
			},
			["거친황야 일꾼"] = {
				["players"] = {
					["샤이아리"] = {
						["taken"] = 90859,
						["done"] = 0,
						["class"] = "MAGE",
						["role"] = "NONE",
					},
				},
				["hdone"] = 0,
				["hdonespell"] = {
				},
				["taken"] = 90859,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
				},
			},
			["갈퀴송곳니 요술사"] = {
				["players"] = {
					["샤이아리"] = {
						["taken"] = 580351,
						["done"] = 0,
						["class"] = "MAGE",
						["role"] = "NONE",
					},
				},
				["hdone"] = 0,
				["hdonespell"] = {
				},
				["taken"] = 580351,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
				},
			},
			["갈퀴송곳니 노예감독"] = {
				["players"] = {
					["샤이아리"] = {
						["taken"] = 920486,
						["done"] = 0,
						["class"] = "MAGE",
						["role"] = "NONE",
					},
				},
				["hdone"] = 0,
				["hdonespell"] = {
				},
				["taken"] = 920486,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
				},
			},
			["토끼"] = {
				["players"] = {
					["샤이아리"] = {
						["taken"] = 150797,
						["done"] = 0,
						["class"] = "MAGE",
						["role"] = "NONE",
					},
				},
				["hdone"] = 0,
				["hdonespell"] = {
				},
				["taken"] = 150797,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
				},
			},
			["루네코-헬스크림"] = {
				["players"] = {
				},
				["hdone"] = 0,
				["hdonespell"] = {
					["생기흡수"] = {
						["min"] = 0,
						["crits"] = 0,
						["max"] = 0,
						["healing"] = 0,
						["overhealing"] = 6353,
						["hits"] = 23,
					},
				},
				["taken"] = 0,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
					["생기흡수"] = {
						["min"] = 0,
						["crits"] = 0,
						["max"] = 0,
						["healing"] = 0,
						["overhealing"] = 6353,
						["hits"] = 23,
					},
				},
			},
			["상급 수렁마크루라"] = {
				["players"] = {
					["샤이아리"] = {
						["taken"] = 1029486,
						["done"] = 0,
						["class"] = "MAGE",
						["role"] = "NONE",
					},
				},
				["hdone"] = 0,
				["hdonespell"] = {
				},
				["taken"] = 1029486,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
				},
			},
			["갈퀴송곳니 기술병"] = {
				["players"] = {
					["샤이아리"] = {
						["taken"] = 802632,
						["done"] = 0,
						["class"] = "MAGE",
						["role"] = "NONE",
					},
				},
				["hdone"] = 0,
				["hdonespell"] = {
				},
				["taken"] = 802632,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
				},
			},
			["갈퀴송곳니 용사"] = {
				["players"] = {
					["샤이아리"] = {
						["taken"] = 1569825,
						["done"] = 0,
						["class"] = "MAGE",
						["role"] = "NONE",
					},
				},
				["hdone"] = 0,
				["hdonespell"] = {
				},
				["taken"] = 1569825,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
				},
			},
			["훈련용 허수아비"] = {
				["players"] = {
					["샤이아리"] = {
						["taken"] = 25643051,
						["done"] = 0,
						["class"] = "MAGE",
						["role"] = "NONE",
					},
				},
				["hdone"] = 0,
				["hdonespell"] = {
				},
				["taken"] = 25643051,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
				},
			},
			["Jakart-듀로탄"] = {
				["players"] = {
				},
				["hdone"] = 0,
				["hdonespell"] = {
				},
				["taken"] = 0,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
					["속죄"] = {
						["min"] = 0,
						["crits"] = 0,
						["max"] = 0,
						["healing"] = 0,
						["overhealing"] = 4442,
						["hits"] = 7,
					},
					["신의 권능: 광휘"] = {
						["min"] = 0,
						["crits"] = 0,
						["max"] = 0,
						["healing"] = 0,
						["overhealing"] = 13360,
						["hits"] = 2,
					},
				},
			},
			["거친황야 노예"] = {
				["players"] = {
					["샤이아리"] = {
						["taken"] = 489048,
						["done"] = 20,
						["class"] = "MAGE",
						["role"] = "NONE",
					},
				},
				["hdone"] = 0,
				["hdonespell"] = {
				},
				["taken"] = 489048,
				["done"] = 20,
				["htaken"] = 0,
				["htakenspell"] = {
				},
			},
			["사제옥선-세나리우스"] = {
				["players"] = {
				},
				["hdone"] = 0,
				["hdonespell"] = {
					["어둠의 치유"] = {
						["min"] = 0,
						["crits"] = 0,
						["max"] = 0,
						["healing"] = 0,
						["overhealing"] = 43550,
						["hits"] = 2,
					},
					["탄원"] = {
						["min"] = 0,
						["crits"] = 0,
						["max"] = 0,
						["healing"] = 0,
						["overhealing"] = 14108,
						["hits"] = 2,
					},
					["속죄"] = {
						["min"] = 0,
						["crits"] = 1,
						["max"] = 0,
						["healing"] = 0,
						["overhealing"] = 18410,
						["hits"] = 29,
					},
					["신의 권능: 광휘"] = {
						["min"] = 0,
						["crits"] = 0,
						["max"] = 0,
						["healing"] = 0,
						["overhealing"] = 40080,
						["hits"] = 6,
					},
				},
				["taken"] = 0,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
					["어둠의 치유"] = {
						["min"] = 0,
						["crits"] = 0,
						["max"] = 0,
						["healing"] = 0,
						["overhealing"] = 43550,
						["hits"] = 2,
					},
					["탄원"] = {
						["min"] = 0,
						["crits"] = 0,
						["max"] = 0,
						["healing"] = 0,
						["overhealing"] = 14108,
						["hits"] = 2,
					},
					["속죄"] = {
						["min"] = 0,
						["crits"] = 1,
						["max"] = 0,
						["healing"] = 0,
						["overhealing"] = 9527,
						["hits"] = 15,
					},
					["신의 권능: 광휘"] = {
						["min"] = 0,
						["crits"] = 0,
						["max"] = 0,
						["healing"] = 0,
						["overhealing"] = 13360,
						["hits"] = 2,
					},
				},
			},
			["모두까기-듀로탄"] = {
				["players"] = {
				},
				["hdone"] = 0,
				["hdonespell"] = {
				},
				["taken"] = 0,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
					["속죄"] = {
						["min"] = 0,
						["crits"] = 0,
						["max"] = 0,
						["healing"] = 0,
						["overhealing"] = 4441,
						["hits"] = 7,
					},
					["신의 권능: 광휘"] = {
						["min"] = 0,
						["crits"] = 0,
						["max"] = 0,
						["healing"] = 0,
						["overhealing"] = 13360,
						["hits"] = 2,
					},
				},
			},
			["다람쥐"] = {
				["players"] = {
					["샤이아리"] = {
						["taken"] = 58220,
						["done"] = 0,
						["class"] = "MAGE",
						["role"] = "NONE",
					},
				},
				["hdone"] = 0,
				["hdonespell"] = {
				},
				["taken"] = 58220,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
				},
			},
			["갈퀴송곳니 공모자"] = {
				["players"] = {
					["샤이아리"] = {
						["taken"] = 1207130,
						["done"] = 0,
						["class"] = "MAGE",
						["role"] = "NONE",
					},
				},
				["hdone"] = 0,
				["hdonespell"] = {
				},
				["taken"] = 1207130,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
				},
			},
			["공격대원의 훈련용 허수아비"] = {
				["players"] = {
					["샤이아리"] = {
						["taken"] = 81915,
						["done"] = 0,
						["class"] = "MAGE",
						["role"] = "NONE",
					},
				},
				["hdone"] = 0,
				["hdonespell"] = {
				},
				["taken"] = 81915,
				["done"] = 0,
				["htaken"] = 0,
				["htakenspell"] = {
				},
			},
		},
		["power"] = {
		},
		["dispells"] = 0,
		["healing"] = 0,
		["mobtaken"] = 34247489,
		["damagetaken"] = 20,
		["overhealing"] = 0,
		["starttime"] = 1471191127,
		["name"] = "전체",
		["alertCount"] = 0,
		["shielding"] = 0,
		["mobhdone"] = 0,
		["last_action"] = 1471191127,
		["multistrikes"] = 0,
	},
}
