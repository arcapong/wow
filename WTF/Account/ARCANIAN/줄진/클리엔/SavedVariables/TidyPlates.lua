
TidyPlatesOptions = {
	["PrimaryProfile"] = "Healer",
	["ForceBlizzardFont"] = false,
	["SecondSpecProfile"] = "Healer",
	["FriendlyAutomation"] = "No Automation",
	["DisableCastBars"] = false,
	["SecondaryTheme"] = "Neon",
	["CompatibilityMode"] = false,
	["ThirdSpecProfile"] = "Damage",
	["FourthSpecProfile"] = "Damage",
	["SecondaryProfile"] = "Damage",
	["EnemyAutomation"] = "No Automation",
	["PrimaryTheme"] = "Neon",
	["ActiveProfile"] = "Damage",
	["ActiveTheme"] = "Neon",
	["WelcomeShown"] = true,
	["FirstSpecProfile"] = "Healer",
}
