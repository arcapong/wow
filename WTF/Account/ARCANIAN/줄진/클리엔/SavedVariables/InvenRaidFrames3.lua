
InvenRaidFrames3CharDB = {
	["clickCasting"] = {
		{
		}, -- [1]
		{
			["shift-2"] = "spell__공중 부양",
			["shift-1"] = "spell__신의의 도약",
			["2"] = "spell__정화",
		}, -- [2]
		{
			["shift-2"] = "spell__공중 부양",
			["2"] = "spell__질병 정화",
		}, -- [3]
		{
		}, -- [4]
	},
	["classBuff2"] = {
	},
	["class"] = "PRIEST",
	["spellTimer"] = {
		{
			["name"] = "소생",
			["scale"] = 1,
			["use"] = 1,
			["display"] = 1,
			["pos"] = "BOTTOMLEFT",
		}, -- [1]
		{
			["name"] = "회복의 기원",
			["scale"] = 1,
			["use"] = 1,
			["display"] = 1,
			["pos"] = "BOTTOM",
		}, -- [2]
		{
			["name"] = "속죄",
			["scale"] = 1,
			["use"] = 1,
			["display"] = 1,
			["pos"] = "BOTTOMRIGHT",
		}, -- [3]
		{
			["name"] = "신의 권능: 보호막",
			["scale"] = 1,
			["use"] = 3,
			["display"] = 1,
			["pos"] = "LEFT",
		}, -- [4]
		{
			["name"] = "의지의 명료함",
			["scale"] = 1,
			["use"] = 1,
			["display"] = 1,
			["pos"] = "RIGHT",
		}, -- [5]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOPLEFT",
		}, -- [6]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOP",
		}, -- [7]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOPRIGHT",
		}, -- [8]
	},
}
