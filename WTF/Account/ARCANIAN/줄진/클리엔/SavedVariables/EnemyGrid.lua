
EnemyGridDBChr = {
	["KeyBinds"] = {
		[257] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_dispel",
			}, -- [2]
		},
		[258] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
		},
		[256] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
		},
	},
	["spellRangeCheck"] = {
		[257] = "성스러운 일격",
		[258] = "정신 분열",
		[256] = "성스러운 일격",
	},
	["debuffsBanned"] = {
	},
	["specEnabled"] = {
		[257] = true,
		[258] = true,
		[256] = true,
	},
}
