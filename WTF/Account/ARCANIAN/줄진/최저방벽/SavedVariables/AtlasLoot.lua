
AtlasLootCharDB = {
	["MiniMapButton"] = {
		["point"] = {
			"CENTER", -- [1]
			nil, -- [2]
			"CENTER", -- [3]
			-65.3499984741211, -- [4]
			-38.7999992370606, -- [5]
		},
		["shown"] = false,
	},
	["GUI"] = {
		["selected"] = {
			"AtlasLoot_BurningCrusade", -- [1]
			"CFRTheSlavePens", -- [2]
			nil, -- [3]
			1, -- [4]
			0, -- [5]
		},
	},
	["__addonrevision"] = 4325,
}
