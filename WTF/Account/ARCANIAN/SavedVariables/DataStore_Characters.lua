
DataStore_CharactersDB = {
	["profileKeys"] = {
		["샤이아리 - 줄진"] = "샤이아리 - 줄진",
		["언제나운무 - 아즈샤라"] = "언제나운무 - 아즈샤라",
		["최저방벽 - 줄진"] = "최저방벽 - 줄진",
		["환상큰소 - 아즈샤라"] = "환상큰소 - 아즈샤라",
		["북경스딸 - 줄진"] = "북경스딸 - 줄진",
		["클리엔 - 줄진"] = "클리엔 - 줄진",
		["사나무엇 - 아즈샤라"] = "사나무엇 - 아즈샤라",
		["아르니타 - 아즈샤라"] = "아르니타 - 아즈샤라",
		["릿데 - 아즈샤라"] = "릿데 - 아즈샤라",
		["안녕김밥 - 줄진"] = "안녕김밥 - 줄진",
		["꼬꼬마빠샤 - 줄진"] = "꼬꼬마빠샤 - 줄진",
		["뚱빼미나무 - 줄진"] = "뚱빼미나무 - 줄진",
		["사나없찐 - 아즈샤라"] = "사나없찐 - 아즈샤라",
		["샤이아리 - 아즈샤라"] = "샤이아리 - 아즈샤라",
		["Wilier - 아즈샤라"] = "Wilier - 아즈샤라",
		["Lezyne - 아즈샤라"] = "Lezyne - 아즈샤라",
		["데로사 - 아즈샤라"] = "데로사 - 아즈샤라",
		["환상큰손 - 아즈샤라"] = "환상큰손 - 아즈샤라",
		["샤말밀레 - 아즈샤라"] = "샤말밀레 - 아즈샤라",
		["재연마사 - 줄진"] = "재연마사 - 줄진",
		["Vlaanderen - 아즈샤라"] = "Vlaanderen - 아즈샤라",
		["징박무시하나연 - 줄진"] = "징박무시하나연 - 줄진",
		["Santini - 아즈샤라"] = "Santini - 아즈샤라",
		["써벨로 - 아즈샤라"] = "써벨로 - 아즈샤라",
		["리떼 - 아즈샤라"] = "리떼 - 아즈샤라",
		["Emonda - 줄진"] = "Emonda - 줄진",
		["뽕나인 - 줄진"] = "뽕나인 - 줄진",
		["Emonda - 아즈샤라"] = "Emonda - 아즈샤라",
		["영석썼숑 - 줄진"] = "영석썼숑 - 줄진",
		["사나정연 - 아즈샤라"] = "사나정연 - 아즈샤라",
	},
	["global"] = {
		["Characters"] = {
			["Default.줄진.뽕나인"] = {
				["XPMax"] = 657000,
				["played"] = 2275706,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1478599584,
				["class"] = "주술사",
				["race"] = "판다렌",
				["level"] = 100,
				["XP"] = 3231,
				["RestXP"] = 1971000,
				["money"] = 10001239,
				["gender"] = 3,
				["englishClass"] = "SHAMAN",
				["name"] = "뽕나인",
				["faction"] = "Alliance",
				["subZone"] = "상업 지구",
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
				["englishRace"] = "Pandaren",
				["guildRankIndex"] = 1,
				["lastLogoutTimestamp"] = 1478599584,
				["zone"] = "스톰윈드",
			},
			["Default.줄진.북경스딸"] = {
				["XPMax"] = 657000,
				["played"] = 227718,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1478598279,
				["class"] = "수도사",
				["race"] = "판다렌",
				["level"] = 100,
				["XP"] = 39432,
				["RestXP"] = 1971000,
				["money"] = 10003528,
				["gender"] = 3,
				["englishClass"] = "MONK",
				["name"] = "북경스딸",
				["subZone"] = "상업 지구",
				["guildRankIndex"] = 1,
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
				["englishRace"] = "Pandaren",
				["faction"] = "Alliance",
				["lastLogoutTimestamp"] = 1478598279,
				["zone"] = "스톰윈드",
			},
			["Default.줄진.영석썼숑"] = {
				["XPMax"] = 657000,
				["played"] = 2592170,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1478600182,
				["class"] = "흑마법사",
				["race"] = "인간",
				["level"] = 100,
				["XP"] = 0,
				["RestXP"] = 899608,
				["money"] = 6276811,
				["gender"] = 3,
				["englishClass"] = "WARLOCK",
				["guildRankIndex"] = 1,
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
				["subZone"] = "상업 지구",
				["faction"] = "Alliance",
				["englishRace"] = "Human",
				["name"] = "영석썼숑",
				["lastLogoutTimestamp"] = 1478600182,
				["zone"] = "스톰윈드",
			},
			["Default.아즈샤라.데로사"] = {
				["XPMax"] = 669000,
				["played"] = 47319,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1494303196,
				["class"] = "악마사냥꾼",
				["race"] = "블러드 엘프",
				["level"] = 102,
				["XP"] = 665210,
				["RestXP"] = 1158462,
				["money"] = 26396540,
				["gender"] = 3,
				["zone"] = "스톰하임",
				["name"] = "데로사",
				["subZone"] = "발디스달",
				["guildRankIndex"] = 1,
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
				["englishRace"] = "BloodElf",
				["faction"] = "Horde",
				["lastLogoutTimestamp"] = 1494303196,
				["englishClass"] = "DEMONHUNTER",
			},
			["Default.아즈샤라.사나정연"] = {
				["XPMax"] = 22500,
				["played"] = 9272,
				["playedThisLevel"] = 78,
				["isResting"] = true,
				["lastUpdate"] = 1513569335,
				["class"] = "수도사",
				["race"] = "판다렌",
				["level"] = 17,
				["XP"] = 3382,
				["RestXP"] = 5334,
				["money"] = 22899,
				["gender"] = 3,
				["zone"] = "오그리마",
				["englishClass"] = "MONK",
				["name"] = "사나정연",
				["faction"] = "Horde",
				["subZone"] = "힘의 골짜기",
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
				["englishRace"] = "Pandaren",
				["guildRankIndex"] = 1,
				["lastLogoutTimestamp"] = 1513569335,
				["guildRankName"] = "뽕뽕뽕뽕",
			},
			["Default.줄진.뚱빼미나무"] = {
				["XPMax"] = 657000,
				["played"] = 1669030,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1471189730,
				["class"] = "드루이드",
				["race"] = "나이트 엘프",
				["level"] = 100,
				["XP"] = 0,
				["RestXP"] = 0,
				["money"] = 5793900,
				["gender"] = 3,
				["zone"] = "달빛내림 터",
				["subZone"] = "뚱빼미나무의 주둔지",
				["name"] = "뚱빼미나무",
				["guildRankIndex"] = 1,
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
				["englishRace"] = "NightElf",
				["faction"] = "Alliance",
				["lastLogoutTimestamp"] = 1471189730,
				["englishClass"] = "DRUID",
			},
			["Default.아즈샤라.Wilier"] = {
				["XPMax"] = 38200,
				["played"] = 9040,
				["isResting"] = true,
				["lastUpdate"] = 1488810483,
				["class"] = "성기사",
				["race"] = "블러드 엘프",
				["level"] = 26,
				["XP"] = 17004,
				["RestXP"] = 40110,
				["money"] = 11198,
				["gender"] = 3,
				["subZone"] = "",
				["name"] = "Wilier",
				["faction"] = "Horde",
				["englishRace"] = "BloodElf",
				["zone"] = "오그리마",
				["lastLogoutTimestamp"] = 1488810483,
				["englishClass"] = "PALADIN",
			},
			["Default.줄진.최저방벽"] = {
				["XPMax"] = 657000,
				["played"] = 823664,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1494291901,
				["class"] = "전사",
				["race"] = "인간",
				["level"] = 100,
				["XP"] = 10156,
				["RestXP"] = 1589744,
				["money"] = 3809529,
				["gender"] = 3,
				["englishClass"] = "WARRIOR",
				["name"] = "최저방벽",
				["guildRankIndex"] = 1,
				["subZone"] = "",
				["faction"] = "Alliance",
				["englishRace"] = "Human",
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
				["lastLogoutTimestamp"] = 1494291901,
				["zone"] = "스톰윈드",
			},
			["Default.줄진.Emonda"] = {
				["XPMax"] = 651000,
				["played"] = 1915,
				["isResting"] = false,
				["lastUpdate"] = 1471190158,
				["class"] = "악마사냥꾼",
				["race"] = "나이트 엘프",
				["level"] = 99,
				["XP"] = 10254,
				["RestXP"] = 9884,
				["money"] = 2790000,
				["gender"] = 3,
				["subZone"] = "일리다리 거점",
				["name"] = "Emonda",
				["faction"] = "Alliance",
				["englishRace"] = "NightElf",
				["zone"] = "마르둠 - 산산조각난 심연",
				["lastLogoutTimestamp"] = 1471190158,
				["englishClass"] = "DEMONHUNTER",
			},
			["Default.아즈샤라.Emonda"] = {
				["XPMax"] = 717000,
				["played"] = 1091335,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1513310708,
				["class"] = "주술사",
				["race"] = "판다렌",
				["level"] = 110,
				["XP"] = 0,
				["RestXP"] = 0,
				["money"] = 76609079,
				["gender"] = 3,
				["englishClass"] = "SHAMAN",
				["playedThisLevel"] = 973798,
				["name"] = "Emonda",
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
				["subZone"] = "달라란",
				["faction"] = "Horde",
				["englishRace"] = "Pandaren",
				["guildRankIndex"] = 1,
				["lastLogoutTimestamp"] = 1513310708,
				["zone"] = "달라란",
			},
			["Default.아즈샤라.리떼"] = {
				["XPMax"] = 717000,
				["played"] = 808643,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1513606734,
				["class"] = "마법사",
				["race"] = "블러드 엘프",
				["level"] = 110,
				["XP"] = 0,
				["RestXP"] = 0,
				["englishClass"] = "MAGE",
				["money"] = 20692698819,
				["gender"] = 3,
				["zone"] = "크로쿠운",
				["subZone"] = "구원호",
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
				["name"] = "리떼",
				["faction"] = "Horde",
				["englishRace"] = "BloodElf",
				["guildRankIndex"] = 1,
				["lastLogoutTimestamp"] = 1513606734,
				["playedThisLevel"] = 734144,
			},
			["Default.아즈샤라.아르니타"] = {
				["XPMax"] = 717000,
				["played"] = 4277997,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1505654238,
				["class"] = "도적",
				["race"] = "블러드 엘프",
				["level"] = 110,
				["XP"] = 0,
				["RestXP"] = 0,
				["money"] = 37980996,
				["gender"] = 3,
				["englishClass"] = "ROGUE",
				["zone"] = "달라란",
				["guildRankIndex"] = 1,
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
				["subZone"] = "달라란",
				["faction"] = "Horde",
				["englishRace"] = "BloodElf",
				["name"] = "아르니타",
				["lastLogoutTimestamp"] = 1505654238,
				["playedThisLevel"] = 16701,
			},
			["Default.줄진.안녕김밥"] = {
				["XPMax"] = 596000,
				["played"] = 18620,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = false,
				["lastUpdate"] = 1471190049,
				["class"] = "죽음의 기사",
				["race"] = "인간",
				["level"] = 90,
				["XP"] = 99,
				["RestXP"] = 894000,
				["money"] = 907255,
				["gender"] = 3,
				["zone"] = "저주받은 땅",
				["subZone"] = "어둠의 문",
				["name"] = "안녕김밥",
				["guildRankIndex"] = 1,
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
				["englishRace"] = "Human",
				["faction"] = "Alliance",
				["lastLogoutTimestamp"] = 1471190049,
				["englishClass"] = "DEATHKNIGHT",
			},
			["Default.아즈샤라.Vlaanderen"] = {
				["XPMax"] = 717000,
				["played"] = 477319,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1505652805,
				["class"] = "사냥꾼",
				["race"] = "블러드 엘프",
				["level"] = 110,
				["XP"] = 0,
				["RestXP"] = 0,
				["money"] = 50147015,
				["gender"] = 3,
				["zone"] = "달라란",
				["playedThisLevel"] = 313074,
				["guildRankIndex"] = 1,
				["faction"] = "Horde",
				["subZone"] = "달라란",
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
				["englishRace"] = "BloodElf",
				["name"] = "Vlaanderen",
				["lastLogoutTimestamp"] = 1505652805,
				["englishClass"] = "HUNTER",
			},
			["Default.줄진.꼬꼬마빠샤"] = {
				["XPMax"] = 657000,
				["played"] = 4202003,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1480651862,
				["class"] = "도적",
				["race"] = "노움",
				["level"] = 100,
				["XP"] = 0,
				["RestXP"] = 1589526,
				["money"] = 7608684,
				["gender"] = 3,
				["zone"] = "스톰윈드",
				["subZone"] = "상업 지구",
				["guildRankIndex"] = 1,
				["name"] = "꼬꼬마빠샤",
				["faction"] = "Alliance",
				["englishRace"] = "Gnome",
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
				["lastLogoutTimestamp"] = 1480651862,
				["englishClass"] = "ROGUE",
			},
			["Default.줄진.클리엔"] = {
				["XPMax"] = 663000,
				["played"] = 6772366,
				["guildRankName"] = "뽕",
				["isResting"] = true,
				["lastUpdate"] = 1493734122,
				["class"] = "사제",
				["race"] = "인간",
				["level"] = 101,
				["XP"] = 115894,
				["RestXP"] = 994500,
				["money"] = 10000118,
				["gender"] = 3,
				["englishClass"] = "PRIEST",
				["name"] = "클리엔",
				["faction"] = "Alliance",
				["subZone"] = "달라란",
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
				["englishRace"] = "Human",
				["guildRankIndex"] = 4,
				["lastLogoutTimestamp"] = 1493734122,
				["zone"] = "달라란",
			},
			["Default.아즈샤라.샤이아리"] = {
				["XPMax"] = 717000,
				["played"] = 885182,
				["guildRankName"] = "뽀옹",
				["isResting"] = true,
				["lastUpdate"] = 1513571551,
				["class"] = "사제",
				["race"] = "블러드 엘프",
				["level"] = 110,
				["XP"] = 0,
				["RestXP"] = 0,
				["money"] = 124424084,
				["gender"] = 3,
				["englishClass"] = "PRIEST",
				["playedThisLevel"] = 757633,
				["guildRankIndex"] = 0,
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
				["name"] = "샤이아리",
				["faction"] = "Horde",
				["englishRace"] = "BloodElf",
				["subZone"] = "구원호",
				["lastLogoutTimestamp"] = 1513571551,
				["zone"] = "크로쿠운",
			},
			["Default.줄진.재연마사"] = {
				["XPMax"] = 675000,
				["played"] = 1038282,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1478598652,
				["class"] = "사냥꾼",
				["race"] = "인간",
				["level"] = 103,
				["XP"] = 226166,
				["RestXP"] = 1012500,
				["money"] = 10009122,
				["gender"] = 3,
				["zone"] = "달라란",
				["subZone"] = "달라란",
				["name"] = "재연마사",
				["guildRankIndex"] = 1,
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
				["englishRace"] = "Human",
				["faction"] = "Alliance",
				["lastLogoutTimestamp"] = 1478598652,
				["englishClass"] = "HUNTER",
			},
			["Default.아즈샤라.샤말밀레"] = {
				["XPMax"] = 717000,
				["played"] = 3984077,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1513497781,
				["class"] = "성기사",
				["race"] = "블러드 엘프",
				["level"] = 110,
				["XP"] = 0,
				["RestXP"] = 0,
				["money"] = 27636834,
				["gender"] = 3,
				["zone"] = "달라란",
				["englishClass"] = "PALADIN",
				["guildRankIndex"] = 1,
				["faction"] = "Horde",
				["subZone"] = "달라란",
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
				["englishRace"] = "BloodElf",
				["name"] = "샤말밀레",
				["lastLogoutTimestamp"] = 1513497781,
				["playedThisLevel"] = 81310,
			},
			["Default.줄진.징박무시하나연"] = {
				["XPMax"] = 657000,
				["played"] = 3844728,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1478600731,
				["class"] = "성기사",
				["race"] = "인간",
				["level"] = 100,
				["XP"] = 0,
				["RestXP"] = 900116,
				["money"] = 8353734,
				["gender"] = 3,
				["zone"] = "스톰윈드",
				["subZone"] = "상업 지구",
				["guildRankIndex"] = 1,
				["name"] = "징박무시하나연",
				["faction"] = "Alliance",
				["englishRace"] = "Human",
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
				["lastLogoutTimestamp"] = 1478600731,
				["englishClass"] = "PALADIN",
			},
			["Default.아즈샤라.Santini"] = {
				["XPMax"] = 717000,
				["played"] = 197112,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1505653958,
				["class"] = "흑마법사",
				["race"] = "블러드 엘프",
				["level"] = 110,
				["XP"] = 0,
				["RestXP"] = 0,
				["money"] = 31459527,
				["gender"] = 3,
				["englishClass"] = "WARLOCK",
				["playedThisLevel"] = 61373,
				["name"] = "Santini",
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
				["subZone"] = "달라란",
				["faction"] = "Horde",
				["englishRace"] = "BloodElf",
				["guildRankIndex"] = 1,
				["lastLogoutTimestamp"] = 1505653958,
				["zone"] = "달라란",
			},
			["Default.아즈샤라.사나없찐"] = {
				["XPMax"] = 3800,
				["played"] = 154,
				["isResting"] = true,
				["lastUpdate"] = 1488376858,
				["class"] = "수도사",
				["race"] = "블러드 엘프",
				["level"] = 6,
				["XP"] = 744,
				["RestXP"] = 0,
				["money"] = 1136,
				["gender"] = 3,
				["name"] = "사나없찐",
				["subZone"] = "",
				["faction"] = "Horde",
				["englishRace"] = "BloodElf",
				["zone"] = "매날개 광장",
				["lastLogoutTimestamp"] = 1488376858,
				["englishClass"] = "MONK",
			},
			["Default.아즈샤라.Lezyne"] = {
				["XPMax"] = 681000,
				["played"] = 1700867,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1494312022,
				["class"] = "드루이드",
				["race"] = "타우렌",
				["level"] = 104,
				["XP"] = 152476,
				["RestXP"] = 1021500,
				["money"] = 48824804,
				["gender"] = 2,
				["englishClass"] = "DRUID",
				["guildRankIndex"] = 1,
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
				["subZone"] = "여행자의 보금자리",
				["faction"] = "Horde",
				["englishRace"] = "Tauren",
				["name"] = "Lezyne",
				["lastLogoutTimestamp"] = 1494312022,
				["zone"] = "발샤라",
			},
			["Default.아즈샤라.릿데"] = {
				["XPMax"] = 717000,
				["played"] = 928757,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1513309870,
				["class"] = "전사",
				["race"] = "판다렌",
				["level"] = 110,
				["XP"] = 0,
				["RestXP"] = 0,
				["money"] = 162981023,
				["gender"] = 3,
				["englishClass"] = "WARRIOR",
				["playedThisLevel"] = 54777,
				["name"] = "릿데",
				["faction"] = "Horde",
				["guildRankIndex"] = 1,
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
				["englishRace"] = "Pandaren",
				["subZone"] = "오딘의 용광로",
				["lastLogoutTimestamp"] = 1513309870,
				["zone"] = "하늘보루",
			},
			["Default.줄진.샤이아리"] = {
				["XPMax"] = 657000,
				["played"] = 8128783,
				["guildRankName"] = "뽕뽕뽕뽕",
				["isResting"] = true,
				["lastUpdate"] = 1493734054,
				["class"] = "마법사",
				["race"] = "인간",
				["level"] = 100,
				["XP"] = 165477,
				["RestXP"] = 985500,
				["money"] = 9465097,
				["gender"] = 3,
				["zone"] = "수호자의 전당",
				["guildRankIndex"] = 1,
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
				["name"] = "샤이아리",
				["faction"] = "Alliance",
				["englishRace"] = "Human",
				["subZone"] = "",
				["lastLogoutTimestamp"] = 1493734054,
				["englishClass"] = "MAGE",
			},
			["Default.아즈샤라.사나무엇"] = {
				["XPMax"] = 2100,
				["played"] = 974,
				["playedThisLevel"] = 221,
				["isResting"] = false,
				["lastUpdate"] = 1513424632,
				["class"] = "수도사",
				["race"] = "판다렌",
				["level"] = 4,
				["XP"] = 1740,
				["RestXP"] = 0,
				["money"] = 634,
				["gender"] = 3,
				["zone"] = "유랑도",
				["englishClass"] = "MONK",
				["guildRankIndex"] = 2,
				["faction"] = "Neutral",
				["subZone"] = "새벽의 골짜기",
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
				["englishRace"] = "Pandaren",
				["name"] = "사나무엇",
				["lastLogoutTimestamp"] = 1513424632,
				["guildRankName"] = "뽕뽕뽕",
			},
		},
	},
}
