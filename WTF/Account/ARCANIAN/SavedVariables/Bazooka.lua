
BazookaDB = {
	["namespaces"] = {
		["LibDualSpec-1.0"] = {
		},
	},
	["profileKeys"] = {
		["샤이아리 - 줄진"] = "Default",
		["언제나운무 - 아즈샤라"] = "Default",
		["최저방벽 - 줄진"] = "Default",
		["환상큰소 - 아즈샤라"] = "Default",
		["북경스딸 - 줄진"] = "Default",
		["클리엔 - 줄진"] = "Default",
		["사나무엇 - 아즈샤라"] = "Default",
		["아르니타 - 아즈샤라"] = "Default",
		["릿데 - 아즈샤라"] = "Default",
		["안녕김밥 - 줄진"] = "Default",
		["꼬꼬마빠샤 - 줄진"] = "Default",
		["뚱빼미나무 - 줄진"] = "Default",
		["사나없찐 - 아즈샤라"] = "Default",
		["샤이아리 - 아즈샤라"] = "Default",
		["Wilier - 아즈샤라"] = "Default",
		["Lezyne - 아즈샤라"] = "Default",
		["재연마사 - 줄진"] = "Default",
		["환상큰손 - 아즈샤라"] = "Default",
		["샤말밀레 - 아즈샤라"] = "Default",
		["데로사 - 아즈샤라"] = "Default",
		["Emonda - 아즈샤라"] = "Default",
		["징박무시하나연 - 줄진"] = "Default",
		["Santini - 아즈샤라"] = "Default",
		["써벨로 - 아즈샤라"] = "Default",
		["리떼 - 아즈샤라"] = "Default",
		["Emonda - 줄진"] = "Default",
		["뽕나인 - 줄진"] = "Default",
		["Vlaanderen - 아즈샤라"] = "Default",
		["영석썼숑 - 줄진"] = "Default",
		["사나정연 - 아즈샤라"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["locked"] = true,
			["plugins"] = {
				["launcher"] = {
					["SavedInstances"] = {
						["showLabel"] = true,
						["area"] = "right",
						["alignment"] = "LEFT",
						["pos"] = 6,
					},
					["InvenRaidFrames3"] = {
						["enabled"] = false,
						["pos"] = 5,
					},
					["Omen"] = {
						["pos"] = 1,
					},
					["Grid2"] = {
						["pos"] = 2,
					},
					["RangeDisplay"] = {
						["enabled"] = false,
						["pos"] = 3,
					},
					["Grid"] = {
						["pos"] = 2,
					},
					["ExRT"] = {
						["enabled"] = false,
						["pos"] = 1,
					},
					["Altoholic"] = {
						["useLabelAsTitle"] = false,
						["showLabel"] = true,
						["enabled"] = false,
						["pos"] = 2,
					},
					["Bazooka"] = {
						["enabled"] = false,
					},
					["Masque"] = {
						["enabled"] = false,
						["pos"] = 1,
					},
					["AtlasLoot"] = {
						["useLabelAsTitle"] = false,
						["showLabel"] = true,
						["area"] = "right",
						["alignment"] = "LEFT",
						["pos"] = 4,
					},
					["BLTRCD"] = {
						["enabled"] = false,
						["pos"] = 2,
					},
					["VuhDo"] = {
						["pos"] = 1,
					},
					["TellMeWhen"] = {
						["enabled"] = false,
						["pos"] = 1,
					},
					["HealBot"] = {
						["pos"] = 1,
					},
				},
				["data source"] = {
					["Skada"] = {
						["enabled"] = false,
						["pos"] = 1,
					},
					["O Item Level"] = {
						["pos"] = 1,
					},
					["Simple iLevel"] = {
						["pos"] = 5,
					},
					["WeakAuras"] = {
						["enabled"] = false,
						["pos"] = 5,
					},
					["GarrisonMissionManager"] = {
						["enabled"] = false,
						["pos"] = 7,
					},
				},
			},
			["bars"] = {
				{
					["bgEnabled"] = false,
					["bgInset"] = 4,
					["y"] = 179.000091552734,
					["x"] = -7.99914073944092,
					["frameWidth"] = 320,
					["pos"] = 0,
					["relPoint"] = "BOTTOMRIGHT",
					["point"] = "BOTTOMRIGHT",
					["frameHeight"] = 20.0000171661377,
					["attach"] = "none",
				}, -- [1]
			},
		},
	},
}
