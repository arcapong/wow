
WQTrackerDB = {
	["profileKeys"] = {
		["써벨로 - 아즈샤라"] = "Default",
		["리떼 - 아즈샤라"] = "Default",
		["릿데 - 아즈샤라"] = "Default",
		["Emonda - 아즈샤라"] = "Default",
		["Vlaanderen - 아즈샤라"] = "Default",
		["아르니타 - 아즈샤라"] = "Default",
		["Lezyne - 아즈샤라"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["worldmap_widgets"] = {
				["scale"] = 1.2,
				["textsize"] = 10,
			},
			["AlertTutorialStep"] = 5,
			["GotTutorial"] = true,
			["quests_tracked"] = {
				["Player-205-060D24B9"] = {
				},
				["Player-205-0679A0BF"] = {
				},
				["Player-205-066DFEFF"] = {
				},
				["Player-205-06D2379A"] = {
				},
				["Player-205-06085B20"] = {
				},
				["Player-205-064C974D"] = {
				},
				["Player-205-06497083"] = {
				},
			},
			["quests_all_characters"] = {
				["Player-205-06D2379A"] = {
					[42177] = {
						["expireAt"] = 1495666836,
						["rewardAmount"] = "|cffc557FF860",
						["rewardTexture"] = "Interface\\ICONS\\INV_Pants_08",
					},
					[41623] = {
						["expireAt"] = 1495666836,
						["rewardAmount"] = 214650,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[42275] = {
						["expireAt"] = 1495602083,
						["rewardAmount"] = "|cffc557FF855",
						["rewardTexture"] = "Interface\\ICONS\\inv_misc_enchantedpearlE",
					},
					[41687] = {
						["expireAt"] = 1495666883,
						["rewardAmount"] = "15",
						["rewardTexture"] = 413584,
					},
					[44788] = {
						["expireAt"] = 1495666883,
						["rewardAmount"] = 260200,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[45878] = {
						["expireAt"] = 1495684103,
						["rewardAmount"] = 318700,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43772] = {
						["expireAt"] = 1495602096,
						["rewardAmount"] = "1",
						["rewardTexture"] = 1417744,
					},
					[42070] = {
						["expireAt"] = 1495666875,
						["rewardAmount"] = 325250,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[44892] = {
						["expireAt"] = 1495796496,
						["rewardAmount"] = 455350,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[44769] = {
						["expireAt"] = 1495666836,
						["rewardAmount"] = "|cffc557FF860",
						["rewardTexture"] = "Interface\\ICONS\\INV_Gauntlets_17",
					},
					[44894] = {
						["expireAt"] = 1495666883,
						["rewardAmount"] = 494350,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41794] = {
						["expireAt"] = 1495649556,
						["rewardAmount"] = 273200,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41549] = {
						["expireAt"] = 1495666823,
						["rewardAmount"] = "10",
						["rewardTexture"] = 1387639,
					},
					[41965] = {
						["expireAt"] = 1495645275,
						["rewardAmount"] = 227650,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41675] = {
						["expireAt"] = 1495602096,
						["rewardAmount"] = "3",
						["rewardTexture"] = 1417744,
					},
					[45985] = {
						["expireAt"] = 1495632263,
						["rewardAmount"] = 331750,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41637] = {
						["expireAt"] = 1495645296,
						["rewardAmount"] = "110",
						["rewardTexture"] = 1387622,
					},
					[42764] = {
						["expireAt"] = 1495839696,
						["rewardAmount"] = 520400,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[46112] = {
						["expireAt"] = 1495666883,
						["rewardAmount"] = "20",
						["rewardTexture"] = 413584,
					},
					[44737] = {
						["expireAt"] = 1495666823,
						["rewardAmount"] = 455350,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43455] = {
						["expireAt"] = 1495623696,
						["rewardAmount"] = 234150,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
				},
				["Player-205-060D24B9"] = {
					[42177] = {
						["expireAt"] = 1485471653,
						["rewardAmount"] = "|cffc557FF840",
						["rewardTexture"] = 1137679,
					},
					[42922] = {
						["expireAt"] = 1485644453,
						["rewardAmount"] = "|cffc557FF820",
						["rewardTexture"] = "Interface\\ICONS\\INV_Belt_15",
					},
					[41931] = {
						["expireAt"] = 1485471653,
						["rewardAmount"] = "15",
						["rewardTexture"] = 413584,
					},
					[40978] = {
						["expireAt"] = 1485450053,
						["rewardAmount"] = 23600,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41623] = {
						["expireAt"] = 1485428453,
						["rewardAmount"] = "1",
						["rewardTexture"] = 1417744,
					},
					[41996] = {
						["expireAt"] = 1485471653,
						["rewardAmount"] = 30750,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[42070] = {
						["expireAt"] = 1485471653,
						["rewardAmount"] = 35750,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43641] = {
						["expireAt"] = 1485471713,
						["rewardAmount"] = "|cffc557FF820",
						["rewardTexture"] = "Interface\\ICONS\\INV_Pants_08",
					},
					[43332] = {
						["expireAt"] = 1485406853,
						["rewardAmount"] = 30750,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41318] = {
						["expireAt"] = 1485428453,
						["rewardAmount"] = "3",
						["rewardTexture"] = 1417744,
					},
					[45070] = {
						["expireAt"] = 1485558053,
						["rewardAmount"] = 28600,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[45071] = {
						["expireAt"] = 1485493313,
						["rewardAmount"] = 28600,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43714] = {
						["expireAt"] = 1485644453,
						["rewardAmount"] = "|cffc557FF820",
						["rewardTexture"] = "Interface\\ICONS\\INV_Jewelry_Necklace_07",
					},
					[42004] = {
						["expireAt"] = 1485488933,
						["rewardAmount"] = 27900,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41695] = {
						["expireAt"] = 1485644453,
						["rewardAmount"] = "|cffc557FF840",
						["rewardTexture"] = "Interface\\ICONS\\inv_misc_enchantedpearlE",
					},
					[45049] = {
						["expireAt"] = 1485622913,
						["rewardAmount"] = 28600,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[40278] = {
						["expireAt"] = 1485471653,
						["rewardAmount"] = "5",
						["rewardTexture"] = 1045109,
					},
					[43620] = {
						["expireAt"] = 1485471653,
						["rewardAmount"] = "|cffc557FF840",
						["rewardTexture"] = "Interface\\ICONS\\INV_Belt_15",
					},
					[44892] = {
						["expireAt"] = 1485428513,
						["rewardAmount"] = "2",
						["rewardTexture"] = 1417744,
					},
					[41700] = {
						["expireAt"] = 1485471653,
						["rewardAmount"] = 73650,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[44305] = {
						["expireAt"] = 1485428453,
						["rewardAmount"] = 50050,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41210] = {
						["expireAt"] = 1485558053,
						["rewardAmount"] = "1",
						["rewardTexture"] = 1394959,
					},
					[43346] = {
						["expireAt"] = 1485558053,
						["rewardAmount"] = "6",
						["rewardTexture"] = 1417744,
					},
					[42169] = {
						["expireAt"] = 1485428453,
						["rewardAmount"] = 26450,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[40280] = {
						["expireAt"] = 1485471653,
						["rewardAmount"] = "1",
						["rewardTexture"] = 667492,
					},
					[43752] = {
						["expireAt"] = 1485437093,
						["rewardAmount"] = "1",
						["rewardTexture"] = 1417744,
					},
					[43598] = {
						["expireAt"] = 1485471653,
						["rewardAmount"] = "3",
						["rewardTexture"] = 1322720,
					},
					[42173] = {
						["expireAt"] = 1485419813,
						["rewardAmount"] = 27900,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[44933] = {
						["expireAt"] = 1485990053,
						["rewardAmount"] = 107250,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41896] = {
						["expireAt"] = 1485406853,
						["rewardAmount"] = 35750,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43943] = {
						["expireAt"] = 1485558053,
						["rewardAmount"] = "",
						["rewardTexture"] = 1409002,
					},
					[41965] = {
						["expireAt"] = 1485450053,
						["rewardAmount"] = 30050,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
				},
				["Player-205-064C974D"] = {
					[42169] = {
						["expireAt"] = 1485428440,
						["rewardAmount"] = 46250,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[44943] = {
						["expireAt"] = 1485298843,
						["rewardAmount"] = 62500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[42173] = {
						["expireAt"] = 1485419800,
						["rewardAmount"] = 48750,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[40978] = {
						["expireAt"] = 1485450040,
						["rewardAmount"] = 41250,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[42177] = {
						["expireAt"] = 1485471640,
						["rewardAmount"] = "|cffc557FF845",
						["rewardTexture"] = "Interface\\ICONS\\INV_Bracer_07",
					},
					[43943] = {
						["expireAt"] = 1485558040,
						["rewardAmount"] = "",
						["rewardTexture"] = 1409002,
					},
					[43063] = {
						["expireAt"] = 1485298903,
						["rewardAmount"] = 116250,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[44892] = {
						["expireAt"] = 1485428500,
						["rewardAmount"] = "2",
						["rewardTexture"] = 1417744,
					},
					[44894] = {
						["expireAt"] = 1485298903,
						["rewardAmount"] = "|cffc557FF845",
						["rewardTexture"] = "Interface\\ICONS\\INV_Boots_Cloth_03",
					},
					[41935] = {
						["expireAt"] = 1485298903,
						["rewardAmount"] = "20",
						["rewardTexture"] = 133675,
					},
					[43764] = {
						["expireAt"] = 1485363703,
						["rewardAmount"] = 50000,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41695] = {
						["expireAt"] = 1485644500,
						["rewardAmount"] = "|cffc557FF845",
						["rewardTexture"] = "Interface\\ICONS\\inv_misc_enchantedpearlE",
					},
					[41927] = {
						["expireAt"] = 1485281563,
						["rewardAmount"] = "|cffc557FF845",
						["rewardTexture"] = "Interface\\ICONS\\INV_Gauntlets_17",
					},
					[43807] = {
						["expireAt"] = 1485298843,
						["rewardAmount"] = 57500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41705] = {
						["expireAt"] = 1485234043,
						["rewardAmount"] = "1",
						["rewardTexture"] = 1417744,
					},
					[41896] = {
						["expireAt"] = 1485406840,
						["rewardAmount"] = 62500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43347] = {
						["expireAt"] = 1485385303,
						["rewardAmount"] = 128750,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41679] = {
						["expireAt"] = 1485298843,
						["rewardAmount"] = "110",
						["rewardTexture"] = 1387622,
					},
					[41965] = {
						["expireAt"] = 1485450040,
						["rewardAmount"] = 52500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[44802] = {
						["expireAt"] = 1485298843,
						["rewardAmount"] = "|cffc557FF845",
						["rewardTexture"] = "Interface\\ICONS\\INV_Shoulder_25",
					},
					[42725] = {
						["expireAt"] = 1485298843,
						["rewardAmount"] = "|cffc557FF840",
						["rewardTexture"] = "Interface\\ICONS\\inv_misc_enchantedpearlE",
					},
					[44932] = {
						["expireAt"] = 1485385243,
						["rewardAmount"] = 187500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43611] = {
						["expireAt"] = 1485298903,
						["rewardAmount"] = 87500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41794] = {
						["expireAt"] = 1485264283,
						["rewardAmount"] = "|cffc557FF845",
						["rewardTexture"] = "Interface\\ICONS\\INV_Jewelry_Necklace_07",
					},
					[42922] = {
						["expireAt"] = 1485644440,
						["rewardAmount"] = "|cffc557FF820",
						["rewardTexture"] = "Interface\\ICONS\\INV_Jewelry_Necklace_07",
					},
					[40278] = {
						["expireAt"] = 1485471700,
						["rewardAmount"] = "5",
						["rewardTexture"] = 1045109,
					},
					[41855] = {
						["expireAt"] = 1485298903,
						["rewardAmount"] = "1",
						["rewardTexture"] = 667492,
					},
					[45070] = {
						["expireAt"] = 1485558040,
						["rewardAmount"] = 50000,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[42087] = {
						["expireAt"] = 1485298843,
						["rewardAmount"] = "1",
						["rewardTexture"] = 1417744,
					},
					[43436] = {
						["expireAt"] = 1485234103,
						["rewardAmount"] = "|cffc557FF840",
						["rewardTexture"] = "Interface\\ICONS\\INV_Chest_Cloth_08",
					},
					[44816] = {
						["expireAt"] = 1485298843,
						["rewardAmount"] = 87500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43620] = {
						["expireAt"] = 1485471700,
						["rewardAmount"] = "|cffc557FF845",
						["rewardTexture"] = "Interface\\ICONS\\INV_Belt_15",
					},
					[41930] = {
						["expireAt"] = 1485247003,
						["rewardAmount"] = 48750,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43598] = {
						["expireAt"] = 1485471640,
						["rewardAmount"] = "3",
						["rewardTexture"] = 1322720,
					},
					[44013] = {
						["expireAt"] = 1485234103,
						["rewardAmount"] = 55000,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43460] = {
						["expireAt"] = 1485255703,
						["rewardAmount"] = "1",
						["rewardTexture"] = 1417744,
					},
					[41623] = {
						["expireAt"] = 1485428440,
						["rewardAmount"] = "1",
						["rewardTexture"] = 1417744,
					},
					[43641] = {
						["expireAt"] = 1485471700,
						["rewardAmount"] = "|cffc557FF820",
						["rewardTexture"] = "Interface\\ICONS\\INV_Chest_Cloth_08",
					},
					[42023] = {
						["expireAt"] = 1485234043,
						["rewardAmount"] = 62500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[42070] = {
						["expireAt"] = 1485471640,
						["rewardAmount"] = 62500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43332] = {
						["expireAt"] = 1485406840,
						["rewardAmount"] = 53750,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41318] = {
						["expireAt"] = 1485428500,
						["rewardAmount"] = "3",
						["rewardTexture"] = 1417744,
					},
					[43714] = {
						["expireAt"] = 1485644440,
						["rewardAmount"] = "|cffc557FF820",
						["rewardTexture"] = "Interface\\ICONS\\INV_Jewelry_Necklace_07",
					},
					[41700] = {
						["expireAt"] = 1485471700,
						["rewardAmount"] = 128750,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43752] = {
						["expireAt"] = 1485437080,
						["rewardAmount"] = "1",
						["rewardTexture"] = 1417744,
					},
					[40280] = {
						["expireAt"] = 1485471700,
						["rewardAmount"] = "1",
						["rewardTexture"] = 667492,
					},
					[41950] = {
						["expireAt"] = 1485402520,
						["rewardAmount"] = "2",
						["rewardTexture"] = 1417744,
					},
					[43346] = {
						["expireAt"] = 1485558100,
						["rewardAmount"] = "6",
						["rewardTexture"] = 1417744,
					},
					[45049] = {
						["expireAt"] = 1485622900,
						["rewardAmount"] = 50000,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43324] = {
						["expireAt"] = 1485255643,
						["rewardAmount"] = 47500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41931] = {
						["expireAt"] = 1485471700,
						["rewardAmount"] = "15",
						["rewardTexture"] = 413584,
					},
					[43932] = {
						["expireAt"] = 1485428440,
						["rewardAmount"] = 87500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[42150] = {
						["expireAt"] = 1485234043,
						["rewardAmount"] = "|cffc557FF840",
						["rewardTexture"] = "Interface\\ICONS\\INV_Jewelry_Ring_22",
					},
					[44933] = {
						["expireAt"] = 1485990040,
						["rewardAmount"] = 187500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[44305] = {
						["expireAt"] = 1485428500,
						["rewardAmount"] = 87500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41996] = {
						["expireAt"] = 1485471640,
						["rewardAmount"] = 53750,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[45071] = {
						["expireAt"] = 1485493300,
						["rewardAmount"] = 50000,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
				},
				["Player-205-06497083"] = {
					[43807] = {
						["expireAt"] = 1484996433,
						["rewardAmount"] = 55000,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43431] = {
						["expireAt"] = 1485212480,
						["rewardAmount"] = 53750,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41984] = {
						["expireAt"] = 1485005073,
						["rewardAmount"] = "|cffc557FF840",
						["rewardTexture"] = "Interface\\ICONS\\INV_Boots_Cloth_03",
					},
					[42744] = {
						["expireAt"] = 1485039693,
						["rewardAmount"] = "|cffc557FF810",
						["rewardTexture"] = "Interface\\ICONS\\INV_Boots_Cloth_03",
					},
					[43943] = {
						["expireAt"] = 1485298820,
						["rewardAmount"] = "",
						["rewardTexture"] = 1409002,
					},
					[43063] = {
						["expireAt"] = 1485298880,
						["rewardAmount"] = 116250,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41931] = {
						["expireAt"] = 1485212480,
						["rewardAmount"] = "16",
						["rewardTexture"] = 133675,
					},
					[44894] = {
						["expireAt"] = 1485298880,
						["rewardAmount"] = "|cffc557FF845",
						["rewardTexture"] = "Interface\\ICONS\\INV_Pants_08",
					},
					[43951] = {
						["expireAt"] = 1484987793,
						["rewardAmount"] = 42500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43764] = {
						["expireAt"] = 1485363680,
						["rewardAmount"] = 50000,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41122] = {
						["expireAt"] = 1485147620,
						["rewardAmount"] = 55000,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43963] = {
						["expireAt"] = 1485212420,
						["rewardAmount"] = 48750,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[42969] = {
						["expireAt"] = 1485039693,
						["rewardAmount"] = 87500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43040] = {
						["expireAt"] = 1485212480,
						["rewardAmount"] = "|cffc557FF840",
						["rewardTexture"] = "Interface\\ICONS\\inv_misc_enchantedpearlE",
					},
					[44932] = {
						["expireAt"] = 1485385220,
						["rewardAmount"] = 187500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[40280] = {
						["expireAt"] = 1485039693,
						["rewardAmount"] = "16",
						["rewardTexture"] = 413584,
					},
					[42172] = {
						["expireAt"] = 1485169280,
						["rewardAmount"] = 50000,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43347] = {
						["expireAt"] = 1485385280,
						["rewardAmount"] = 128750,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41926] = {
						["expireAt"] = 1485195140,
						["rewardAmount"] = "|cffc557FF845",
						["rewardTexture"] = 1137679,
					},
					[43459] = {
						["expireAt"] = 1485169280,
						["rewardAmount"] = "|cffc557FF845",
						["rewardTexture"] = "Interface\\ICONS\\INV_Shoulder_25",
					},
					[42119] = {
						["expireAt"] = 1485212480,
						["rewardAmount"] = "2",
						["rewardTexture"] = 1417744,
					},
					[41995] = {
						["expireAt"] = 1485169220,
						["rewardAmount"] = "1",
						["rewardTexture"] = 1417744,
					},
					[44816] = {
						["expireAt"] = 1485212420,
						["rewardAmount"] = 87500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43324] = {
						["expireAt"] = 1485039633,
						["rewardAmount"] = 52500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[42190] = {
						["expireAt"] = 1485212480,
						["rewardAmount"] = 25000,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[44291] = {
						["expireAt"] = 1485147680,
						["rewardAmount"] = 87500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43777] = {
						["expireAt"] = 1485169280,
						["rewardAmount"] = 50000,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41692] = {
						["expireAt"] = 1485039633,
						["rewardAmount"] = "2",
						["rewardTexture"] = 1417744,
					},
					[41836] = {
						["expireAt"] = 1485190880,
						["rewardAmount"] = "|cffc557FF840",
						["rewardTexture"] = "Interface\\ICONS\\inv_misc_enchantedpearlE",
					},
					[45047] = {
						["expireAt"] = 1485104493,
						["rewardAmount"] = 50000,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41950] = {
						["expireAt"] = 1485229700,
						["rewardAmount"] = "|cffc557FF840",
						["rewardTexture"] = "Interface\\ICONS\\INV_Jewelry_Talisman_07",
					},
					[44157] = {
						["expireAt"] = 1485039633,
						["rewardAmount"] = "|cffc557FF840",
						["rewardTexture"] = "Interface\\ICONS\\INV_Boots_Cloth_03",
					},
					[44033] = {
						["expireAt"] = 1485190820,
						["rewardAmount"] = 47500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[44923] = {
						["expireAt"] = 1485298820,
						["rewardAmount"] = "1",
						["rewardTexture"] = 1417744,
					},
					[41706] = {
						["expireAt"] = 1485212420,
						["rewardAmount"] = 47500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43598] = {
						["expireAt"] = 1485212480,
						["rewardAmount"] = 62500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[42277] = {
						["expireAt"] = 1485039633,
						["rewardAmount"] = 53750,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[43600] = {
						["expireAt"] = 1485039633,
						["rewardAmount"] = "|cffc557FF810",
						["rewardTexture"] = "Interface\\ICONS\\INV_Shoulder_25",
					},
					[41622] = {
						["expireAt"] = 1485169220,
						["rewardAmount"] = "|cffc557FF840",
						["rewardTexture"] = "Interface\\ICONS\\INV_Pants_08",
					},
					[42070] = {
						["expireAt"] = 1485212480,
						["rewardAmount"] = "|cffc557FF840",
						["rewardTexture"] = "Interface\\ICONS\\INV_Jewelry_Ring_22",
					},
					[41944] = {
						["expireAt"] = 1485212480,
						["rewardAmount"] = "15",
						["rewardTexture"] = 133675,
					},
					[44303] = {
						["expireAt"] = 1485018093,
						["rewardAmount"] = 87500,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[45048] = {
						["expireAt"] = 1484974893,
						["rewardAmount"] = 50000,
						["rewardTexture"] = "Interface\\AddOns\\WorldQuestTracker\\media\\icon_artifactpower_red_roundT",
					},
					[41794] = {
						["expireAt"] = 1485039633,
						["rewardAmount"] = "|cffc557FF845",
						["rewardTexture"] = "Interface\\ICONS\\INV_Jewelry_Ring_22",
					},
					[41089] = {
						["expireAt"] = 1485126093,
						["rewardAmount"] = "|cffc557FF845",
						["rewardTexture"] = "Interface\\ICONS\\INV_Shoulder_25",
					},
				},
			},
			["zonemap_widgets"] = {
				["scale"] = 1.15,
			},
			["player_names"] = {
				["Player-205-060D24B9"] = {
					["class"] = "HUNTER",
					["name"] = "Vlaanderen",
					["realm"] = "아즈샤라",
				},
				["Player-205-0679A0BF"] = {
					["class"] = "ROGUE",
					["name"] = "아르니타",
					["realm"] = "아즈샤라",
				},
				["Player-205-066DFEFF"] = {
					["class"] = "DRUID",
					["name"] = "Lezyne",
					["realm"] = "아즈샤라",
				},
				["Player-205-06D2379A"] = {
					["class"] = "WARRIOR",
					["name"] = "릿데",
					["realm"] = "아즈샤라",
				},
				["Player-205-06085B20"] = {
					["class"] = "PRIEST",
					["name"] = "써벨로",
					["realm"] = "아즈샤라",
				},
				["Player-205-064C974D"] = {
					["class"] = "MAGE",
					["name"] = "리떼",
					["realm"] = "아즈샤라",
				},
				["Player-205-06497083"] = {
					["class"] = "SHAMAN",
					["name"] = "Emonda",
					["realm"] = "아즈샤라",
				},
			},
			["history"] = {
				["period"] = {
					["global"] = {
						["170126"] = {
							["blood"] = 10,
							["resource"] = 3000,
							["quest"] = 5,
							["profession"] = {
								[124124] = 1,
							},
						},
						["170524"] = {
							["artifact"] = 318700,
							["quest"] = 3,
							["gold"] = 1679100,
						},
						["170123"] = {
							["quest"] = 1,
						},
						["170121"] = {
							["quest"] = 1,
							["artifact"] = 100000,
						},
					},
					["character"] = {
						["Player-205-060D24B9"] = {
							["170126"] = {
								["quest"] = 1,
							},
						},
						["Player-205-06085B20"] = {
							["170126"] = {
								["blood"] = 10,
								["quest"] = 2,
								["resource"] = 1000,
								["profession"] = {
									[124124] = 1,
								},
							},
						},
						["Player-205-06D2379A"] = {
							["170524"] = {
								["artifact"] = 318700,
								["quest"] = 3,
								["gold"] = 1679100,
							},
						},
						["Player-205-064C974D"] = {
							["170126"] = {
								["quest"] = 1,
								["resource"] = 1000,
							},
						},
						["Player-205-06497083"] = {
							["170126"] = {
								["quest"] = 1,
								["resource"] = 1000,
							},
							["170123"] = {
								["quest"] = 1,
							},
							["170121"] = {
								["quest"] = 1,
								["artifact"] = 100000,
							},
						},
					},
				},
				["quest"] = {
					["global"] = {
						[43753] = 1,
						["total"] = 10,
						[43641] = 1,
						[41865] = 1,
						[41650] = 1,
						[44305] = 1,
						[42124] = 1,
						[42269] = 4,
					},
					["character"] = {
						["Player-205-060D24B9"] = {
							[42269] = 1,
							["total"] = 1,
						},
						["Player-205-06085B20"] = {
							[42269] = 1,
							["total"] = 2,
							[41650] = 1,
						},
						["Player-205-06D2379A"] = {
							[43753] = 1,
							["total"] = 3,
							[44305] = 1,
							[42124] = 1,
						},
						["Player-205-064C974D"] = {
							[42269] = 1,
							["total"] = 1,
						},
						["Player-205-06497083"] = {
							[42269] = 1,
							["total"] = 3,
							[41865] = 1,
							[43641] = 1,
						},
					},
				},
				["reward"] = {
					["global"] = {
						["artifact"] = 418700,
						["resource"] = 3000,
						["profession"] = {
							[124124] = 1,
						},
						["gold"] = 1679100,
						["blood"] = 10,
					},
					["character"] = {
						["Player-205-060D24B9"] = {
						},
						["Player-205-06085B20"] = {
							["blood"] = 10,
							["resource"] = 1000,
							["profession"] = {
								[124124] = 1,
							},
						},
						["Player-205-06D2379A"] = {
							["artifact"] = 318700,
							["gold"] = 1679100,
						},
						["Player-205-064C974D"] = {
							["resource"] = 1000,
						},
						["Player-205-06497083"] = {
							["artifact"] = 100000,
							["resource"] = 1000,
						},
					},
				},
			},
			["TutorialTaxyMap"] = true,
		},
	},
}
