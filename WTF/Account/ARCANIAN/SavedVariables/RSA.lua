
RSADB = {
	["namespaces"] = {
		["LibDualSpec-1.0"] = {
		},
	},
	["profileKeys"] = {
		["샤이아리 - 줄진"] = "Default",
		["릿데 - 아즈샤라"] = "Default",
		["최저방벽 - 줄진"] = "Default",
		["환상큰소 - 아즈샤라"] = "Default",
		["북경스딸 - 줄진"] = "Default",
		["클리엔 - 줄진"] = "Default",
		["아르니타 - 아즈샤라"] = "Default",
		["Emonda - 아즈샤라"] = "Default",
		["샤이아리 - 아즈샤라"] = "Default",
		["뚱빼미나무 - 줄진"] = "Default",
		["사나없찐 - 아즈샤라"] = "Default",
		["꼬꼬마빠샤 - 줄진"] = "Default",
		["Wilier - 아즈샤라"] = "Default",
		["Lezyne - 아즈샤라"] = "Default",
		["안녕김밥 - 줄진"] = "Default",
		["환상큰손 - 아즈샤라"] = "Default",
		["샤말밀레 - 아즈샤라"] = "Default",
		["재연마사 - 줄진"] = "Default",
		["데로사 - 아즈샤라"] = "Default",
		["써벨로 - 아즈샤라"] = "Default",
		["Santini - 아즈샤라"] = "Default",
		["징박무시하나연 - 줄진"] = "Default",
		["리떼 - 아즈샤라"] = "Default",
		["Vlaanderen - 아즈샤라"] = "Default",
		["Emonda - 줄진"] = "Default",
		["뽕나인 - 줄진"] = "Default",
		["영석썼숑 - 줄진"] = "Default",
		["사나정연 - 아즈샤라"] = "Default",
	},
	["global"] = {
		["version"] = "3.2752",
	},
	["profiles"] = {
		["Default"] = {
			["Warrior"] = {
				["Spells"] = {
					["Pummel"] = {
						["SmartGroup"] = false,
						["Say"] = true,
					},
				},
			},
			["Paladin"] = {
				["Spells"] = {
					["Redemption"] = {
						["SmartGroup"] = false,
						["Whisper"] = false,
					},
					["HandOfSacrifice"] = {
						["Whisper"] = false,
					},
				},
			},
			["Shaman"] = {
				["Spells"] = {
					["WindShear"] = {
						["SmartGroup"] = false,
						["Say"] = true,
					},
					["Reincarnation"] = {
						["SmartGroup"] = false,
					},
					["AncestralProtection"] = {
						["SmartGroup"] = false,
					},
					["Purge"] = {
						["SmartGroup"] = false,
					},
					["AncestralSpirit"] = {
						["Whisper"] = false,
						["Local"] = true,
						["SmartGroup"] = false,
					},
					["Heroism"] = {
						["SmartGroup"] = false,
					},
					["Hex"] = {
						["SmartGroup"] = false,
					},
					["WindRushTotem"] = {
						["SmartGroup"] = false,
					},
				},
			},
			["Rogue"] = {
				["Spells"] = {
					["Tricks"] = {
						["Whisper"] = false,
					},
				},
			},
			["Mage"] = {
				["Spells"] = {
					["RefreshmentTable"] = {
						["SmartGroup"] = false,
					},
					["Polymorph"] = {
						["SmartGroup"] = false,
					},
					["Counterspell"] = {
						["SmartGroup"] = false,
						["Say"] = true,
					},
				},
			},
			["Modules"] = {
				["Warrior"] = true,
				["Paladin"] = true,
				["Shaman"] = true,
				["Monk"] = true,
				["Rogue"] = true,
				["Mage"] = true,
				["DemonHunter"] = true,
				["Druid"] = true,
				["Priest"] = true,
				["Hunter"] = true,
				["Warlock"] = true,
				["DeathKnight"] = true,
			},
			["Druid"] = {
				["Spells"] = {
					["RemoveCorruption"] = {
						["SmartGroup"] = false,
					},
					["Ironbark"] = {
						["SmartGroup"] = false,
					},
				},
			},
			["Priest"] = {
				["Spells"] = {
					["DispelMagic"] = {
						["Say"] = true,
						["SmartGroup"] = false,
					},
					["Silence"] = {
						["SmartGroup"] = false,
						["Say"] = true,
					},
					["Resurrection"] = {
						["SmartGroup"] = false,
						["Whisper"] = false,
					},
					["PainSuppression"] = {
						["SmartGroup"] = false,
					},
					["ShackleUndead"] = {
						["SmartGroup"] = false,
						["Say"] = true,
					},
					["BodyAndSoul"] = {
						["Whisper"] = false,
					},
					["MassRess"] = {
						["SmartGroup"] = false,
					},
					["LeapOfFaith"] = {
						["Whisper"] = false,
					},
					["SpiritShell"] = {
						["SmartGroup"] = false,
					},
				},
			},
			["Hunter"] = {
				["Spells"] = {
					["SilencingShot"] = {
						["SmartGroup"] = false,
						["Say"] = true,
					},
					["WyvernSting"] = {
						["SmartGroup"] = false,
					},
					["FreezingTrap"] = {
						["SmartGroup"] = false,
					},
					["Muzzle"] = {
						["SmartGroup"] = false,
						["Say"] = true,
					},
					["RoarOfSacrifice"] = {
						["Whisper"] = false,
					},
					["AncientHysteria"] = {
						["SmartGroup"] = false,
					},
					["DistractingShot"] = {
						["SmartGroup"] = false,
						["Say"] = false,
					},
					["MastersCall"] = {
						["Whisper"] = false,
					},
					["EternalGuardian"] = {
						["SmartGroup"] = false,
						["Whisper"] = false,
					},
				},
			},
			["General"] = {
				["Race"] = "BloodElf",
				["Class"] = "MAGE",
			},
		},
	},
}
