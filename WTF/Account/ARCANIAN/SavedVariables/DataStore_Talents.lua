
DataStore_TalentsDB = {
	["profileKeys"] = {
		["샤이아리 - 줄진"] = "샤이아리 - 줄진",
		["언제나운무 - 아즈샤라"] = "언제나운무 - 아즈샤라",
		["최저방벽 - 줄진"] = "최저방벽 - 줄진",
		["환상큰소 - 아즈샤라"] = "환상큰소 - 아즈샤라",
		["북경스딸 - 줄진"] = "북경스딸 - 줄진",
		["클리엔 - 줄진"] = "클리엔 - 줄진",
		["사나무엇 - 아즈샤라"] = "사나무엇 - 아즈샤라",
		["아르니타 - 아즈샤라"] = "아르니타 - 아즈샤라",
		["릿데 - 아즈샤라"] = "릿데 - 아즈샤라",
		["안녕김밥 - 줄진"] = "안녕김밥 - 줄진",
		["꼬꼬마빠샤 - 줄진"] = "꼬꼬마빠샤 - 줄진",
		["뚱빼미나무 - 줄진"] = "뚱빼미나무 - 줄진",
		["사나없찐 - 아즈샤라"] = "사나없찐 - 아즈샤라",
		["샤이아리 - 아즈샤라"] = "샤이아리 - 아즈샤라",
		["Wilier - 아즈샤라"] = "Wilier - 아즈샤라",
		["Lezyne - 아즈샤라"] = "Lezyne - 아즈샤라",
		["데로사 - 아즈샤라"] = "데로사 - 아즈샤라",
		["환상큰손 - 아즈샤라"] = "환상큰손 - 아즈샤라",
		["샤말밀레 - 아즈샤라"] = "샤말밀레 - 아즈샤라",
		["재연마사 - 줄진"] = "재연마사 - 줄진",
		["Vlaanderen - 아즈샤라"] = "Vlaanderen - 아즈샤라",
		["징박무시하나연 - 줄진"] = "징박무시하나연 - 줄진",
		["Santini - 아즈샤라"] = "Santini - 아즈샤라",
		["써벨로 - 아즈샤라"] = "써벨로 - 아즈샤라",
		["리떼 - 아즈샤라"] = "리떼 - 아즈샤라",
		["Emonda - 줄진"] = "Emonda - 줄진",
		["뽕나인 - 줄진"] = "뽕나인 - 줄진",
		["Emonda - 아즈샤라"] = "Emonda - 아즈샤라",
		["영석썼숑 - 줄진"] = "영석썼숑 - 줄진",
		["사나정연 - 아즈샤라"] = "사나정연 - 아즈샤라",
	},
	["global"] = {
		["Characters"] = {
			["Default.줄진.뽕나인"] = {
				["lastUpdate"] = 1478599465,
				["Specializations"] = {
					[3] = 0,
				},
				["Class"] = "SHAMAN",
			},
			["Default.줄진.북경스딸"] = {
				["lastUpdate"] = 1478598113,
				["Specializations"] = {
					[3] = 11770,
					[2] = 3801,
				},
				["Class"] = "MONK",
			},
			["Default.줄진.영석썼숑"] = {
				["lastUpdate"] = 1478600082,
				["Specializations"] = {
					[3] = 0,
				},
				["Class"] = "WARLOCK",
			},
			["Default.아즈샤라.데로사"] = {
				["EquippedArtifact"] = "기만자의 쌍날검",
				["ArtifactKnowledgeMultiplier"] = 1,
				["Specializations"] = {
					46, -- [1]
					38, -- [2]
				},
				["lastUpdate"] = 1494303185,
				["Class"] = "DEMONHUNTER",
				["ArtifactKnowledge"] = 0,
				["Artifacts"] = {
					["알드라치 전투검"] = {
						["rank"] = 1,
						["pointsRemaining"] = 745,
					},
					["기만자의 쌍날검"] = {
						["rank"] = 3,
						["pointsRemaining"] = 95,
						["tier"] = 1,
					},
				},
			},
			["Default.아즈샤라.사나정연"] = {
				["lastUpdate"] = 1513569252,
				["Specializations"] = {
					0, -- [1]
					3, -- [2]
					2, -- [3]
				},
				["Class"] = "MONK",
			},
			["Default.줄진.뚱빼미나무"] = {
				["lastUpdate"] = 1471189673,
				["Specializations"] = {
					[4] = 0,
				},
				["Class"] = "DRUID",
			},
			["Default.줄진.최저방벽"] = {
				["lastUpdate"] = 1494291818,
				["Specializations"] = {
					[2] = 0,
				},
				["Class"] = "WARRIOR",
			},
			["Default.아즈샤라.Wilier"] = {
				["lastUpdate"] = 1488810464,
				["Specializations"] = {
					[3] = 1,
				},
				["Class"] = "PALADIN",
			},
			["Default.아즈샤라.리떼"] = {
				["EquippedArtifact"] = "칠흑한기 - 알로디의 대지팡이",
				["ArtifactKnowledgeMultiplier"] = 6300001,
				["Specializations"] = {
					9591, -- [1]
					13718, -- [2]
					8182, -- [3]
				},
				["lastUpdate"] = 1513606513,
				["Class"] = "MAGE",
				["ArtifactKnowledge"] = 55,
				["Artifacts"] = {
					["펠로멜로른"] = {
						["tier"] = 2,
						["rank"] = 35,
						["pointsRemaining"] = 29687,
					},
					["알루네스 - 마그나의 대지팡이"] = {
						["tier"] = 2,
						["pointsRemaining"] = 100,
					},
					["칠흑한기"] = {
						["pointsRemaining"] = 3440,
					},
					["칠흑한기 - 알로디의 대지팡이"] = {
						["pointsRemaining"] = 17738078890,
						["rank"] = 63,
						["tier"] = 2,
					},
				},
			},
			["Default.아즈샤라.아르니타"] = {
				["EquippedArtifact"] = "국왕 시해자",
				["ArtifactKnowledgeMultiplier"] = 270401,
				["Specializations"] = {
					5621, -- [1]
					11223, -- [2]
				},
				["lastUpdate"] = 1505653979,
				["Class"] = "ROGUE",
				["ArtifactKnowledge"] = 43,
				["Artifacts"] = {
					["공포의 검"] = {
						["rank"] = 13,
						["pointsRemaining"] = 4835,
						["tier"] = 1,
					},
					["국왕 시해자"] = {
						["rank"] = 1,
						["tier"] = 1,
					},
				},
			},
			["Default.아즈샤라.샤말밀레"] = {
				["EquippedArtifact"] = "은빛 손",
				["ArtifactKnowledgeMultiplier"] = 270401,
				["Specializations"] = {
					7901, -- [1]
					15038, -- [2]
				},
				["lastUpdate"] = 1513497776,
				["Class"] = "PALADIN",
				["ArtifactKnowledge"] = 43,
				["Artifacts"] = {
					["진실의 수호자"] = {
						["pointsRemaining"] = 4075,
						["rank"] = 19,
					},
					["은빛 손"] = {
						["pointsRemaining"] = 757860,
						["rank"] = 33,
						["tier"] = 1,
					},
				},
			},
			["Default.아즈샤라.Vlaanderen"] = {
				["EquippedArtifact"] = "티탄분쇄자",
				["Artifacts"] = {
					["티탄분쇄자"] = {
						["tier"] = 2,
						["pointsRemaining"] = 12873057,
						["rank"] = 38,
					},
					["칼날갈퀴 - 야생 신의 창"] = {
						["pointsRemaining"] = 100,
					},
					["타스도라 - 윈드러너의 유산"] = {
						["pointsRemaining"] = 388690,
						["rank"] = 31,
					},
				},
				["Class"] = "HUNTER",
				["lastUpdate"] = 1505652736,
				["Specializations"] = {
					9626, -- [1]
					15829, -- [2]
				},
				["ArtifactKnowledge"] = 43,
				["ArtifactKnowledgeMultiplier"] = 270401,
			},
			["Default.줄진.꼬꼬마빠샤"] = {
				["lastUpdate"] = 1480651844,
				["Specializations"] = {
					[2] = 12251,
				},
				["Class"] = "ROGUE",
			},
			["Default.줄진.클리엔"] = {
				["EquippedArtifact"] = "잘아타스 - 검은 제국의 비수",
				["Artifacts"] = {
					["잘아타스 - 검은 제국의 비수"] = {
						["rank"] = 1,
						["tier"] = 1,
					},
				},
				["Class"] = "PRIEST",
				["lastUpdate"] = 1493734087,
				["Specializations"] = {
					[3] = 7854,
					[2] = 6011,
				},
				["ArtifactKnowledge"] = 0,
				["ArtifactKnowledgeMultiplier"] = 1,
			},
			["Default.줄진.Emonda"] = {
				["lastUpdate"] = 1471190063,
				["Specializations"] = {
					1, -- [1]
				},
				["Class"] = "DEMONHUNTER",
			},
			["Default.줄진.재연마사"] = {
				["lastUpdate"] = 1478598376,
				["Specializations"] = {
					5526, -- [1]
					7127, -- [2]
				},
				["Class"] = "HUNTER",
			},
			["Default.아즈샤라.Emonda"] = {
				["EquippedArtifact"] = "샤라스달 - 해일의 홀",
				["Artifacts"] = {
					["샤라스달 - 해일의 홀"] = {
						["pointsRemaining"] = 858117209,
						["rank"] = 53,
						["tier"] = 2,
					},
					["라덴의 주먹"] = {
						["rank"] = 36,
						["pointsRemaining"] = 6939455,
						["tier"] = 2,
					},
					["둠해머"] = {
						["rank"] = 1,
						["tier"] = 2,
					},
				},
				["Class"] = "SHAMAN",
				["lastUpdate"] = 1513310646,
				["Specializations"] = {
					8085, -- [1]
					10203, -- [2]
					14999, -- [3]
				},
				["ArtifactKnowledge"] = 55,
				["ArtifactKnowledgeMultiplier"] = 6300001,
			},
			["Default.줄진.징박무시하나연"] = {
				["lastUpdate"] = 1478600440,
				["Specializations"] = {
					0, -- [1]
				},
				["Class"] = "PALADIN",
			},
			["Default.줄진.안녕김밥"] = {
				["lastUpdate"] = 1471189983,
				["Specializations"] = {
					0, -- [1]
					[3] = 0,
				},
				["Class"] = "DEATHKNIGHT",
			},
			["Default.아즈샤라.Santini"] = {
				["EquippedArtifact"] = "울타레쉬 - 저승바람 수확기",
				["Artifacts"] = {
					["살게라스의 홀"] = {
						["tier"] = 1,
						["pointsRemaining"] = 277735,
						["rank"] = 28,
					},
					["울타레쉬 - 저승바람 수확기"] = {
						["rank"] = 25,
						["pointsRemaining"] = 52040,
						["tier"] = 1,
					},
					["만아리의 해골"] = {
						["pointsRemaining"] = 100,
					},
				},
				["Class"] = "WARLOCK",
				["lastUpdate"] = 1505653512,
				["Specializations"] = {
					5786, -- [1]
					8031, -- [2]
					6877, -- [3]
				},
				["ArtifactKnowledge"] = 43,
				["ArtifactKnowledgeMultiplier"] = 270401,
			},
			["Default.아즈샤라.Lezyne"] = {
				["EquippedArtifact"] = "우르속의 발톱",
				["ArtifactKnowledgeMultiplier"] = 1,
				["Specializations"] = {
					[3] = 12157,
					[4] = 9967,
				},
				["lastUpdate"] = 1494311962,
				["Class"] = "DRUID",
				["ArtifactKnowledge"] = 0,
				["Artifacts"] = {
					["그하니르 - 어머니 나무"] = {
						["pointsRemaining"] = 200,
						["rank"] = 2,
					},
					["우르속의 발톱"] = {
						["rank"] = 4,
						["pointsRemaining"] = 155,
						["tier"] = 1,
					},
				},
			},
			["Default.아즈샤라.릿데"] = {
				["EquippedArtifact"] = "스트롬카르 - 전쟁파괴자",
				["ArtifactKnowledgeMultiplier"] = 6300001,
				["Specializations"] = {
					6749, -- [1]
					12029, -- [2]
					10661, -- [3]
				},
				["lastUpdate"] = 1513309860,
				["Class"] = "WARRIOR",
				["ArtifactKnowledge"] = 55,
				["Artifacts"] = {
					["스트롬카르 - 전쟁파괴자"] = {
						["rank"] = 36,
						["pointsRemaining"] = 880270,
						["tier"] = 2,
					},
					["발라리아르의 전쟁검"] = {
						["pointsRemaining"] = 2916705,
						["tier"] = 2,
						["rank"] = 41,
					},
					["대지의 수호자의 비늘"] = {
						["tier"] = 2,
						["pointsRemaining"] = 100,
					},
				},
			},
			["Default.줄진.샤이아리"] = {
				["EquippedArtifact"] = "칠흑한기 - 알로디의 대지팡이",
				["ArtifactKnowledgeMultiplier"] = 1,
				["Specializations"] = {
					[3] = 14835,
				},
				["lastUpdate"] = 1493734015,
				["Class"] = "MAGE",
				["ArtifactKnowledge"] = 0,
				["Artifacts"] = {
					["칠흑한기 - 알로디의 대지팡이"] = {
						["tier"] = 1,
						["rank"] = 1,
					},
				},
			},
			["Default.아즈샤라.샤이아리"] = {
				["EquippedArtifact"] = "잘아타스 - 검은 제국의 비수",
				["ArtifactKnowledgeMultiplier"] = 6300001,
				["Specializations"] = {
					[3] = 7769,
					[2] = 10105,
				},
				["lastUpdate"] = 1513571150,
				["Class"] = "PRIEST",
				["ArtifactKnowledge"] = 55,
				["Artifacts"] = {
					["투우레 - 나루의 봉화"] = {
						["tier"] = 2,
						["rank"] = 38,
						["pointsRemaining"] = 3518305,
					},
					["잘아타스 - 검은 제국의 비수"] = {
						["tier"] = 2,
						["rank"] = 63,
						["pointsRemaining"] = 15796867795,
					},
					["빛의 분노"] = {
						["tier"] = 2,
						["pointsRemaining"] = 100,
					},
				},
			},
		},
	},
}
DataStore_TalentsRefDB = {
	["profileKeys"] = {
		["샤이아리 - 줄진"] = "샤이아리 - 줄진",
		["언제나운무 - 아즈샤라"] = "언제나운무 - 아즈샤라",
		["최저방벽 - 줄진"] = "최저방벽 - 줄진",
		["환상큰소 - 아즈샤라"] = "환상큰소 - 아즈샤라",
		["북경스딸 - 줄진"] = "북경스딸 - 줄진",
		["클리엔 - 줄진"] = "클리엔 - 줄진",
		["사나무엇 - 아즈샤라"] = "사나무엇 - 아즈샤라",
		["아르니타 - 아즈샤라"] = "아르니타 - 아즈샤라",
		["릿데 - 아즈샤라"] = "릿데 - 아즈샤라",
		["안녕김밥 - 줄진"] = "안녕김밥 - 줄진",
		["꼬꼬마빠샤 - 줄진"] = "꼬꼬마빠샤 - 줄진",
		["뚱빼미나무 - 줄진"] = "뚱빼미나무 - 줄진",
		["사나없찐 - 아즈샤라"] = "사나없찐 - 아즈샤라",
		["샤이아리 - 아즈샤라"] = "샤이아리 - 아즈샤라",
		["Wilier - 아즈샤라"] = "Wilier - 아즈샤라",
		["Lezyne - 아즈샤라"] = "Lezyne - 아즈샤라",
		["데로사 - 아즈샤라"] = "데로사 - 아즈샤라",
		["환상큰손 - 아즈샤라"] = "환상큰손 - 아즈샤라",
		["샤말밀레 - 아즈샤라"] = "샤말밀레 - 아즈샤라",
		["재연마사 - 줄진"] = "재연마사 - 줄진",
		["Vlaanderen - 아즈샤라"] = "Vlaanderen - 아즈샤라",
		["징박무시하나연 - 줄진"] = "징박무시하나연 - 줄진",
		["Santini - 아즈샤라"] = "Santini - 아즈샤라",
		["써벨로 - 아즈샤라"] = "써벨로 - 아즈샤라",
		["리떼 - 아즈샤라"] = "리떼 - 아즈샤라",
		["Emonda - 줄진"] = "Emonda - 줄진",
		["뽕나인 - 줄진"] = "뽕나인 - 줄진",
		["Emonda - 아즈샤라"] = "Emonda - 아즈샤라",
		["영석썼숑 - 줄진"] = "영석썼숑 - 줄진",
		["사나정연 - 아즈샤라"] = "사나정연 - 아즈샤라",
	},
	["global"] = {
		["DEATHKNIGHT"] = {
			["Locale"] = "koKR",
			["Specializations"] = {
				{
					["id"] = 250,
					["icon"] = "Interface\\Icons\\Spell_Deathknight_BloodPresence",
					["name"] = "혈기",
				}, -- [1]
				[3] = {
					["id"] = 252,
					["talents"] = {
						22024, -- [1]
						22025, -- [2]
						22026, -- [3]
						22027, -- [4]
						22028, -- [5]
						22029, -- [6]
						22516, -- [7]
						22518, -- [8]
						22520, -- [9]
						22522, -- [10]
						22524, -- [11]
						22526, -- [12]
						22528, -- [13]
						22530, -- [14]
						22022, -- [15]
						22532, -- [16]
						22534, -- [17]
						22536, -- [18]
					},
					["icon"] = "Interface\\Icons\\Spell_Deathknight_UnholyPresence",
					["name"] = "부정",
				},
			},
			["Version"] = 22423,
		},
		["WARRIOR"] = {
			["Locale"] = "koKR",
			["Specializations"] = {
				{
					["id"] = 71,
					["talents"] = {
						22624, -- [1]
						22360, -- [2]
						22371, -- [3]
						22373, -- [4]
						22625, -- [5]
						22409, -- [6]
						22380, -- [7]
						22489, -- [8]
						19138, -- [9]
						15757, -- [10]
						22627, -- [11]
						22628, -- [12]
						22383, -- [13]
						22393, -- [14]
						22800, -- [15]
						22394, -- [16]
						22397, -- [17]
						22399, -- [18]
						21204, -- [19]
						22407, -- [20]
						21667, -- [21]
					},
				}, -- [1]
				{
					["id"] = 72,
					["talents"] = {
						22632, -- [1]
						22633, -- [2]
						22491, -- [3]
						22374, -- [4]
						22372, -- [5]
						22409, -- [6]
						22379, -- [7]
						22381, -- [8]
						19138, -- [9]
						22635, -- [10]
						22627, -- [11]
						22382, -- [12]
						22384, -- [13]
						22391, -- [14]
						19140, -- [15]
						22395, -- [16]
						22544, -- [17]
						22400, -- [18]
						22405, -- [19]
						22402, -- [20]
						16037, -- [21]
					},
					["icon"] = "Interface\\Icons\\Ability_Warrior_InnerRage",
					["name"] = "분노",
				}, -- [2]
				{
					["id"] = 73,
					["talents"] = {
						15760, -- [1]
						15759, -- [2]
						15774, -- [3]
						19676, -- [4]
						22629, -- [5]
						22789, -- [6]
						22378, -- [7]
						22626, -- [8]
						19138, -- [9]
						22630, -- [10]
						22627, -- [11]
						22488, -- [12]
						22392, -- [13]
						22631, -- [14]
						22362, -- [15]
						22396, -- [16]
						22398, -- [17]
						22401, -- [18]
						21204, -- [19]
						22406, -- [20]
						22801, -- [21]
					},
				}, -- [3]
			},
			["Version"] = 25549,
		},
		["SHAMAN"] = {
			["Locale"] = "koKR",
			["Specializations"] = {
				{
					["id"] = 262,
					["talents"] = {
						22356, -- [1]
						22357, -- [2]
						22358, -- [3]
						19259, -- [4]
						22139, -- [5]
						21963, -- [6]
						19275, -- [7]
						19260, -- [8]
						22127, -- [9]
						19271, -- [10]
						19272, -- [11]
						19273, -- [12]
						22144, -- [13]
						22172, -- [14]
						19270, -- [15]
						22145, -- [16]
						19266, -- [17]
						21968, -- [18]
						21198, -- [19]
						21199, -- [20]
						21972, -- [21]
					},
					["icon"] = "Interface\\Icons\\Spell_Nature_Lightning",
					["name"] = "정기",
				}, -- [1]
				{
					["id"] = 263,
					["talents"] = {
						22354, -- [1]
						22355, -- [2]
						22353, -- [3]
						22636, -- [4]
						22150, -- [5]
						21963, -- [6]
						19275, -- [7]
						19260, -- [8]
						22127, -- [9]
						22162, -- [10]
						19272, -- [11]
						22171, -- [12]
						22137, -- [13]
						22149, -- [14]
						22147, -- [15]
						21973, -- [16]
						22352, -- [17]
						22351, -- [18]
						21969, -- [19]
						22148, -- [20]
						22359, -- [21]
					},
					["icon"] = "Interface\\Icons\\Spell_Shaman_ImprovedStormstrike",
					["name"] = "고양",
				}, -- [2]
				{
					["id"] = 264,
					["talents"] = {
						19262, -- [1]
						19263, -- [2]
						19264, -- [3]
						19259, -- [4]
						22492, -- [5]
						21963, -- [6]
						19275, -- [7]
						19260, -- [8]
						22127, -- [9]
						22152, -- [10]
						22322, -- [11]
						22323, -- [12]
						22539, -- [13]
						19269, -- [14]
						21966, -- [15]
						19265, -- [16]
						21971, -- [17]
						21968, -- [18]
						21970, -- [19]
						22153, -- [20]
						21675, -- [21]
					},
					["icon"] = "Interface\\Icons\\Spell_Nature_MagicImmunity",
					["name"] = "복원",
				}, -- [3]
			},
			["Version"] = 25549,
		},
		["MAGE"] = {
			["Locale"] = "koKR",
			["Specializations"] = {
				{
					["id"] = 62,
					["talents"] = {
						22458, -- [1]
						22461, -- [2]
						22464, -- [3]
						22442, -- [4]
						22443, -- [5]
						16025, -- [6]
						22444, -- [7]
						22445, -- [8]
						22447, -- [9]
						22453, -- [10]
						22467, -- [11]
						22470, -- [12]
						22446, -- [13]
						22448, -- [14]
						22471, -- [15]
						22455, -- [16]
						22449, -- [17]
						22474, -- [18]
						21630, -- [19]
						21144, -- [20]
						21145, -- [21]
					},
					["icon"] = "Interface\\Icons\\Spell_Holy_MagicalSentry",
					["name"] = "비전",
				}, -- [1]
				{
					["id"] = 63,
					["talents"] = {
						22456, -- [1]
						22459, -- [2]
						22462, -- [3]
						22442, -- [4]
						22908, -- [5]
						22905, -- [6]
						22444, -- [7]
						22445, -- [8]
						22447, -- [9]
						22450, -- [10]
						22465, -- [11]
						22468, -- [12]
						22904, -- [13]
						22448, -- [14]
						22471, -- [15]
						22451, -- [16]
						22449, -- [17]
						22472, -- [18]
						21631, -- [19]
						22220, -- [20]
						21633, -- [21]
					},
					["icon"] = "Interface\\Icons\\Spell_Fire_FireBolt02",
					["name"] = "화염",
				}, -- [2]
				{
					["id"] = 64,
					["talents"] = {
						22457, -- [1]
						22460, -- [2]
						22463, -- [3]
						22442, -- [4]
						22903, -- [5]
						16025, -- [6]
						22444, -- [7]
						22445, -- [8]
						22447, -- [9]
						22452, -- [10]
						22466, -- [11]
						22469, -- [12]
						22446, -- [13]
						22448, -- [14]
						22471, -- [15]
						22454, -- [16]
						22449, -- [17]
						22473, -- [18]
						21632, -- [19]
						22309, -- [20]
						21634, -- [21]
					},
					["icon"] = "Interface\\Icons\\Spell_Frost_FrostBolt02",
					["name"] = "냉기",
				}, -- [3]
			},
			["Version"] = 25549,
		},
		["PRIEST"] = {
			["Locale"] = "koKR",
			["Specializations"] = {
				{
					["id"] = 256,
					["talents"] = {
						22328, -- [1]
						19753, -- [2]
						22329, -- [3]
						22315, -- [4]
						22316, -- [5]
						19758, -- [6]
						22440, -- [7]
						22094, -- [8]
						19755, -- [9]
						19759, -- [10]
						19769, -- [11]
						19761, -- [12]
						22330, -- [13]
						19765, -- [14]
						19766, -- [15]
						22161, -- [16]
						19760, -- [17]
						19763, -- [18]
						21183, -- [19]
						21184, -- [20]
						21185, -- [21]
					},
					["icon"] = "Interface\\Icons\\Spell_Holy_PowerWordShield",
					["name"] = "수양",
				}, -- [1]
				{
					["id"] = 257,
					["talents"] = {
						19752, -- [1]
						22136, -- [2]
						19754, -- [3]
						22315, -- [4]
						22326, -- [5]
						21976, -- [6]
						22440, -- [7]
						22095, -- [8]
						22562, -- [9]
						21750, -- [10]
						21977, -- [11]
						21752, -- [12]
						19764, -- [13]
						22327, -- [14]
						21754, -- [15]
						19767, -- [16]
						19760, -- [17]
						19763, -- [18]
						21636, -- [19]
						21644, -- [20]
						21638, -- [21]
					},
					["icon"] = "Interface\\Icons\\Spell_Holy_GuardianSpirit",
					["name"] = "신성",
				}, -- [2]
				{
					["id"] = 258,
					["talents"] = {
						22312, -- [1]
						22313, -- [2]
						22314, -- [3]
						22325, -- [4]
						22316, -- [5]
						19758, -- [6]
						22487, -- [7]
						22094, -- [8]
						19755, -- [9]
						21751, -- [10]
						22317, -- [11]
						21753, -- [12]
						22310, -- [13]
						22311, -- [14]
						21755, -- [15]
						21718, -- [16]
						21719, -- [17]
						21720, -- [18]
						21637, -- [19]
						21978, -- [20]
						21979, -- [21]
					},
					["icon"] = "Interface\\Icons\\Spell_Shadow_ShadowWordPain",
					["name"] = "암흑",
				}, -- [3]
			},
			["Version"] = 25549,
		},
		["ROGUE"] = {
			["Locale"] = "koKR",
			["Specializations"] = {
				{
					["id"] = 259,
					["talents"] = {
						22337, -- [1]
						22338, -- [2]
						22339, -- [3]
						22331, -- [4]
						22332, -- [5]
						22333, -- [6]
						19239, -- [7]
						19240, -- [8]
						19241, -- [9]
						22340, -- [10]
						22122, -- [11]
						22123, -- [12]
						22341, -- [13]
						22114, -- [14]
						22115, -- [15]
						22343, -- [16]
						19249, -- [17]
						22344, -- [18]
						21186, -- [19]
						22133, -- [20]
						21188, -- [21]
					},
					["icon"] = "Interface\\Icons\\Ability_Rogue_DeadlyBrew",
					["name"] = "암살",
				}, -- [1]
				{
					["id"] = 260,
					["talents"] = {
						22118, -- [1]
						22119, -- [2]
						22120, -- [3]
						19236, -- [4]
						19237, -- [5]
						19238, -- [6]
						19239, -- [7]
						19240, -- [8]
						19241, -- [9]
						22121, -- [10]
						22122, -- [11]
						22123, -- [12]
						19245, -- [13]
						22114, -- [14]
						22124, -- [15]
						21990, -- [16]
						19249, -- [17]
						19250, -- [18]
						22125, -- [19]
						22133, -- [20]
						21188, -- [21]
					},
					["icon"] = "Interface\\Icons\\INV_Sword_30",
					["name"] = "무법",
				}, -- [2]
			},
			["Version"] = 25021,
		},
		["WARLOCK"] = {
			["Locale"] = "koKR",
			["Specializations"] = {
				{
					["id"] = 265,
					["talents"] = {
						19290, -- [1]
						22090, -- [2]
						22040, -- [3]
						22044, -- [4]
						21180, -- [5]
						22088, -- [6]
						19280, -- [7]
						19285, -- [8]
						22476, -- [9]
						19279, -- [10]
						19292, -- [11]
						22046, -- [12]
						22047, -- [13]
						19291, -- [14]
						19288, -- [15]
						21182, -- [16]
						19294, -- [17]
						19295, -- [18]
						19284, -- [19]
						19281, -- [20]
						19293, -- [21]
					},
					["icon"] = "Interface\\Icons\\Spell_Shadow_DeathCoil",
					["name"] = "고통",
				}, -- [1]
				{
					["id"] = 266,
					["talents"] = {
						22038, -- [1]
						22036, -- [2]
						22041, -- [3]
						22045, -- [4]
						21694, -- [5]
						22089, -- [6]
						19280, -- [7]
						19285, -- [8]
						19286, -- [9]
						22477, -- [10]
						22042, -- [11]
						22046, -- [12]
						22047, -- [13]
						19291, -- [14]
						19288, -- [15]
						21182, -- [16]
						19294, -- [17]
						21717, -- [18]
						22478, -- [19]
						22479, -- [20]
						19293, -- [21]
					},
					["icon"] = "Interface\\Icons\\Spell_Shadow_Metamorphosis",
					["name"] = "악마",
				}, -- [2]
				{
					["id"] = 267,
					["talents"] = {
						22039, -- [1]
						22048, -- [2]
						22052, -- [3]
						21181, -- [4]
						21695, -- [5]
						22088, -- [6]
						19280, -- [7]
						19285, -- [8]
						19286, -- [9]
						22480, -- [10]
						22043, -- [11]
						22046, -- [12]
						22047, -- [13]
						19291, -- [14]
						19288, -- [15]
						21182, -- [16]
						19294, -- [17]
						19295, -- [18]
						22481, -- [19]
						22482, -- [20]
						19293, -- [21]
					},
					["icon"] = "Interface\\Icons\\Spell_Shadow_RainOfFire",
					["name"] = "파괴",
				}, -- [3]
			},
			["Version"] = 25021,
		},
		["DEMONHUNTER"] = {
			["Locale"] = "koKR",
			["Specializations"] = {
				{
					["id"] = 577,
					["talents"] = {
						21854, -- [1]
						22493, -- [2]
						22416, -- [3]
						21857, -- [4]
						22765, -- [5]
						22799, -- [6]
						22909, -- [7]
						22494, -- [8]
						21862, -- [9]
					},
					["icon"] = "Interface\\Icons\\ability_demonhunter_specdps",
					["name"] = "파멸",
				}, -- [1]
				{
					["id"] = 581,
					["talents"] = {
						22502, -- [1]
						22503, -- [2]
						22504, -- [3]
						22505, -- [4]
						22766, -- [5]
						22507, -- [6]
						22324, -- [7]
						22541, -- [8]
						22540, -- [9]
					},
					["icon"] = "Interface\\Icons\\ability_demonhunter_spectank",
					["name"] = "복수",
				}, -- [2]
			},
			["Version"] = 24015,
		},
		["PALADIN"] = {
			["Locale"] = "koKR",
			["Specializations"] = {
				{
					["id"] = 65,
					["talents"] = {
						17565, -- [1]
						17567, -- [2]
						17569, -- [3]
						22176, -- [4]
						17575, -- [5]
						17577, -- [6]
						22179, -- [7]
						22180, -- [8]
						21811, -- [9]
						22181, -- [10]
						17591, -- [11]
						17593, -- [12]
						17597, -- [13]
						17599, -- [14]
						22164, -- [15]
						22189, -- [16]
						22190, -- [17]
						22484, -- [18]
						21668, -- [19]
						21671, -- [20]
						21203, -- [21]
					},
					["icon"] = "Interface\\Icons\\Spell_Holy_HolyBolt",
					["name"] = "신성",
				}, -- [1]
				{
					["id"] = 66,
					["talents"] = {
						22428, -- [1]
						22558, -- [2]
						22430, -- [3]
						22431, -- [4]
						22604, -- [5]
						22594, -- [6]
						22179, -- [7]
						22180, -- [8]
						21811, -- [9]
						22433, -- [10]
						22434, -- [11]
						22435, -- [12]
						22705, -- [13]
						21795, -- [14]
						17601, -- [15]
						22564, -- [16]
						22438, -- [17]
						22484, -- [18]
						21201, -- [19]
						21202, -- [20]
						22645, -- [21]
					},
				}, -- [2]
				{
					["id"] = 70,
					["talents"] = {
						22590, -- [1]
						22557, -- [2]
						22175, -- [3]
					},
					["icon"] = "Interface\\Icons\\Spell_Holy_AuraOfLight",
					["name"] = "징벌",
				}, -- [3]
			},
			["Version"] = 25549,
		},
		["DRUID"] = {
			["Locale"] = "koKR",
			["Specializations"] = {
				[4] = {
					["id"] = 105,
					["talents"] = {
						18569, -- [1]
						18574, -- [2]
						18572, -- [3]
						19283, -- [4]
						18570, -- [5]
						18571, -- [6]
						22366, -- [7]
						22367, -- [8]
						22160, -- [9]
						21778, -- [10]
						18576, -- [11]
						18577, -- [12]
						21710, -- [13]
						21707, -- [14]
						21704, -- [15]
						21716, -- [16]
						21713, -- [17]
						22165, -- [18]
						22403, -- [19]
						21651, -- [20]
						22404, -- [21]
					},
					["icon"] = "Interface\\Icons\\Spell_Nature_HealingTouch",
					["name"] = "회복",
				},
				[3] = {
					["id"] = 104,
					["talents"] = {
						22419, -- [1]
						22418, -- [2]
						22420, -- [3]
						22424, -- [4]
						22916, -- [5]
						18571, -- [6]
						22163, -- [7]
						22156, -- [8]
						22159, -- [9]
						21778, -- [10]
						18576, -- [11]
						18577, -- [12]
						21709, -- [13]
						21706, -- [14]
						22421, -- [15]
						22423, -- [16]
						21712, -- [17]
						22422, -- [18]
						22426, -- [19]
						22427, -- [20]
						22425, -- [21]
					},
				},
			},
			["Version"] = 24015,
		},
		["MONK"] = {
			["Locale"] = "koKR",
			["Specializations"] = {
				{
					["id"] = 268,
					["talents"] = {
						19823, -- [1]
						22091, -- [2]
						20185, -- [3]
					},
				}, -- [1]
				{
					["id"] = 270,
					["talents"] = {
						19823, -- [1]
						19820, -- [2]
						20185, -- [3]
					},
					["icon"] = "Interface\\Icons\\spell_monk_mistweaver_spec",
					["name"] = "운무",
				}, -- [2]
				{
					["id"] = 269,
					["talents"] = {
						19823, -- [1]
						22091, -- [2]
						20185, -- [3]
					},
					["icon"] = "Interface\\Icons\\spell_monk_windwalker_spec",
					["name"] = "풍운",
				}, -- [3]
			},
			["Version"] = 25549,
		},
		["HUNTER"] = {
			["Locale"] = "koKR",
			["Specializations"] = {
				{
					["id"] = 253,
					["talents"] = {
						22291, -- [1]
						22280, -- [2]
						22282, -- [3]
						21997, -- [4]
						22769, -- [5]
						22290, -- [6]
						19347, -- [7]
						19348, -- [8]
						22318, -- [9]
						22441, -- [10]
						22347, -- [11]
						22269, -- [12]
						22284, -- [13]
						22276, -- [14]
						22293, -- [15]
						19357, -- [16]
						22002, -- [17]
						22287, -- [18]
						22273, -- [19]
						21986, -- [20]
						22295, -- [21]
					},
					["icon"] = "INTERFACE\\ICONS\\ability_hunter_bestialdiscipline",
					["name"] = "야수",
				}, -- [1]
				{
					["id"] = 254,
					["talents"] = {
						22279, -- [1]
						22501, -- [2]
						22289, -- [3]
						22495, -- [4]
						22497, -- [5]
						22498, -- [6]
						19347, -- [7]
						19348, -- [8]
						22318, -- [9]
						22267, -- [10]
						22286, -- [11]
						21998, -- [12]
						22284, -- [13]
						22276, -- [14]
						22499, -- [15]
						19357, -- [16]
						22002, -- [17]
						22287, -- [18]
						22274, -- [19]
						22308, -- [20]
						22288, -- [21]
					},
					["icon"] = "Interface\\Icons\\Ability_Hunter_FocusedAim",
					["name"] = "사격",
				}, -- [2]
			},
			["Version"] = 25021,
		},
	},
}
