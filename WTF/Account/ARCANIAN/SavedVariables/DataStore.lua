
DataStoreDB = {
	["profileKeys"] = {
		["샤이아리 - 줄진"] = "샤이아리 - 줄진",
		["언제나운무 - 아즈샤라"] = "언제나운무 - 아즈샤라",
		["최저방벽 - 줄진"] = "최저방벽 - 줄진",
		["환상큰소 - 아즈샤라"] = "환상큰소 - 아즈샤라",
		["북경스딸 - 줄진"] = "북경스딸 - 줄진",
		["클리엔 - 줄진"] = "클리엔 - 줄진",
		["사나무엇 - 아즈샤라"] = "사나무엇 - 아즈샤라",
		["아르니타 - 아즈샤라"] = "아르니타 - 아즈샤라",
		["릿데 - 아즈샤라"] = "릿데 - 아즈샤라",
		["안녕김밥 - 줄진"] = "안녕김밥 - 줄진",
		["꼬꼬마빠샤 - 줄진"] = "꼬꼬마빠샤 - 줄진",
		["뚱빼미나무 - 줄진"] = "뚱빼미나무 - 줄진",
		["사나없찐 - 아즈샤라"] = "사나없찐 - 아즈샤라",
		["샤이아리 - 아즈샤라"] = "샤이아리 - 아즈샤라",
		["Wilier - 아즈샤라"] = "Wilier - 아즈샤라",
		["Lezyne - 아즈샤라"] = "Lezyne - 아즈샤라",
		["데로사 - 아즈샤라"] = "데로사 - 아즈샤라",
		["환상큰손 - 아즈샤라"] = "환상큰손 - 아즈샤라",
		["샤말밀레 - 아즈샤라"] = "샤말밀레 - 아즈샤라",
		["재연마사 - 줄진"] = "재연마사 - 줄진",
		["Vlaanderen - 아즈샤라"] = "Vlaanderen - 아즈샤라",
		["징박무시하나연 - 줄진"] = "징박무시하나연 - 줄진",
		["Santini - 아즈샤라"] = "Santini - 아즈샤라",
		["써벨로 - 아즈샤라"] = "써벨로 - 아즈샤라",
		["리떼 - 아즈샤라"] = "리떼 - 아즈샤라",
		["Emonda - 줄진"] = "Emonda - 줄진",
		["뽕나인 - 줄진"] = "뽕나인 - 줄진",
		["Emonda - 아즈샤라"] = "Emonda - 아즈샤라",
		["영석썼숑 - 줄진"] = "영석썼숑 - 줄진",
		["사나정연 - 아즈샤라"] = "사나정연 - 아즈샤라",
	},
	["global"] = {
		["Guilds"] = {
			["Default.줄진.뽕뽕뿡뽕뽕뿡뽕뽕뿡"] = {
				["faction"] = "Alliance",
			},
			["Default.아즈샤라.이러려고 길드 만들었나 자괴감 들고 괴로워"] = {
				["faction"] = "Horde",
			},
			["Default.아즈샤라.너도한방 나도한방 헬조선 흙수저들의 죽창군단"] = {
				["faction"] = "Horde",
			},
			["Default.아즈샤라.실수할 수도 있지"] = {
				["faction"] = "Horde",
			},
		},
		["Version"] = 1,
		["Characters"] = {
			["Default.줄진.뽕나인"] = {
				["faction"] = "Alliance",
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
			},
			["Default.줄진.북경스딸"] = {
				["faction"] = "Alliance",
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
			},
			["Default.줄진.영석썼숑"] = {
				["faction"] = "Alliance",
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
			},
			["Default.아즈샤라.데로사"] = {
				["faction"] = "Horde",
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
			},
			["Default.아즈샤라.사나정연"] = {
				["faction"] = "Horde",
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
			},
			["Default.줄진.뚱빼미나무"] = {
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
				["faction"] = "Alliance",
			},
			["Default.아즈샤라.Wilier"] = {
				["faction"] = "Horde",
			},
			["Default.줄진.최저방벽"] = {
				["faction"] = "Alliance",
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
			},
			["Default.줄진.Emonda"] = {
				["faction"] = "Alliance",
			},
			["Default.아즈샤라.Emonda"] = {
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
				["faction"] = "Horde",
			},
			["Default.아즈샤라.리떼"] = {
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
				["faction"] = "Horde",
			},
			["Default.아즈샤라.아르니타"] = {
				["faction"] = "Horde",
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
			},
			["Default.줄진.안녕김밥"] = {
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
				["faction"] = "Alliance",
			},
			["Default.아즈샤라.Vlaanderen"] = {
				["faction"] = "Horde",
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
			},
			["Default.줄진.꼬꼬마빠샤"] = {
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
				["faction"] = "Alliance",
			},
			["Default.줄진.클리엔"] = {
				["faction"] = "Alliance",
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
			},
			["Default.아즈샤라.샤이아리"] = {
				["faction"] = "Horde",
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
			},
			["Default.줄진.재연마사"] = {
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
				["faction"] = "Alliance",
			},
			["Default.아즈샤라.샤말밀레"] = {
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
				["faction"] = "Horde",
			},
			["Default.줄진.징박무시하나연"] = {
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
				["faction"] = "Alliance",
			},
			["Default.아즈샤라.Santini"] = {
				["faction"] = "Horde",
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
			},
			["Default.아즈샤라.사나없찐"] = {
				["faction"] = "Horde",
			},
			["Default.아즈샤라.Lezyne"] = {
				["faction"] = "Horde",
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
			},
			["Default.아즈샤라.릿데"] = {
				["guildName"] = "이러려고 길드 만들었나 자괴감 들고 괴로워",
				["faction"] = "Horde",
			},
			["Default.줄진.샤이아리"] = {
				["guildName"] = "뽕뽕뿡뽕뽕뿡뽕뽕뿡",
				["faction"] = "Alliance",
			},
			["Default.아즈샤라.사나무엇"] = {
				["faction"] = "Neutral",
			},
		},
		["ShortToLongRealmNames"] = {
			["줄진"] = "줄진",
			["아즈샤라"] = "아즈샤라",
		},
	},
}
