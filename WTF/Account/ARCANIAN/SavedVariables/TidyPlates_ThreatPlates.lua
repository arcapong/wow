
ThreatPlatesDB = {
	["char"] = {
		["리떼 - 아즈샤라"] = {
			["welcome"] = true,
			["spec"] = {
				[3] = false,
			},
		},
		["Emonda - 아즈샤라"] = {
			["welcome"] = true,
			["spec"] = {
				[3] = false,
			},
		},
	},
	["global"] = {
		["version"] = "8.2.2",
	},
	["profileKeys"] = {
		["리떼 - 아즈샤라"] = "Default",
		["Emonda - 아즈샤라"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["nameplate"] = {
				["toggle"] = {
					["Totem"] = false,
				},
			},
			["uniqueSettings"] = {
				[33] = {
				},
				[34] = {
				},
				[35] = {
				},
				[36] = {
				},
				[37] = {
				},
				[38] = {
				},
				[39] = {
				},
				[40] = {
				},
				[41] = {
				},
				[42] = {
				},
				[43] = {
				},
				[44] = {
				},
				[45] = {
				},
				[46] = {
				},
				[48] = {
				},
				[49] = {
				},
				[50] = {
				},
				["list"] = {
					"Shadow Fiend", -- [1]
					"Spirit Wolf", -- [2]
					"Ebon Gargoyle", -- [3]
					"Water Elemental", -- [4]
					"Treant", -- [5]
					"Viper", -- [6]
					"Venomous Snake", -- [7]
					"Army of the Dead Ghoul", -- [8]
					"Shadowy Apparition", -- [9]
					"Shambling Horror", -- [10]
					"Web Wrap", -- [11]
					"Immortal Guardian", -- [12]
					"Marked Immortal Guardian", -- [13]
					"Empowered Adherent", -- [14]
					"Deformed Fanatic", -- [15]
					"Reanimated Adherent", -- [16]
					"Reanimated Fanatic", -- [17]
					"Bone Spike", -- [18]
					"Onyxian Whelp", -- [19]
					"Gas Cloud", -- [20]
					"Volatile Ooze", -- [21]
					"Darnavan", -- [22]
					"Val'kyr Shadowguard", -- [23]
					"Kinetic Bomb", -- [24]
					"Lich King", -- [25]
					"Raging Spirit", -- [26]
					"Drudge Ghoul", -- [27]
					"Living Inferno", -- [28]
					"Living Ember", -- [29]
					"Fanged Pit Viper", -- [30]
					"Canal Crab", -- [31]
					"Muddy Crawfish", -- [32]
					"", -- [33]
					"", -- [34]
					"", -- [35]
					"", -- [36]
					"", -- [37]
					"", -- [38]
					"", -- [39]
					"", -- [40]
					"", -- [41]
					"", -- [42]
					"", -- [43]
					"", -- [44]
					"", -- [45]
					"", -- [46]
					"", -- [47]
					"", -- [48]
					"", -- [49]
					"", -- [50]
					"", -- [51]
					"", -- [52]
					"", -- [53]
					"", -- [54]
					"", -- [55]
					"", -- [56]
					"", -- [57]
					"", -- [58]
					"", -- [59]
					"", -- [60]
					"", -- [61]
					"", -- [62]
					"", -- [63]
					"", -- [64]
					"", -- [65]
					"", -- [66]
					"", -- [67]
					"", -- [68]
					"", -- [69]
					"", -- [70]
					"", -- [71]
					"", -- [72]
					"", -- [73]
					"", -- [74]
					"", -- [75]
					"", -- [76]
					"", -- [77]
					"", -- [78]
					"", -- [79]
					"", -- [80]
				},
			},
			["cache"] = {
			},
		},
	},
}
