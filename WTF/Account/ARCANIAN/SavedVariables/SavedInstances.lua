
SavedInstancesDB = {
	["DBVersion"] = 12,
	["Toons"] = {
		["샤이아리 - 줄진"] = {
			["lastbossyell"] = "배반자 멘누: 일반",
			["IL"] = 698.1875,
			["Zone"] = "수호자의 전당",
			["Order"] = 50,
			["Class"] = "MAGE",
			["currency"] = {
				[777] = {
					["amount"] = 41847,
				},
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 6796,
				},
				[81] = {
					["amount"] = 31,
				},
				[738] = {
					["amount"] = 57,
				},
				[416] = {
					["amount"] = 25,
				},
				[697] = {
					["totalMax"] = 20,
					["amount"] = 16,
				},
				[515] = {
					["amount"] = 5,
				},
				[776] = {
					["totalMax"] = 20,
					["amount"] = 8,
				},
				[823] = {
					["amount"] = 14520,
				},
				[391] = {
					["amount"] = 12,
				},
				[752] = {
					["totalMax"] = 20,
					["amount"] = 0,
				},
				[994] = {
					["totalMax"] = 20,
					["amount"] = 10,
				},
				[241] = {
					["amount"] = 30,
				},
				[402] = {
					["amount"] = 2,
				},
			},
			["Level"] = 100,
			["LClass"] = "마법사",
			["RBGrating"] = 0,
			["lastbosstime"] = 1471192717,
			["PlayedTotal"] = 8128925,
			["lastbossyelltime"] = 1471192717,
			["Money"] = 9465097,
			["Race"] = "인간",
			["DailyResetTime"] = 1513637999,
			["lastboss"] = "배반자 멘누: 일반",
			["Show"] = "saved",
			["WeeklyResetTime"] = 1513810799,
			["PlayedLevel"] = 668735,
			["Faction"] = "Alliance",
			["ILe"] = 698.1875,
			["Skills"] = {
			},
			["Quests"] = {
			},
			["LastSeen"] = 1493734054,
		},
		["언제나운무 - 아즈샤라"] = {
			["IL"] = 0.25,
			["Zone"] = "유랑도",
			["Order"] = 50,
			["Class"] = "MONK",
			["currency"] = {
			},
			["Level"] = 1,
			["LClass"] = "수도사",
			["RBGrating"] = 0,
			["PlayedTotal"] = 29,
			["Money"] = 0,
			["WeeklyResetTime"] = 1513810799,
			["LastSeen"] = 1488375653,
			["Show"] = "saved",
			["DailyResetTime"] = 1513637999,
			["PlayedLevel"] = 29,
			["Faction"] = "Neutral",
			["ILe"] = 0.25,
			["Skills"] = {
			},
			["Quests"] = {
			},
			["Race"] = "판다렌 ()",
		},
		["최저방벽 - 줄진"] = {
			["IL"] = 653.8125,
			["Zone"] = "스톰윈드",
			["Order"] = 50,
			["Class"] = "WARRIOR",
			["currency"] = {
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 5942,
				},
				[777] = {
					["amount"] = 24966,
				},
				[994] = {
					["totalMax"] = 20,
					["amount"] = 10,
				},
				[823] = {
					["amount"] = 9083,
				},
				[776] = {
					["totalMax"] = 20,
					["amount"] = 10,
				},
				[752] = {
					["totalMax"] = 20,
					["amount"] = 0,
				},
				[738] = {
					["amount"] = 17,
				},
				[402] = {
					["amount"] = 0,
				},
			},
			["Level"] = 100,
			["LClass"] = "전사",
			["RBGrating"] = 0,
			["PlayedTotal"] = 823747,
			["Money"] = 3809529,
			["LastSeen"] = 1494291901,
			["WeeklyResetTime"] = 1513810799,
			["Show"] = "saved",
			["Skills"] = {
			},
			["PlayedLevel"] = 53256,
			["Faction"] = "Alliance",
			["ILe"] = 649.4375,
			["DailyResetTime"] = 1513637999,
			["Quests"] = {
			},
			["Race"] = "인간",
		},
		["환상큰소 - 아즈샤라"] = {
			["lastbossyell"] = "무역왕 갤리윅스",
			["IL"] = 29.375,
			["LastSeen"] = 1472276725,
			["Order"] = 50,
			["Class"] = "DRUID",
			["currency"] = {
				[1226] = {
					["totalMax"] = 2000,
					["amount"] = 52,
				},
			},
			["Level"] = 35,
			["LClass"] = "드루이드",
			["RBGrating"] = 0,
			["lastbosstime"] = 1470660698,
			["PlayedTotal"] = 21002,
			["lastbossyelltime"] = 1472276715,
			["Money"] = 113595,
			["Race"] = "타우렌",
			["DailyResetTime"] = 1513637999,
			["lastboss"] = "아쿠마이: 일반",
			["Show"] = "saved",
			["Zone"] = "오그리마",
			["PlayedLevel"] = 533,
			["Faction"] = "Horde",
			["ILe"] = 29.375,
			["Skills"] = {
			},
			["Quests"] = {
			},
			["WeeklyResetTime"] = 1513810799,
		},
		["북경스딸 - 줄진"] = {
			["lastbossyell"] = "아즈카 블레이드퓨리",
			["IL"] = 591.25,
			["Zone"] = "스톰윈드",
			["Order"] = 50,
			["Class"] = "MONK",
			["currency"] = {
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 2848,
				},
				[823] = {
					["amount"] = 72,
				},
			},
			["Level"] = 100,
			["LClass"] = "수도사",
			["RBGrating"] = 0,
			["lastbosstime"] = 1470654550,
			["LastSeen"] = 1478598279,
			["lastbossyelltime"] = 1470657928,
			["Money"] = 10003708,
			["WeeklyResetTime"] = 1513810799,
			["Skills"] = {
			},
			["Show"] = "saved",
			["lastboss"] = "대현자 비릭스: 일반",
			["Race"] = "판다렌 (얼라이언스)",
			["PlayedLevel"] = 8080,
			["Faction"] = "Alliance",
			["ILe"] = 585.6875,
			["DailyResetTime"] = 1513637999,
			["Quests"] = {
			},
			["PlayedTotal"] = 227882,
		},
		["클리엔 - 줄진"] = {
			["lastbossyell"] = "신왕 스코발드",
			["IL"] = 702.6875,
			["Zone"] = "달라란",
			["Order"] = 50,
			["Class"] = "PRIEST",
			["currency"] = {
				[402] = {
					["amount"] = 25,
				},
				[697] = {
					["totalMax"] = 20,
					["amount"] = 15,
				},
				[776] = {
					["totalMax"] = 20,
					["amount"] = 8,
				},
				[823] = {
					["amount"] = 48242,
				},
				[994] = {
					["totalMax"] = 20,
					["amount"] = 9,
				},
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 8650,
				},
				[515] = {
					["amount"] = 20,
				},
				[1220] = {
					["amount"] = 483,
				},
				[391] = {
					["amount"] = 31,
				},
				[1129] = {
					["weeklyMax"] = 3,
					["totalMax"] = 20,
					["amount"] = 1,
				},
				[752] = {
					["totalMax"] = 20,
					["amount"] = 0,
				},
				[1101] = {
					["totalMax"] = 100000,
					["amount"] = 2020,
				},
				[738] = {
					["amount"] = 27,
				},
				[416] = {
					["amount"] = 53,
				},
				[241] = {
					["amount"] = 15,
				},
				[81] = {
					["amount"] = 8,
				},
				[777] = {
					["amount"] = 19287,
				},
			},
			["Level"] = 101,
			["LClass"] = "사제",
			["RBGrating"] = 0,
			["PlayedTotal"] = 6772400,
			["lastbossyelltime"] = 1474185880,
			["Money"] = 10000118,
			["WeeklyResetTime"] = 1513810799,
			["Race"] = "인간",
			["Show"] = "saved",
			["DailyResetTime"] = 1513637999,
			["PlayedLevel"] = 1741,
			["Faction"] = "Alliance",
			["ILe"] = 702.6875,
			["Skills"] = {
			},
			["Quests"] = {
			},
			["LastSeen"] = 1493734122,
		},
		["사나무엇 - 아즈샤라"] = {
			["lastbossyell"] = "민 딤윈드",
			["IL"] = 6,
			["Zone"] = "유랑도",
			["Order"] = 50,
			["Class"] = "MONK",
			["currency"] = {
			},
			["Level"] = 4,
			["LClass"] = "수도사",
			["RBGrating"] = 0,
			["MythicKeyBest"] = {
				["level"] = 0,
				["ResetTime"] = 1513810799,
				["WeeklyReward"] = false,
			},
			["PlayedTotal"] = 1094,
			["MythicKey"] = {
			},
			["lastbossyelltime"] = 1513311983,
			["Money"] = 525,
			["DailyWorldQuest"] = {
			},
			["DailyResetTime"] = 1513637999,
			["LastSeen"] = 1513424632,
			["Show"] = "saved",
			["WeeklyResetTime"] = 1513810799,
			["PlayedLevel"] = 341,
			["Faction"] = "Neutral",
			["ILe"] = 6,
			["Skills"] = {
			},
			["Quests"] = {
			},
			["Race"] = "판다렌 ()",
		},
		["아르니타 - 아즈샤라"] = {
			["lastbossyell"] = "군주 칼고라스: 일반 시나리오",
			["IL"] = 840.4375,
			["Zone"] = "달라란",
			["Order"] = 50,
			["Class"] = "ROGUE",
			["currency"] = {
				[402] = {
					["amount"] = 1,
				},
				[697] = {
					["totalMax"] = 20,
					["amount"] = 18,
				},
				[776] = {
					["totalMax"] = 20,
					["amount"] = 7,
				},
				[823] = {
					["amount"] = 11499,
				},
				[994] = {
					["totalMax"] = 20,
					["amount"] = 4,
				},
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 81,
				},
				[515] = {
					["amount"] = 15,
				},
				[1155] = {
					["totalMax"] = 300,
					["amount"] = 4,
				},
				[1220] = {
					["amount"] = 25,
				},
				[391] = {
					["amount"] = 6,
				},
				[752] = {
					["totalMax"] = 20,
					["amount"] = 0,
				},
				[777] = {
					["amount"] = 31424,
				},
				[416] = {
					["amount"] = 16,
				},
				[738] = {
					["amount"] = 18,
				},
				[1166] = {
					["amount"] = 155,
				},
				[1273] = {
					["weeklyMax"] = 3,
					["totalMax"] = 6,
					["amount"] = 6,
				},
				[81] = {
					["amount"] = 14,
				},
				[241] = {
					["amount"] = 15,
				},
			},
			["Level"] = 110,
			["LClass"] = "도적",
			["RBGrating"] = 0,
			["LastSeen"] = 1505654238,
			["PlayedTotal"] = 4278253,
			["lastbosstime"] = 1484196952,
			["lastbossyelltime"] = 1492435546,
			["Money"] = 31571358,
			["DailyResetTime"] = 1513637999,
			["Race"] = "블러드 엘프",
			["Show"] = "saved",
			["lastboss"] = "샤르토스",
			["BonusRoll"] = {
				{
					["time"] = 1484196960,
					["name"] = "샤르토스",
					["item"] = "|cffa335ee|Hitem:141487::::::::110:260::3:3:3466:1477:3336:::|h[깨어나는 악몽의 의복]|h|r",
					["currencyID"] = 1273,
				}, -- [1]
				{
					["money"] = 309308,
					["name"] = "바다떠돌이",
					["time"] = 1483591350,
					["currencyID"] = 1273,
				}, -- [2]
				{
					["money"] = 275151,
					["time"] = 1482983513,
					["name"] = "냉혈의 드루곤",
					["currencyID"] = 1273,
				}, -- [3]
				{
					["item"] = "|cffa335ee|Hitem:141430::::::::110:260::3:3:3466:1477:3336:::|h[마나에 그슬린 덧신]|h|r",
					["time"] = 1482381559,
					["name"] = "칼라미르",
					["currencyID"] = 1273,
				}, -- [4]
				{
					["money"] = 255934,
					["name"] = "니소그",
					["time"] = 1481020845,
					["currencyID"] = 1273,
				}, -- [5]
				{
					["money"] = 343290,
					["name"] = "마귀 나자크",
					["time"] = 1481020031,
					["currencyID"] = 1273,
				}, -- [6]
			},
			["PlayedLevel"] = 16957,
			["Faction"] = "Horde",
			["ILe"] = 835.5625,
			["Skills"] = {
			},
			["Quests"] = {
			},
			["WeeklyResetTime"] = 1513810799,
		},
		["릿데 - 아즈샤라"] = {
			["lastbossyell"] = "마틸다 스콥티도티어",
			["IL"] = 891.875,
			["Zone"] = "하늘보루",
			["Order"] = 50,
			["Class"] = "WARRIOR",
			["Race"] = "판다렌 (호드)",
			["currency"] = {
				[1220] = {
					["amount"] = 4063,
				},
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 6317,
				},
				[1273] = {
					["weeklyMax"] = 3,
					["totalMax"] = 6,
					["amount"] = 4,
				},
				[738] = {
					["amount"] = 17,
				},
				[777] = {
					["amount"] = 24966,
				},
				[402] = {
					["amount"] = 0,
				},
				[1342] = {
					["totalMax"] = 1000,
					["amount"] = 27,
				},
				[776] = {
					["totalMax"] = 20,
					["amount"] = 10,
				},
				[823] = {
					["amount"] = 9683,
				},
				[1226] = {
					["amount"] = 4258,
				},
				[752] = {
					["totalMax"] = 20,
					["amount"] = 0,
				},
				[994] = {
					["totalMax"] = 20,
					["amount"] = 10,
				},
				[1275] = {
					["amount"] = 3,
				},
				[1155] = {
					["totalMax"] = 500,
					["amount"] = 100,
				},
			},
			["DailyWorldQuest"] = {
				["days0"] = {
					["questneed"] = 4,
					["name"] = "높은산 부족들",
					["isfinish"] = false,
					["iscompleted"] = false,
					["dayleft"] = 0,
					["questdone"] = 0,
				},
			},
			["DailyResetTime"] = 1513637999,
			["Level"] = 110,
			["LClass"] = "전사",
			["RBGrating"] = 0,
			["MythicKeyBest"] = {
				["level"] = 0,
				["ResetTime"] = 1513810799,
				["WeeklyReward"] = false,
			},
			["PlayedTotal"] = 928765,
			["LastSeen"] = 1513309870,
			["lastbossyelltime"] = 1513309862,
			["Money"] = 162981023,
			["MythicKey"] = {
			},
			["lastbosstime"] = 1497501146,
			["Show"] = "saved",
			["lastboss"] = "말리피쿠스",
			["BonusRoll"] = {
				{
					["name"] = "말리피쿠스",
					["item"] = "|cffa335ee|Hitem:147750::::::::110:72::3:3:3572:1497:3337:::|h[악의적인 광신도의 다리갑옷]|h|r",
					["time"] = 1497501151,
					["currencyID"] = 1273,
				}, -- [1]
				{
					["name"] = "브루탈루스",
					["time"] = 1497051042,
					["currencyID"] = 1273,
				}, -- [2]
				{
					["time"] = 1496897201,
					["name"] = "칼라미르",
					["currencyID"] = 1273,
				}, -- [3]
				{
					["time"] = 1496291557,
					["name"] = "아포크론",
					["currencyID"] = 1273,
				}, -- [4]
				{
					["item"] = "|cffa335ee|Hitem:141492::::::::110:72::3:4:1808:3466:1472:3528:::|h[때묻은 수라마르 상업소 인장]|h|r",
					["time"] = 1496291203,
					["name"] = "메마른 짐",
					["currencyID"] = 1273,
				}, -- [5]
				{
					["name"] = "아포크론",
					["time"] = 1496147991,
					["currencyID"] = 1273,
				}, -- [6]
				{
					["time"] = 1495288588,
					["name"] = "브루탈루스",
					["currencyID"] = 1273,
				}, -- [7]
			},
			["PlayedLevel"] = 54785,
			["Faction"] = "Horde",
			["ILe"] = 884.625,
			["Skills"] = {
			},
			["Quests"] = {
			},
			["WeeklyResetTime"] = 1513810799,
		},
		["Vlaanderen - 아즈샤라"] = {
			["lastbossyell"] = "굴단: 영웅",
			["IL"] = 895.4375,
			["Zone"] = "달라란",
			["Order"] = 50,
			["Class"] = "HUNTER",
			["currency"] = {
				[1226] = {
					["amount"] = 1575,
				},
				[1314] = {
					["weeklyMax"] = 20,
					["totalMax"] = 40,
					["amount"] = 8,
				},
				[1220] = {
					["amount"] = 1558,
				},
				[1273] = {
					["weeklyMax"] = 3,
					["totalMax"] = 6,
					["amount"] = 5,
				},
				[1275] = {
					["amount"] = 20,
				},
				[1155] = {
					["totalMax"] = 1100,
					["amount"] = 550,
				},
				[1166] = {
					["amount"] = 1520,
				},
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 100,
				},
			},
			["Level"] = 110,
			["LClass"] = "사냥꾼",
			["RBGrating"] = 0,
			["LastSeen"] = 1505652805,
			["lastbosstime"] = 1491733800,
			["WeeklyResetTime"] = 1513810799,
			["lastbossyelltime"] = 1491733795,
			["Money"] = 47691715,
			["DailyResetTime"] = 1513637999,
			["PlayedTotal"] = 477385,
			["lastboss"] = "굴단: 영웅",
			["Show"] = "saved",
			["BonusRoll"] = {
				{
					["name"] = "굴단: 영웅",
					["time"] = 1491733826,
					["currencyID"] = 1273,
				}, -- [1]
				{
					["name"] = "고위 식물학자 텔아른: 영웅",
					["time"] = 1491732065,
					["currencyID"] = 1273,
				}, -- [2]
				{
					["name"] = "마법검사 알루리엘: 영웅",
					["time"] = 1491730819,
					["currencyID"] = 1273,
				}, -- [3]
				{
					["time"] = 1491729188,
					["name"] = "스코르파이론: 영웅",
					["currencyID"] = 1273,
				}, -- [4]
				{
					["time"] = 1491728547,
					["name"] = "대마법학자 엘리산드: 영웅",
					["currencyID"] = 1273,
				}, -- [5]
				{
					["item"] = "|cffa335ee|Hitem:140841::::::::110:253::5:3:3516:1487:1813:::|h[천국의 폭풍우]|h|r",
					["time"] = 1489411296,
					["name"] = "별 점술가 에트레우스: 영웅",
					["currencyID"] = 1273,
				}, -- [6]
				{
					["time"] = 1489406746,
					["name"] = "스코르파이론: 영웅",
					["currencyID"] = 1273,
				}, -- [7]
				{
					["time"] = 1489406072,
					["name"] = "굴단: 영웅",
					["currencyID"] = 1273,
				}, -- [8]
				{
					["time"] = 1489404520,
					["name"] = "대마법학자 엘리산드: 영웅",
					["currencyID"] = 1273,
				}, -- [9]
				{
					["name"] = "크로서스: 일반",
					["time"] = 1489220150,
					["currencyID"] = 1273,
				}, -- [10]
				{
					["name"] = "대마법학자 엘리산드: 일반",
					["time"] = 1489216959,
					["currencyID"] = 1273,
				}, -- [11]
				{
					["time"] = 1489162993,
					["name"] = "우르속: 영웅",
					["currencyID"] = 1273,
				}, -- [12]
				{
					["item"] = "|cffa335ee|Hitem:139261::::::::110:253::5:2:1805:1487:::|h[무쇠가죽 뭉치]|h|r",
					["time"] = 1488980985,
					["name"] = "우르속: 영웅",
					["currencyID"] = 1273,
				}, -- [13]
				{
					["name"] = "대마법학자 엘리산드: 일반",
					["time"] = 1488703729,
					["currencyID"] = 1273,
				}, -- [14]
				{
					["time"] = 1488355363,
					["name"] = "트릴리악스: 영웅",
					["currencyID"] = 1273,
				}, -- [15]
				{
					["time"] = 1488354220,
					["name"] = "스코르파이론: 영웅",
					["currencyID"] = 1273,
				}, -- [16]
				{
					["time"] = 1487509495,
					["name"] = "굴단: 일반",
					["currencyID"] = 1273,
				}, -- [17]
				{
					["time"] = 1487508917,
					["name"] = "대마법학자 엘리산드: 일반",
					["currencyID"] = 1273,
				}, -- [18]
				{
					["time"] = 1487508255,
					["name"] = "크로서스: 일반",
					["currencyID"] = 1273,
				}, -- [19]
				{
					["name"] = "굴단: 일반",
					["item"] = "|cffa335ee|Hitem:138344::::::::110:253::3:3:3514:1472:1813:::|h[독수리발톱 다리사슬]|h|r",
					["time"] = 1486754286,
					["currencyID"] = 1273,
				}, -- [20]
				{
					["name"] = "대마법학자 엘리산드: 일반",
					["time"] = 1486753540,
					["currencyID"] = 1273,
				}, -- [21]
				{
					["name"] = "크로서스: 일반",
					["time"] = 1486750963,
					["currencyID"] = 1273,
				}, -- [22]
				{
					["name"] = "굴단: 일반",
					["time"] = 1486484221,
					["currencyID"] = 1273,
				}, -- [23]
				{
					["name"] = "대마법학자 엘리산드: 일반",
					["time"] = 1486482943,
					["currencyID"] = 1273,
				}, -- [24]
				{
					["name"] = "별 점술가 에트레우스: 일반",
					["item"] = "|cffa335ee|Hitem:140900::::::::110:253::3:3:3514:1472:1813:::|h[별빛 점술가의 브로치]|h|r",
					["time"] = 1486481977,
					["currencyID"] = 1273,
				}, -- [25]
			},
			["PlayedLevel"] = 313140,
			["Faction"] = "Horde",
			["ILe"] = 895.4375,
			["Skills"] = {
			},
			["Quests"] = {
			},
			["Race"] = "블러드 엘프",
		},
		["뚱빼미나무 - 줄진"] = {
			["IL"] = 661.5625,
			["Zone"] = "달빛내림 터",
			["Order"] = 50,
			["Class"] = "DRUID",
			["currency"] = {
				[777] = {
					["amount"] = 13007,
				},
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 3556,
				},
				[738] = {
					["amount"] = 40,
				},
				[416] = {
					["amount"] = 15,
				},
				[402] = {
					["amount"] = 1,
				},
				[776] = {
					["totalMax"] = 20,
					["amount"] = 3,
				},
				[823] = {
					["amount"] = 11375,
				},
				[241] = {
					["amount"] = 18,
				},
				[752] = {
					["totalMax"] = 20,
					["amount"] = 0,
				},
				[994] = {
					["totalMax"] = 20,
					["amount"] = 10,
				},
			},
			["Level"] = 100,
			["LClass"] = "드루이드",
			["RBGrating"] = 0,
			["PlayedTotal"] = 1669063,
			["Money"] = 5793900,
			["WeeklyResetTime"] = 1513810799,
			["Race"] = "나이트 엘프",
			["Show"] = "saved",
			["DailyResetTime"] = 1513637999,
			["PlayedLevel"] = 97607,
			["Faction"] = "Alliance",
			["ILe"] = 661.5625,
			["Skills"] = {
			},
			["Quests"] = {
			},
			["LastSeen"] = 1471189730,
		},
		["사나없찐 - 아즈샤라"] = {
			["IL"] = 6.75,
			["Zone"] = "매날개 광장",
			["Order"] = 50,
			["Class"] = "MONK",
			["currency"] = {
			},
			["Level"] = 1,
			["LClass"] = "수도사",
			["RBGrating"] = 0,
			["PlayedTotal"] = 1243,
			["Money"] = 501,
			["WeeklyResetTime"] = 1513810799,
			["LastSeen"] = 1488376858,
			["Show"] = "saved",
			["Skills"] = {
			},
			["PlayedLevel"] = 1243,
			["Faction"] = "Horde",
			["ILe"] = 6.5625,
			["DailyResetTime"] = 1513637999,
			["Quests"] = {
			},
			["Race"] = "블러드 엘프",
		},
		["꼬꼬마빠샤 - 줄진"] = {
			["lastbossyell"] = "바라딘 경비병",
			["IL"] = 684.625,
			["Zone"] = "스톰윈드",
			["Order"] = 50,
			["Class"] = "ROGUE",
			["currency"] = {
				[777] = {
					["amount"] = 31424,
				},
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 4811,
				},
				[402] = {
					["amount"] = 1,
				},
				[738] = {
					["amount"] = 18,
				},
				[515] = {
					["amount"] = 15,
				},
				[697] = {
					["totalMax"] = 20,
					["amount"] = 18,
				},
				[391] = {
					["amount"] = 6,
				},
				[776] = {
					["totalMax"] = 20,
					["amount"] = 7,
				},
				[823] = {
					["amount"] = 11460,
				},
				[241] = {
					["amount"] = 15,
				},
				[752] = {
					["totalMax"] = 20,
					["amount"] = 0,
				},
				[994] = {
					["totalMax"] = 20,
					["amount"] = 4,
				},
				[416] = {
					["amount"] = 16,
				},
				[81] = {
					["amount"] = 14,
				},
			},
			["Level"] = 100,
			["LClass"] = "도적",
			["RBGrating"] = 0,
			["PlayedTotal"] = 4202020,
			["lastbossyelltime"] = 1470223611,
			["Money"] = 7608684,
			["WeeklyResetTime"] = 1513810799,
			["Race"] = "노움",
			["Show"] = "saved",
			["DailyResetTime"] = 1513637999,
			["PlayedLevel"] = 256170,
			["Faction"] = "Alliance",
			["ILe"] = 684.625,
			["Skills"] = {
			},
			["Quests"] = {
			},
			["LastSeen"] = 1480651862,
		},
		["Wilier - 아즈샤라"] = {
			["lastbossyell"] = "무역왕 갤리윅스",
			["IL"] = 19.375,
			["LastSeen"] = 1488810483,
			["Order"] = 50,
			["Class"] = "PALADIN",
			["currency"] = {
				[1226] = {
					["totalMax"] = 2000,
					["amount"] = 56,
				},
			},
			["Level"] = 26,
			["LClass"] = "성기사",
			["RBGrating"] = 0,
			["PlayedTotal"] = 9058,
			["lastbossyelltime"] = 1472277225,
			["Money"] = 11198,
			["Zone"] = "오그리마",
			["WeeklyResetTime"] = 1513810799,
			["Show"] = "saved",
			["Skills"] = {
			},
			["PlayedLevel"] = 62,
			["Faction"] = "Horde",
			["ILe"] = 19.375,
			["DailyResetTime"] = 1513637999,
			["Quests"] = {
			},
			["Race"] = "블러드 엘프",
		},
		["뽕나인 - 줄진"] = {
			["IL"] = 687.125,
			["Zone"] = "스톰윈드",
			["Order"] = 50,
			["Class"] = "SHAMAN",
			["currency"] = {
				[777] = {
					["amount"] = 29748,
				},
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 6402,
				},
				[738] = {
					["amount"] = 45,
				},
				[416] = {
					["amount"] = 15,
				},
				[402] = {
					["amount"] = 1,
				},
				[776] = {
					["totalMax"] = 20,
					["amount"] = 7,
				},
				[823] = {
					["amount"] = 11581,
				},
				[752] = {
					["totalMax"] = 20,
					["amount"] = 0,
				},
				[994] = {
					["totalMax"] = 20,
					["amount"] = 4,
				},
				[697] = {
					["totalMax"] = 20,
					["amount"] = 14,
				},
			},
			["Level"] = 100,
			["LClass"] = "주술사",
			["RBGrating"] = 0,
			["PlayedTotal"] = 2275825,
			["Money"] = 10002169,
			["LastSeen"] = 1478599584,
			["Race"] = "판다렌 (얼라이언스)",
			["Show"] = "saved",
			["Skills"] = {
			},
			["PlayedLevel"] = 433293,
			["Faction"] = "Alliance",
			["ILe"] = 686.375,
			["DailyResetTime"] = 1513637999,
			["Quests"] = {
			},
			["WeeklyResetTime"] = 1513810799,
		},
		["데로사 - 아즈샤라"] = {
			["lastbossyell"] = "로라무스 탈리페데스",
			["IL"] = 710.0625,
			["Zone"] = "스톰하임",
			["Order"] = 50,
			["Class"] = "DEMONHUNTER",
			["currency"] = {
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 1465,
				},
				[1220] = {
					["amount"] = 1722,
				},
				[823] = {
					["amount"] = 181,
				},
				[1166] = {
					["amount"] = 350,
				},
				[1226] = {
					["amount"] = 50,
				},
			},
			["Level"] = 102,
			["LClass"] = "악마사냥꾼",
			["RBGrating"] = 0,
			["PlayedTotal"] = 47329,
			["lastbossyelltime"] = 1475670200,
			["Money"] = 26396540,
			["LastSeen"] = 1494303196,
			["WeeklyResetTime"] = 1513810799,
			["Show"] = "saved",
			["DailyResetTime"] = 1513637999,
			["PlayedLevel"] = 6365,
			["Faction"] = "Horde",
			["ILe"] = 708.6875,
			["Skills"] = {
			},
			["Quests"] = {
			},
			["Race"] = "블러드 엘프",
		},
		["환상큰손 - 아즈샤라"] = {
			["lastbossyell"] = "환상큰손",
			["IL"] = 711.125,
			["Zone"] = "정조준 오두막",
			["Order"] = 50,
			["Class"] = "HUNTER",
			["currency"] = {
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 300,
				},
				[1220] = {
					["amount"] = 1159,
				},
				[1166] = {
					["amount"] = 225,
				},
				[1226] = {
					["totalMax"] = 2000,
					["amount"] = 168,
				},
			},
			["Level"] = 102,
			["LClass"] = "사냥꾼",
			["RBGrating"] = 0,
			["PlayedTotal"] = 121771,
			["lastbossyelltime"] = 1479698058,
			["Money"] = 94546990,
			["WeeklyResetTime"] = 1513810799,
			["LastSeen"] = 1479699985,
			["Show"] = "saved",
			["Skills"] = {
			},
			["PlayedLevel"] = 2674,
			["Faction"] = "Horde",
			["ILe"] = 711.125,
			["DailyResetTime"] = 1513637999,
			["Quests"] = {
			},
			["Race"] = "판다렌 (호드)",
		},
		["Emonda - 줄진"] = {
			["IL"] = 666.25,
			["Zone"] = "마르둠 - 산산조각난 심연",
			["Order"] = 50,
			["Class"] = "DEMONHUNTER",
			["currency"] = {
			},
			["Level"] = 99,
			["LClass"] = "악마사냥꾼",
			["RBGrating"] = 0,
			["PlayedTotal"] = 1987,
			["Money"] = 2790000,
			["LastSeen"] = 1471190158,
			["Race"] = "나이트 엘프",
			["Show"] = "saved",
			["DailyResetTime"] = 1513637999,
			["PlayedLevel"] = 142,
			["Faction"] = "Alliance",
			["ILe"] = 666.25,
			["Skills"] = {
			},
			["Quests"] = {
			},
			["WeeklyResetTime"] = 1513810799,
		},
		["재연마사 - 줄진"] = {
			["lastbossyell"] = "티콘드리우스",
			["IL"] = 717.0625,
			["Zone"] = "달라란",
			["Order"] = 50,
			["Class"] = "HUNTER",
			["currency"] = {
				[1220] = {
					["amount"] = 1089,
				},
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 1640,
				},
				[738] = {
					["amount"] = 31,
				},
				[416] = {
					["amount"] = 15,
				},
				[402] = {
					["amount"] = 1,
				},
				[776] = {
					["totalMax"] = 20,
					["amount"] = 3,
				},
				[823] = {
					["amount"] = 8549,
				},
				[391] = {
					["amount"] = 6,
				},
				[752] = {
					["totalMax"] = 20,
					["amount"] = 2,
				},
				[994] = {
					["totalMax"] = 20,
					["amount"] = 5,
				},
				[777] = {
					["amount"] = 9241,
				},
				[697] = {
					["totalMax"] = 20,
					["amount"] = 3,
				},
			},
			["Level"] = 103,
			["LClass"] = "사냥꾼",
			["RBGrating"] = 0,
			["PlayedTotal"] = 1038572,
			["lastbossyelltime"] = 1475911301,
			["Money"] = 10009752,
			["Race"] = "인간",
			["WeeklyResetTime"] = 1513810799,
			["Show"] = "saved",
			["DailyResetTime"] = 1513637999,
			["PlayedLevel"] = 3138,
			["Faction"] = "Alliance",
			["ILe"] = 716.9375,
			["Skills"] = {
			},
			["Quests"] = {
			},
			["LastSeen"] = 1478598652,
		},
		["안녕김밥 - 줄진"] = {
			["IL"] = 487,
			["Zone"] = "저주받은 땅",
			["Order"] = 50,
			["Class"] = "DEATHKNIGHT",
			["currency"] = {
			},
			["Level"] = 90,
			["LClass"] = "죽음의 기사",
			["RBGrating"] = 0,
			["PlayedTotal"] = 18662,
			["Money"] = 907255,
			["WeeklyResetTime"] = 1513810799,
			["Race"] = "인간",
			["Show"] = "saved",
			["DailyResetTime"] = 1513637999,
			["PlayedLevel"] = 3886,
			["Faction"] = "Alliance",
			["ILe"] = 483,
			["Skills"] = {
			},
			["Quests"] = {
			},
			["LastSeen"] = 1471190049,
		},
		["Emonda - 아즈샤라"] = {
			["lastbossyell"] = "의기양양한 파멸의 예언자",
			["IL"] = 909.625,
			["LastSeen"] = 1513310708,
			["Order"] = 50,
			["Class"] = "SHAMAN",
			["currency"] = {
				[1501] = {
					["amount"] = 8,
				},
				[1220] = {
					["amount"] = 34121,
				},
				[1314] = {
					["weeklyMax"] = 20,
					["totalMax"] = 40,
					["amount"] = 40,
				},
				[1273] = {
					["weeklyMax"] = 3,
					["totalMax"] = 6,
					["amount"] = 5,
				},
				[1101] = {
					["totalMax"] = 100000,
					["amount"] = 5,
				},
				[1191] = {
					["totalMax"] = 5000,
					["amount"] = 150,
				},
				[1166] = {
					["amount"] = 2895,
				},
				[1275] = {
					["amount"] = 62,
				},
				[1342] = {
					["totalMax"] = 1000,
					["amount"] = 25,
				},
				[1226] = {
					["amount"] = 2783,
				},
				[1155] = {
					["totalMax"] = 1500,
					["amount"] = 1212,
				},
				[823] = {
					["amount"] = 1355,
				},
				[1149] = {
					["totalMax"] = 5000,
					["amount"] = 687,
				},
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 38,
				},
			},
			["Race"] = "판다렌 (호드)",
			["Quests"] = {
			},
			["Skills"] = {
			},
			["Level"] = 110,
			["LClass"] = "주술사",
			["RBGrating"] = 0,
			["MythicKeyBest"] = {
				["level"] = 0,
				["ResetTime"] = 1513810799,
				["WeeklyReward"] = false,
			},
			["PlayedTotal"] = 1091394,
			["lastbosstime"] = 1504095316,
			["lastbossyelltime"] = 1504095079,
			["Money"] = 76609079,
			["Zone"] = "달라란",
			["MythicKey"] = {
			},
			["lastboss"] = "시바쉬",
			["Show"] = "saved",
			["BonusRoll"] = {
				{
					["item"] = "|cffa335ee|Hitem:147045::::::::110:264::4:4:3564:1808:1467:3528:::|h[단열 지느러미덧대]|h|r",
					["time"] = 1503812862,
					["name"] = "하르자탄: 공격대 찾기",
					["currencyID"] = 1273,
				}, -- [1]
				{
					["name"] = "달의 자매: 공격대 찾기",
					["item"] = "|cffa335ee|Hitem:147054::::::::110:264::4:4:3564:1808:1467:3528:::|h[저무는 광휘의 어깨덧옷]|h|r",
					["time"] = 1503755082,
					["currencyID"] = 1273,
				}, -- [2]
				{
					["name"] = "킬제덴: 공격대 찾기",
					["time"] = 1503754207,
					["currencyID"] = 1273,
				}, -- [3]
				{
					["name"] = "몰락한 화신: 공격대 찾기",
					["item"] = "|cffa335ee|Hitem:147107::::::::110:264::4:3:3564:1482:3337:::|h[용맹의 한 줄기 희망]|h|r",
					["time"] = 1503753423,
					["currencyID"] = 1273,
				}, -- [4]
				{
					["time"] = 1497499626,
					["name"] = "말리피쿠스",
					["currencyID"] = 1273,
				}, -- [5]
				{
					["name"] = "트릴리악스: 신화",
					["time"] = 1496658251,
					["currencyID"] = 1273,
				}, -- [6]
				{
					["name"] = "시간 변형체: 신화",
					["time"] = 1496655699,
					["currencyID"] = 1273,
				}, -- [7]
				{
					["name"] = "크로서스: 영웅",
					["time"] = 1496484254,
					["currencyID"] = 1273,
				}, -- [8]
				{
					["item"] = "|cffa335ee|Hitem:140803::::::::110:264::5:3:3516:1487:3528:::|h[에트레우스의 천상 지도]|h|r",
					["time"] = 1496483782,
					["name"] = "별 점술가 에트레우스: 영웅",
					["currencyID"] = 1273,
				}, -- [9]
				{
					["name"] = "마법검사 알루리엘: 영웅",
					["time"] = 1496481584,
					["currencyID"] = 1273,
				}, -- [10]
				{
					["name"] = "트릴리악스: 영웅",
					["time"] = 1496481092,
					["currencyID"] = 1273,
				}, -- [11]
				{
					["name"] = "굴단: 영웅",
					["time"] = 1496479863,
					["currencyID"] = 1273,
				}, -- [12]
				{
					["name"] = "아포크론",
					["time"] = 1496292112,
					["currencyID"] = 1273,
				}, -- [13]
				{
					["time"] = 1496148407,
					["name"] = "아포크론",
					["currencyID"] = 1273,
				}, -- [14]
				{
					["name"] = "트릴리악스: 신화",
					["time"] = 1495963057,
					["currencyID"] = 1273,
				}, -- [15]
				{
					["time"] = 1495962185,
					["name"] = "시간 변형체: 신화",
					["item"] = "|cffa335ee|Hitem:140872::::::::110:264::6:3:3445:1497:3528:::|h[뒤틀린 기억의 견갑]|h|r",
					["currencyID"] = 1273,
				}, -- [16]
				{
					["name"] = "고위 식물학자 텔아른: 영웅",
					["time"] = 1495117210,
					["currencyID"] = 1273,
				}, -- [17]
				{
					["item"] = "|cffa335ee|Hitem:140895::::::::110:264::5:4:3516:40:1487:3528:::|h[마법검사의 보석 박힌 인장]|h|r",
					["time"] = 1495116064,
					["name"] = "마법검사 알루리엘: 영웅",
					["currencyID"] = 1273,
				}, -- [18]
				{
					["name"] = "시간 변형체: 영웅",
					["time"] = 1495115253,
					["currencyID"] = 1273,
				}, -- [19]
				{
					["item"] = "|cffa335ee|Hitem:140809::::::::110:264::5:3:3517:1492:3528:::|h[어둠 속의 속삭임]|h|r",
					["time"] = 1495114395,
					["name"] = "굴단: 영웅",
					["currencyID"] = 1273,
				}, -- [20]
				{
					["name"] = "고위 식물학자 텔아른: 영웅",
					["time"] = 1494078469,
					["currencyID"] = 1273,
				}, -- [21]
				{
					["name"] = "별 점술가 에트레우스: 영웅",
					["time"] = 1494077847,
					["currencyID"] = 1273,
				}, -- [22]
				{
					["name"] = "마법검사 알루리엘: 영웅",
					["time"] = 1494076066,
					["currencyID"] = 1273,
				}, -- [23]
				{
					["time"] = 1494075562,
					["name"] = "트릴리악스: 영웅",
					["item"] = "|cffa335ee|Hitem:140838::::::::110:264::5:3:3444:1482:3528:::|h[만들어진 성격의 구체]|h|r",
					["currencyID"] = 1273,
				}, -- [24]
				{
					["name"] = "시간 변형체: 영웅",
					["time"] = 1494075077,
					["currencyID"] = 1273,
				}, -- [25]
			},
			["PlayedLevel"] = 973857,
			["Faction"] = "Horde",
			["ILe"] = 904.9375,
			["DailyResetTime"] = 1513637999,
			["DailyWorldQuest"] = {
				["days0"] = {
					["questneed"] = 4,
					["name"] = "높은산 부족들",
					["isfinish"] = false,
					["dayleft"] = 0,
					["iscompleted"] = false,
					["questdone"] = 0,
				},
			},
			["WeeklyResetTime"] = 1513810799,
		},
		["Santini - 아즈샤라"] = {
			["lastbossyell"] = "의기양양한 파멸의 예언자",
			["IL"] = 847.5625,
			["LastSeen"] = 1505653958,
			["Order"] = 50,
			["Class"] = "WARLOCK",
			["currency"] = {
				[1226] = {
					["amount"] = 650,
				},
				[1220] = {
					["amount"] = 5276,
				},
				[1273] = {
					["weeklyMax"] = 3,
					["totalMax"] = 6,
					["amount"] = 5,
				},
				[1275] = {
					["amount"] = 5,
				},
				[1155] = {
					["totalMax"] = 500,
					["amount"] = 364,
				},
				[1166] = {
					["amount"] = 860,
				},
				[1149] = {
					["totalMax"] = 5000,
					["amount"] = 10,
				},
			},
			["Level"] = 110,
			["LClass"] = "흑마법사",
			["RBGrating"] = 0,
			["Zone"] = "달라란",
			["PlayedTotal"] = 198008,
			["lastbosstime"] = 1490285729,
			["lastbossyelltime"] = 1505653936,
			["Money"] = 28971993,
			["Skills"] = {
			},
			["Race"] = "블러드 엘프",
			["Show"] = "saved",
			["lastboss"] = "휴몽그리스",
			["BonusRoll"] = {
				{
					["name"] = "휴몽그리스",
					["time"] = 1490285734,
					["currencyID"] = 1273,
				}, -- [1]
				{
					["name"] = "샤르토스",
					["time"] = 1490102031,
					["currencyID"] = 1273,
				}, -- [2]
				{
					["item"] = "|cffa335ee|Hitem:141523::::::::110:265::3:3:3466:1477:3336:::|h[지옥 냄새가 나는 미끼]|h|r",
					["time"] = 1486810160,
					["name"] = "레반투스",
					["currencyID"] = 1273,
				}, -- [3]
				{
					["time"] = 1484196474,
					["name"] = "샤르토스",
					["currencyID"] = 1273,
				}, -- [4]
				{
					["money"] = 478355,
					["time"] = 1483590479,
					["name"] = "바다떠돌이",
					["currencyID"] = 1273,
				}, -- [5]
				{
					["name"] = "냉혈의 드루곤",
					["item"] = "|cffa335ee|Hitem:141538::::::::110:265::3:2:3466:1472:::|h[거인의 손수건]|h|r",
					["time"] = 1482983134,
					["currencyID"] = 1273,
				}, -- [6]
				{
					["money"] = 271632,
					["time"] = 1482381074,
					["name"] = "칼라미르",
					["currencyID"] = 1273,
				}, -- [7]
				{
					["item"] = "|cffa335ee|Hitem:141488::::::::110:265::3:2:3466:1472:::|h[마나 탐지 반지]|h|r",
					["time"] = 1480998580,
					["name"] = "마귀 나자크",
					["currencyID"] = 1273,
				}, -- [8]
				{
					["item"] = "|cffa335ee|Hitem:141434::::::::110:267::3:2:3466:1472:::|h[보관된 영혼의 장식끈]|h|r",
					["time"] = 1479961194,
					["name"] = "영혼약탈자",
					["currencyID"] = 1273,
				}, -- [9]
				{
					["money"] = 272822,
					["name"] = "휴몽그리스",
					["time"] = 1479358282,
					["currencyID"] = 1273,
				}, -- [10]
				{
					["money"] = 299926,
					["name"] = "샤르토스",
					["time"] = 1478750724,
					["currencyID"] = 1273,
				}, -- [11]
				{
					["money"] = 294277,
					["time"] = 1478148087,
					["name"] = "바다떠돌이",
					["currencyID"] = 1273,
				}, -- [12]
				{
					["item"] = "|cffa335ee|Hitem:141428::::::::110:267::3:2:3466:1472:::|h[눈더미 팔보호구]|h|r",
					["time"] = 1477539960,
					["name"] = "냉혈의 드루곤",
					["currencyID"] = 1273,
				}, -- [13]
				{
					["money"] = 340973,
					["name"] = "칼라미르",
					["time"] = 1476939480,
					["currencyID"] = 1273,
				}, -- [14]
			},
			["PlayedLevel"] = 62269,
			["Faction"] = "Horde",
			["ILe"] = 845.3125,
			["DailyResetTime"] = 1513637999,
			["Quests"] = {
			},
			["WeeklyResetTime"] = 1513810799,
		},
		["징박무시하나연 - 줄진"] = {
			["IL"] = 689.3125,
			["Zone"] = "스톰윈드",
			["Order"] = 50,
			["Class"] = "PALADIN",
			["currency"] = {
				[777] = {
					["amount"] = 19456,
				},
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 7843,
				},
				[738] = {
					["amount"] = 28,
				},
				[515] = {
					["amount"] = 25,
				},
				[402] = {
					["amount"] = 1,
				},
				[776] = {
					["totalMax"] = 20,
					["amount"] = 4,
				},
				[823] = {
					["amount"] = 12812,
				},
				[391] = {
					["amount"] = 55,
				},
				[752] = {
					["totalMax"] = 20,
					["amount"] = 0,
				},
				[994] = {
					["totalMax"] = 20,
					["amount"] = 10,
				},
				[241] = {
					["amount"] = 9,
				},
				[697] = {
					["totalMax"] = 20,
					["amount"] = 10,
				},
			},
			["Level"] = 100,
			["LClass"] = "성기사",
			["RBGrating"] = 0,
			["PlayedTotal"] = 3845018,
			["Money"] = 5571749,
			["LastSeen"] = 1478600731,
			["WeeklyResetTime"] = 1513810799,
			["Show"] = "saved",
			["DailyResetTime"] = 1513637999,
			["PlayedLevel"] = 342749,
			["Faction"] = "Alliance",
			["ILe"] = 688.375,
			["Skills"] = {
			},
			["Quests"] = {
			},
			["Race"] = "인간",
		},
		["리떼 - 아즈샤라"] = {
			["lastbossyell"] = "일리단 스톰레이지",
			["IL"] = 930.125,
			["Zone"] = "크로쿠운",
			["Order"] = 50,
			["Class"] = "MAGE",
			["lastboss"] = "르우라: 영웅",
			["WeeklyResetTime"] = 1513810799,
			["currency"] = {
				[1501] = {
					["amount"] = 44,
				},
				[1220] = {
					["amount"] = 47055,
				},
				[1314] = {
					["weeklyMax"] = 20,
					["totalMax"] = 40,
					["amount"] = 26,
				},
				[1273] = {
					["weeklyMax"] = 3,
					["totalMax"] = 6,
					["amount"] = 6,
				},
				[1275] = {
					["amount"] = 65,
				},
				[1342] = {
					["totalMax"] = 1000,
					["amount"] = 72,
				},
				[1226] = {
					["amount"] = 12034,
				},
				[1155] = {
					["totalMax"] = 1700,
					["amount"] = 1700,
				},
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 300,
				},
				[1508] = {
					["totalMax"] = 2000,
					["amount"] = 283,
				},
				[1166] = {
					["amount"] = 1340,
				},
			},
			["LastSeen"] = 1513606734,
			["Quests"] = {
				[48910] = {
					["Expires"] = 1513810799,
					["Title"] = "크로쿠운 물자 조달",
					["Zone"] = "크로쿠운",
					["isDaily"] = true,
				},
				[49293] = {
					["Expires"] = 1513810799,
					["Title"] = "침공 공습",
					["Link"] = "|cffffff00|Hquest:49293:90|h[침공 공습]|h|r",
					["Zone"] = "크로쿠운",
					["isDaily"] = false,
				},
			},
			["Skills"] = {
			},
			["Level"] = 110,
			["LClass"] = "마법사",
			["RBGrating"] = 0,
			["MythicKey"] = {
			},
			["PlayedTotal"] = 810371,
			["MythicKeyBest"] = {
				["level"] = 0,
				["ResetTime"] = 1513810799,
				["WeeklyReward"] = false,
			},
			["lastbossyelltime"] = 1513606495,
			["Money"] = 20692698819,
			["lastbosstime"] = 1505448515,
			["Show"] = "saved",
			["BonusRoll"] = {
				{
					["name"] = "달의 자매: 영웅",
					["item"] = "|cffa335ee|Hitem:147021::::::::110:64::5:4:3562:42:1497:3528:::|h[야타이의 엄지 반지]|h|r",
					["time"] = 1504534634,
					["currencyID"] = 1273,
				}, -- [1]
				{
					["time"] = 1504533870,
					["name"] = "여군주 사스즈인: 영웅",
					["currencyID"] = 1273,
				}, -- [2]
				{
					["time"] = 1504533267,
					["name"] = "하르자탄: 영웅",
					["currencyID"] = 1273,
				}, -- [3]
				{
					["time"] = 1504532286,
					["name"] = "킬제덴: 영웅",
					["currencyID"] = 1273,
				}, -- [4]
				{
					["time"] = 1504529544,
					["name"] = "경계의 여신: 영웅",
					["currencyID"] = 1273,
				}, -- [5]
				{
					["name"] = "악마 심문관: 영웅",
					["item"] = "|cffa335ee|Hitem:146996::::::::110:64::5:3:3562:1502:3336:::|h[부서진 영혼의 어깨덧옷]|h|r",
					["time"] = 1504528870,
					["currencyID"] = 1273,
				}, -- [6]
				{
					["time"] = 1504447944,
					["name"] = "킬제덴: 공격대 찾기",
					["item"] = "|cffa335ee|Hitem:147195::::::::110:64::4:3:3564:1477:3528:::|h[두 번째 쌍두 정치의 인장]|h|r",
					["currencyID"] = 1273,
				}, -- [7]
				{
					["time"] = 1503737321,
					["name"] = "여군주 사스즈인: 공격대 찾기",
					["item"] = "|cffa335ee|Hitem:147016::::::::110:64::4:4:3564:1808:1467:3528:::|h[아래에서 덮치는 공포]|h|r",
					["currencyID"] = 1273,
				}, -- [8]
				{
					["name"] = "하르자탄: 공격대 찾기",
					["time"] = 1503736898,
					["currencyID"] = 1273,
				}, -- [9]
				{
					["name"] = "킬제덴: 공격대 찾기",
					["time"] = 1503736053,
					["currencyID"] = 1273,
				}, -- [10]
				{
					["name"] = "달의 자매: 공격대 찾기",
					["time"] = 1503734511,
					["currencyID"] = 1273,
				}, -- [11]
				{
					["time"] = 1503733950,
					["name"] = "악마 심문관: 공격대 찾기",
					["item"] = "|cffa335ee|Hitem:146996::::::::110:64::4:3:3564:1467:3528:::|h[부서진 영혼의 어깨덧옷]|h|r",
					["currencyID"] = 1273,
				}, -- [12]
				{
					["time"] = 1503733036,
					["name"] = "경계의 여신: 공격대 찾기",
					["item"] = "|cffa335ee|Hitem:147194::::::::110:64::4:3:3564:1467:3528:::|h[철회된 진실의 고리]|h|r",
					["currencyID"] = 1273,
				}, -- [13]
				{
					["item"] = "|cffa335ee|Hitem:147764::::::::110:64::3:3:3572:1482:3528:::|h[자라나는 불신의 망토]|h|r",
					["time"] = 1497500646,
					["name"] = "말리피쿠스",
					["currencyID"] = 1273,
				}, -- [14]
				{
					["time"] = 1497052356,
					["name"] = "브루탈루스",
					["currencyID"] = 1273,
				}, -- [15]
				{
					["time"] = 1496896765,
					["name"] = "칼라미르",
					["item"] = "|cffa335ee|Hitem:141438::::::::110:64::3:4:1808:3466:1472:3528:::|h[차가운 불꽃의 펜던트]|h|r",
					["currencyID"] = 1273,
				}, -- [16]
				{
					["time"] = 1496495777,
					["name"] = "굴단: 영웅",
					["currencyID"] = 1273,
				}, -- [17]
				{
					["name"] = "마법검사 알루리엘: 영웅",
					["time"] = 1496492775,
					["currencyID"] = 1273,
				}, -- [18]
				{
					["name"] = "시간 변형체: 영웅",
					["time"] = 1496491958,
					["currencyID"] = 1273,
				}, -- [19]
				{
					["name"] = "스코르파이론: 영웅",
					["time"] = 1496491589,
					["currencyID"] = 1273,
				}, -- [20]
				{
					["name"] = "아포크론",
					["time"] = 1496317281,
					["currencyID"] = 1273,
				}, -- [21]
				{
					["name"] = "별 점술가 에트레우스: 영웅",
					["time"] = 1496156367,
					["currencyID"] = 1273,
				}, -- [22]
				{
					["name"] = "마법검사 알루리엘: 영웅",
					["item"] = "|cffa335ee|Hitem:140895::::::::110:64::5:3:3516:1487:3528:::|h[마법검사의 보석 박힌 인장]|h|r",
					["time"] = 1496154861,
					["currencyID"] = 1273,
				}, -- [23]
				{
					["name"] = "시간 변형체: 영웅",
					["time"] = 1496154007,
					["currencyID"] = 1273,
				}, -- [24]
				{
					["name"] = "굴단: 영웅",
					["time"] = 1496153080,
					["currencyID"] = 1273,
				}, -- [25]
			},
			["PlayedLevel"] = 735872,
			["Faction"] = "Horde",
			["ILe"] = 920.4375,
			["DailyResetTime"] = 1513637999,
			["DailyWorldQuest"] = {
				["days2"] = {
					["questneed"] = 4,
					["name"] = "발라리아르",
					["iscompleted"] = false,
					["isfinish"] = false,
					["dayleft"] = 2,
					["questdone"] = 0,
				},
				["days1"] = {
					["questneed"] = 4,
					["name"] = "파론디스의 궁정",
					["iscompleted"] = false,
					["isfinish"] = false,
					["dayleft"] = 1,
					["questdone"] = 0,
				},
				["days0"] = {
					["questneed"] = 4,
					["name"] = "군단척결군",
					["iscompleted"] = false,
					["isfinish"] = false,
					["dayleft"] = 0,
					["questdone"] = 0,
				},
			},
			["Race"] = "블러드 엘프",
		},
		["샤말밀레 - 아즈샤라"] = {
			["lastbossyell"] = "바수니스트",
			["IL"] = 853.5,
			["LastSeen"] = 1513497781,
			["Order"] = 50,
			["Class"] = "PALADIN",
			["currency"] = {
				[402] = {
					["amount"] = 1,
				},
				[697] = {
					["totalMax"] = 20,
					["amount"] = 10,
				},
				[776] = {
					["totalMax"] = 20,
					["amount"] = 4,
				},
				[823] = {
					["amount"] = 12861,
				},
				[777] = {
					["amount"] = 19456,
				},
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 9,
				},
				[515] = {
					["amount"] = 25,
				},
				[1155] = {
					["totalMax"] = 600,
					["amount"] = 144,
				},
				[1220] = {
					["amount"] = 1348,
				},
				[391] = {
					["amount"] = 55,
				},
				[752] = {
					["totalMax"] = 20,
					["amount"] = 0,
				},
				[1273] = {
					["weeklyMax"] = 3,
					["totalMax"] = 6,
					["amount"] = 6,
				},
				[738] = {
					["amount"] = 28,
				},
				[1166] = {
					["amount"] = 250,
				},
				[1275] = {
					["amount"] = 4,
				},
				[241] = {
					["amount"] = 9,
				},
				[994] = {
					["totalMax"] = 20,
					["amount"] = 10,
				},
			},
			["Race"] = "블러드 엘프",
			["Quests"] = {
			},
			["Skills"] = {
			},
			["Level"] = 110,
			["LClass"] = "성기사",
			["RBGrating"] = 0,
			["MythicKeyBest"] = {
				["level"] = 0,
				["ResetTime"] = 1513810799,
				["WeeklyReward"] = false,
			},
			["lastbosstime"] = 1490179129,
			["PlayedTotal"] = 3984080,
			["lastbossyelltime"] = 1490178940,
			["Money"] = 27636834,
			["Zone"] = "달라란",
			["MythicKey"] = {
			},
			["lastboss"] = "샤르토스",
			["Show"] = "saved",
			["BonusRoll"] = {
				{
					["name"] = "샤르토스",
					["time"] = 1490179135,
					["currencyID"] = 1273,
				}, -- [1]
				{
					["time"] = 1488429435,
					["name"] = "냉혈의 드루곤",
					["currencyID"] = 1273,
				}, -- [2]
				{
					["name"] = "칼라미르",
					["time"] = 1487822883,
					["currencyID"] = 1273,
				}, -- [3]
				{
					["time"] = 1487220444,
					["name"] = "감시자의 섬",
					["currencyID"] = 1273,
				}, -- [4]
				{
					["item"] = "|cffa335ee|Hitem:141441::::::::110:66::3:4:43:3466:1492:3337:::|h[밑밥 먹보 건틀릿]|h|r",
					["time"] = 1486887104,
					["name"] = "레반투스",
					["currencyID"] = 1273,
				}, -- [5]
				{
					["name"] = "샤르토스",
					["time"] = 1484195909,
					["currencyID"] = 1273,
				}, -- [6]
				{
					["money"] = 477485,
					["name"] = "바다떠돌이",
					["time"] = 1483589667,
					["currencyID"] = 1273,
				}, -- [7]
				{
					["money"] = 387626,
					["time"] = 1482982702,
					["name"] = "냉혈의 드루곤",
					["currencyID"] = 1273,
				}, -- [8]
				{
					["money"] = 377728,
					["name"] = "칼라미르",
					["time"] = 1482380682,
					["currencyID"] = 1273,
				}, -- [9]
				{
					["name"] = "마귀 나자크",
					["item"] = "|cffa335ee|Hitem:141534::::::::110:65::3:3:40:3466:1472:::|h[반질반질한 자갈의 실반지]|h|r",
					["time"] = 1480997791,
					["currencyID"] = 1273,
				}, -- [10]
			},
			["PlayedLevel"] = 81313,
			["Faction"] = "Horde",
			["ILe"] = 853.5,
			["DailyResetTime"] = 1513637999,
			["DailyWorldQuest"] = {
				["days0"] = {
					["questneed"] = 4,
					["name"] = "파론디스의 궁정",
					["isfinish"] = false,
					["dayleft"] = 0,
					["iscompleted"] = false,
					["questdone"] = 0,
				},
			},
			["WeeklyResetTime"] = 1513810799,
		},
		["Lezyne - 아즈샤라"] = {
			["lastbossyell"] = "아즈샤라의 분노: 일반",
			["IL"] = 725.25,
			["LastSeen"] = 1494312022,
			["Order"] = 50,
			["Class"] = "DRUID",
			["currency"] = {
				[1220] = {
					["amount"] = 1205,
				},
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 3556,
				},
				[738] = {
					["amount"] = 40,
				},
				[416] = {
					["amount"] = 15,
				},
				[402] = {
					["amount"] = 1,
				},
				[776] = {
					["totalMax"] = 20,
					["amount"] = 3,
				},
				[823] = {
					["amount"] = 11375,
				},
				[241] = {
					["amount"] = 18,
				},
				[752] = {
					["totalMax"] = 20,
					["amount"] = 0,
				},
				[994] = {
					["totalMax"] = 20,
					["amount"] = 10,
				},
				[1166] = {
					["amount"] = 350,
				},
				[777] = {
					["amount"] = 13007,
				},
			},
			["Level"] = 104,
			["LClass"] = "드루이드",
			["RBGrating"] = 0,
			["lastbosstime"] = 1490018830,
			["Zone"] = "발샤라",
			["lastbossyelltime"] = 1490018830,
			["Money"] = 48824834,
			["WeeklyResetTime"] = 1513810799,
			["DailyResetTime"] = 1513637999,
			["Show"] = "saved",
			["lastboss"] = "아즈샤라의 분노: 일반",
			["Race"] = "타우렌",
			["PlayedLevel"] = 1165,
			["Faction"] = "Horde",
			["ILe"] = 725.25,
			["Skills"] = {
			},
			["Quests"] = {
			},
			["PlayedTotal"] = 1700925,
		},
		["샤이아리 - 아즈샤라"] = {
			["lastbossyell"] = "일리단 스톰레이지",
			["IL"] = 916.125,
			["Zone"] = "크로쿠운",
			["Order"] = 50,
			["Class"] = "PRIEST",
			["currency"] = {
				[1501] = {
					["amount"] = 11,
				},
				[1220] = {
					["amount"] = 30375,
				},
				[1149] = {
					["totalMax"] = 5000,
					["amount"] = 220,
				},
				[1273] = {
					["weeklyMax"] = 3,
					["totalMax"] = 6,
					["amount"] = 6,
				},
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 800,
				},
				[1155] = {
					["totalMax"] = 1700,
					["amount"] = 1109,
				},
				[1342] = {
					["totalMax"] = 1000,
					["amount"] = 86,
				},
				[1166] = {
					["amount"] = 2185,
				},
				[1275] = {
					["amount"] = 53,
				},
				[1226] = {
					["amount"] = 18263,
				},
				[1508] = {
					["totalMax"] = 2000,
					["amount"] = 355,
				},
				[1314] = {
					["weeklyMax"] = 20,
					["totalMax"] = 40,
					["amount"] = 31,
				},
			},
			["LastSeen"] = 1513571551,
			["DailyWorldQuest"] = {
				["days2"] = {
					["questneed"] = 4,
					["name"] = "발라리아르",
					["isfinish"] = false,
					["dayleft"] = 2,
					["iscompleted"] = false,
					["questdone"] = 0,
				},
				["days0"] = {
					["questneed"] = 4,
					["name"] = "군단척결군",
					["isfinish"] = false,
					["dayleft"] = 0,
					["iscompleted"] = false,
					["questdone"] = 0,
				},
				["days1"] = {
					["questneed"] = 4,
					["name"] = "파론디스의 궁정",
					["isfinish"] = false,
					["dayleft"] = 1,
					["iscompleted"] = false,
					["questdone"] = 0,
				},
			},
			["DailyResetTime"] = 1513637999,
			["Level"] = 110,
			["LClass"] = "사제",
			["RBGrating"] = 0,
			["MythicKey"] = {
			},
			["PlayedTotal"] = 887379,
			["lastbosstime"] = 1505642210,
			["lastbossyelltime"] = 1513571092,
			["Money"] = 124399084,
			["Race"] = "블러드 엘프",
			["MythicKeyBest"] = {
				["level"] = 0,
				["ResetTime"] = 1513810799,
				["WeeklyReward"] = false,
			},
			["Show"] = "saved",
			["lastboss"] = "임프 어미",
			["BonusRoll"] = {
				{
					["name"] = "임프 어미",
					["time"] = 1505642246,
					["currencyID"] = 1273,
				}, -- [1]
				{
					["name"] = "킬제덴: 공격대 찾기",
					["time"] = 1504957028,
					["currencyID"] = 1273,
				}, -- [2]
				{
					["name"] = "킬제덴: 공격대 찾기",
					["item"] = "|cffa335ee|Hitem:147195::::::::110:258::4:3:3564:1477:3528:::|h[두 번째 쌍두 정치의 인장]|h|r",
					["time"] = 1503815981,
					["currencyID"] = 1273,
				}, -- [3]
				{
					["time"] = 1503815213,
					["name"] = "황폐의 숙주: 공격대 찾기",
					["currencyID"] = 1273,
				}, -- [4]
				{
					["time"] = 1497533477,
					["name"] = "굴단: 영웅",
					["item"] = "|cffa335ee|Hitem:140914::::::::110:258::5:3:3517:1492:3528:::|h[추방된 방랑자의 넝마신]|h|r",
					["currencyID"] = 1273,
				}, -- [5]
				{
					["time"] = 1497529298,
					["name"] = "시간 변형체: 영웅",
					["currencyID"] = 1273,
				}, -- [6]
				{
					["time"] = 1497528858,
					["name"] = "스코르파이론: 영웅",
					["currencyID"] = 1273,
				}, -- [7]
				{
					["name"] = "말리피쿠스",
					["time"] = 1497500257,
					["currencyID"] = 1273,
				}, -- [8]
				{
					["name"] = "티콘드리우스: 영웅",
					["time"] = 1496413525,
					["currencyID"] = 1273,
				}, -- [9]
				{
					["name"] = "트릴리악스: 영웅",
					["time"] = 1496412151,
					["currencyID"] = 1273,
				}, -- [10]
				{
					["name"] = "시간 변형체: 영웅",
					["time"] = 1496411853,
					["currencyID"] = 1273,
				}, -- [11]
				{
					["item"] = "|cffa335ee|Hitem:140809::::::::110:258::5:3:3517:1492:3528:::|h[어둠 속의 속삭임]|h|r",
					["time"] = 1496410645,
					["name"] = "굴단: 영웅",
					["currencyID"] = 1273,
				}, -- [12]
				{
					["name"] = "대마법학자 엘리산드: 영웅",
					["time"] = 1496410129,
					["currencyID"] = 1273,
				}, -- [13]
				{
					["time"] = 1496292930,
					["name"] = "메마른 짐",
					["currencyID"] = 1273,
				}, -- [14]
				{
					["time"] = 1496292489,
					["name"] = "아포크론",
					["currencyID"] = 1273,
				}, -- [15]
				{
					["name"] = "아포크론",
					["item"] = "|cffa335ee|Hitem:147766::::::::110:258::3:3:3572:1502:3337:::|h[어둠의 세월 고리]|h|r",
					["time"] = 1496148682,
					["currencyID"] = 1273,
				}, -- [16]
				{
					["name"] = "크로서스: 영웅",
					["time"] = 1495548030,
					["currencyID"] = 1273,
				}, -- [17]
				{
					["name"] = "시간 변형체: 영웅",
					["time"] = 1495545109,
					["currencyID"] = 1273,
				}, -- [18]
				{
					["name"] = "굴단: 영웅",
					["time"] = 1495544138,
					["currencyID"] = 1273,
				}, -- [19]
				{
					["name"] = "대마법학자 엘리산드: 영웅",
					["item"] = "|cffa335ee|Hitem:138313::::::::110:258::5:4:3516:41:1487:3528:::|h[정화자의 목가리개]|h|r",
					["time"] = 1495543510,
					["currencyID"] = 1273,
				}, -- [20]
				{
					["name"] = "티콘드리우스: 영웅",
					["time"] = 1494675470,
					["currencyID"] = 1273,
				}, -- [21]
				{
					["time"] = 1494672785,
					["name"] = "시간 변형체: 영웅",
					["item"] = "|cffa335ee|Hitem:140894::::::::110:258::5:3:3444:1487:3336:::|h[열광적인 시간석 펜던트]|h|r",
					["currencyID"] = 1273,
				}, -- [22]
				{
					["time"] = 1494671586,
					["name"] = "굴단: 영웅",
					["currencyID"] = 1273,
				}, -- [23]
				{
					["time"] = 1494671009,
					["name"] = "대마법학자 엘리산드: 영웅",
					["currencyID"] = 1273,
				}, -- [24]
				{
					["name"] = "말리피쿠스",
					["time"] = 1494039162,
					["currencyID"] = 1273,
				}, -- [25]
			},
			["PlayedLevel"] = 759830,
			["Faction"] = "Horde",
			["ILe"] = 912.375,
			["Skills"] = {
			},
			["Quests"] = {
				[48799] = {
					["Expires"] = 1513810799,
					["Title"] = "저주받은 세계의 자원",
					["Link"] = "|cffffff00|Hquest:48799:90|h[저주받은 세계의 자원]|h|r",
					["Zone"] = "크로쿠운",
					["isDaily"] = false,
				},
				[49293] = {
					["Expires"] = 1513810799,
					["Title"] = "침공 공습",
					["Link"] = "|cffffff00|Hquest:49293:90|h[침공 공습]|h|r",
					["Zone"] = "크로쿠운",
					["isDaily"] = false,
				},
			},
			["WeeklyResetTime"] = 1513810799,
		},
		["영석썼숑 - 줄진"] = {
			["IL"] = 685.5,
			["Zone"] = "스톰윈드",
			["Order"] = 50,
			["Class"] = "WARLOCK",
			["currency"] = {
				[824] = {
					["totalMax"] = 10000,
					["amount"] = 8007,
				},
				[777] = {
					["amount"] = 14794,
				},
				[994] = {
					["totalMax"] = 20,
					["amount"] = 9,
				},
				[823] = {
					["amount"] = 15096,
				},
				[776] = {
					["totalMax"] = 20,
					["amount"] = 10,
				},
				[752] = {
					["totalMax"] = 20,
					["amount"] = 0,
				},
				[738] = {
					["amount"] = 18,
				},
				[402] = {
					["amount"] = 6,
				},
			},
			["Level"] = 100,
			["LClass"] = "흑마법사",
			["RBGrating"] = 0,
			["PlayedTotal"] = 2592269,
			["Money"] = 6277441,
			["LastSeen"] = 1478600182,
			["WeeklyResetTime"] = 1513810799,
			["Show"] = "saved",
			["Skills"] = {
			},
			["PlayedLevel"] = 255004,
			["Faction"] = "Alliance",
			["ILe"] = 684.5625,
			["DailyResetTime"] = 1513637999,
			["Quests"] = {
			},
			["Race"] = "인간",
		},
		["사나정연 - 아즈샤라"] = {
			["lastbossyell"] = "암흑주술사 코란살: 일반",
			["IL"] = 14.25,
			["Zone"] = "오그리마",
			["Order"] = 50,
			["Class"] = "MONK",
			["currency"] = {
			},
			["Race"] = "판다렌 (호드)",
			["DailyWorldQuest"] = {
			},
			["Level"] = 17,
			["LClass"] = "수도사",
			["RBGrating"] = 0,
			["MythicKeyBest"] = {
				["level"] = 0,
				["ResetTime"] = 1513810799,
				["WeeklyReward"] = false,
			},
			["PlayedTotal"] = 9353,
			["DailyResetTime"] = 1513637999,
			["lastbossyelltime"] = 1513500755,
			["Money"] = 22899,
			["lastbosstime"] = 1513500900,
			["LastSeen"] = 1513569335,
			["Show"] = "saved",
			["lastboss"] = "용암경비병 고르도스: 일반",
			["WeeklyResetTime"] = 1513810799,
			["PlayedLevel"] = 159,
			["Faction"] = "Horde",
			["ILe"] = 14.25,
			["Skills"] = {
			},
			["Quests"] = {
			},
			["MythicKey"] = {
			},
		},
	},
	["Tooltip"] = {
		["CategorySort"] = "EXPANSION",
		["ShowSoloCategory"] = false,
		["ShowServer"] = false,
		["NumberFormat"] = true,
		["ShowExpired"] = false,
		["RaidsFirst"] = true,
		["CombineWorldBosses"] = false,
		["HistoryText"] = false,
		["NewFirst"] = true,
		["Currency738"] = false,
		["CurrencyEarned"] = true,
		["Currency776"] = false,
		["CurrencyValueColor"] = true,
		["ShowRandom"] = true,
		["Currency994"] = false,
		["TrackWeeklyQuests"] = true,
		["ReportResets"] = true,
		["CombineLFR"] = true,
		["LimitWarn"] = true,
		["SelfAlways"] = false,
		["Scale"] = 1,
		["RowHighlight"] = 0.1,
		["DailyWorldQuest"] = true,
		["Currency824"] = false,
		["AbbreviateKeystone"] = true,
		["ShowCategories"] = false,
		["ServerOnly"] = false,
		["Currency1226"] = false,
		["Currency1273"] = true,
		["TrackLFG"] = true,
		["Currency1149"] = true,
		["TrackDailyQuests"] = true,
		["Currency823"] = false,
		["CategorySpaces"] = false,
		["Currency1220"] = true,
		["ShowHints"] = true,
		["TrackBonus"] = false,
		["TrackFarm"] = true,
		["SelfFirst"] = true,
		["ShowHoliday"] = true,
		["AugmentBonus"] = true,
		["Currency1129"] = false,
		["TrackDeserter"] = true,
		["FitToScreen"] = true,
		["ReverseInstances"] = false,
		["Currency1191"] = false,
		["CurrencyMax"] = true,
		["ServerSort"] = true,
		["TrackPlayed"] = true,
		["Currency1155"] = true,
		["TrackSkills"] = true,
		["ConnectedRealms"] = "group",
		["MythicKeyBest"] = true,
		["Currency1101"] = false,
		["Currency1166"] = true,
		["MythicKey"] = true,
	},
	["spelltip"] = {
		[71041] = {
			"던전 탈영병", -- [1]
			"파티에서 빠져나왔으므로, 던전 찾기 또는 공격대 찾기를 재사용하려면 기다려야 합니다.", -- [2]
		},
	},
	["Instances"] = {
		["공격대 찾기: 잊혀진 심연"] = {
			["LFDID"] = 836,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["카라잔"] = {
			["LFDID"] = 175,
			["Expansion"] = 1,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 70,
		},
		["화산 심장부"] = {
			["LFDID"] = 48,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 60,
		},
		["무작위 대격변 (영웅)"] = {
			["Show"] = "saved",
			["Expansion"] = 3,
			["LFDID"] = 301,
			["RecLevel"] = 0,
			["Random"] = true,
			["Raid"] = false,
		},
		["감시관의 금고"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["LFDID"] = 1044,
			["Raid"] = false,
			["RecLevel"] = 100,
		},
		["영혼의 아귀"] = {
			["LFDID"] = 1192,
			["Expansion"] = 6,
			["RecLevel"] = 110,
			["Raid"] = false,
			["Show"] = "saved",
		},
		["아포크론"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 1956,
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["아쉬란"] = {
			["LFDID"] = 1127,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 100,
		},
		["영원의 눈"] = {
			["LFDID"] = 237,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = true,
		},
		["톨비르의 잃어버린 도시"] = {
			["LFDID"] = 1151,
			["Expansion"] = 3,
			["Show"] = "saved",
			["RecLevel"] = 84,
			["Raid"] = false,
		},
		["스톰윈드 지하감옥"] = {
			["LFDID"] = 12,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 24,
		},
		["줄파락"] = {
			["LFDID"] = 24,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 48,
		},
		["지옥의 군주 바일머스"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 2015,
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["놈리건"] = {
			["LFDID"] = 14,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 28,
		},
		["다시 찾은 카라잔 하층"] = {
			["LFDID"] = 1475,
			["Expansion"] = 6,
			["RecLevel"] = 110,
			["Raid"] = false,
			["Show"] = "saved",
		},
		["무작위 판다리아의 안개 시나리오"] = {
			["Show"] = "saved",
			["Expansion"] = 4,
			["Scenario"] = true,
			["LFDID"] = 493,
			["RecLevel"] = 0,
			["Random"] = true,
			["Raid"] = false,
		},
		["무작위 군단 영웅"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["LFDID"] = 1046,
			["RecLevel"] = 110,
			["Random"] = true,
			["Raid"] = false,
		},
		["황혼의 시간"] = {
			["LFDID"] = 439,
			["Expansion"] = 3,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 85,
		},
		["파괴의 / 영생의"] = {
			["Show"] = "saved",
			["Expansion"] = 5,
			["WorldBoss"] = 1211,
			["RecLevel"] = 100,
			["Raid"] = true,
		},
		["과거의 하이잘"] = {
			["LFDID"] = 195,
			["Expansion"] = 1,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 70,
		},
		["번개의 전당"] = {
			["LFDID"] = 1018,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = false,
		},
		["가시덩굴 우리"] = {
			["LFDID"] = 16,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 34,
		},
		["줄구룹"] = {
			["LFDID"] = 334,
			["Expansion"] = 3,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 85,
		},
		["폭풍우 요새"] = {
			["LFDID"] = 193,
			["Expansion"] = 1,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 70,
		},
		["석양문"] = {
			["LFDID"] = 1464,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = false,
		},
		["공포의 심장"] = {
			["LFDID"] = 534,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["알카트라즈"] = {
			["LFDID"] = 1011,
			["Expansion"] = 1,
			["Show"] = "saved",
			["RecLevel"] = 70,
			["Raid"] = false,
		},
		["공격대 찾기: 고통받는 수호자들"] = {
			["LFDID"] = 1288,
			["Expansion"] = 6,
			["RecLevel"] = 110,
			["Raid"] = true,
			["Show"] = "saved",
		},
		["별의 궁정"] = {
			["LFDID"] = 1319,
			["Expansion"] = 6,
			["RecLevel"] = 110,
			["Raid"] = false,
			["Show"] = "saved",
		},
		["붉은십자군 전당"] = {
			["LFDID"] = 473,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 30,
			["Raid"] = false,
		},
		["공격대 찾기: 용맹의 시험"] = {
			["LFDID"] = 1411,
			["Expansion"] = 6,
			["Raid"] = true,
			["RecLevel"] = 110,
			["Show"] = "saved",
		},
		["심문관 메토"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 2012,
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["무작위 시간여행 던전 (대격변)"] = {
			["Show"] = "saved",
			["Expansion"] = 4,
			["LFDID"] = 1146,
			["Raid"] = false,
			["RecLevel"] = 0,
			["Random"] = true,
			["Holiday"] = true,
		},
		["지하수렁"] = {
			["LFDID"] = 186,
			["Expansion"] = 1,
			["Show"] = "saved",
			["RecLevel"] = 64,
			["Raid"] = false,
		},
		["공격대 찾기: 피의 전당"] = {
			["LFDID"] = 1367,
			["Expansion"] = 5,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 100,
		},
		["용의 영혼"] = {
			["LFDID"] = 448,
			["Expansion"] = 3,
			["Show"] = "saved",
			["RecLevel"] = 85,
			["Raid"] = true,
		},
		["검은심연 나락"] = {
			["LFDID"] = 10,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 24,
		},
		["서리 군주 아훈"] = {
			["LFDID"] = 286,
			["Expansion"] = 1,
			["Raid"] = false,
			["RecLevel"] = 100,
			["Holiday"] = true,
			["Show"] = "saved",
		},
		["오그리마 공성전"] = {
			["LFDID"] = 766,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["브루탈루스"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 1883,
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["대공주 테라드라스"] = {
			["Show"] = "saved",
			["Expansion"] = 0,
			["RecLevel"] = 80,
			["Raid"] = false,
			["Holiday"] = true,
			["LFDID"] = 309,
		},
		["공격대 찾기: 기만자의 몰락"] = {
			["LFDID"] = 1497,
			["Expansion"] = 6,
			["Show"] = "saved",
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["용맹의 시험"] = {
			["LFDID"] = 1439,
			["Expansion"] = 6,
			["Raid"] = true,
			["RecLevel"] = 110,
			["Show"] = "saved",
		},
		["시바쉬"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 1885,
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["휴몽그리스"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 1770,
			["Raid"] = true,
			["RecLevel"] = 110,
		},
		["어둠의 문 열기"] = {
			["LFDID"] = 1012,
			["Expansion"] = 1,
			["Show"] = "saved",
			["RecLevel"] = 70,
			["Raid"] = false,
		},
		["소타나토르"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 2014,
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["스트라솔름 - 정문"] = {
			["LFDID"] = 40,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 46,
		},
		["공격대 찾기: 응보의 성문"] = {
			["LFDID"] = 840,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["공격대 찾기: 알른의 균열"] = {
			["LFDID"] = 1289,
			["Expansion"] = 6,
			["RecLevel"] = 110,
			["Raid"] = true,
			["Show"] = "saved",
		},
		["붉은십자군 수도원"] = {
			["LFDID"] = 474,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 32,
			["Raid"] = false,
		},
		["피의 용광로"] = {
			["LFDID"] = 187,
			["Expansion"] = 1,
			["Show"] = "saved",
			["RecLevel"] = 62,
			["Raid"] = false,
		},
		["강철 선착장"] = {
			["LFDID"] = 1007,
			["Expansion"] = 5,
			["Show"] = "saved",
			["RecLevel"] = 93,
			["Raid"] = false,
		},
		["샤르토스"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 1763,
			["Raid"] = true,
			["RecLevel"] = 110,
		},
		["무작위 판다리아의 안개 던전 (영웅)"] = {
			["Show"] = "saved",
			["Expansion"] = 4,
			["LFDID"] = 462,
			["RecLevel"] = 0,
			["Random"] = true,
			["Raid"] = false,
		},
		["공격대 찾기: 지옥돌파"] = {
			["LFDID"] = 1366,
			["Expansion"] = 5,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 100,
		},
		["공격대 찾기: 고룡쉼터 사원 탈환"] = {
			["LFDID"] = 843,
			["Expansion"] = 3,
			["Show"] = "saved",
			["RecLevel"] = 85,
			["Raid"] = true,
		},
		["아즈샤라의 눈"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["LFDID"] = 1175,
			["RecLevel"] = 100,
			["Raid"] = false,
		},
		["마력의 눈"] = {
			["LFDID"] = 211,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = false,
		},
		["나락크"] = {
			["Show"] = "saved",
			["Expansion"] = 4,
			["WorldBoss"] = 814,
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["시간여행 공격대: 검은 사원"] = {
			["Show"] = "saved",
			["Expansion"] = 1,
			["LFDID"] = 1533,
			["Raid"] = true,
			["Holiday"] = true,
			["RecLevel"] = 110,
		},
		["우트가드 성채"] = {
			["LFDID"] = 242,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 72,
			["Raid"] = false,
		},
		["줄아만"] = {
			["LFDID"] = 340,
			["Expansion"] = 3,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 85,
		},
		["네 바람의 왕좌"] = {
			["LFDID"] = 318,
			["Expansion"] = 3,
			["Show"] = "saved",
			["RecLevel"] = 85,
			["Raid"] = true,
		},
		["살게라스의 무덤"] = {
			["LFDID"] = 1527,
			["Expansion"] = 6,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 110,
		},
		["소용돌이 누각"] = {
			["LFDID"] = 1147,
			["Expansion"] = 3,
			["Show"] = "saved",
			["RecLevel"] = 83,
			["Raid"] = false,
		},
		["무작위 불타는 성전 던전"] = {
			["Show"] = "saved",
			["Expansion"] = 1,
			["LFDID"] = 259,
			["RecLevel"] = 65,
			["Random"] = true,
			["Raid"] = false,
		},
		["검은바위 나락 - 상부 도시"] = {
			["LFDID"] = 276,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 55,
		},
		["안토러스 - 불타는 왕좌"] = {
			["LFDID"] = 1642,
			["Expansion"] = 6,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 110,
		},
		["바위심장부"] = {
			["LFDID"] = 1148,
			["Expansion"] = 3,
			["Raid"] = false,
			["RecLevel"] = 83,
			["Show"] = "saved",
		},
		["무작위 드레노어의 전쟁군주 던전"] = {
			["Show"] = "saved",
			["Expansion"] = 5,
			["LFDID"] = 788,
			["RecLevel"] = 0,
			["Random"] = true,
			["Raid"] = false,
		},
		["십자군의 시험장"] = {
			["LFDID"] = 248,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = true,
		},
		["혈투의 전장 - 수도 정원"] = {
			["LFDID"] = 36,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 43,
		},
		["모구샨 금고"] = {
			["LFDID"] = 532,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["오닉시아의 둥지"] = {
			["LFDID"] = 257,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = true,
		},
		["죽음의 폐광"] = {
			["LFDID"] = 326,
			["Expansion"] = 3,
			["Show"] = "saved",
			["RecLevel"] = 16,
			["Raid"] = false,
		},
		["대사자 화염채찍"] = {
			["LFDID"] = 308,
			["Expansion"] = 0,
			["Raid"] = false,
			["RecLevel"] = 80,
			["Holiday"] = true,
			["Show"] = "saved",
		},
		["신록의 정원"] = {
			["LFDID"] = 191,
			["Expansion"] = 1,
			["Show"] = "saved",
			["RecLevel"] = 70,
			["Raid"] = false,
		},
		["공격대 찾기: 폭군의 몰락"] = {
			["LFDID"] = 842,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["시초의 전당"] = {
			["LFDID"] = 321,
			["Expansion"] = 3,
			["Show"] = "saved",
			["RecLevel"] = 85,
			["Raid"] = false,
		},
		["아킨둔"] = {
			["LFDID"] = 1008,
			["Expansion"] = 5,
			["Show"] = "saved",
			["RecLevel"] = 96,
			["Raid"] = false,
		},
		["돌의 전당"] = {
			["LFDID"] = 213,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 78,
			["Raid"] = false,
		},
		["마그테리돈의 둥지"] = {
			["LFDID"] = 176,
			["Expansion"] = 1,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 70,
		},
		["지옥불 성채"] = {
			["LFDID"] = 989,
			["Expansion"] = 5,
			["Show"] = "saved",
			["RecLevel"] = 100,
			["Raid"] = true,
		},
		["냉혈의 드루곤"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 1789,
			["Raid"] = true,
			["RecLevel"] = 110,
		},
		["네 천신"] = {
			["Show"] = "saved",
			["Expansion"] = 4,
			["WorldBoss"] = 857,
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["영원의 샘"] = {
			["LFDID"] = 437,
			["Expansion"] = 3,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 85,
		},
		["무작위 시간여행 던전 (판다리아의 안개)"] = {
			["Show"] = "saved",
			["Expansion"] = 5,
			["LFDID"] = 1453,
			["Holiday"] = true,
			["Raid"] = false,
			["Random"] = true,
			["RecLevel"] = 0,
		},
		["검은바위 첨탑 하층"] = {
			["LFDID"] = 32,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 58,
		},
		["공격대 찾기: 빛의 틈"] = {
			["LFDID"] = 1610,
			["Expansion"] = 6,
			["Show"] = "saved",
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["황혼의 요새"] = {
			["LFDID"] = 316,
			["Expansion"] = 3,
			["Show"] = "saved",
			["RecLevel"] = 85,
			["Raid"] = true,
		},
		["용사의 시험장"] = {
			["LFDID"] = 249,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = false,
		},
		["지옥불 성루"] = {
			["LFDID"] = 188,
			["Expansion"] = 1,
			["Show"] = "saved",
			["RecLevel"] = 61,
			["Raid"] = false,
		},
		["검은바위 동굴"] = {
			["LFDID"] = 323,
			["Expansion"] = 3,
			["Show"] = "saved",
			["RecLevel"] = 81,
			["Raid"] = false,
		},
		["Lord Kazzak"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 9002,
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["코렌 다이어브루"] = {
			["LFDID"] = 287,
			["Expansion"] = 0,
			["Raid"] = false,
			["RecLevel"] = 110,
			["Holiday"] = true,
			["Show"] = "saved",
		},
		["음영파 수도원"] = {
			["LFDID"] = 1468,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 88,
			["Raid"] = false,
		},
		["시간의 끝"] = {
			["LFDID"] = 1152,
			["Expansion"] = 3,
			["Show"] = "saved",
			["RecLevel"] = 85,
			["Raid"] = false,
		},
		["바다떠돌이"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 1795,
			["Raid"] = true,
			["RecLevel"] = 110,
		},
		["스톰스타우트 양조장"] = {
			["LFDID"] = 1466,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 86,
			["Raid"] = false,
		},
		["영원한 봄의 정원"] = {
			["LFDID"] = 536,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["무작위 불타는 성전 (영웅)"] = {
			["Show"] = "saved",
			["Expansion"] = 1,
			["LFDID"] = 260,
			["RecLevel"] = 70,
			["Random"] = true,
			["Raid"] = false,
		},
		["아나무즈"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 1790,
			["Raid"] = true,
			["RecLevel"] = 110,
		},
		["으스러진 손의 전당"] = {
			["LFDID"] = 1014,
			["Expansion"] = 1,
			["Show"] = "saved",
			["RecLevel"] = 70,
			["Raid"] = false,
		},
		["공격대 찾기: 폭풍의 첨탑"] = {
			["LFDID"] = 838,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["무작위 드레노어의 전쟁군주 던전 (영웅)"] = {
			["Show"] = "saved",
			["Expansion"] = 5,
			["LFDID"] = 789,
			["RecLevel"] = 0,
			["Random"] = true,
			["Raid"] = false,
		},
		["운다스타"] = {
			["Show"] = "saved",
			["Expansion"] = 4,
			["WorldBoss"] = 826,
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["아카본 석실"] = {
			["LFDID"] = 240,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = true,
		},
		["어둠달 지하묘지"] = {
			["LFDID"] = 1009,
			["Expansion"] = 5,
			["Show"] = "saved",
			["RecLevel"] = 100,
			["Raid"] = false,
		},
		["마력의 탑"] = {
			["LFDID"] = 1019,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 73,
			["Raid"] = false,
		},
		["공격대 찾기: 높은 벽의 도시"] = {
			["LFDID"] = 1363,
			["Expansion"] = 5,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 100,
		},
		["검은 사원"] = {
			["LFDID"] = 196,
			["Expansion"] = 1,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 70,
		},
		["공격대 찾기: 셰크지르의 악몽"] = {
			["LFDID"] = 833,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["흑요석 성소"] = {
			["LFDID"] = 238,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = true,
		},
		["니소그"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 1749,
			["Raid"] = true,
			["RecLevel"] = 110,
		},
		["레반투스"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 1769,
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["그림자송곳니 성채"] = {
			["LFDID"] = 327,
			["Expansion"] = 3,
			["Show"] = "saved",
			["RecLevel"] = 20,
			["Raid"] = false,
		},
		["바라딘 요새"] = {
			["LFDID"] = 329,
			["Expansion"] = 3,
			["Show"] = "saved",
			["RecLevel"] = 85,
			["Raid"] = true,
		},
		["검은바위 첨탑 상층"] = {
			["Show"] = "saved",
			["Expansion"] = 5,
			["RecLevel"] = 90,
			["Raid"] = false,
			["LFDID"] = 1004,
		},
		["스칼로맨스"] = {
			["Show"] = "saved",
			["Expansion"] = 4,
			["RecLevel"] = 42,
			["Raid"] = false,
			["LFDID"] = 472,
		},
		["군드락"] = {
			["LFDID"] = 1017,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 78,
			["Raid"] = false,
		},
		["공격대 찾기: 모구샨의 수호자"] = {
			["LFDID"] = 830,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["루비 성소"] = {
			["LFDID"] = 294,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = true,
		},
		["공격대 찾기: 다가오는 공포"] = {
			["LFDID"] = 832,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["왕관화학회사"] = {
			["LFDID"] = 288,
			["Expansion"] = 0,
			["Raid"] = false,
			["RecLevel"] = 100,
			["Holiday"] = true,
			["Show"] = "saved",
		},
		["공격대 찾기: 검은 제련소"] = {
			["LFDID"] = 1360,
			["Expansion"] = 5,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 100,
		},
		["공격대 찾기: 파괴자의 탑"] = {
			["LFDID"] = 1369,
			["Expansion"] = 5,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 100,
		},
		["용맹의 전당"] = {
			["LFDID"] = 1194,
			["Expansion"] = 6,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 100,
		},
		["마라우돈 - 악의 동굴"] = {
			["LFDID"] = 272,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 34,
		},
		["옛 스트라솔름"] = {
			["LFDID"] = 210,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = false,
		},
		["마귀 나자크"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 1783,
			["Raid"] = true,
			["RecLevel"] = 110,
		},
		["파도의 왕좌"] = {
			["LFDID"] = 1150,
			["Expansion"] = 3,
			["Show"] = "saved",
			["RecLevel"] = 81,
			["Raid"] = false,
		},
		["비전로"] = {
			["LFDID"] = 1190,
			["Expansion"] = 6,
			["Raid"] = false,
			["RecLevel"] = 110,
			["Show"] = "saved",
		},
		["혈투의 전장 - 굽이나무 지구"] = {
			["LFDID"] = 34,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 40,
		},
		["통곡의 동굴"] = {
			["LFDID"] = 1,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 19,
		},
		["검은 떼까마귀 요새"] = {
			["LFDID"] = 1205,
			["Expansion"] = 6,
			["RecLevel"] = 100,
			["Raid"] = false,
			["Show"] = "saved",
		},
		["증기 저장고"] = {
			["LFDID"] = 185,
			["Expansion"] = 1,
			["Show"] = "saved",
			["RecLevel"] = 70,
			["Raid"] = false,
		},
		["메카나르"] = {
			["LFDID"] = 192,
			["Expansion"] = 1,
			["Show"] = "saved",
			["RecLevel"] = 68,
			["Raid"] = false,
		},
		["메마른 짐"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 1796,
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["검은바위 용광로"] = {
			["LFDID"] = 900,
			["Expansion"] = 5,
			["Show"] = "saved",
			["RecLevel"] = 100,
			["Raid"] = true,
		},
		["강제 노역소"] = {
			["LFDID"] = 1015,
			["Expansion"] = 1,
			["Show"] = "saved",
			["RecLevel"] = 63,
			["Raid"] = false,
		},
		["안퀴라즈 폐허"] = {
			["LFDID"] = 160,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 60,
		},
		["공격대 찾기: 검은가지"] = {
			["LFDID"] = 1287,
			["Expansion"] = 6,
			["Show"] = "saved",
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["무작위 오리지널 던전"] = {
			["Show"] = "saved",
			["Expansion"] = 0,
			["LFDID"] = 258,
			["RecLevel"] = 55,
			["Random"] = true,
			["Raid"] = false,
		},
		["영원한 밤의 대성당"] = {
			["LFDID"] = 1488,
			["Expansion"] = 6,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 110,
		},
		["높은망치"] = {
			["LFDID"] = 897,
			["Expansion"] = 5,
			["Show"] = "saved",
			["RecLevel"] = 100,
			["Raid"] = true,
		},
		["공격대 찾기: 지옥의 관문"] = {
			["LFDID"] = 1494,
			["Expansion"] = 6,
			["Show"] = "saved",
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["공격대 찾기: 높은군주의 탑"] = {
			["LFDID"] = 1365,
			["Expansion"] = 5,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 100,
		},
		["에메랄드의 악몽"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["LFDID"] = 1350,
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["여군주 알루라델"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 2011,
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["낙스라마스"] = {
			["LFDID"] = 227,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = true,
		},
		["불의 땅"] = {
			["LFDID"] = 362,
			["Expansion"] = 3,
			["Show"] = "saved",
			["RecLevel"] = 85,
			["Raid"] = true,
		},
		["불뱀 제단"] = {
			["LFDID"] = 194,
			["Expansion"] = 1,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 70,
		},
		["얼음왕관 성채"] = {
			["LFDID"] = 280,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = true,
		},
		["검은바위 나락 - 감금 구역"] = {
			["LFDID"] = 30,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 51,
		},
		["가시덩굴 구릉"] = {
			["LFDID"] = 20,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 44,
		},
		["공격대 찾기: 금지된 내리막"] = {
			["LFDID"] = 1611,
			["Expansion"] = 6,
			["Show"] = "saved",
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["공격대 찾기: 비전 성소"] = {
			["LFDID"] = 1364,
			["Expansion"] = 5,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 100,
		},
		["어둠심장 숲"] = {
			["LFDID"] = 1202,
			["Expansion"] = 6,
			["Show"] = "saved",
			["RecLevel"] = 100,
			["Raid"] = false,
		},
		["영혼의 제련소"] = {
			["LFDID"] = 252,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = false,
		},
		["하늘탑"] = {
			["LFDID"] = 1010,
			["Expansion"] = 5,
			["Show"] = "saved",
			["RecLevel"] = 98,
			["Raid"] = false,
		},
		["무작위 군단 던전"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["LFDID"] = 1045,
			["RecLevel"] = 0,
			["Random"] = true,
			["Raid"] = false,
		},
		["음영파 결전지"] = {
			["LFDID"] = 1428,
			["Expansion"] = 0,
			["Raid"] = false,
			["RecLevel"] = 100,
			["Show"] = "saved",
		},
		["오큘라러스"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 2013,
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["공격대 찾기: 잿가루작업장"] = {
			["LFDID"] = 1361,
			["Expansion"] = 5,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 100,
		},
		["공격대 찾기: 밤의 첨탑"] = {
			["LFDID"] = 1292,
			["Expansion"] = 6,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 110,
		},
		["공격대 찾기: 강철 설비 시설"] = {
			["LFDID"] = 1362,
			["Expansion"] = 5,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 100,
		},
		["천둥의 왕좌"] = {
			["LFDID"] = 634,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["다시 찾은 카라잔"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["Raid"] = false,
			["RecLevel"] = 110,
			["LFDID"] = 1347,
		},
		["혈투의 전장 - 고르독 광장"] = {
			["LFDID"] = 38,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 46,
		},
		["Dragon of Nightmare"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 9004,
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["공격대 찾기: 살점구체자의 전당"] = {
			["LFDID"] = 837,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["상록숲"] = {
			["LFDID"] = 1003,
			["Expansion"] = 5,
			["Show"] = "saved",
			["RecLevel"] = 100,
			["Raid"] = false,
		},
		["검은날개 강림지"] = {
			["LFDID"] = 314,
			["Expansion"] = 3,
			["Show"] = "saved",
			["RecLevel"] = 85,
			["Raid"] = true,
		},
		["드락타론 성채"] = {
			["LFDID"] = 215,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 75,
			["Raid"] = false,
		},
		["공격대 찾기: 희망의 끝"] = {
			["LFDID"] = 1612,
			["Expansion"] = 6,
			["Show"] = "saved",
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["공격대 찾기: 검은 문"] = {
			["LFDID"] = 1370,
			["Expansion"] = 5,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 100,
		},
		["공격대 찾기: 잔달라 부족 최후의 저항"] = {
			["LFDID"] = 835,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["마나 무덤"] = {
			["LFDID"] = 1013,
			["Expansion"] = 1,
			["Show"] = "saved",
			["RecLevel"] = 65,
			["Raid"] = false,
		},
		["무작위 시간여행 던전 (불타는 성전)"] = {
			["Show"] = "saved",
			["Expansion"] = 2,
			["LFDID"] = 744,
			["Holiday"] = true,
			["Raid"] = false,
			["Random"] = true,
			["RecLevel"] = 0,
		},
		["태양샘"] = {
			["LFDID"] = 199,
			["Expansion"] = 1,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 70,
		},
		["왕자 사르사룬"] = {
			["LFDID"] = 310,
			["Expansion"] = 0,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Holiday"] = true,
			["Raid"] = false,
		},
		["아키나이 납골당"] = {
			["LFDID"] = 178,
			["Expansion"] = 1,
			["Show"] = "saved",
			["RecLevel"] = 66,
			["Raid"] = false,
		},
		["성난불길 협곡"] = {
			["LFDID"] = 4,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 16,
		},
		["공격대 찾기: 어둠의 보루"] = {
			["LFDID"] = 1368,
			["Expansion"] = 5,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 100,
		},
		["삼두정의 권좌"] = {
			["LFDID"] = 1535,
			["Expansion"] = 6,
			["Show"] = "saved",
			["RecLevel"] = 110,
			["Raid"] = false,
		},
		["저주받은 기사"] = {
			["LFDID"] = 285,
			["Expansion"] = 0,
			["Raid"] = false,
			["RecLevel"] = 110,
			["Holiday"] = true,
			["Show"] = "saved",
		},
		["카이주 가즈릴라"] = {
			["LFDID"] = 306,
			["Expansion"] = 0,
			["Raid"] = false,
			["RecLevel"] = 80,
			["Holiday"] = true,
			["Show"] = "saved",
		},
		["갈레온"] = {
			["Show"] = "saved",
			["Expansion"] = 4,
			["WorldBoss"] = 725,
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["마법학자의 정원"] = {
			["LFDID"] = 1154,
			["Expansion"] = 1,
			["Show"] = "saved",
			["RecLevel"] = 68,
			["Raid"] = false,
		},
		["검은날개 둥지"] = {
			["Show"] = "saved",
			["Expansion"] = 0,
			["RecLevel"] = 60,
			["Raid"] = true,
			["LFDID"] = 50,
		},
		["공격대 찾기: 데스윙의 추락"] = {
			["LFDID"] = 844,
			["Expansion"] = 3,
			["Show"] = "saved",
			["RecLevel"] = 85,
			["Raid"] = true,
		},
		["공격대 찾기: 비전의 수로"] = {
			["LFDID"] = 1290,
			["Expansion"] = 6,
			["Raid"] = true,
			["RecLevel"] = 110,
			["Show"] = "saved",
		},
		["파멸철로 정비소"] = {
			["LFDID"] = 1006,
			["Expansion"] = 5,
			["Show"] = "saved",
			["RecLevel"] = 100,
			["Raid"] = false,
		},
		["무작위 시간여행 던전 (리치 왕의 분노)"] = {
			["Show"] = "saved",
			["Expansion"] = 4,
			["LFDID"] = 995,
			["Holiday"] = true,
			["Raid"] = false,
			["Random"] = true,
			["RecLevel"] = 0,
		},
		["대모 폴누나"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 2010,
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["무작위 판다리아의 안개 던전"] = {
			["Show"] = "saved",
			["Expansion"] = 4,
			["LFDID"] = 463,
			["RecLevel"] = 0,
			["Random"] = true,
			["Raid"] = false,
		},
		["피망치 잿가루 광산"] = {
			["LFDID"] = 1005,
			["Expansion"] = 5,
			["Show"] = "saved",
			["RecLevel"] = 91,
			["Raid"] = false,
		},
		["주둔지 우두머리"] = {
			["Show"] = "saved",
			["Expansion"] = 5,
			["WorldBoss"] = 9001,
			["RecLevel"] = 100,
			["Raid"] = true,
		},
		["사론의 구덩이"] = {
			["LFDID"] = 1153,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = false,
		},
		["안카헤트: 고대 왕국"] = {
			["LFDID"] = 1016,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 75,
			["Raid"] = false,
		},
		["Azuregos"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 9003,
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["공격대 찾기: 화신의 방"] = {
			["LFDID"] = 1496,
			["Expansion"] = 6,
			["Show"] = "saved",
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["십자군 사령관의 시험장"] = {
			["LFDID"] = 250,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = true,
		},
		["칼라미르"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 1774,
			["Raid"] = true,
			["RecLevel"] = 110,
		},
		["무작위 리치 왕 (영웅)"] = {
			["Show"] = "saved",
			["Expansion"] = 2,
			["LFDID"] = 262,
			["RecLevel"] = 80,
			["Random"] = true,
			["Raid"] = false,
		},
		["어둠의 미궁"] = {
			["LFDID"] = 181,
			["Expansion"] = 1,
			["Show"] = "saved",
			["RecLevel"] = 70,
			["Raid"] = false,
		},
		["그룰의 둥지"] = {
			["LFDID"] = 177,
			["Expansion"] = 1,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 70,
		},
		["고위 군주 카자크"] = {
			["Show"] = "saved",
			["Expansion"] = 5,
			["WorldBoss"] = 1452,
			["RecLevel"] = 100,
			["Raid"] = true,
		},
		["마라우돈 - 썩은포자 동굴"] = {
			["LFDID"] = 26,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 36,
		},
		["공격대 찾기: 통곡의 전당"] = {
			["LFDID"] = 1495,
			["Expansion"] = 6,
			["Show"] = "saved",
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["말리피쿠스"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 1884,
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["공격대 찾기: 지하요새"] = {
			["LFDID"] = 841,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["가라앉은 사원"] = {
			["LFDID"] = 28,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 54,
		},
		["다시 찾은 카라잔 상층"] = {
			["LFDID"] = 1474,
			["Expansion"] = 6,
			["RecLevel"] = 110,
			["Raid"] = false,
			["Show"] = "saved",
		},
		["울다만"] = {
			["LFDID"] = 22,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 39,
		},
		["공격대 찾기: 왕립 도서관"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["LFDID"] = 1291,
			["RecLevel"] = 100,
			["Raid"] = true,
		},
		["오르도스"] = {
			["Show"] = "saved",
			["Expansion"] = 4,
			["WorldBoss"] = 861,
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["루크마르"] = {
			["Show"] = "saved",
			["Expansion"] = 5,
			["WorldBoss"] = 1262,
			["RecLevel"] = 100,
			["Raid"] = true,
		},
		["던홀드 탈출"] = {
			["LFDID"] = 183,
			["Expansion"] = 1,
			["Show"] = "saved",
			["RecLevel"] = 67,
			["Raid"] = false,
		},
		["공격대 찾기: 영원한 봄의 정원"] = {
			["LFDID"] = 834,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["옥룡사"] = {
			["LFDID"] = 1469,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 86,
			["Raid"] = false,
		},
		["무작위 황혼의 시간 (영웅)"] = {
			["Show"] = "saved",
			["Expansion"] = 3,
			["LFDID"] = 434,
			["RecLevel"] = 0,
			["Random"] = true,
			["Raid"] = false,
		},
		["보랏빛 요새 침공"] = {
			["LFDID"] = 1209,
			["Expansion"] = 6,
			["Show"] = "saved",
			["RecLevel"] = 105,
			["Raid"] = false,
		},
		["세데크 전당"] = {
			["LFDID"] = 180,
			["Expansion"] = 1,
			["Show"] = "saved",
			["RecLevel"] = 68,
			["Raid"] = false,
		},
		["무작위 대격변 던전"] = {
			["Show"] = "saved",
			["Expansion"] = 3,
			["LFDID"] = 300,
			["RecLevel"] = 0,
			["Random"] = true,
			["Raid"] = false,
		},
		["공격대 찾기: 영원한 슬픔의 골짜기"] = {
			["LFDID"] = 839,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["분노의 샤"] = {
			["Show"] = "saved",
			["Expansion"] = 4,
			["WorldBoss"] = 691,
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["안퀴라즈 사원"] = {
			["LFDID"] = 161,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 60,
		},
		["모구샨 궁전"] = {
			["LFDID"] = 1467,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 88,
			["Raid"] = false,
		},
		["밤의 요새"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["Raid"] = true,
			["RecLevel"] = 110,
			["LFDID"] = 1353,
		},
		["우트가드 첨탑"] = {
			["LFDID"] = 1020,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = false,
		},
		["공격대 찾기: 블랙핸드의 도가니"] = {
			["LFDID"] = 1359,
			["Expansion"] = 5,
			["Show"] = "saved",
			["Raid"] = true,
			["RecLevel"] = 100,
		},
		["니우짜오 사원 공성전투"] = {
			["LFDID"] = 1465,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 89,
			["Raid"] = false,
		},
		["공격대 찾기: 배반자의 탑"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["Raid"] = true,
			["RecLevel"] = 110,
			["LFDID"] = 1293,
		},
		["울두아르"] = {
			["LFDID"] = 244,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = true,
		},
		["공격대 찾기: 신비의 금고"] = {
			["LFDID"] = 831,
			["Expansion"] = 4,
			["Show"] = "saved",
			["RecLevel"] = 90,
			["Raid"] = true,
		},
		["무작위 판다리아의 안개 영웅 시나리오"] = {
			["Show"] = "saved",
			["Expansion"] = 4,
			["Scenario"] = true,
			["LFDID"] = 641,
			["RecLevel"] = 0,
			["Random"] = true,
			["Raid"] = false,
		},
		["스트라솔름 - 공무용 입구"] = {
			["LFDID"] = 274,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 50,
		},
		["아졸네룹"] = {
			["LFDID"] = 241,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 74,
			["Raid"] = false,
		},
		["보랏빛 요새"] = {
			["LFDID"] = 221,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 76,
			["Raid"] = false,
		},
		["그림 바톨"] = {
			["LFDID"] = 1149,
			["Expansion"] = 3,
			["Show"] = "saved",
			["RecLevel"] = 84,
			["Raid"] = false,
		},
		["영혼약탈자"] = {
			["Show"] = "saved",
			["Expansion"] = 6,
			["WorldBoss"] = 1756,
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["공격대 찾기: 판테온의 권좌"] = {
			["LFDID"] = 1613,
			["Expansion"] = 6,
			["Show"] = "saved",
			["RecLevel"] = 110,
			["Raid"] = true,
		},
		["마라우돈 - 대지노래 폭포"] = {
			["LFDID"] = 273,
			["Expansion"] = 0,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 38,
		},
		["넬타리온의 둥지"] = {
			["LFDID"] = 1207,
			["Expansion"] = 6,
			["Show"] = "saved",
			["Raid"] = false,
			["RecLevel"] = 100,
		},
		["무작위 리치 왕 던전"] = {
			["LFDID"] = 261,
			["Expansion"] = 2,
			["Show"] = "saved",
			["Raid"] = false,
			["Random"] = true,
			["RecLevel"] = 80,
		},
		["투영의 전당"] = {
			["LFDID"] = 256,
			["Expansion"] = 2,
			["Show"] = "saved",
			["RecLevel"] = 80,
			["Raid"] = false,
		},
	},
	["Indicators"] = {
		["R2ClassColor"] = true,
		["D2Indicator"] = "BLANK",
		["R7Color"] = {
			1, -- [1]
			1, -- [2]
			0, -- [3]
		},
		["R5Color"] = {
			0, -- [1]
			0, -- [2]
			1, -- [3]
		},
		["R1Text"] = "KILLED/TOTAL",
		["R4Indicator"] = "BLANK",
		["R1Color"] = {
			0.6, -- [1]
			0.6, -- [2]
			0, -- [3]
		},
		["R0Indicator"] = "BLANK",
		["R8ClassColor"] = true,
		["D2ClassColor"] = true,
		["R4ClassColor"] = true,
		["R6ClassColor"] = true,
		["D2Color"] = {
			0, -- [1]
			1, -- [2]
			0, -- [3]
		},
		["D1Text"] = "KILLED/TOTAL",
		["R0ClassColor"] = true,
		["R1ClassColor"] = true,
		["D1Indicator"] = "BLANK",
		["R2Indicator"] = "BLANK",
		["R8Color"] = {
			1, -- [1]
			0, -- [2]
			0, -- [3]
		},
		["D3ClassColor"] = true,
		["D2Text"] = "KILLED/TOTALH",
		["R6Color"] = {
			0, -- [1]
			1, -- [2]
			0, -- [3]
		},
		["D3Text"] = "KILLED/TOTALM",
		["R4Color"] = {
			1, -- [1]
			0, -- [2]
			0, -- [3]
		},
		["R8Text"] = "KILLED/TOTALM",
		["R7Indicator"] = "BLANK",
		["D3Color"] = {
			1, -- [1]
			0, -- [2]
			0, -- [3]
		},
		["R0Text"] = "KILLED/TOTAL",
		["R0Color"] = {
			0.6, -- [1]
			0.6, -- [2]
			0, -- [3]
		},
		["R6Text"] = "KILLED/TOTAL",
		["R1Indicator"] = "BLANK",
		["R3Indicator"] = "BLANK",
		["R7ClassColor"] = true,
		["R3Text"] = "KILLED/TOTALH",
		["D1ClassColor"] = true,
		["R4Text"] = "KILLED/TOTALH",
		["R3Color"] = {
			1, -- [1]
			1, -- [2]
			0, -- [3]
		},
		["R3ClassColor"] = true,
		["R5Indicator"] = "BLANK",
		["R5ClassColor"] = true,
		["R2Color"] = {
			0.6, -- [1]
			0, -- [2]
			0, -- [3]
		},
		["R8Indicator"] = "BLANK",
		["R2Text"] = "KILLED/TOTAL",
		["D1Color"] = {
			0, -- [1]
			0.6, -- [2]
			0, -- [3]
		},
		["R6Indicator"] = "BLANK",
		["D3Indicator"] = "BLANK",
		["R7Text"] = "KILLED/TOTALH",
		["R5Text"] = "KILLED/TOTAL",
	},
	["History"] = {
	},
	["histGeneration"] = 1364,
	["RealmMap"] = {
		{
			"가로나", -- [1]
			"굴단", -- [2]
			"줄진", -- [3]
		}, -- [1]
		["굴단"] = 1,
		["줄진"] = 1,
		["가로나"] = 1,
	},
	["QuestDB"] = {
		["Daily"] = {
			[41062] = 1052,
			[44101] = 1057,
			[45839] = 1017,
			[45406] = 1017,
			[46182] = 1024,
			[37968] = 945,
			[39582] = 945,
			[48635] = 1170,
			[39586] = 945,
			[38440] = 945,
			[39433] = 945,
			[45572] = 1024,
			[45840] = 1024,
			[36648] = 971,
			[39574] = 945,
			[38045] = 945,
			[37147] = 971,
			[41037] = 1052,
			[43461] = 321,
			[39569] = 945,
			[36679] = 971,
			[44159] = 1014,
			[37146] = 971,
			[46110] = 1017,
		},
		["Darkmoon"] = {
			["expires"] = 1512917940,
		},
		["AccountDaily"] = {
			[40753] = 1068,
			[34774] = 1015,
			[31752] = -1,
		},
		["Weekly"] = {
			[36056] = -1,
			[36058] = -1,
			[40787] = -1,
			[37452] = -1,
			[37454] = -1,
			[37456] = -1,
			[37458] = -1,
			[43510] = -1,
			[40173] = 504,
			[43892] = 1014,
			[43894] = -1,
			[43896] = 1014,
			[46292] = -1,
			[44164] = -1,
			[44166] = -1,
			[33334] = -1,
			[44172] = -1,
			[33338] = -1,
			[37459] = -1,
			[36055] = -1,
			[36057] = -1,
			[48911] = 1170,
			[40786] = 321,
			[49293] = 1135,
			[37453] = -1,
			[37455] = -1,
			[37457] = -1,
			[48799] = 1135,
			[48910] = 1135,
			[39565] = 945,
			[43893] = 1014,
			[47864] = -1,
			[43897] = -1,
			[47851] = -1,
			[45539] = -1,
			[47865] = -1,
			[44167] = 1014,
			[45799] = 1014,
			[44174] = -1,
			["expires"] = 1513810799,
			[32640] = -1,
			[32641] = -1,
			[44171] = 1014,
			[44173] = -1,
			[44175] = 1014,
			[45563] = 951,
			[43895] = 1014,
			[36054] = -1,
			[40168] = 481,
		},
		["AccountWeekly"] = {
			["expires"] = 1513810799,
		},
	},
	["DailyResetTime"] = 1513637999,
	["Quests"] = {
	},
	["MinimapIcon"] = {
		["hide"] = true,
	},
}
