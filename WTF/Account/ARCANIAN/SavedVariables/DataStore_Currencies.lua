
DataStore_CurrenciesDB = {
	["profileKeys"] = {
		["샤이아리 - 줄진"] = "샤이아리 - 줄진",
		["언제나운무 - 아즈샤라"] = "언제나운무 - 아즈샤라",
		["최저방벽 - 줄진"] = "최저방벽 - 줄진",
		["환상큰소 - 아즈샤라"] = "환상큰소 - 아즈샤라",
		["북경스딸 - 줄진"] = "북경스딸 - 줄진",
		["클리엔 - 줄진"] = "클리엔 - 줄진",
		["사나무엇 - 아즈샤라"] = "사나무엇 - 아즈샤라",
		["아르니타 - 아즈샤라"] = "아르니타 - 아즈샤라",
		["릿데 - 아즈샤라"] = "릿데 - 아즈샤라",
		["안녕김밥 - 줄진"] = "안녕김밥 - 줄진",
		["꼬꼬마빠샤 - 줄진"] = "꼬꼬마빠샤 - 줄진",
		["뚱빼미나무 - 줄진"] = "뚱빼미나무 - 줄진",
		["사나없찐 - 아즈샤라"] = "사나없찐 - 아즈샤라",
		["샤이아리 - 아즈샤라"] = "샤이아리 - 아즈샤라",
		["Wilier - 아즈샤라"] = "Wilier - 아즈샤라",
		["Lezyne - 아즈샤라"] = "Lezyne - 아즈샤라",
		["데로사 - 아즈샤라"] = "데로사 - 아즈샤라",
		["환상큰손 - 아즈샤라"] = "환상큰손 - 아즈샤라",
		["샤말밀레 - 아즈샤라"] = "샤말밀레 - 아즈샤라",
		["재연마사 - 줄진"] = "재연마사 - 줄진",
		["Vlaanderen - 아즈샤라"] = "Vlaanderen - 아즈샤라",
		["징박무시하나연 - 줄진"] = "징박무시하나연 - 줄진",
		["Santini - 아즈샤라"] = "Santini - 아즈샤라",
		["써벨로 - 아즈샤라"] = "써벨로 - 아즈샤라",
		["리떼 - 아즈샤라"] = "리떼 - 아즈샤라",
		["Emonda - 줄진"] = "Emonda - 줄진",
		["뽕나인 - 줄진"] = "뽕나인 - 줄진",
		["Emonda - 아즈샤라"] = "Emonda - 아즈샤라",
		["영석썼숑 - 줄진"] = "영석썼숑 - 줄진",
		["사나정연 - 아즈샤라"] = "사나정연 - 아즈샤라",
	},
	["global"] = {
		["Reference"] = {
			["Currencies"] = {
				"드레노어의 전쟁군주|", -- [1]
				"담금질된 운명의 인장|Interface\\Icons\\ability_animusorbs", -- [2]
				"석유|Interface\\Icons\\garrison_oil", -- [3]
				"에펙시스 수정|Interface\\Icons\\inv_apexis_draenor", -- [4]
				"유물 조각|Interface\\Icons\\inv_ashran_artifact", -- [5]
				"주둔지 자원|Interface\\Icons\\inv_garrison_resource", -- [6]
				"필연적인 운명의 인장|Interface\\Icons\\achievement_battleground_templeofkotmogu_02_green", -- [7]
				"기타|", -- [8]
				"다크문 축제 상품권|Interface\\Icons\\inv_misc_ticket_darkmoon_01", -- [9]
				"미식가의 상|Interface\\Icons\\INV_Misc_Ribbon_01", -- [10]
				"아이언포우 징표|Interface\\Icons\\inv_relics_idolofferocity", -- [11]
				"플레이어 대 플레이어|", -- [12]
				"톨 바라드 증표|Interface\\Icons\\achievement_zone_tolbarad", -- [13]
				"판다리아의 안개|", -- [14]
				"영원의 주화|Interface\\Icons\\timelesscoin", -- [15]
				"운명의 모구 룬|Interface\\Icons\\archaeology_5_0_mogucoin", -- [16]
				"전쟁벼림 인장|Interface\\Icons\\inv_arcane_orb", -- [17]
				"행운의 장로 부적|Interface\\Icons\\inv_misc_coin_17", -- [18]
				"행운의 하급 부적|Interface\\Icons\\inv_misc_coin_18", -- [19]
				"대격변|", -- [20]
				"세계수의 징표|Interface\\Icons\\inv_misc_markoftheworldtree", -- [21]
				"암흑의 티끌|Interface\\Icons\\spell_shadow_sealofkings", -- [22]
				"저명한 보석세공사의 징표|Interface\\Icons\\inv_misc_token_argentdawn3", -- [23]
				"타락한 데스윙의 정수|Interface\\Icons\\inv_elemental_primal_shadow", -- [24]
				"리치 왕의 분노|", -- [25]
				"용사의 인장|Interface\\Icons\\Ability_Paladin_ArtofWar", -- [26]
				"달라란 보석세공사의 징표|Interface\\Icons\\INV_Misc_Gem_Variety_01", -- [27]
				"군단|", -- [28]
				"황천의 파편|Interface\\Icons\\inv_datacrystal01", -- [29]
				"던전 및 공격대|", -- [30]
				"용맹 점수|Interface\\Icons\\pvecurrency-valor", -- [31]
				"연맹 자원|Interface\\Icons\\inv_orderhall_orderresources", -- [32]
				"고대 마나|Interface\\Icons\\inv_misc_ancient_mana", -- [33]
				"보이지 않는 눈|Interface\\Icons\\achievement_reputation_kirintor_offensive", -- [34]
				"수수께끼의 동전|Interface\\Icons\\inv_misc_elvencoins", -- [35]
				"부서진 운명의 인장|Interface\\Icons\\inv_misc_elvencoins", -- [36]
				"시간왜곡의 훈장|Interface\\Icons\\pvecurrency-justice", -- [37]
				"머무는 영혼 파편|Interface\\Icons\\spell_warlock_demonsoul", -- [38]
				"군단척결 전쟁 보급품|Interface\\Icons\\inv_misc_summonable_boss_token", -- [39]
				"비틀린 정수|893779", -- [40]
				"장막의 아르거나이트|1064188", -- [41]
				"아르거스 차원석|399041", -- [42]
			},
			["CurrencyTextRev"] = {
				["행운의 장로 부적"] = 18,
				["군단"] = 28,
				["담금질된 운명의 인장"] = 2,
				["기타"] = 8,
				["저명한 보석세공사의 징표"] = 23,
				["영원의 주화"] = 15,
				["유물 조각"] = 5,
				["다크문 축제 상품권"] = 9,
				["던전 및 공격대"] = 30,
				["암흑의 티끌"] = 22,
				["톨 바라드 증표"] = 13,
				["필연적인 운명의 인장"] = 7,
				["연맹 자원"] = 32,
				["운명의 모구 룬"] = 16,
				["용맹 점수"] = 31,
				["플레이어 대 플레이어"] = 12,
				["행운의 하급 부적"] = 19,
				["부서진 운명의 인장"] = 36,
				["에펙시스 수정"] = 4,
				["타락한 데스윙의 정수"] = 24,
				["시간왜곡의 훈장"] = 37,
				["전쟁벼림 인장"] = 17,
				["군단척결 전쟁 보급품"] = 39,
				["머무는 영혼 파편"] = 38,
				["아르거스 차원석"] = 42,
				["주둔지 자원"] = 6,
				["수수께끼의 동전"] = 35,
				["대격변"] = 20,
				["리치 왕의 분노"] = 25,
				["황천의 파편"] = 29,
				["석유"] = 3,
				["달라란 보석세공사의 징표"] = 27,
				["보이지 않는 눈"] = 34,
				["판다리아의 안개"] = 14,
				["드레노어의 전쟁군주"] = 1,
				["고대 마나"] = 33,
				["아이언포우 징표"] = 11,
				["비틀린 정수"] = 40,
				["세계수의 징표"] = 21,
				["장막의 아르거나이트"] = 41,
				["미식가의 상"] = 10,
				["용사의 인장"] = 26,
			},
		},
		["Characters"] = {
			["Default.줄진.뽕나인"] = {
				["Currencies"] = {
					3, -- [1]
					516, -- [2]
					1482376, -- [3]
					819468, -- [4]
					17, -- [5]
					150, -- [6]
					29, -- [7]
					3807774, -- [8]
					32, -- [9]
					930, -- [10]
					1828, -- [11]
					5798, -- [12]
					41, -- [13]
					1962, -- [14]
				},
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					72, -- [4]
					79, -- [5]
					138, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["lastUpdate"] = 1478599465,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-10-3000",
					[390] = "0-0-0-0",
					[823] = "11581-0-0-0",
					[392] = "0-0-0-0",
					[824] = "6402-0-0-10000",
					[994] = "4-0-0-20",
				},
			},
			["Default.줄진.북경스딸"] = {
				["lastUpdate"] = 1478598113,
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					0, -- [4]
					0, -- [5]
					0, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["Currencies"] = {
					3, -- [1]
					9224, -- [2]
					364556, -- [3]
				},
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-0-0",
					[390] = "0-0-0-0",
					[823] = "72-0-0-0",
					[392] = "0-0-0-0",
					[824] = "2848-0-0-10000",
					[994] = "0-0-0-0",
				},
			},
			["Default.줄진.영석썼숑"] = {
				["Currencies"] = {
					3, -- [1]
					1156, -- [2]
					1932296, -- [3]
					1024908, -- [4]
					17, -- [5]
					790, -- [6]
					29, -- [7]
					1893662, -- [8]
					32, -- [9]
					1314, -- [10]
					2342, -- [11]
				},
				["lastUpdate"] = 1478600082,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-10-3000",
					[390] = "0-0-0-0",
					[823] = "15096-0-0-0",
					[392] = "0-0-0-0",
					[824] = "8007-0-0-10000",
					[994] = "9-0-0-20",
				},
			},
			["Default.아즈샤라.데로사"] = {
				["lastUpdate"] = 1494303185,
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					0, -- [4]
					0, -- [5]
					0, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["Currencies"] = {
					57, -- [1]
					220480, -- [2]
					6458, -- [3]
					61, -- [4]
					44874, -- [5]
					3, -- [6]
					23176, -- [7]
					187532, -- [8]
				},
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-0-0",
					[1220] = "1722-0-0-0",
					[824] = "1465-0-0-10000",
					[1273] = "0-0-0-0",
					[1191] = "0-0-0-0",
					[823] = "181-0-0-0",
					[1226] = "50-0-0-0",
					[392] = "0-0-0-0",
					[994] = "0-0-0-0",
					[390] = "0-0-0-0",
					[1342] = "0-0-0-0",
				},
			},
			["Default.아즈샤라.사나정연"] = {
				["lastUpdate"] = 1513569252,
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					0, -- [4]
					0, -- [5]
					0, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[1220] = "0-0-0-0",
					[824] = "0-0-0-0",
					[1273] = "0-0-0-0",
					[1191] = "0-0-0-0",
					[823] = "0-0-0-0",
					[1226] = "0-0-0-0",
					[994] = "0-0-0-0",
					[1342] = "0-0-0-0",
				},
			},
			["Default.줄진.뚱빼미나무"] = {
				["Currencies"] = {
					3, -- [1]
					1284, -- [2]
					1456008, -- [3]
					455180, -- [4]
					17, -- [5]
					150, -- [6]
					29, -- [7]
					1664926, -- [8]
					32, -- [9]
					418, -- [10]
					5158, -- [11]
					41, -- [12]
					1962, -- [13]
					51, -- [14]
					2356, -- [15]
				},
				["lastUpdate"] = 1471189673,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-10-3000",
					[390] = "0-0-0-0",
					[823] = "11375-0-0-0",
					[392] = "0-0-0-0",
					[994] = "10-0-0-20",
					[824] = "3556-0-0-10000",
				},
			},
			["Default.아즈샤라.Wilier"] = {
				["Currencies"] = {
					57, -- [1]
					7226, -- [2]
				},
				["lastUpdate"] = 1488810464,
				["CurrencyInfo"] = {
					[824] = "0-0-0-0",
					[396] = "0-0-0-0",
					[1220] = "0-0-0-0",
					[823] = "0-0-0-0",
					[1273] = "0-0-0-0",
					[994] = "0-0-0-0",
					[395] = "0-0-0-0",
				},
			},
			["Default.줄진.최저방벽"] = {
				["Currencies"] = {
					3, -- [1]
					1284, -- [2]
					1162632, -- [3]
					760588, -- [4]
					17, -- [5]
					22, -- [6]
					29, -- [7]
					3195678, -- [8]
					32, -- [9]
					1314, -- [10]
					2214, -- [11]
				},
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					30, -- [4]
					17, -- [5]
					18, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["lastUpdate"] = 1494291818,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-10-3000",
					[1220] = "0-0-0-0",
					[824] = "5942-0-0-10000",
					[1273] = "0-0-0-0",
					[1191] = "0-0-0-0",
					[823] = "9083-0-0-0",
					[1226] = "0-0-0-0",
					[392] = "0-0-0-0",
					[994] = "10-0-0-20",
					[390] = "0-0-0-0",
					[1342] = "0-0-0-0",
				},
			},
			["Default.줄진.Emonda"] = {
				["lastUpdate"] = 1471190063,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-0-0",
					[390] = "0-0-0-0",
					[823] = "0-0-0-0",
					[392] = "0-0-0-0",
					[994] = "0-0-0-0",
					[824] = "0-0-0-0",
				},
			},
			["Default.아즈샤라.Emonda"] = {
				["lastUpdate"] = 1513310648,
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					0, -- [4]
					0, -- [5]
					0, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["Currencies"] = {
					57, -- [1]
					155202, -- [2]
					3278, -- [3]
					5196, -- [4]
					88004, -- [5]
					712, -- [6]
					8006, -- [7]
					4367552, -- [8]
					356282, -- [9]
					61, -- [10]
					370634, -- [11]
					19262, -- [12]
					3, -- [13]
					646, -- [14]
					173448, -- [15]
					4876, -- [16]
				},
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-0-0",
					[1220] = "34121-0-0-0",
					[824] = "38-0-0-10000",
					[1273] = "5-0-0-6",
					[1191] = "150-0-0-5000",
					[823] = "1355-0-0-0",
					[1226] = "2783-0-0-0",
					[392] = "0-0-0-0",
					[994] = "0-0-0-0",
					[390] = "0-0-0-0",
					[1342] = "25-0-0-1000",
				},
			},
			["Default.아즈샤라.리떼"] = {
				["Currencies"] = {
					57, -- [1]
					217666, -- [2]
					9294, -- [3]
					3404, -- [4]
					840, -- [5]
					8390, -- [6]
					6023104, -- [7]
					36306, -- [8]
					1540410, -- [9]
					61, -- [10]
					171594, -- [11]
					3, -- [12]
					38412, -- [13]
				},
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					0, -- [4]
					0, -- [5]
					0, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["lastUpdate"] = 1513606694,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-0-0",
					[1220] = "47055-0-0-0",
					[824] = "300-0-0-10000",
					[1273] = "6-0-0-6",
					[1191] = "0-0-0-0",
					[823] = "0-0-0-0",
					[1226] = "12034-0-0-0",
					[392] = "0-0-0-0",
					[994] = "0-0-0-0",
					[390] = "0-0-0-0",
					[1342] = "72-0-0-1000",
				},
			},
			["Default.아즈샤라.아르니타"] = {
				["lastUpdate"] = 1505653981,
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					52, -- [4]
					57, -- [5]
					78, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					20, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					20, -- [15]
					9, -- [16]
					0, -- [17]
					38, -- [18]
				},
				["Currencies"] = {
					57, -- [1]
					578, -- [2]
					840, -- [3]
					3264, -- [4]
					17, -- [5]
					1938, -- [6]
					1812, -- [7]
					150, -- [8]
					61, -- [9]
					19914, -- [10]
					25, -- [11]
					794, -- [12]
					3, -- [13]
					516, -- [14]
					1471880, -- [15]
					10380, -- [16]
					29, -- [17]
					4022302, -- [18]
					32, -- [19]
					930, -- [20]
					2340, -- [21]
					2342, -- [22]
					41, -- [23]
					2090, -- [24]
					44, -- [25]
					51, -- [26]
					54, -- [27]
					1972, -- [28]
				},
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-10-3000",
					[1220] = "25-0-0-0",
					[824] = "81-0-0-10000",
					[1273] = "6-0-0-6",
					[1191] = "0-0-0-0",
					[823] = "11499-0-0-0",
					[1226] = "0-0-0-0",
					[994] = "4-0-0-20",
					[1342] = "0-0-0-0",
				},
			},
			["Default.줄진.안녕김밥"] = {
				["lastUpdate"] = 1471189983,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-0-0",
					[390] = "0-0-0-0",
					[823] = "0-0-0-0",
					[392] = "0-0-0-0",
					[994] = "0-0-0-0",
					[824] = "0-0-0-0",
				},
			},
			["Default.아즈샤라.Vlaanderen"] = {
				["lastUpdate"] = 1505652736,
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					0, -- [4]
					0, -- [5]
					0, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["Currencies"] = {
					57, -- [1]
					70466, -- [2]
					12110, -- [3]
					1100, -- [4]
					712, -- [5]
					2630, -- [6]
					199488, -- [7]
					201658, -- [8]
					61, -- [9]
					194634, -- [10]
					3, -- [11]
					12812, -- [12]
				},
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-0-0",
					[1220] = "1558-0-0-0",
					[824] = "100-0-0-10000",
					[1273] = "5-0-0-6",
					[1191] = "0-0-0-0",
					[823] = "0-0-0-0",
					[1226] = "1575-0-0-0",
					[392] = "0-0-0-0",
					[994] = "0-0-0-0",
					[390] = "0-0-0-0",
					[1342] = "94-0-0-1000",
				},
			},
			["Default.줄진.꼬꼬마빠샤"] = {
				["Currencies"] = {
					3, -- [1]
					516, -- [2]
					1466888, -- [3]
					615820, -- [4]
					17, -- [5]
					1938, -- [6]
					1812, -- [7]
					150, -- [8]
					25, -- [9]
					794, -- [10]
					29, -- [11]
					4022302, -- [12]
					32, -- [13]
					930, -- [14]
					2340, -- [15]
					2342, -- [16]
					41, -- [17]
					2090, -- [18]
					44, -- [19]
					51, -- [20]
					54, -- [21]
					1972, -- [22]
				},
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					52, -- [4]
					57, -- [5]
					78, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					20, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					20, -- [15]
					9, -- [16]
					0, -- [17]
					38, -- [18]
				},
				["lastUpdate"] = 1480651844,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-10-3000",
					[1220] = "0-0-0-0",
					[824] = "4811-0-0-10000",
					[1273] = "0-0-0-0",
					[823] = "11460-0-0-0",
					[392] = "0-0-0-0",
					[994] = "4-0-0-20",
					[390] = "0-0-0-0",
				},
			},
			["Default.줄진.클리엔"] = {
				["Currencies"] = {
					57, -- [1]
					61888, -- [2]
					17, -- [3]
					2578, -- [4]
					1044, -- [5]
					3222, -- [6]
					25, -- [7]
					3994, -- [8]
					3, -- [9]
					1156, -- [10]
					258566, -- [11]
					6174984, -- [12]
					10, -- [13]
					1107212, -- [14]
					142, -- [15]
					29, -- [16]
					2468766, -- [17]
					32, -- [18]
					1058, -- [19]
					1956, -- [20]
					3494, -- [21]
					41, -- [22]
					6826, -- [23]
					44, -- [24]
					46, -- [25]
					48, -- [26]
					51, -- [27]
					1972, -- [28]
				},
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					114, -- [4]
					48, -- [5]
					79, -- [6]
					0, -- [7]
					32, -- [8]
					0, -- [9]
					0, -- [10]
					7, -- [11]
					0, -- [12]
					0, -- [13]
					5, -- [14]
					3, -- [15]
					2, -- [16]
					0, -- [17]
					43, -- [18]
				},
				["lastUpdate"] = 1493734086,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-10-3000",
					[1220] = "483-0-0-0",
					[824] = "8650-0-0-10000",
					[1273] = "0-0-0-0",
					[1191] = "0-0-0-0",
					[823] = "48242-0-0-0",
					[1226] = "0-0-0-0",
					[392] = "0-0-0-0",
					[994] = "9-0-0-20",
					[390] = "0-0-0-0",
					[1342] = "0-0-0-0",
				},
			},
			["Default.아즈샤라.샤이아리"] = {
				["Currencies"] = {
					57, -- [1]
					142018, -- [2]
					11086, -- [3]
					4044, -- [4]
					28228, -- [5]
					840, -- [6]
					6854, -- [7]
					3888064, -- [8]
					45522, -- [9]
					2337722, -- [10]
					61, -- [11]
					279754, -- [12]
					3, -- [13]
					102412, -- [14]
				},
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					0, -- [4]
					0, -- [5]
					0, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["lastUpdate"] = 1513571342,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-0-0",
					[1220] = "30375-0-0-0",
					[824] = "800-0-0-10000",
					[1273] = "6-0-0-6",
					[1191] = "0-0-0-0",
					[823] = "0-0-0-0",
					[1226] = "18263-0-0-0",
					[994] = "0-0-0-0",
					[1342] = "86-0-0-1000",
				},
			},
			["Default.줄진.재연마사"] = {
				["Currencies"] = {
					57, -- [1]
					139456, -- [2]
					17, -- [3]
					150, -- [4]
					25, -- [5]
					794, -- [6]
					3, -- [7]
					644, -- [8]
					1094280, -- [9]
					209932, -- [10]
					29, -- [11]
					1182878, -- [12]
					288, -- [13]
					418, -- [14]
					420, -- [15]
					4006, -- [16]
					41, -- [17]
					1962, -- [18]
				},
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					35, -- [4]
					40, -- [5]
					25, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["lastUpdate"] = 1478598376,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-10-3000",
					[390] = "0-0-0-0",
					[823] = "8549-0-0-0",
					[392] = "0-0-0-0",
					[994] = "5-0-0-20",
					[824] = "1640-0-0-10000",
				},
			},
			["Default.아즈샤라.샤말밀레"] = {
				["Currencies"] = {
					57, -- [1]
					18498, -- [2]
					840, -- [3]
					582, -- [4]
					172608, -- [5]
					17, -- [6]
					3218, -- [7]
					150, -- [8]
					61, -- [9]
					32074, -- [10]
					25, -- [11]
					7066, -- [12]
					3, -- [13]
					1284, -- [14]
					1646216, -- [15]
					1164, -- [16]
					29, -- [17]
					2490398, -- [18]
					32, -- [19]
					546, -- [20]
					1316, -- [21]
					3622, -- [22]
					41, -- [23]
					44, -- [24]
					3886, -- [25]
					48, -- [26]
					51, -- [27]
					1204, -- [28]
				},
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					77, -- [4]
					104, -- [5]
					142, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["lastUpdate"] = 1513497776,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-10-3000",
					[1220] = "1348-0-0-0",
					[824] = "9-0-0-10000",
					[1273] = "6-0-0-6",
					[1191] = "0-0-0-0",
					[823] = "12861-0-0-0",
					[1226] = "0-0-0-0",
					[392] = "0-0-0-0",
					[994] = "10-0-0-20",
					[1342] = "0-0-0-0",
					[390] = "0-0-0-0",
				},
			},
			["Default.줄진.징박무시하나연"] = {
				["Currencies"] = {
					3, -- [1]
					1284, -- [2]
					1639944, -- [3]
					1003916, -- [4]
					17, -- [5]
					3218, -- [6]
					150, -- [7]
					25, -- [8]
					7066, -- [9]
					29, -- [10]
					2490398, -- [11]
					32, -- [12]
					546, -- [13]
					1316, -- [14]
					3622, -- [15]
					41, -- [16]
					44, -- [17]
					3886, -- [18]
					48, -- [19]
					51, -- [20]
					1204, -- [21]
				},
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					77, -- [4]
					104, -- [5]
					142, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["lastUpdate"] = 1478600440,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-10-3000",
					[390] = "0-0-0-0",
					[823] = "12812-0-0-0",
					[392] = "0-0-0-0",
					[994] = "10-0-0-20",
					[824] = "7843-0-0-10000",
				},
			},
			["Default.아즈샤라.Santini"] = {
				["Currencies"] = {
					57, -- [1]
					46658, -- [2]
					1348, -- [3]
					712, -- [4]
					710, -- [5]
					675392, -- [6]
					83258, -- [7]
					61, -- [8]
					110154, -- [9]
				},
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					0, -- [4]
					0, -- [5]
					0, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["lastUpdate"] = 1505653061,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-0-0",
					[1220] = "5276-0-0-0",
					[824] = "0-0-0-0",
					[1273] = "5-0-0-6",
					[1191] = "0-0-0-0",
					[823] = "0-0-0-0",
					[1226] = "650-0-0-0",
					[392] = "0-0-0-0",
					[994] = "0-0-0-0",
					[390] = "0-0-0-0",
					[1342] = "0-0-0-0",
				},
			},
			["Default.아즈샤라.사나없찐"] = {
				["lastUpdate"] = 1488375768,
				["CurrencyInfo"] = {
					[824] = "0-0-0-0",
					[396] = "0-0-0-0",
					[1220] = "0-0-0-0",
					[823] = "0-0-0-0",
					[1273] = "0-0-0-0",
					[994] = "0-0-0-0",
					[395] = "0-0-0-0",
				},
			},
			["Default.아즈샤라.Lezyne"] = {
				["Currencies"] = {
					57, -- [1]
					154304, -- [2]
					17, -- [3]
					150, -- [4]
					61, -- [5]
					44874, -- [6]
					3, -- [7]
					1284, -- [8]
					1456008, -- [9]
					455180, -- [10]
					29, -- [11]
					1664926, -- [12]
					32, -- [13]
					418, -- [14]
					5158, -- [15]
					41, -- [16]
					1962, -- [17]
					51, -- [18]
					2356, -- [19]
				},
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					3, -- [4]
					16, -- [5]
					9, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["lastUpdate"] = 1494311964,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-10-3000",
					[1220] = "1205-0-0-0",
					[824] = "3556-0-0-10000",
					[1273] = "0-0-0-0",
					[1191] = "0-0-0-0",
					[823] = "11375-0-0-0",
					[1226] = "0-0-0-0",
					[392] = "0-0-0-0",
					[994] = "10-0-0-20",
					[390] = "0-0-0-0",
					[1342] = "0-0-0-0",
				},
			},
			["Default.아즈샤라.릿데"] = {
				["Currencies"] = {
					57, -- [1]
					12866, -- [2]
					3534, -- [3]
					584, -- [4]
					454, -- [5]
					520128, -- [6]
					545082, -- [7]
					17, -- [8]
					22, -- [9]
					3, -- [10]
					1284, -- [11]
					1239432, -- [12]
					808588, -- [13]
					29, -- [14]
					3195678, -- [15]
					32, -- [16]
					1314, -- [17]
					2214, -- [18]
				},
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					30, -- [4]
					17, -- [5]
					18, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					0, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					0, -- [15]
					0, -- [16]
					0, -- [17]
					0, -- [18]
				},
				["lastUpdate"] = 1513309860,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[1220] = "4063-0-0-0",
					[824] = "6317-0-0-10000",
					[1273] = "4-0-0-6",
					[1191] = "0-0-0-0",
					[823] = "9683-0-0-0",
					[1226] = "4258-0-0-0",
					[994] = "10-0-0-20",
					[1342] = "27-0-0-1000",
				},
			},
			["Default.줄진.샤이아리"] = {
				["Currencies"] = {
					3, -- [1]
					1284, -- [2]
					1858568, -- [3]
					869900, -- [4]
					17, -- [5]
					658, -- [6]
					3988, -- [7]
					278, -- [8]
					25, -- [9]
					1562, -- [10]
					29, -- [11]
					5356446, -- [12]
					32, -- [13]
					1058, -- [14]
					2084, -- [15]
					7334, -- [16]
					41, -- [17]
					3242, -- [18]
					44, -- [19]
					48, -- [20]
					51, -- [21]
					3892, -- [22]
				},
				["Archeology"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					118, -- [4]
					131, -- [5]
					109, -- [6]
					0, -- [7]
					0, -- [8]
					0, -- [9]
					0, -- [10]
					26, -- [11]
					0, -- [12]
					0, -- [13]
					0, -- [14]
					19, -- [15]
					45, -- [16]
					0, -- [17]
					30, -- [18]
				},
				["lastUpdate"] = 1493734015,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[396] = "0-0-10-3000",
					[1220] = "0-0-0-0",
					[824] = "6796-0-0-10000",
					[1273] = "0-0-0-0",
					[1191] = "0-0-0-0",
					[823] = "14520-0-0-0",
					[1226] = "0-0-0-0",
					[392] = "0-0-0-0",
					[994] = "10-0-0-20",
					[1342] = "0-0-0-0",
					[390] = "0-0-0-0",
				},
			},
			["Default.아즈샤라.사나무엇"] = {
				["lastUpdate"] = 1513424512,
				["CurrencyInfo"] = {
					[395] = "0-0-0-0",
					[1220] = "0-0-0-0",
					[824] = "0-0-0-0",
					[1273] = "0-0-0-0",
					[1191] = "0-0-0-0",
					[823] = "0-0-0-0",
					[1226] = "0-0-0-0",
					[994] = "0-0-0-0",
					[1342] = "0-0-0-0",
				},
			},
		},
	},
}
