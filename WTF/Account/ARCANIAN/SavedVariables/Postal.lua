
Postal3DB = {
	["global"] = {
		["BlackBook"] = {
			["alts"] = {
				"Emonda|아즈샤라|Horde|110|SHAMAN", -- [1]
				"Emonda|아즈샤라|Neutral|1|SHAMAN", -- [2]
				"Emonda|줄진|Alliance|99|DEMONHUNTER", -- [3]
				"Lezyne|아즈샤라|Horde|104|DRUID", -- [4]
				"Santini|아즈샤라|Horde|110|WARLOCK", -- [5]
				"Vlaanderen|아즈샤라|Horde|110|HUNTER", -- [6]
				"Wilier|아즈샤라|Horde|26|PALADIN", -- [7]
				"Wilier|아즈샤라|Neutral|2|SHAMAN", -- [8]
				"꼬꼬마빠샤|줄진|Alliance|100|ROGUE", -- [9]
				"데로사|아즈샤라|Horde|102|DEMONHUNTER", -- [10]
				"뚱빼미나무|줄진|Alliance|100|DRUID", -- [11]
				"리떼|아즈샤라|Horde|110|MAGE", -- [12]
				"릿데|아즈샤라|Horde|110|WARRIOR", -- [13]
				"북경스딸|줄진|Alliance|100|MONK", -- [14]
				"뽕나인|줄진|Alliance|100|SHAMAN", -- [15]
				"사나무엇|아즈샤라|Horde|3|MONK", -- [16]
				"사나무엇|아즈샤라|Neutral|4|MONK", -- [17]
				"사나없찐|아즈샤라|Horde|1|MONK", -- [18]
				"사나정연|아즈샤라|Horde|17|MONK", -- [19]
				"사나정연|아즈샤라|Neutral|12|MONK", -- [20]
				"샤말밀레|아즈샤라|Horde|110|PALADIN", -- [21]
				"샤이아리|아즈샤라|Horde|110|PRIEST", -- [22]
				"샤이아리|줄진|Alliance|100|MAGE", -- [23]
				"써벨로|아즈샤라|Horde|110|PRIEST", -- [24]
				"아르니타|아즈샤라|Horde|110|ROGUE", -- [25]
				"안녕김밥|줄진|Alliance|90|DEATHKNIGHT", -- [26]
				"언제나운무|아즈샤라|Neutral|1|MONK", -- [27]
				"영석썼숑|줄진|Alliance|100|WARLOCK", -- [28]
				"재연마사|줄진|Alliance|103|HUNTER", -- [29]
				"징박무시하나연|줄진|Alliance|100|PALADIN", -- [30]
				"최저방벽|줄진|Alliance|100|WARRIOR", -- [31]
				"클리엔|줄진|Alliance|101|PRIEST", -- [32]
				"환상큰소|아즈샤라|Horde|35|DRUID", -- [33]
				"환상큰손|아즈샤라|Horde|102|HUNTER", -- [34]
			},
		},
	},
	["profileKeys"] = {
		["샤이아리 - 줄진"] = "샤이아리 - 줄진",
		["언제나운무 - 아즈샤라"] = "언제나운무 - 아즈샤라",
		["최저방벽 - 줄진"] = "최저방벽 - 줄진",
		["환상큰소 - 아즈샤라"] = "환상큰소 - 아즈샤라",
		["북경스딸 - 줄진"] = "북경스딸 - 줄진",
		["클리엔 - 줄진"] = "클리엔 - 줄진",
		["사나무엇 - 아즈샤라"] = "사나무엇 - 아즈샤라",
		["아르니타 - 아즈샤라"] = "아르니타 - 아즈샤라",
		["릿데 - 아즈샤라"] = "릿데 - 아즈샤라",
		["안녕김밥 - 줄진"] = "안녕김밥 - 줄진",
		["꼬꼬마빠샤 - 줄진"] = "꼬꼬마빠샤 - 줄진",
		["뚱빼미나무 - 줄진"] = "뚱빼미나무 - 줄진",
		["사나없찐 - 아즈샤라"] = "사나없찐 - 아즈샤라",
		["샤이아리 - 아즈샤라"] = "샤이아리 - 아즈샤라",
		["Wilier - 아즈샤라"] = "Wilier - 아즈샤라",
		["Lezyne - 아즈샤라"] = "Lezyne - 아즈샤라",
		["재연마사 - 줄진"] = "재연마사 - 줄진",
		["환상큰손 - 아즈샤라"] = "환상큰손 - 아즈샤라",
		["샤말밀레 - 아즈샤라"] = "샤말밀레 - 아즈샤라",
		["데로사 - 아즈샤라"] = "데로사 - 아즈샤라",
		["Emonda - 아즈샤라"] = "Emonda - 아즈샤라",
		["징박무시하나연 - 줄진"] = "징박무시하나연 - 줄진",
		["Santini - 아즈샤라"] = "Santini - 아즈샤라",
		["써벨로 - 아즈샤라"] = "써벨로 - 아즈샤라",
		["리떼 - 아즈샤라"] = "리떼 - 아즈샤라",
		["Emonda - 줄진"] = "Emonda - 줄진",
		["뽕나인 - 줄진"] = "뽕나인 - 줄진",
		["Vlaanderen - 아즈샤라"] = "Vlaanderen - 아즈샤라",
		["영석썼숑 - 줄진"] = "영석썼숑 - 줄진",
		["사나정연 - 아즈샤라"] = "사나정연 - 아즈샤라",
	},
	["profiles"] = {
		["샤이아리 - 줄진"] = {
			["BlackBook"] = {
				["recent"] = {
					"징박무시하나연|줄진|Alliance", -- [1]
					"클리엔|줄진|Alliance", -- [2]
					"뽕나인|줄진|Alliance", -- [3]
					"북경스딸|줄진|Alliance", -- [4]
				},
			},
		},
		["언제나운무 - 아즈샤라"] = {
		},
		["최저방벽 - 줄진"] = {
			["BlackBook"] = {
				["recent"] = {
					"징박무시하나연|줄진|Alliance", -- [1]
				},
			},
		},
		["환상큰소 - 아즈샤라"] = {
		},
		["북경스딸 - 줄진"] = {
			["BlackBook"] = {
				["recent"] = {
					"징박무시하나연|줄진|Alliance", -- [1]
				},
			},
		},
		["클리엔 - 줄진"] = {
			["BlackBook"] = {
				["recent"] = {
					"징박무시하나연|줄진|Alliance", -- [1]
					"샤이아리|줄진|Alliance", -- [2]
				},
			},
		},
		["사나무엇 - 아즈샤라"] = {
		},
		["아르니타 - 아즈샤라"] = {
			["BlackBook"] = {
				["recent"] = {
					"리떼|아즈샤라|Horde", -- [1]
				},
			},
		},
		["릿데 - 아즈샤라"] = {
			["BlackBook"] = {
				["recent"] = {
					"샤말밀레|아즈샤라|Horde", -- [1]
				},
			},
		},
		["안녕김밥 - 줄진"] = {
		},
		["꼬꼬마빠샤 - 줄진"] = {
			["BlackBook"] = {
				["recent"] = {
					"징박무시하나연|줄진|Alliance", -- [1]
				},
			},
		},
		["뚱빼미나무 - 줄진"] = {
		},
		["사나없찐 - 아즈샤라"] = {
		},
		["샤이아리 - 아즈샤라"] = {
			["BlackBook"] = {
				["recent"] = {
					"아르니타|아즈샤라|Horde", -- [1]
					"Vlaanderen|아즈샤라|Horde", -- [2]
					"샤말밀레|아즈샤라|Horde", -- [3]
					"릿데|아즈샤라|Horde", -- [4]
					"Santini|아즈샤라|Horde", -- [5]
					"Emonda|아즈샤라|Horde", -- [6]
					"리떼|아즈샤라|Horde", -- [7]
				},
			},
		},
		["Wilier - 아즈샤라"] = {
		},
		["Lezyne - 아즈샤라"] = {
			["BlackBook"] = {
				["recent"] = {
					"릿데|아즈샤라|Horde", -- [1]
					"Emonda|아즈샤라|Horde", -- [2]
					"리떼|아즈샤라|Horde", -- [3]
				},
			},
		},
		["재연마사 - 줄진"] = {
			["BlackBook"] = {
				["recent"] = {
					"징박무시하나연|줄진|Alliance", -- [1]
				},
			},
		},
		["환상큰손 - 아즈샤라"] = {
		},
		["샤말밀레 - 아즈샤라"] = {
			["BlackBook"] = {
				["recent"] = {
					"릿데|아즈샤라|Horde", -- [1]
					"리떼|아즈샤라|Horde", -- [2]
					"Emonda|아즈샤라|Horde", -- [3]
					"써벨로|아즈샤라|Horde", -- [4]
					"Vlaanderen|아즈샤라|Horde", -- [5]
					"Santini|아즈샤라|Horde", -- [6]
					"Lezyne|아즈샤라|Horde", -- [7]
				},
			},
		},
		["데로사 - 아즈샤라"] = {
			["BlackBook"] = {
				["recent"] = {
					"Emonda|아즈샤라|Horde", -- [1]
					"리떼|아즈샤라|Horde", -- [2]
					"써벨로|아즈샤라|Horde", -- [3]
					"Santini|아즈샤라|Horde", -- [4]
				},
			},
		},
		["Emonda - 아즈샤라"] = {
			["BlackBook"] = {
				["recent"] = {
					"리떼|아즈샤라|Horde", -- [1]
					"샤이아리|아즈샤라|Horde", -- [2]
					"릿데|아즈샤라|Horde", -- [3]
					"Vlaanderen|아즈샤라|Horde", -- [4]
					"써벨로|아즈샤라|Horde", -- [5]
					"샤말밀레|아즈샤라|Horde", -- [6]
					"아르니타|아즈샤라|Horde", -- [7]
					"Santini|아즈샤라|Horde", -- [8]
				},
			},
		},
		["징박무시하나연 - 줄진"] = {
		},
		["Santini - 아즈샤라"] = {
			["BlackBook"] = {
				["recent"] = {
					"리떼|아즈샤라|Horde", -- [1]
					"Vlaanderen|아즈샤라|Horde", -- [2]
					"써벨로|아즈샤라|Horde", -- [3]
					"Emonda|아즈샤라|Horde", -- [4]
				},
			},
		},
		["써벨로 - 아즈샤라"] = {
			["BlackBook"] = {
				["recent"] = {
					"리떼|아즈샤라|Horde", -- [1]
					"Emonda|아즈샤라|Horde", -- [2]
					"Vlaanderen|아즈샤라|Horde", -- [3]
					"샤말밀레|아즈샤라|Horde", -- [4]
					"아르니타|아즈샤라|Horde", -- [5]
					"Santini|아즈샤라|Horde", -- [6]
				},
			},
		},
		["리떼 - 아즈샤라"] = {
			["BlackBook"] = {
				["recent"] = {
					"샤말밀레|아즈샤라|Horde", -- [1]
					"Vlaanderen|아즈샤라|Horde", -- [2]
					"Santini|아즈샤라|Horde", -- [3]
					"아르니타|아즈샤라|Horde", -- [4]
					"샤이아리|아즈샤라|Horde", -- [5]
					"릿데|아즈샤라|Horde", -- [6]
					"Emonda|아즈샤라|Horde", -- [7]
					"Lezyne|아즈샤라|Horde", -- [8]
					"이게멍미|아즈샤라|Horde", -- [9]
					"써벨로|아즈샤라|Horde", -- [10]
				},
			},
		},
		["Emonda - 줄진"] = {
		},
		["뽕나인 - 줄진"] = {
			["BlackBook"] = {
				["recent"] = {
					"징박무시하나연|줄진|Alliance", -- [1]
					"클리엔|줄진|Alliance", -- [2]
				},
			},
		},
		["Vlaanderen - 아즈샤라"] = {
			["BlackBook"] = {
				["recent"] = {
					"릿데|아즈샤라|Horde", -- [1]
					"Emonda|아즈샤라|Horde", -- [2]
					"리떼|아즈샤라|Horde", -- [3]
					"샤이아리|아즈샤라|Horde", -- [4]
					"써벨로|아즈샤라|Horde", -- [5]
					"Lezyne|아즈샤라|Horde", -- [6]
				},
			},
		},
		["영석썼숑 - 줄진"] = {
			["BlackBook"] = {
				["recent"] = {
					"징박무시하나연|줄진|Alliance", -- [1]
				},
			},
		},
		["사나정연 - 아즈샤라"] = {
		},
	},
}
