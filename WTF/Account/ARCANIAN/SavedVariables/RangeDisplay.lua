
RangeDisplayDB3 = {
	["namespaces"] = {
		["LibDualSpec-1.0"] = {
		},
	},
	["profileKeys"] = {
		["샤이아리 - 줄진"] = "Default",
		["언제나운무 - 아즈샤라"] = "Default",
		["최저방벽 - 줄진"] = "Default",
		["환상큰소 - 아즈샤라"] = "Default",
		["북경스딸 - 줄진"] = "Default",
		["클리엔 - 줄진"] = "Default",
		["사나무엇 - 아즈샤라"] = "Default",
		["아르니타 - 아즈샤라"] = "Default",
		["릿데 - 아즈샤라"] = "Default",
		["안녕김밥 - 줄진"] = "Default",
		["꼬꼬마빠샤 - 줄진"] = "Default",
		["뚱빼미나무 - 줄진"] = "Default",
		["사나없찐 - 아즈샤라"] = "Default",
		["샤이아리 - 아즈샤라"] = "Default",
		["Wilier - 아즈샤라"] = "Default",
		["Lezyne - 아즈샤라"] = "Default",
		["재연마사 - 줄진"] = "Default",
		["환상큰손 - 아즈샤라"] = "Default",
		["샤말밀레 - 아즈샤라"] = "Default",
		["데로사 - 아즈샤라"] = "Default",
		["Emonda - 아즈샤라"] = "Default",
		["징박무시하나연 - 줄진"] = "Default",
		["Santini - 아즈샤라"] = "Default",
		["써벨로 - 아즈샤라"] = "Default",
		["리떼 - 아즈샤라"] = "Default",
		["Emonda - 줄진"] = "Default",
		["뽕나인 - 줄진"] = "Default",
		["Vlaanderen - 아즈샤라"] = "Default",
		["영석썼숑 - 줄진"] = "Default",
		["사나정연 - 아즈샤라"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["locked"] = true,
			["units"] = {
				["pet"] = {
				},
				["playertarget"] = {
					["font"] = "데미지 글꼴",
				},
				["focus"] = {
					["fontSize"] = 16,
					["font"] = "데미지 글꼴",
					["y"] = -112.000022888184,
					["x"] = 432.000671386719,
				},
				["arena2"] = {
				},
				["mouseover"] = {
					["enabled"] = false,
				},
				["arena5"] = {
				},
				["arena4"] = {
				},
			},
		},
	},
}
