
MasqueDB = {
	["namespaces"] = {
		["LibDualSpec-1.0"] = {
		},
	},
	["profileKeys"] = {
		["Lezyne - 아즈샤라"] = "Default",
		["Emonda - 아즈샤라"] = "Default",
		["데로사 - 아즈샤라"] = "Default",
		["Vlaanderen - 아즈샤라"] = "Default",
		["Santini - 아즈샤라"] = "Default",
		["써벨로 - 아즈샤라"] = "Default",
		["리떼 - 아즈샤라"] = "Default",
		["샤말밀레 - 아즈샤라"] = "Default",
		["아르니타 - 아즈샤라"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["Groups"] = {
				["WeakAuras_9_bf_active"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_8_ks_use"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_8_cb_use_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_rune_of_power_cd_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_living_bomb__cd_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_8_cb_cd_2_2"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_flame_on_use!_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_unstable_magic_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_2_buried_treasure_neg_2_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_1_true_bearing_neg_2_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_8_ks_use_2_2"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_7_snd_inactive"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_blast_wave__2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_2_buried_treasure_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_3_broadsides_neg_2_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_cinderstorm_cd__2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_olph1"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_9_arti_rdy"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_3_broadsides_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_fire_blast_cd_"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_6_shark_infested_waters_2_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_olfiller3_rtb_refresh"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_combustion_cd__2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_pyroblast_off"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_kindling"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_7_mfd_cd_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_rune_of_power_1_charge_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_5_jolly_roger_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_7_snd_progress_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_olfiller2"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_공포의_검의_저주"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_4_grand_melee_2_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_8_ks_cd"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_4._grand_melee_neg_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_6_shark_infested_waters_neg_2_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_8_cb_use_2_2"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_meteor_cd__2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_7_dfa_cd"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_1_true_bearing_2_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_cinderstorm_3"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_8_ks_cd_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_8_cb_use"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_fire_blast"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_7_dfa_cd_2_2"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_9_ar_rdy"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_7_snd_inactive_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_유령의_일격_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_incanter's_flow_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_controlled_burn_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_2_buried_treasure_2_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_공포의_검의_저주_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_7_mfd_cd"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_pyroblast"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_공포의_검의_저주_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_3_broadsides_neg_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_9_ar_cd_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_1_true_bearing_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_olfiller_ds"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_phoenix's_flames_glow"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_4_grand_melee_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_8_cb_cd_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_7_snd_inactive_2_2"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_8_alacrity_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_7_snd_progress_2_2"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_7_snd_progress"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_은신"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_7_mfd_cd_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_5_jolly_roger_2_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_5_jolly_roger_neg_2_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_living_bomb__2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_5_jolly_roger_2_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_4_grand_melee_2_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_7_mfd_use"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_rune_of_power_2_charges_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_olfiller4_ds"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_7_dfa_use_2_2"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_7_dfa_use_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_fire_blast_glow"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_flame_on_3"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_유령의_일격"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_mirror_image_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_olfiller4"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_olfiller3_rtb"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_8_alacrity_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_7_mfd_use_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_1_true_bearing_neg_2_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_olfiller3_snd"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_1_true_bearing_2_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_유령의_일격_2_2"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_9_ar_rdy_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_3_broadsides_neg_2_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_phoenix's_flames_cd"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_6_shark_infested_waters_2_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_3_broadsides_2_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_9_bf_active_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_3_broadsides_2_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_blast_wave_cd__2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_7_dfa_use"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_2_buried_treasure_neg_2_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_5_jolly_roger_neg_2_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_4._grand_melee_neg_2_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_2_buried_treasure_2_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_6_shark_infested_waters_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_olfiller4_dfa_ds"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_9_arti_cd"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_olfiller_2.5_ds"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_mirror_image_cd__2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_공포의_검의_저주_2_2"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_유령의_일격_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_olfiller_3_snd_refresh"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_2_buried_treasure_neg_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_6_shark_infested_waters_neg_2_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_5_jolly_roger_neg_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_frost_nova_cd_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_8_ks_cd_2_2"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_9_bf_active_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_flamepatch"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_meteor_3"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_7_dfa_cd_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_9_ar_cd"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_7_mfd_use_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_4._grand_melee_neg_2_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Trinity: Round",
				},
				["WeakAuras_9_ar_rdy_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_dragon's_breath_cd_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_8_cb_cd"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_8_ks_use_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_9_ar_cd_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_phoenix's_flames"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_6_shark_infested_waters_neg_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_combustion_4"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_flame_on_cd__2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_1_true_bearing_neg_2"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
				["WeakAuras_8_alacrity"] = {
					["SkinID"] = "Trinity: Round",
					["Inherit"] = false,
				},
			},
		},
	},
}
