
AUCTIONATOR_SAVEDVARS = {
	["_50000"] = 500,
	["_2000"] = 100,
	["_200000"] = 1000,
	["_10000"] = 200,
	["_1000000"] = 2500,
	["_5000000"] = 10000,
	["STARTING_DISCOUNT"] = 5,
	["_500"] = 5,
	["LOG_DE_DATA_X"] = true,
}
AUCTIONATOR_PRICING_HISTORY = {
}
AUCTIONATOR_SHOPPING_LISTS = {
	{
		["items"] = {
			"지맥광 조각", -- [1]
			"쾌속의 새벽빛", -- [2]
			"마귀 가죽", -- [3]
			"아주 날카로운 예언의 눈", -- [4]
			"아즈샤리 샐러드", -- [5]
			"아르카나", -- [6]
			"악마강철 주괴", -- [7]
			"지맥석 광석", -- [8]
			"에이트릴", -- [9]
			"나이트본 진미 모둠", -- [10]
			"더럽혀진 증강의 룬", -- [11]
			"속삭인 서약의 영약", -- [12]
			"지속되는 힘의 물약", -- [13]
			"지능의 호랑이", -- [14]
			"지옥판암", -- [15]
			"지옥나물", -- [16]
			"고요한 정신의 고서", -- [17]
			"피아른스카글", -- [18]
			"별빛 장미", -- [19]
			"배고픈 마법학자", -- [20]
			"강력한 암흑루비", -- [21]
			"일곱 번째 악마의 영약", -- [22]
			"이세랄린 씨앗", -- [23]
			"지옥가죽", -- [24]
			"꿈잎사귀", -- [25]
			"사술매듭 가방", -- [26]
		},
		["isRecents"] = 1,
		["name"] = "Recent Searches",
	}, -- [1]
}
AUCTIONATOR_SHOPPING_LISTS_MIGRATED_V2 = true
AUCTIONATOR_PRICE_DATABASE = {
	["줄진_Alliance"] = {
	},
	["아즈샤라_Horde"] = {
		["지맥광 조각"] = {
			["H2498"] = 947996,
			["mr"] = 900000,
			["sc"] = 12,
			["id"] = "124441:0:0:0:0",
			["L2498"] = 900000,
			["cc"] = 7,
		},
		["아르카나"] = {
			["H2498"] = 328000,
			["mr"] = 328000,
			["sc"] = 12,
			["id"] = "124440:0:0:0:0",
			["cc"] = 7,
		},
		["쾌속의 새벽빛"] = {
			["mr"] = 14000000,
			["sc"] = 7,
			["L2489"] = 14000000,
			["id"] = "130220:0:0:0:0",
			["cc"] = 3,
			["H2489"] = 22980000,
		},
		["마귀 가죽"] = {
			["mr"] = 2000000,
			["sc"] = 6,
			["id"] = "151566:0:0:0:0",
			["cc"] = 7,
			["H2489"] = 2000000,
		},
		["아주 날카로운 예언의 눈"] = {
			["H2494"] = 8700000,
			["mr"] = 8700000,
			["sc"] = 5,
			["id"] = "130219:0:0:0:0",
			["cc"] = 3,
		},
	},
	["아즈샤라_Neutral"] = {
	},
	["__dbversion"] = 4,
}
AUCTIONATOR_LAST_SCAN_TIME = nil
AUCTIONATOR_TOONS = {
	["북경스딸"] = {
		["firstSeen"] = 1470219983,
		["firstVersion"] = "4.0.8",
	},
	["환상큰손"] = {
		["firstSeen"] = 1470237463,
		["firstVersion"] = "4.0.8",
	},
	["샤말밀레"] = {
		["firstSeen"] = 1478664152,
		["firstVersion"] = "4.0.16",
	},
	["징박무시하나연"] = {
		["firstSeen"] = 1470223446,
		["firstVersion"] = "4.0.8",
	},
	["써벨로"] = {
		["firstSeen"] = 1470215096,
		["firstVersion"] = "4.0.8",
	},
	["데로사"] = {
		["firstSeen"] = 1470237939,
		["firstVersion"] = "4.0.8",
	},
	["안녕김밥"] = {
		["firstSeen"] = 1470222690,
		["firstVersion"] = "4.0.8",
		["guid"] = "Player-2116-058C33B3",
	},
	["아르니타"] = {
		["firstSeen"] = 1480651684,
		["firstVersion"] = "4.0.16",
	},
	["최저방벽"] = {
		["firstSeen"] = 1470223831,
		["firstVersion"] = "4.0.8",
	},
	["사나없찐"] = {
		["firstSeen"] = 1488375439,
		["firstVersion"] = "4.0.16",
	},
	["꼬꼬마빠샤"] = {
		["firstSeen"] = 1470223552,
		["firstVersion"] = "4.0.8",
	},
	["샤이아리"] = {
		["firstSeen"] = 1470214891,
		["firstVersion"] = "4.0.8",
	},
	["뚱빼미나무"] = {
		["firstSeen"] = 1470223737,
		["guid"] = "Player-2116-04D35831",
		["firstVersion"] = "4.0.8",
	},
	["사나정연"] = {
		["firstSeen"] = 1513466087,
		["firstVersion"] = "4.0.19",
	},
	["클리엔"] = {
		["firstSeen"] = 1470211098,
		["firstVersion"] = "4.0.8",
	},
	["환상큰소"] = {
		["firstSeen"] = 1470237573,
		["firstVersion"] = "4.0.8",
	},
	["재연마사"] = {
		["firstSeen"] = 1470214775,
		["firstVersion"] = "4.0.8",
	},
	["리떼"] = {
		["firstSeen"] = 1472093014,
		["firstVersion"] = "4.0.10",
	},
	["Emonda"] = {
		["firstSeen"] = 1470237751,
		["firstVersion"] = "4.0.8",
	},
	["릿데"] = {
		["firstSeen"] = 1494308946,
		["firstVersion"] = "4.0.17",
	},
	["Wilier"] = {
		["firstSeen"] = 1470238088,
		["firstVersion"] = "4.0.8",
	},
	["뽕나인"] = {
		["firstSeen"] = 1470223185,
		["firstVersion"] = "4.0.8",
	},
	["영석썼숑"] = {
		["firstSeen"] = 1470223629,
		["firstVersion"] = "4.0.8",
	},
	["Santini"] = {
		["firstSeen"] = 1471922380,
		["firstVersion"] = "4.0.9",
	},
	["Lezyne"] = {
		["firstSeen"] = 1478476107,
		["firstVersion"] = "4.0.16",
	},
	["언제나운무"] = {
		["firstSeen"] = 1488375619,
		["firstVersion"] = "4.0.16",
	},
	["Vlaanderen"] = {
		["firstSeen"] = 1472359170,
		["firstVersion"] = "4.0.10",
	},
	["사나무엇"] = {
		["firstSeen"] = 1513267178,
		["firstVersion"] = "4.0.19",
	},
}
AUCTIONATOR_STACKING_PREFS = {
}
AUCTIONATOR_SCAN_MINLEVEL = 1
AUCTIONATOR_DB_MAXITEM_AGE = 180
AUCTIONATOR_DB_MAXHIST_AGE = -1
AUCTIONATOR_DB_MAXHIST_DAYS = 5
AUCTIONATOR_FS_CHUNK = nil
AUCTIONATOR_DE_DATA = nil
AUCTIONATOR_DE_DATA_BAK = nil
ITEM_ID_VERSION = "3.2.6"
AUCTIONATOR_SHOW_MAILBOX_TIPS = nil
