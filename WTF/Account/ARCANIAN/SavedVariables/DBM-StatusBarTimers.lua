
DBT_AllPersistentOptions = {
	["Default"] = {
		["DBM"] = {
			["FontSize"] = 10,
			["HugeTimerY"] = -120,
			["Decimal"] = 60,
			["Scale"] = 0.899999976158142,
			["EnlargeBarsPercent"] = 0.125,
			["TimerY"] = 263.500183105469,
			["HugeWidth"] = 200,
			["TimerX"] = -246.999603271484,
			["EnlargeBarTime"] = 11,
			["TimerPoint"] = "RIGHT",
			["Width"] = 183,
			["HugeTimerPoint"] = "CENTER",
			["HugeBarYOffset"] = 0,
			["BarYOffset"] = 0,
			["HugeScale"] = 1.04999995231628,
			["HugeTimerX"] = 0,
			["Height"] = 20,
			["BarXOffset"] = 0,
			["HugeBarXOffset"] = 0,
		},
	},
}
