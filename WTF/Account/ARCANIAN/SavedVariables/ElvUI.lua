
ElvDB = {
	["profileKeys"] = {
		["샤이아리 - 줄진"] = "Default",
		["릿데 - 아즈샤라"] = "Default",
		["최저방벽 - 줄진"] = "Default",
		["환상큰소 - 아즈샤라"] = "Default",
		["북경스딸 - 줄진"] = "Default",
		["클리엔 - 줄진"] = "Default",
		["사나무엇 - 아즈샤라"] = "Default",
		["아르니타 - 아즈샤라"] = "Default",
		["Vlaanderen - 아즈샤라"] = "Default",
		["꼬꼬마빠샤 - 줄진"] = "Default",
		["뚱빼미나무 - 줄진"] = "Default",
		["사나없찐 - 아즈샤라"] = "Default",
		["샤이아리 - 아즈샤라"] = "Default",
		["Wilier - 아즈샤라"] = "Default",
		["뽕나인 - 줄진"] = "Default",
		["Emonda - 줄진"] = "Default",
		["환상큰손 - 아즈샤라"] = "Default",
		["샤말밀레 - 아즈샤라"] = "Default",
		["재연마사 - 줄진"] = "Default",
		["안녕김밥 - 줄진"] = "Default",
		["징박무시하나연 - 줄진"] = "Default",
		["Santini - 아즈샤라"] = "Default",
		["써벨로 - 아즈샤라"] = "Default",
		["리떼 - 아즈샤라"] = "Default",
		["데로사 - 아즈샤라"] = "Default",
		["Emonda - 아즈샤라"] = "Default",
		["Lezyne - 아즈샤라"] = "Default",
		["영석썼숑 - 줄진"] = "Default",
		["사나정연 - 아즈샤라"] = "Default",
	},
	["gold"] = {
		["줄진"] = {
			["샤이아리"] = 9465097,
			["클리엔"] = 10000118,
			["최저방벽"] = 3809529,
		},
		["아즈샤라"] = {
			["Santini"] = 31459527,
			["사나정연"] = 22899,
			["샤말밀레"] = 27636834,
			["Lezyne"] = 48824804,
			["Emonda"] = 76609079,
			["리떼"] = 20692698819,
			["아르니타"] = 37980996,
			["샤이아리"] = 124424084,
			["릿데"] = 162981023,
			["데로사"] = 26396540,
			["Vlaanderen"] = 50147015,
			["사나무엇"] = 634,
		},
	},
	["namespaces"] = {
		["LibDualSpec-1.0"] = {
		},
	},
	["global"] = {
		["general"] = {
			["commandBarSetting"] = "DISABLED",
		},
		["uiScale"] = "1.0",
		["screenwidth"] = 1920.89,
		["unitframe"] = {
			["buffwatchBackup"] = {
				["DEATHKNIGHT"] = {
					[49016] = {
					},
				},
				["WARRIOR"] = {
					[114030] = {
					},
					[3411] = {
					},
					[114029] = {
					},
				},
				["SHAMAN"] = {
					[61295] = {
					},
					[51945] = {
					},
					[974] = {
					},
				},
				["MAGE"] = {
					[111264] = {
					},
				},
				["PRIEST"] = {
					[47788] = {
					},
					[17] = {
					},
					[6788] = {
					},
					[33206] = {
					},
					[139] = {
					},
					[123258] = {
					},
					[10060] = {
					},
					[41635] = {
					},
				},
				["ROGUE"] = {
					[57934] = {
					},
				},
				["HUNTER"] = {
				},
				["PET"] = {
					[19615] = {
					},
					[136] = {
					},
				},
				["DRUID"] = {
					[33763] = {
					},
					[8936] = {
					},
					[774] = {
					},
					[48438] = {
					},
				},
				["MONK"] = {
					[119611] = {
					},
					[132120] = {
					},
					[116849] = {
					},
					[124081] = {
					},
				},
				["PALADIN"] = {
					[53563] = {
					},
					[1022] = {
					},
					[1038] = {
					},
					[156322] = {
					},
					[6940] = {
					},
					[114039] = {
					},
					[1044] = {
					},
					[148039] = {
					},
				},
			},
			["ChannelTicks"] = {
				["회개"] = 3,
			},
		},
		["screenheight"] = 1080,
		["userInformedNewChanges1"] = true,
	},
	["profiles"] = {
		["Minimalistic"] = {
			["currentTutorial"] = 2,
			["general"] = {
				["fontSize"] = 11,
				["bottomPanel"] = false,
				["backdropfadecolor"] = {
					["a"] = 0.80000001192093,
					["r"] = 0.058823529411765,
					["g"] = 0.058823529411765,
					["b"] = 0.058823529411765,
				},
				["valuecolor"] = {
					["a"] = 1,
					["r"] = 1,
					["g"] = 1,
					["b"] = 1,
				},
				["bordercolor"] = {
					["r"] = 0.30588235294118,
					["g"] = 0.30588235294118,
					["b"] = 0.30588235294118,
				},
				["font"] = "Expressway",
				["reputation"] = {
					["orientation"] = "HORIZONTAL",
					["textFormat"] = "PERCENT",
					["height"] = 16,
					["width"] = 200,
				},
			},
			["movers"] = {
				["PetAB"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-50,-428",
				["ElvUF_RaidMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,51,120",
				["LeftChatMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,50,50",
				["GMMover"] = "TOPLEFT,ElvUIParent,TOPLEFT,250,-50",
				["BossButton"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-117,-298",
				["LootFrameMover"] = "TOPLEFT,ElvUIParent,TOPLEFT,249,-216",
				["ElvUF_RaidpetMover"] = "TOPLEFT,ElvUIParent,BOTTOMLEFT,50,827",
				["MicrobarMover"] = "TOPLEFT,ElvUIParent,TOPLEFT,4,-52",
				["VehicleSeatMover"] = "TOPLEFT,ElvUIParent,TOPLEFT,51,-87",
				["ElvUF_TargetTargetMover"] = "BOTTOM,ElvUIParent,BOTTOM,0,143",
				["ElvUF_Raid40Mover"] = "TOPLEFT,ElvUIParent,BOTTOMLEFT,392,1073",
				["ElvAB_1"] = "BOTTOM,ElvUIParent,BOTTOM,0,50",
				["ElvAB_2"] = "BOTTOM,ElvUIParent,BOTTOM,0,90",
				["ElvAB_4"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-50,-394",
				["AltPowerBarMover"] = "TOP,ElvUIParent,TOP,0,-186",
				["ElvAB_3"] = "BOTTOM,ElvUIParent,BOTTOM,305,50",
				["ElvAB_5"] = "BOTTOM,ElvUIParent,BOTTOM,-305,50",
				["MinimapMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-50,-50",
				["ElvUF_TargetMover"] = "BOTTOM,ElvUIParent,BOTTOM,230,140",
				["ObjectiveFrameMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-122,-393",
				["BNETMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,50,232",
				["ShiftAB"] = "TOPLEFT,ElvUIParent,BOTTOMLEFT,50,1080",
				["ElvUF_PlayerCastbarMover"] = "BOTTOM,ElvUIParent,BOTTOM,0,133",
				["ElvUF_PartyMover"] = "TOPLEFT,ElvUIParent,BOTTOMLEFT,184,773",
				["ElvAB_6"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-488,330",
				["TooltipMover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-50,50",
				["ElvUF_TankMover"] = "TOPLEFT,ElvUIParent,BOTTOMLEFT,50,995",
				["ElvUF_PetMover"] = "BOTTOM,ElvUIParent,BOTTOM,0,200",
				["ElvUF_PlayerMover"] = "BOTTOM,ElvUIParent,BOTTOM,-230,140",
				["TotemBarMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,463,50",
				["RightChatMover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-50,50",
				["AlertFrameMover"] = "TOP,ElvUIParent,TOP,0,-50",
				["ReputationBarMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-50,-228",
				["ElvUF_AssistMover"] = "TOPLEFT,ElvUIParent,BOTTOMLEFT,51,937",
			},
			["bags"] = {
				["countFontSize"] = 9,
				["itemLevelFontSize"] = 9,
			},
			["hideTutorial"] = true,
			["chat"] = {
				["chatHistory"] = false,
				["fontSize"] = 11,
				["tabFont"] = "Expressway",
				["fadeUndockedTabs"] = false,
				["font"] = "Expressway",
				["fadeTabsNoBackdrop"] = false,
				["editBoxPosition"] = "ABOVE_CHAT",
				["tapFontSize"] = 11,
				["panelBackdrop"] = "HIDEBOTH",
			},
			["layoutSet"] = "dpsMelee",
			["unitframe"] = {
				["fontSize"] = 9,
				["fontOutline"] = "THICKOUTLINE",
				["smoothbars"] = true,
				["font"] = "Expressway",
				["statusbar"] = "ElvUI Blank",
				["units"] = {
					["tank"] = {
						["enable"] = false,
					},
					["targettarget"] = {
						["infoPanel"] = {
							["enable"] = true,
						},
						["debuffs"] = {
							["enable"] = false,
						},
						["name"] = {
							["attachTextTo"] = "InfoPanel",
							["yOffset"] = -2,
							["position"] = "TOP",
						},
						["height"] = 50,
						["width"] = 122,
					},
					["pet"] = {
						["infoPanel"] = {
							["height"] = 14,
							["enable"] = true,
						},
						["debuffs"] = {
							["enable"] = true,
						},
						["portrait"] = {
							["camDistanceScale"] = 2,
						},
						["castbar"] = {
							["width"] = 122,
						},
						["height"] = 50,
						["threatStyle"] = "NONE",
						["width"] = 122,
					},
					["player"] = {
						["debuffs"] = {
							["perrow"] = 7,
						},
						["castbar"] = {
							["height"] = 35,
							["iconSize"] = 54,
							["iconAttached"] = false,
							["width"] = 478,
						},
						["combatfade"] = true,
						["infoPanel"] = {
							["enable"] = true,
						},
						["name"] = {
							["attachTextTo"] = "InfoPanel",
							["text_format"] = "[namecolor][name]",
						},
						["height"] = 80,
						["health"] = {
							["attachTextTo"] = "InfoPanel",
							["text_format"] = "[healthcolor][health:current-max]",
						},
						["classbar"] = {
							["height"] = 15,
							["autoHide"] = true,
						},
						["power"] = {
							["attachTextTo"] = "InfoPanel",
							["text_format"] = "[powercolor][power:current-max]",
							["height"] = 15,
						},
					},
					["party"] = {
						["horizontalSpacing"] = 3,
						["debuffs"] = {
							["anchorPoint"] = "BOTTOM",
							["numrows"] = 4,
							["perrow"] = 1,
						},
						["power"] = {
							["text_format"] = "",
							["height"] = 5,
						},
						["enable"] = false,
						["healPrediction"] = true,
						["growthDirection"] = "RIGHT_DOWN",
						["infoPanel"] = {
							["enable"] = true,
						},
						["health"] = {
							["attachTextTo"] = "InfoPanel",
							["orientation"] = "VERTICAL",
							["text_format"] = "[healthcolor][health:current]",
							["position"] = "RIGHT",
						},
						["name"] = {
							["attachTextTo"] = "InfoPanel",
							["text_format"] = "[namecolor][name:short]",
							["position"] = "LEFT",
						},
						["width"] = 110,
						["height"] = 59,
						["verticalSpacing"] = 0,
						["roleIcon"] = {
							["position"] = "TOPRIGHT",
						},
						["rdebuffs"] = {
							["font"] = "Expressway",
						},
					},
					["raid40"] = {
						["enable"] = false,
						["rdebuffs"] = {
							["font"] = "Expressway",
						},
					},
					["focus"] = {
						["infoPanel"] = {
							["enable"] = true,
							["height"] = 17,
						},
						["health"] = {
							["attachTextTo"] = "InfoPanel",
							["text_format"] = "[healthcolor][health:current]",
						},
						["castbar"] = {
							["iconSize"] = 26,
							["width"] = 122,
						},
						["height"] = 56,
						["name"] = {
							["attachTextTo"] = "InfoPanel",
							["position"] = "LEFT",
						},
						["threatStyle"] = "NONE",
						["width"] = 189,
					},
					["assist"] = {
						["enable"] = false,
					},
					["arena"] = {
						["castbar"] = {
							["width"] = 246,
						},
						["spacing"] = 26,
					},
					["raid"] = {
						["roleIcon"] = {
							["position"] = "RIGHT",
						},
						["debuffs"] = {
							["enable"] = true,
							["sizeOverride"] = 27,
							["perrow"] = 4,
						},
						["rdebuffs"] = {
							["enable"] = false,
							["font"] = "Expressway",
						},
						["growthDirection"] = "UP_RIGHT",
						["health"] = {
							["yOffset"] = -6,
						},
						["width"] = 140,
						["height"] = 28,
						["name"] = {
							["position"] = "LEFT",
						},
						["visibility"] = "[nogroup] hide;show",
						["groupsPerRowCol"] = 5,
					},
					["target"] = {
						["debuffs"] = {
							["perrow"] = 7,
						},
						["power"] = {
							["attachTextTo"] = "InfoPanel",
							["hideonnpc"] = false,
							["text_format"] = "[powercolor][power:current-max]",
							["height"] = 15,
						},
						["infoPanel"] = {
							["enable"] = true,
						},
						["name"] = {
							["attachTextTo"] = "InfoPanel",
							["text_format"] = "[namecolor][name]",
						},
						["health"] = {
							["attachTextTo"] = "InfoPanel",
							["text_format"] = "[healthcolor][health:current-max]",
						},
						["height"] = 80,
						["buffs"] = {
							["perrow"] = 7,
						},
						["smartAuraPosition"] = "DEBUFFS_ON_BUFFS",
						["castbar"] = {
							["iconSize"] = 54,
							["iconAttached"] = false,
						},
					},
				},
			},
			["datatexts"] = {
				["minimapPanels"] = false,
				["fontSize"] = 11,
				["font"] = "Expressway",
				["goldFormat"] = "SHORT",
				["panelTransparency"] = true,
				["leftChatPanel"] = false,
				["panels"] = {
					["LeftMiniPanel"] = "",
					["RightMiniPanel"] = "",
					["RightChatDataPanel"] = {
						["right"] = "",
						["left"] = "",
						["middle"] = "",
					},
					["BottomMiniPanel"] = "Time",
					["LeftChatDataPanel"] = {
						["right"] = "",
						["left"] = "",
						["middle"] = "",
					},
				},
				["rightChatPanel"] = false,
			},
			["actionbar"] = {
				["bar3"] = {
					["inheritGlobalFade"] = true,
					["buttonsize"] = 38,
					["buttonsPerRow"] = 3,
				},
				["bar6"] = {
					["buttonsize"] = 38,
				},
				["bar2"] = {
					["enabled"] = true,
					["inheritGlobalFade"] = true,
					["buttonsize"] = 38,
				},
				["bar1"] = {
					["heightMult"] = 2,
					["inheritGlobalFade"] = true,
					["buttonsize"] = 38,
				},
				["bar5"] = {
					["inheritGlobalFade"] = true,
					["buttonsize"] = 38,
					["buttonsPerRow"] = 3,
				},
				["globalFadeAlpha"] = 0.87,
				["stanceBar"] = {
					["inheritGlobalFade"] = true,
				},
				["fontSize"] = 9,
				["bar4"] = {
					["enabled"] = false,
					["backdrop"] = false,
					["buttonsize"] = 38,
				},
			},
			["nameplates"] = {
				["filters"] = {
				},
			},
			["auras"] = {
				["fontSize"] = 11,
				["buffs"] = {
					["maxWraps"] = 2,
				},
				["font"] = "Expressway",
			},
			["bossAuraFiltersConverted"] = true,
			["tooltip"] = {
				["textFontSize"] = 11,
				["font"] = "Expressway",
				["healthBar"] = {
					["font"] = "Expressway",
				},
				["smallTextFontSize"] = 11,
				["fontSize"] = 11,
				["headerFontSize"] = 11,
			},
		},
		["Default"] = {
			["databars"] = {
				["reputation"] = {
					["enable"] = true,
				},
			},
			["currentTutorial"] = 1,
			["bags"] = {
				["bagSize"] = 32,
				["alignToChat"] = false,
				["countFontOutline"] = "OUTLINE",
				["itemLevelFontOutline"] = "OUTLINE",
				["ignoreItems"] = "",
			},
			["hideTutorial"] = true,
			["auras"] = {
				["fontOutline"] = "OUTLINE",
			},
			["layoutSet"] = "healer",
			["thinBorderColorSet"] = true,
			["bagSortIgnoreItemsReset"] = true,
			["movers"] = {
				["ElvUF_Raid40Mover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMLEFT,660,450",
				["ElvUF_PlayerCastbarMover"] = "BOTTOM,ElvUIParent,BOTTOM,0,98",
				["ElvUF_RaidMover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMLEFT,660,450",
				["ElvAB_2"] = "BOTTOM,ElvUIParent,BOTTOM,208,4",
				["ElvAB_4"] = "BOTTOM,ElvUIParent,BOTTOM,208,38",
				["ElvAB_3"] = "BOTTOM,ElvUIParent,BOTTOM,-208,38",
				["ElvAB_5"] = "TOP,ElvUIParent,TOP,316,-490",
				["ElvUF_RaidpetMover"] = "TOPLEFT,ElvUIParent,BOTTOMLEFT,5,736",
				["ShiftAB"] = "TOPLEFT,ElvUIParent,BOTTOMLEFT,549,96",
				["ElvUF_TargetTargetMover"] = "BOTTOM,ElvUIParent,BOTTOM,0,132",
				["ElvUF_FocusMover"] = "BOTTOM,ElvUIParent,BOTTOM,223,223",
				["ElvUF_AssistMover"] = "TOPLEFT,ElvUIParent,BOTTOMLEFT,12,832",
				["ElvAB_1"] = "BOTTOM,ElvUIParent,BOTTOM,-208,4",
				["ElvUF_TankMover"] = "TOPLEFT,ElvUIParent,BOTTOMLEFT,12,894",
				["BossHeaderMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-412,-232",
				["ElvUF_PetMover"] = "BOTTOM,ElvUIParent,BOTTOM,0,176",
				["ElvUF_PlayerMover"] = "BOTTOM,ElvUIParent,BOTTOM,-278,148",
				["ElvUF_PartyMover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMLEFT,660,450",
				["BossButton"] = "BOTTOM,ElvUIParent,BOTTOM,0,275",
				["PetAB"] = "BOTTOM,ElvUIParent,BOTTOM,0,72",
				["ElvUF_TargetMover"] = "BOTTOM,ElvUIParent,BOTTOM,278,148",
			},
			["bossAuraFiltersConverted"] = true,
			["unitframe"] = {
				["fontSize"] = 11,
				["units"] = {
					["party"] = {
						["debuffs"] = {
							["xOffset"] = -4,
							["yOffset"] = -7,
							["anchorPoint"] = "TOPRIGHT",
							["sizeOverride"] = 16,
						},
						["healPrediction"] = true,
						["name"] = {
							["position"] = "TOP",
							["text_format"] = "[namecolor][name:short]",
						},
						["height"] = 45,
						["verticalSpacing"] = 9,
						["horizontalSpacing"] = 9,
						["enable"] = false,
						["growthDirection"] = "LEFT_UP",
						["roleIcon"] = {
							["position"] = "BOTTOMRIGHT",
						},
						["power"] = {
							["text_format"] = "",
						},
						["width"] = 80,
						["health"] = {
							["frequentUpdates"] = true,
							["position"] = "BOTTOM",
							["text_format"] = "[healthcolor][health:deficit]",
						},
						["buffs"] = {
							["enable"] = true,
							["yOffset"] = -6,
							["clickThrough"] = true,
							["useBlacklist"] = false,
							["noDuration"] = false,
							["playerOnly"] = false,
							["perrow"] = 1,
							["useFilter"] = "TurtleBuffs",
							["sizeOverride"] = 22,
							["xOffset"] = 50,
						},
					},
					["boss"] = {
						["debuffs"] = {
							["numrows"] = 1,
							["sizeOverride"] = 16,
							["perrow"] = 8,
							["anchorPoint"] = "RIGHT",
						},
						["spacing"] = 24,
						["width"] = 160,
						["height"] = 32,
						["buffs"] = {
							["sizeOverride"] = 16,
							["yOffset"] = 0,
						},
					},
					["raid40"] = {
						["growthDirection"] = "LEFT_UP",
						["enable"] = false,
						["healPrediction"] = true,
						["health"] = {
							["frequentUpdates"] = true,
						},
						["height"] = 30,
					},
					["focus"] = {
						["width"] = 160,
						["castbar"] = {
							["width"] = 160,
						},
					},
					["target"] = {
						["debuffs"] = {
							["sizeOverride"] = 28,
						},
						["portrait"] = {
							["overlay"] = true,
							["enable"] = true,
						},
						["buffs"] = {
							["sizeOverride"] = 28,
						},
					},
					["raid"] = {
						["horizontalSpacing"] = 9,
						["debuffs"] = {
							["enable"] = true,
							["yOffset"] = -7,
							["anchorPoint"] = "TOPRIGHT",
							["sizeOverride"] = 16,
							["xOffset"] = -4,
						},
						["growthDirection"] = "LEFT_UP",
						["verticalSpacing"] = 9,
						["rdebuffs"] = {
							["enable"] = false,
						},
						["healPrediction"] = true,
						["health"] = {
							["frequentUpdates"] = true,
						},
						["enable"] = false,
						["height"] = 45,
						["buffs"] = {
							["enable"] = true,
							["yOffset"] = -6,
							["clickThrough"] = true,
							["useBlacklist"] = false,
							["noDuration"] = false,
							["playerOnly"] = false,
							["perrow"] = 1,
							["useFilter"] = "TurtleBuffs",
							["sizeOverride"] = 22,
							["xOffset"] = 50,
						},
					},
					["player"] = {
						["debuffs"] = {
							["sizeOverride"] = 28,
						},
						["portrait"] = {
							["overlay"] = true,
							["enable"] = true,
						},
						["castbar"] = {
							["insideInfoPanel"] = false,
							["width"] = 406,
							["height"] = 28,
						},
						["buffs"] = {
							["sizeOverride"] = 28,
						},
					},
					["assist"] = {
						["enable"] = false,
					},
				},
				["colors"] = {
					["auraBarBuff"] = {
						["b"] = 0.1,
						["g"] = 0.1,
						["r"] = 0.1,
					},
					["healthclass"] = true,
					["castColor"] = {
						["b"] = 0.1,
						["g"] = 0.1,
						["r"] = 0.1,
					},
					["health"] = {
						["b"] = 0.1,
						["g"] = 0.1,
						["r"] = 0.1,
					},
				},
				["fontOutline"] = "OUTLINE",
			},
			["datatexts"] = {
				["panels"] = {
					["RightChatDataPanel"] = {
						["right"] = "Orderhall",
						["left"] = "Gold",
					},
					["LeftChatDataPanel"] = {
						["right"] = "Talent/Loot Specialization",
						["left"] = "System",
					},
				},
			},
			["actionbar"] = {
				["bar3"] = {
					["buttons"] = 12,
					["buttonsPerRow"] = 12,
				},
				["fontOutline"] = "OUTLINE",
				["keyDown"] = false,
				["bar2"] = {
					["enabled"] = true,
				},
				["bar5"] = {
					["buttons"] = 12,
					["buttonsPerRow"] = 4,
				},
				["bar4"] = {
					["backdrop"] = false,
					["point"] = "BOTTOMLEFT",
					["buttonsPerRow"] = 12,
				},
				["macrotext"] = true,
				["stanceBar"] = {
					["buttonsize"] = 24,
				},
				["barPet"] = {
					["point"] = "TOPLEFT",
					["backdrop"] = false,
					["buttonsPerRow"] = 10,
					["buttonsize"] = 24,
				},
				["backdropSpacingConverted"] = true,
			},
			["general"] = {
				["afk"] = false,
				["autoRepair"] = "PLAYER",
				["minimap"] = {
					["locationText"] = "SHOW",
				},
				["bottomPanel"] = false,
				["backdropfadecolor"] = {
					["b"] = 0.054,
					["g"] = 0.054,
					["r"] = 0.054,
				},
				["valuecolor"] = {
					["b"] = 0.819,
					["g"] = 0.513,
					["r"] = 0.09,
				},
				["vendorGrays"] = true,
				["bordercolor"] = {
					["r"] = 0,
					["g"] = 0,
					["b"] = 0,
				},
			},
			["chat"] = {
				["chatHistory"] = false,
				["throttleInterval"] = 0,
				["panelWidth"] = 450,
				["timeStampFormat"] = "%H:%M:%S ",
				["keywords"] = "ElvUI,노손,노손팟,3넴,3신화,3+",
				["panelHeight"] = 200,
				["panelTabBackdrop"] = true,
			},
		},
	},
}
ElvPrivateDB = {
	["profileKeys"] = {
		["샤이아리 - 줄진"] = "샤이아리 - 줄진",
		["언제나운무 - 아즈샤라"] = "언제나운무 - 아즈샤라",
		["최저방벽 - 줄진"] = "최저방벽 - 줄진",
		["환상큰소 - 아즈샤라"] = "환상큰소 - 아즈샤라",
		["북경스딸 - 줄진"] = "북경스딸 - 줄진",
		["클리엔 - 줄진"] = "클리엔 - 줄진",
		["사나무엇 - 아즈샤라"] = "사나무엇 - 아즈샤라",
		["아르니타 - 아즈샤라"] = "아르니타 - 아즈샤라",
		["릿데 - 아즈샤라"] = "릿데 - 아즈샤라",
		["안녕김밥 - 줄진"] = "안녕김밥 - 줄진",
		["꼬꼬마빠샤 - 줄진"] = "꼬꼬마빠샤 - 줄진",
		["뚱빼미나무 - 줄진"] = "뚱빼미나무 - 줄진",
		["사나없찐 - 아즈샤라"] = "사나없찐 - 아즈샤라",
		["샤이아리 - 아즈샤라"] = "샤이아리 - 아즈샤라",
		["Wilier - 아즈샤라"] = "Wilier - 아즈샤라",
		["뽕나인 - 줄진"] = "뽕나인 - 줄진",
		["Emonda - 줄진"] = "Emonda - 줄진",
		["환상큰손 - 아즈샤라"] = "환상큰손 - 아즈샤라",
		["샤말밀레 - 아즈샤라"] = "샤말밀레 - 아즈샤라",
		["재연마사 - 줄진"] = "재연마사 - 줄진",
		["데로사 - 아즈샤라"] = "데로사 - 아즈샤라",
		["징박무시하나연 - 줄진"] = "징박무시하나연 - 줄진",
		["Santini - 아즈샤라"] = "Santini - 아즈샤라",
		["써벨로 - 아즈샤라"] = "써벨로 - 아즈샤라",
		["리떼 - 아즈샤라"] = "리떼 - 아즈샤라",
		["Emonda - 아즈샤라"] = "Emonda - 아즈샤라",
		["Lezyne - 아즈샤라"] = "Lezyne - 아즈샤라",
		["Vlaanderen - 아즈샤라"] = "Vlaanderen - 아즈샤라",
		["영석썼숑 - 줄진"] = "영석썼숑 - 줄진",
		["사나정연 - 아즈샤라"] = "사나정연 - 아즈샤라",
	},
	["profiles"] = {
		["샤이아리 - 줄진"] = {
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.14",
		},
		["언제나운무 - 아즈샤라"] = {
		},
		["최저방벽 - 줄진"] = {
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.14",
		},
		["환상큰소 - 아즈샤라"] = {
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.14",
		},
		["북경스딸 - 줄진"] = {
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.14",
		},
		["클리엔 - 줄진"] = {
			["nameplates"] = {
				["enable"] = false,
			},
			["theme"] = "default",
			["install_complete"] = "10.14",
		},
		["사나무엇 - 아즈샤라"] = {
			["equipment"] = {
				["itemlevel"] = {
					["enable"] = false,
				},
			},
			["general"] = {
				["dmgfont"] = "데미지 글꼴",
			},
			["nameplates"] = {
				["enable"] = false,
			},
			["watchframe"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.68",
		},
		["아르니타 - 아즈샤라"] = {
			["equipment"] = {
				["itemlevel"] = {
					["enable"] = false,
				},
			},
			["general"] = {
				["replaceBlizzFonts"] = false,
				["selectquestreward"] = false,
				["dmgfont"] = "데미지 글꼴",
			},
			["nameplates"] = {
				["enable"] = false,
			},
			["watchframe"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.31",
		},
		["릿데 - 아즈샤라"] = {
			["equipment"] = {
				["itemlevel"] = {
					["enable"] = false,
				},
			},
			["general"] = {
				["replaceBlizzFonts"] = false,
				["dmgfont"] = "데미지 글꼴",
				["selectquestreward"] = false,
			},
			["nameplates"] = {
				["enable"] = false,
			},
			["watchframe"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.51",
		},
		["안녕김밥 - 줄진"] = {
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.14",
		},
		["꼬꼬마빠샤 - 줄진"] = {
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.14",
		},
		["뚱빼미나무 - 줄진"] = {
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.14",
		},
		["사나없찐 - 아즈샤라"] = {
			["equipment"] = {
				["itemlevel"] = {
					["enable"] = false,
				},
			},
			["general"] = {
				["selectquestreward"] = false,
			},
			["nameplates"] = {
				["enable"] = false,
			},
			["watchframe"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.44",
		},
		["샤이아리 - 아즈샤라"] = {
			["equipment"] = {
				["itemlevel"] = {
					["enable"] = false,
				},
			},
			["general"] = {
				["replaceBlizzFonts"] = false,
				["selectquestreward"] = false,
				["dmgfont"] = "데미지 글꼴",
			},
			["nameplates"] = {
				["enable"] = false,
			},
			["watchframe"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.44",
		},
		["Wilier - 아즈샤라"] = {
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.15",
		},
		["뽕나인 - 줄진"] = {
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.14",
		},
		["Emonda - 줄진"] = {
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.14",
		},
		["환상큰손 - 아즈샤라"] = {
			["nameplates"] = {
				["enable"] = false,
			},
			["watchframe"] = {
				["enable"] = false,
			},
			["general"] = {
				["selectquestreward"] = false,
			},
			["install_complete"] = "10.14",
		},
		["샤말밀레 - 아즈샤라"] = {
			["equipment"] = {
				["itemlevel"] = {
					["enable"] = false,
				},
			},
			["general"] = {
				["replaceBlizzFonts"] = false,
				["selectquestreward"] = false,
				["dmgfont"] = "데미지 글꼴",
			},
			["watchframe"] = {
				["enable"] = false,
			},
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.29",
		},
		["재연마사 - 줄진"] = {
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.14",
		},
		["데로사 - 아즈샤라"] = {
			["nameplates"] = {
				["enable"] = false,
			},
			["watchframe"] = {
				["enable"] = false,
			},
			["general"] = {
				["replaceBlizzFonts"] = false,
				["selectquestreward"] = false,
				["dmgfont"] = "데미지 글꼴",
			},
			["install_complete"] = "10.14",
		},
		["징박무시하나연 - 줄진"] = {
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.14",
		},
		["Santini - 아즈샤라"] = {
			["equipment"] = {
				["itemlevel"] = {
					["enable"] = false,
				},
			},
			["general"] = {
				["replaceBlizzFonts"] = false,
				["dmgfont"] = "데미지 글꼴",
				["selectquestreward"] = false,
			},
			["watchframe"] = {
				["enable"] = false,
			},
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.15",
		},
		["써벨로 - 아즈샤라"] = {
			["equipment"] = {
				["itemlevel"] = {
					["enable"] = false,
				},
			},
			["general"] = {
				["replaceBlizzFonts"] = false,
				["selectquestreward"] = false,
				["dmgfont"] = "데미지 글꼴",
			},
			["watchframe"] = {
				["enable"] = false,
			},
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.14",
		},
		["리떼 - 아즈샤라"] = {
			["general"] = {
				["dmgfont"] = "데미지 글꼴",
				["replaceBlizzFonts"] = false,
				["selectquestreward"] = false,
			},
			["nameplates"] = {
				["enable"] = false,
			},
			["equipment"] = {
				["itemlevel"] = {
					["enable"] = false,
				},
			},
			["watchframe"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.15",
		},
		["Emonda - 아즈샤라"] = {
			["equipment"] = {
				["itemlevel"] = {
					["enable"] = false,
				},
			},
			["general"] = {
				["replaceBlizzFonts"] = false,
				["selectquestreward"] = false,
				["dmgfont"] = "데미지 글꼴",
			},
			["nameplates"] = {
				["enable"] = false,
			},
			["watchframe"] = {
				["enable"] = false,
			},
			["theme"] = "default",
			["install_complete"] = "10.41",
		},
		["Lezyne - 아즈샤라"] = {
			["equipment"] = {
				["itemlevel"] = {
					["enable"] = false,
				},
			},
			["general"] = {
				["replaceBlizzFonts"] = false,
				["selectquestreward"] = false,
				["dmgfont"] = "데미지 글꼴",
			},
			["watchframe"] = {
				["enable"] = false,
			},
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.29",
		},
		["Vlaanderen - 아즈샤라"] = {
			["equipment"] = {
				["itemlevel"] = {
					["enable"] = false,
				},
			},
			["general"] = {
				["replaceBlizzFonts"] = false,
				["selectquestreward"] = false,
				["dmgfont"] = "데미지 글꼴",
			},
			["nameplates"] = {
				["enable"] = false,
			},
			["watchframe"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.15",
		},
		["영석썼숑 - 줄진"] = {
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.14",
		},
		["사나정연 - 아즈샤라"] = {
			["equipment"] = {
				["itemlevel"] = {
					["enable"] = false,
				},
			},
			["general"] = {
				["dmgfont"] = "데미지 글꼴",
			},
			["watchframe"] = {
				["enable"] = false,
			},
			["nameplates"] = {
				["enable"] = false,
			},
			["install_complete"] = "10.68",
		},
	},
}
