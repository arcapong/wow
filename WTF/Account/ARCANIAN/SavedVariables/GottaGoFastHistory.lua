
GottaGoFastHistoryDB = {
	["profileKeys"] = {
		["샤이아리 - 줄진"] = "Default",
		["언제나운무 - 아즈샤라"] = "Default",
		["최저방벽 - 줄진"] = "Default",
		["Emonda - 아즈샤라"] = "Default",
		["사나정연 - 아즈샤라"] = "Default",
		["아르니타 - 아즈샤라"] = "Default",
		["사나무엇 - 아즈샤라"] = "Default",
		["사나없찐 - 아즈샤라"] = "Default",
		["샤이아리 - 아즈샤라"] = "Default",
		["Wilier - 아즈샤라"] = "Default",
		["Lezyne - 아즈샤라"] = "Default",
		["샤말밀레 - 아즈샤라"] = "Default",
		["데로사 - 아즈샤라"] = "Default",
		["Santini - 아즈샤라"] = "Default",
		["써벨로 - 아즈샤라"] = "Default",
		["리떼 - 아즈샤라"] = "Default",
		["클리엔 - 줄진"] = "Default",
		["Vlaanderen - 아즈샤라"] = "Default",
		["릿데 - 아즈샤라"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["History"] = {
				[1492] = {
					["objectives"] = {
						"타락한 왕 이미론", -- [1]
						"하르바론", -- [2]
						"헬리아", -- [3]
						"적 병력", -- [4]
					},
					["zoneID"] = 1492,
					["name"] = "영혼의 아귀",
					["runs"] = {
						{
							["corrupt"] = false,
							["deaths"] = 31,
							["affixes"] = {
								[5] = {
									["name"] = "무리",
									["desc"] = "던전 내의 우두머리가 아닌 적의 수가 증가합니다.",
								},
								[2] = {
									["name"] = "변덕",
									["desc"] = "모든 적에 대해 방어 전담이 생성하는 위협 수준이 감소합니다.",
								},
							},
							["active"] = false,
							["endTime"] = 434109.011,
							["timeStamp"] = {
								["day"] = 14,
								["month"] = 3,
								["hours"] = 13,
								["year"] = 2017,
								["mins"] = 37,
							},
							["startTime"] = 432745.158,
							["level"] = 8,
							["objectiveTimes"] = {
								"03:05.062", -- [1]
								"09:53.944", -- [2]
								"25:18.263", -- [3]
								"14:43.988", -- [4]
							},
							["players"] = {
								{
									["class"] = "악마사냥꾼",
									["name"] = "와이프뿔났다",
									["role"] = "TANK",
								}, -- [1]
								{
									["class"] = "사냥꾼",
									["name"] = "홍유나",
									["role"] = "DAMAGER",
								}, -- [2]
								{
									["class"] = "수도사",
									["name"] = "미련탱",
									["role"] = "DAMAGER",
								}, -- [3]
								{
									["class"] = "악마사냥꾼",
									["name"] = "Veraverto",
									["role"] = "DAMAGER",
								}, -- [4]
								{
									["class"] = "사제",
									["name"] = "샤이아리",
									["role"] = "HEALER",
								}, -- [5]
							},
						}, -- [1]
					},
				},
				[1493] = {
					["objectives"] = {
						"티라손 살데릴", -- [1]
						"심문관 토르멘토룸", -- [2]
						"잿바위거수", -- [3]
						"꿈벅마", -- [4]
						"콜다나 펠송", -- [5]
						"적 병력", -- [6]
					},
					["zoneID"] = 1493,
					["name"] = "감시관의 금고",
					["runs"] = {
						{
							["corrupt"] = false,
							["deaths"] = 0,
							["affixes"] = {
							},
							["active"] = false,
							["endTime"] = 92782.476,
							["timeStamp"] = {
								["day"] = 27,
								["month"] = 1,
								["hours"] = 11,
								["year"] = 2017,
								["mins"] = 18,
							},
							["level"] = 2,
							["startTime"] = 91740.001,
							["objectiveTimes"] = {
								"03:47.664", -- [1]
								"07:30.189", -- [2]
								"09:30.181", -- [3]
								"11:21.744", -- [4]
								"17:22.174", -- [5]
								"14:43.460", -- [6]
							},
							["players"] = {
								{
									["class"] = "수도사",
									["name"] = "황기둥(*)",
									["role"] = "TANK",
								}, -- [1]
								{
									["class"] = "주술사",
									["name"] = "듬해머(*)",
									["role"] = "DAMAGER",
								}, -- [2]
								{
									["class"] = "사냥꾼",
									["name"] = "행버거(*)",
									["role"] = "DAMAGER",
								}, -- [3]
								{
									["class"] = "도적",
									["name"] = "탁구왕지단(*)",
									["role"] = "DAMAGER",
								}, -- [4]
								{
									["class"] = "사제",
									["name"] = "써벨로",
									["role"] = "HEALER",
								}, -- [5]
							},
						}, -- [1]
					},
				},
				[1466] = {
					["runs"] = {
						{
							["corrupt"] = false,
							["deaths"] = 0,
							["affixes"] = {
								[7] = {
									["name"] = "강화",
									["desc"] = "우두머리가 아닌 적이 죽으면 그 죽음의 메아리가 주위의 아군을 강화하여, 최대 생명력과 공격력이 20%만큼 증가합니다.",
								},
							},
							["active"] = false,
							["endTime"] = 52166.634,
							["timeStamp"] = {
								["day"] = 27,
								["month"] = 1,
								["hours"] = 0,
								["mins"] = 2,
								["year"] = 2017,
							},
							["players"] = {
								{
									["class"] = "사냥꾼",
									["name"] = "사냥성엘프(*)",
									["role"] = "DAMAGER",
								}, -- [1]
								{
									["class"] = "사냥꾼",
									["name"] = "브리츠전사(*)",
									["role"] = "DAMAGER",
								}, -- [2]
								{
									["class"] = "사냥꾼",
									["name"] = "준칼",
									["role"] = "DAMAGER",
								}, -- [3]
								{
									["class"] = "성기사",
									["name"] = "지쿠다",
									["role"] = "TANK",
								}, -- [4]
								{
									["class"] = "주술사",
									["name"] = "Emonda",
									["role"] = "HEALER",
								}, -- [5]
							},
							["startTime"] = 51236.725,
							["objectiveTimes"] = {
								"04:19.168", -- [1]
								"08:49.636", -- [2]
								"12:05.348", -- [3]
								"15:29.341", -- [4]
								"14:19.738", -- [5]
							},
							["level"] = 4,
						}, -- [1]
						{
							["corrupt"] = false,
							["deaths"] = 0,
							["affixes"] = {
								[7] = {
									["name"] = "강화",
									["desc"] = "우두머리가 아닌 적이 죽으면 그 죽음의 메아리가 주위의 아군을 강화하여, 최대 생명력과 공격력이 20%만큼 증가합니다.",
								},
							},
							["active"] = false,
							["endTime"] = 2350538.545,
							["timeStamp"] = {
								["day"] = 27,
								["month"] = 2,
								["hours"] = 0,
								["year"] = 2017,
								["mins"] = 5,
							},
							["level"] = 4,
							["startTime"] = 2349600.419,
							["objectiveTimes"] = {
								"04:20.742", -- [1]
								"09:01.528", -- [2]
								"12:10.860", -- [3]
								"15:37.804", -- [4]
								"14:40.259", -- [5]
							},
							["players"] = {
								{
									["class"] = "전사",
									["name"] = "맛나통닭",
									["role"] = "TANK",
								}, -- [1]
								{
									["class"] = "흑마법사",
									["name"] = "혜설",
									["role"] = "DAMAGER",
								}, -- [2]
								{
									["class"] = "사제",
									["name"] = "사제유니",
									["role"] = "DAMAGER",
								}, -- [3]
								{
									["class"] = "사제",
									["name"] = "미스티레이크(*)",
									["role"] = "HEALER",
								}, -- [4]
								{
									["class"] = "사제",
									["name"] = "써벨로",
									["role"] = "DAMAGER",
								}, -- [5]
							},
						}, -- [2]
						{
							["corrupt"] = false,
							["deaths"] = 0,
							["affixes"] = {
								[7] = {
									["name"] = "강화",
									["desc"] = "우두머리가 아닌 적이 죽으면 그 죽음의 메아리가 주위의 아군을 강화하여, 최대 생명력과 공격력이 20%만큼 증가합니다.",
								},
							},
							["active"] = false,
							["endTime"] = 2354912.118,
							["timeStamp"] = {
								["day"] = 27,
								["month"] = 2,
								["hours"] = 1,
								["year"] = 2017,
								["mins"] = 18,
							},
							["startTime"] = 2353858.476,
							["level"] = 5,
							["objectiveTimes"] = {
								"05:31.952", -- [1]
								"10:51.394", -- [2]
								"13:43.081", -- [3]
								"17:33.082", -- [4]
								"16:26.420", -- [5]
							},
							["players"] = {
								{
									["class"] = "성기사",
									["name"] = "카운트갑니다",
									["role"] = "TANK",
								}, -- [1]
								{
									["class"] = "마법사",
									["name"] = "법사이긔(*)",
									["role"] = "DAMAGER",
								}, -- [2]
								{
									["class"] = "사냥꾼",
									["name"] = "청주제일매너남(*)",
									["role"] = "DAMAGER",
								}, -- [3]
								{
									["class"] = "사제",
									["name"] = "오레오(*)",
									["role"] = "DAMAGER",
								}, -- [4]
								{
									["class"] = "주술사",
									["name"] = "Emonda",
									["role"] = "HEALER",
								}, -- [5]
							},
						}, -- [3]
					},
					["name"] = "어둠심장 숲",
					["zoneID"] = 1466,
					["objectives"] = {
						"대드루이드 글라이달리스", -- [1]
						"나무심장", -- [2]
						"드레사론", -- [3]
						"자비우스의 망령", -- [4]
						"적 병력", -- [5]
					},
				},
				[1501] = {
					["runs"] = {
						{
							["corrupt"] = false,
							["deaths"] = 4,
							["affixes"] = {
								[7] = {
									["name"] = "강화",
									["desc"] = "우두머리가 아닌 적이 죽으면 그 죽음의 메아리가 주위의 아군을 강화하여, 최대 생명력과 공격력이 20%만큼 증가합니다.",
								},
							},
							["active"] = false,
							["endTime"] = 610118.658,
							["timeStamp"] = {
								["day"] = 31,
								["month"] = 1,
								["hours"] = 13,
								["mins"] = 47,
								["year"] = 2017,
							},
							["players"] = {
								{
									["class"] = "드루이드",
									["name"] = "응가냄시",
									["role"] = "TANK",
								}, -- [1]
								{
									["class"] = "사제",
									["name"] = "악마의똥가루",
									["role"] = "HEALER",
								}, -- [2]
								{
									["class"] = "사냥꾼",
									["name"] = "블엘냥꾼괜춘한가",
									["role"] = "DAMAGER",
								}, -- [3]
								{
									["class"] = "주술사",
									["name"] = "스마슈(*)",
									["role"] = "DAMAGER",
								}, -- [4]
								{
									["class"] = "사냥꾼",
									["name"] = "Vlaanderen",
									["role"] = "DAMAGER",
								}, -- [5]
							},
							["startTime"] = 609048.134,
							["objectiveTimes"] = {
								"02:18.724", -- [1]
								"08:50.481", -- [2]
								"14:55.067", -- [3]
								"18:10.113", -- [4]
								"16:27.482", -- [5]
							},
							["level"] = 5,
						}, -- [1]
						{
							["corrupt"] = false,
							["deaths"] = 7,
							["affixes"] = {
								[7] = {
									["name"] = "강화",
									["desc"] = "우두머리가 아닌 적이 죽으면 그 죽음의 메아리가 주위의 아군을 강화하여, 최대 생명력과 공격력이 20%만큼 증가합니다.",
								},
							},
							["active"] = false,
							["endTime"] = 48859.292,
							["timeStamp"] = {
								["day"] = 25,
								["month"] = 3,
								["hours"] = 13,
								["mins"] = 35,
								["year"] = 2017,
							},
							["players"] = {
								{
									["class"] = "성기사",
									["name"] = "Cgsib(*)",
									["role"] = "TANK",
								}, -- [1]
								{
									["class"] = "흑마법사",
									["name"] = "니코크더만",
									["role"] = "DAMAGER",
								}, -- [2]
								{
									["class"] = "흑마법사",
									["name"] = "암흑사진",
									["role"] = "DAMAGER",
								}, -- [3]
								{
									["class"] = "사제",
									["name"] = "로얄발렌타인(*)",
									["role"] = "HEALER",
								}, -- [4]
								{
									["class"] = "마법사",
									["name"] = "리떼",
									["role"] = "DAMAGER",
								}, -- [5]
							},
							["level"] = 6,
							["objectiveTimes"] = {
								"04:20.378", -- [1]
								"11:50.349", -- [2]
								"21:14.224", -- [3]
								"25:49.996", -- [4]
								"23:47.096", -- [5]
							},
							["startTime"] = 47343.766,
						}, -- [2]
					},
					["name"] = "검은 떼까마귀 요새",
					["zoneID"] = 1501,
					["objectives"] = {
						"영혼의 융합체", -- [1]
						"일리산나 레이븐크레스트", -- [2]
						"혐오스러운 원한강타", -- [3]
						"군주 쿠르탈로스 레이븐크레스트", -- [4]
						"적 병력", -- [5]
					},
				},
				[1477] = {
					["runs"] = {
						{
							["corrupt"] = false,
							["deaths"] = 2,
							["affixes"] = {
								[7] = {
									["name"] = "강화",
									["desc"] = "우두머리가 아닌 적이 죽으면 그 죽음의 메아리가 주위의 아군을 강화하여, 최대 생명력과 공격력이 20%만큼 증가합니다.",
								},
								[2] = {
									["name"] = "변덕",
									["desc"] = "모든 적에 대해 방어 전담이 생성하는 위협 수준이 감소합니다.",
								},
							},
							["active"] = false,
							["endTime"] = 2600652.415,
							["timeStamp"] = {
								["day"] = 1,
								["month"] = 3,
								["hours"] = 21,
								["mins"] = 34,
								["year"] = 2017,
							},
							["players"] = {
								{
									["class"] = "드루이드",
									["name"] = "산삼뿌엉",
									["role"] = "TANK",
								}, -- [1]
								{
									["class"] = "악마사냥꾼",
									["name"] = "슬픈신사",
									["role"] = "DAMAGER",
								}, -- [2]
								{
									["class"] = "수도사",
									["name"] = "마리루즈",
									["role"] = "HEALER",
								}, -- [3]
								{
									["class"] = "마법사",
									["name"] = "또또잠보",
									["role"] = "DAMAGER",
								}, -- [4]
								{
									["class"] = "사냥꾼",
									["name"] = "Vlaanderen",
									["role"] = "DAMAGER",
								}, -- [5]
							},
							["startTime"] = 2599361.486,
							["objectiveTimes"] = {
								"01:58.645", -- [1]
								"09:10.563", -- [2]
								"14:05.066", -- [3]
								"19:50.412", -- [4]
								"21:40.352", -- [5]
								"19:50.412", -- [6]
							},
							["level"] = 7,
						}, -- [1]
					},
					["name"] = "용맹의 전당",
					["zoneID"] = 1477,
					["objectives"] = {
						"하임달", -- [1]
						"히리아", -- [2]
						"펜리르", -- [3]
						"신왕 스코발드", -- [4]
						"오딘", -- [5]
						"적 병력", -- [6]
					},
				},
				[1516] = {
					["objectives"] = {
						"아이반니르", -- [1]
						"코스틸락스", -- [2]
						"장군 자칼", -- [3]
						"날티라", -- [4]
						"조언자 반드로스", -- [5]
						"적 병력", -- [6]
					},
					["zoneID"] = 1516,
					["name"] = "비전로",
					["runs"] = {
						{
							["corrupt"] = true,
							["deaths"] = 1,
							["affixes"] = {
							},
							["active"] = false,
							["endTime"] = 419781.2,
							["timeStamp"] = {
								["day"] = 4,
								["month"] = 2,
								["hours"] = 15,
								["mins"] = 47,
								["year"] = 2017,
							},
							["players"] = {
								{
									["class"] = "악마사냥꾼",
									["name"] = "바라자스",
									["role"] = "TANK",
								}, -- [1]
								{
									["class"] = "마법사",
									["name"] = "브랜드선스트레더",
									["role"] = "DAMAGER",
								}, -- [2]
								{
									["class"] = "흑마법사",
									["name"] = "헤움(*)",
									["role"] = "DAMAGER",
								}, -- [3]
								{
									["class"] = "마법사",
									["name"] = "Nostradamux",
									["role"] = "DAMAGER",
								}, -- [4]
								{
									["class"] = "주술사",
									["name"] = "Emonda",
									["role"] = "HEALER",
								}, -- [5]
							},
							["startTime"] = 418469.2,
							["objectiveTimes"] = {
								"21:52", -- [1]
								"21:52", -- [2]
								"21:52", -- [3]
								"21:52", -- [4]
								"21:52", -- [5]
								"21:52", -- [6]
							},
							["level"] = 3,
						}, -- [1]
						{
							["corrupt"] = false,
							["deaths"] = 14,
							["affixes"] = {
								[7] = {
									["name"] = "강화",
									["desc"] = "우두머리가 아닌 적이 죽으면 그 죽음의 메아리가 주위의 아군을 강화하여, 최대 생명력과 공격력이 20%만큼 증가합니다.",
								},
							},
							["active"] = false,
							["endTime"] = 2315472.526,
							["timeStamp"] = {
								["day"] = 26,
								["month"] = 2,
								["hours"] = 14,
								["year"] = 2017,
								["mins"] = 21,
							},
							["level"] = 6,
							["startTime"] = 2313446.93,
							["objectiveTimes"] = {
								"10:58.054", -- [1]
								"14:38.993", -- [2]
								"28:05.950", -- [3]
								"19:29.338", -- [4]
								"34:55.152", -- [5]
								"28:05.950", -- [6]
							},
							["players"] = {
								{
									["class"] = "죽음의 기사",
									["name"] = "갈죽긔",
									["role"] = "TANK",
								}, -- [1]
								{
									["class"] = "성기사",
									["name"] = "Bloodshot",
									["role"] = "HEALER",
								}, -- [2]
								{
									["class"] = "마법사",
									["name"] = "쓰벌창허벌창",
									["role"] = "DAMAGER",
								}, -- [3]
								{
									["class"] = "흑마법사",
									["name"] = "사려깊음",
									["role"] = "DAMAGER",
								}, -- [4]
								{
									["class"] = "마법사",
									["name"] = "리떼",
									["role"] = "DAMAGER",
								}, -- [5]
							},
						}, -- [2]
						{
							["corrupt"] = false,
							["deaths"] = 1,
							["affixes"] = {
								[8] = {
									["name"] = "피웅덩이",
									["desc"] = "우두머리가 아닌 적이 죽으면 수액 웅덩이가 남아, 적의 아군을 치유하고 플레이어에게 피해를 입힙니다.",
								},
							},
							["active"] = false,
							["endTime"] = 771948.826,
							["timeStamp"] = {
								["day"] = 3,
								["month"] = 3,
								["hours"] = 13,
								["mins"] = 59,
								["year"] = 2017,
							},
							["players"] = {
								{
									["class"] = "성기사",
									["name"] = "셰르비아",
									["role"] = "TANK",
								}, -- [1]
								{
									["class"] = "사제",
									["name"] = "다주빠뿌(*)",
									["role"] = "DAMAGER",
								}, -- [2]
								{
									["class"] = "악마사냥꾼",
									["name"] = "꼬마악마루시",
									["role"] = "DAMAGER",
								}, -- [3]
								{
									["class"] = "흑마법사",
									["name"] = "헉털이",
									["role"] = "DAMAGER",
								}, -- [4]
								{
									["class"] = "주술사",
									["name"] = "Emonda",
									["role"] = "HEALER",
								}, -- [5]
							},
							["level"] = 5,
							["objectiveTimes"] = {
								"12:51.748", -- [1]
								"09:19.993", -- [2]
								"18:50.602", -- [3]
								"05:34.511", -- [4]
								"21:50.833", -- [5]
								"18:50.602", -- [6]
							},
							["startTime"] = 770642.53,
						}, -- [3]
					},
				},
				[1458] = {
					["runs"] = {
						{
							["corrupt"] = false,
							["deaths"] = 0,
							["affixes"] = {
							},
							["active"] = false,
							["endTime"] = 177662.009,
							["timeStamp"] = {
								["day"] = 26,
								["month"] = 1,
								["hours"] = 13,
								["mins"] = 39,
								["year"] = 2017,
							},
							["players"] = {
								{
									["class"] = "성기사",
									["name"] = "사무치는고독(*)",
									["role"] = "DAMAGER",
								}, -- [1]
								{
									["class"] = "사제",
									["name"] = "카마네(*)",
									["role"] = "DAMAGER",
								}, -- [2]
								{
									["class"] = "주술사",
									["name"] = "가비앤제이문",
									["role"] = "DAMAGER",
								}, -- [3]
								{
									["class"] = "전사",
									["name"] = "포악한콩알(*)",
									["role"] = "TANK",
								}, -- [4]
								{
									["class"] = "주술사",
									["name"] = "Emonda",
									["role"] = "HEALER",
								}, -- [5]
							},
							["level"] = 2,
							["objectiveTimes"] = {
								"03:31.028", -- [1]
								"09:32.337", -- [2]
								"13:24.950", -- [3]
								"16:41.094", -- [4]
								"17:09.761", -- [5]
							},
							["startTime"] = 176632.069,
						}, -- [1]
						{
							["corrupt"] = false,
							["deaths"] = 1,
							["affixes"] = {
								[6] = {
									["name"] = "분노",
									["desc"] = "우두머리가 아닌 적의 남은 생명력이 30% 이하로 떨어지면 격노 상태가 되어, 죽기 전까지 공격력이 100%만큼 증가합니다.",
								},
								[3] = {
									["name"] = "화산",
									["desc"] = "전투 중 주기적으로 적이 멀리 떨어진 플레이어의 발 밑에서 불길이 솟아나오게 합니다.",
								},
							},
							["active"] = false,
							["endTime"] = 1276640.585,
							["timeStamp"] = {
								["day"] = 22,
								["month"] = 2,
								["hours"] = 13,
								["year"] = 2017,
								["mins"] = 24,
							},
							["level"] = 7,
							["startTime"] = 1275731.271,
							["objectiveTimes"] = {
								"03:01.676", -- [1]
								"08:57.527", -- [2]
								"12:06.769", -- [3]
								"14:55.359", -- [4]
								"15:13.936", -- [5]
							},
							["players"] = {
								{
									["class"] = "도적",
									["name"] = "Korki(*)",
									["role"] = "DAMAGER",
								}, -- [1]
								{
									["class"] = "악마사냥꾼",
									["name"] = "드루악",
									["role"] = "DAMAGER",
								}, -- [2]
								{
									["class"] = "악마사냥꾼",
									["name"] = "악사뎅이(*)",
									["role"] = "DAMAGER",
								}, -- [3]
								{
									["class"] = "죽음의 기사",
									["name"] = "다크니스쥬(*)",
									["role"] = "TANK",
								}, -- [4]
								{
									["class"] = "주술사",
									["name"] = "Emonda",
									["role"] = "HEALER",
								}, -- [5]
							},
						}, -- [2]
						{
							["corrupt"] = false,
							["deaths"] = 3,
							["affixes"] = {
								[5] = {
									["name"] = "무리",
									["desc"] = "던전 내의 우두머리가 아닌 적의 수가 증가합니다.",
								},
							},
							["active"] = false,
							["endTime"] = 432152.739,
							["timeStamp"] = {
								["day"] = 14,
								["month"] = 3,
								["hours"] = 13,
								["year"] = 2017,
								["mins"] = 4,
							},
							["startTime"] = 431181.146,
							["level"] = 5,
							["objectiveTimes"] = {
								"04:01.773", -- [1]
								"09:44.058", -- [2]
								"13:26.707", -- [3]
								"16:25.800", -- [4]
								"15:27.551", -- [5]
							},
							["players"] = {
								{
									["class"] = "악마사냥꾼",
									["name"] = "와이프뿔났다",
									["role"] = "TANK",
								}, -- [1]
								{
									["class"] = "사냥꾼",
									["name"] = "홍유나",
									["role"] = "DAMAGER",
								}, -- [2]
								{
									["class"] = "주술사",
									["name"] = "꼬햄술사",
									["role"] = "DAMAGER",
								}, -- [3]
								{
									["class"] = "죽음의 기사",
									["name"] = "심심해서만든팥죽",
									["role"] = "DAMAGER",
								}, -- [4]
								{
									["class"] = "사제",
									["name"] = "샤이아리",
									["role"] = "HEALER",
								}, -- [5]
							},
						}, -- [3]
					},
					["name"] = "넬타리온의 둥지",
					["zoneID"] = 1458,
					["objectives"] = {
						"로크모라", -- [1]
						"바위구체자 울라로그", -- [2]
						"나락사스", -- [3]
						"다르그룰", -- [4]
						"적 병력", -- [5]
					},
				},
			},
		},
	},
}
