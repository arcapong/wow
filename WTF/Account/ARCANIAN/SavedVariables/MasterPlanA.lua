
MasterPlanAG = {
	["줄진"] = {
		["북경스딸"] = {
			["lastCacheTime"] = 1471189951,
			["class"] = "MONK",
			["curRes"] = 1748,
			["faction"] = "Alliance",
		},
		["클리엔"] = {
			["summary"] = {
				["ti2"] = 122486,
				["ti3"] = 127853,
				["tt1"] = true,
				["ti1"] = 118531,
				["tt3"] = true,
				["tt2"] = true,
			},
			["curOil"] = 2020,
			["faction"] = "Alliance",
			["class"] = "PRIEST",
			["curRes"] = 8650,
			["lastCacheTime"] = 1471188186,
		},
		["징박무시하나연"] = {
			["class"] = "PALADIN",
			["summary"] = {
				["ti1"] = 118531,
				["inProgress"] = {
					[670] = 1470730529,
					[312] = 1470737744,
					[667] = 1470730533,
					[496] = 1470744937,
					[685] = 1470737717,
					[188] = 1470718848,
					[323] = 1470744921,
					[210] = 1470721541,
				},
				["tt1"] = 1470719076,
			},
			["faction"] = "Alliance",
		},
		["재연마사"] = {
			["class"] = "HUNTER",
			["summary"] = {
				["tt2"] = true,
				["tt1"] = true,
				["ti1"] = 118531,
				["inProgress"] = {
					[261] = 1431267395,
					[192] = 1431256583,
					[193] = 1431258380,
					[127] = 1431267401,
					[194] = 1431258385,
					[202] = 1431256363,
					[495] = 1431281790,
					[125] = 1431267397,
					[133] = 1431258378,
					[396] = 1431281793,
				},
				["ti2"] = 122486,
			},
			["faction"] = "Alliance",
		},
		["Emonda"] = {
			["class"] = "DEMONHUNTER",
			["faction"] = "Alliance",
		},
		["뚱빼미나무"] = {
			["class"] = "DRUID",
			["faction"] = "Alliance",
		},
		["뽕나인"] = {
			["class"] = "SHAMAN",
			["summary"] = {
				["ti1"] = 118531,
				["inProgress"] = {
					[269] = 1470730437,
					[242] = 1470721443,
					[176] = 1470718734,
					[683] = 1470737620,
					[402] = 1470737623,
					[323] = 1470719617,
					[282] = 1470723230,
					[214] = 1470723227,
				},
				["tt1"] = 1470718954,
			},
			["faction"] = "Alliance",
		},
		["최저방벽"] = {
			["faction"] = "Alliance",
			["class"] = "WARRIOR",
		},
		["안녕김밥"] = {
			["faction"] = "Alliance",
			["class"] = "DEATHKNIGHT",
		},
		["꼬꼬마빠샤"] = {
			["class"] = "ROGUE",
			["summary"] = {
				["ti1"] = 118531,
				["inProgress"] = {
					[311] = 1470737857,
					[410] = 1470802244,
					[212] = 1470723010,
					[188] = 1470718964,
					[394] = 1470745049,
					[322] = 1470730641,
				},
				["tt1"] = 1470719190,
			},
			["faction"] = "Alliance",
		},
		["영석썼숑"] = {
			["faction"] = "Alliance",
			["class"] = "WARLOCK",
		},
		["샤이아리"] = {
			["class"] = "MAGE",
			["summary"] = {
				["inProgress"] = {
					[217] = 1470717698,
					[242] = 1470721302,
					[176] = 1470718604,
					[336] = 1470733883,
					[396] = 1470744677,
					[210] = 1470721289,
					[671] = 1470723072,
				},
			},
			["faction"] = "Alliance",
		},
	},
	["아즈샤라"] = {
		["Santini"] = {
			["faction"] = "Horde",
			["class"] = "WARLOCK",
		},
		["환상큰손"] = {
			["faction"] = "Horde",
			["class"] = "HUNTER",
		},
		["환상큰소"] = {
			["class"] = "DRUID",
			["faction"] = "Horde",
		},
		["써벨로"] = {
			["class"] = "PRIEST",
			["faction"] = "Horde",
		},
		["Emonda"] = {
			["recruitTime"] = 1471581289,
			["curOil"] = 5,
			["class"] = "SHAMAN",
			["lastCacheTime"] = 1471927631,
			["curRes"] = 1108,
			["faction"] = "Horde",
		},
		["리떼"] = {
			["class"] = "MAGE",
			["faction"] = "Horde",
		},
		["Wilier"] = {
			["faction"] = "Horde",
			["class"] = "PALADIN",
		},
		["Vlaanderen"] = {
			["class"] = "ROGUE",
			["faction"] = "Horde",
		},
		["데로사"] = {
			["class"] = "DEMONHUNTER",
			["lastCacheTime"] = 1471659814,
			["curRes"] = 1465,
			["faction"] = "Horde",
		},
	},
}
