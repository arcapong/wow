
EnemyGridDB = {
	["profileKeys"] = {
		["샤이아리 - 줄진"] = "Default",
		["언제나운무 - 아즈샤라"] = "Default",
		["최저방벽 - 줄진"] = "Default",
		["Emonda - 아즈샤라"] = "Default",
		["아르니타 - 아즈샤라"] = "Default",
		["사나없찐 - 아즈샤라"] = "Default",
		["샤이아리 - 아즈샤라"] = "Default",
		["Wilier - 아즈샤라"] = "Default",
		["Lezyne - 아즈샤라"] = "Default",
		["샤말밀레 - 아즈샤라"] = "Default",
		["데로사 - 아즈샤라"] = "Default",
		["Santini - 아즈샤라"] = "Default",
		["써벨로 - 아즈샤라"] = "Default",
		["리떼 - 아즈샤라"] = "Default",
		["클리엔 - 줄진"] = "Default",
		["Vlaanderen - 아즈샤라"] = "Default",
		["릿데 - 아즈샤라"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["cast_statusbar_height"] = 23,
			["cast_statusbar_width"] = 115,
			["frame_locked"] = true,
			["npc_friendly_color"] = {
				0.4, -- [1]
				nil, -- [2]
				0, -- [3]
				1, -- [4]
			},
			["horizontal_gap_size"] = 0,
			["x"] = 288.000122070313,
			["percent_text_color"] = {
				0.972549019607843, -- [1]
				[3] = 0.972549019607843,
			},
			["tank"] = {
				["colors"] = {
					["nocombat"] = {
						0.341176470588235, -- [1]
						0.00392156862745098, -- [2]
						0.0117647058823529, -- [3]
						1, -- [4]
					},
				},
			},
			["iconIndicator_anchor"] = {
				["x"] = 0,
			},
			["scale"] = 1,
			["cast_statusbar_color"] = {
				nil, -- [1]
				0.701960784313726, -- [2]
			},
			["npc_neutral_color"] = {
				nil, -- [1]
				0.843137254901961, -- [2]
				0, -- [3]
				1, -- [4]
			},
			["npc_enemy_color"] = {
				nil, -- [1]
				0.0901960784313726, -- [2]
				0, -- [3]
				1, -- [4]
			},
			["aura_height"] = 22,
			["bar_width"] = 115,
			["aura_width"] = 26,
			["frame_backdropcolor"] = {
				0.643137254901961, -- [1]
				0.643137254901961, -- [2]
				0.643137254901961, -- [3]
				0.210000038146973, -- [4]
			},
			["bar_height"] = 25,
			["debuff_anchor_grow_direction"] = "left",
			["percent_text_anchor"] = {
				["x"] = 2,
			},
			["cast_statusbar_anchor"] = {
				["side"] = 10,
			},
			["bar_texture"] = "DGround",
			["debuff_anchor"] = {
				["x"] = -2,
				["side"] = 2,
			},
			["point"] = "CENTER",
			["grow_direction"] = 2,
			["frame_backdropbordercolor"] = {
				nil, -- [1]
				nil, -- [2]
				nil, -- [3]
				0.310000002384186, -- [4]
			},
			["cast_statusbar_bgcolor"] = {
				nil, -- [1]
				nil, -- [2]
				nil, -- [3]
				0.724460273981094, -- [4]
			},
			["first_run"] = true,
			["percent_text_size"] = 11,
			["name_text_stringsize"] = 80,
			["dps"] = {
				["colors"] = {
					["aggro"] = {
						nil, -- [1]
						0.345098039215686, -- [2]
						0.294117647058824, -- [3]
						1, -- [4]
					},
				},
			},
			["name_text_size"] = 11,
			["bar_color"] = {
				0.980392156862745, -- [1]
				0, -- [2]
				1, -- [3]
			},
			["cast_statusbar_texture"] = "PlaterTexture",
			["vertical_gap_size"] = 0,
			["path_7_11_warning"] = true,
			["frame_range_alpha"] = 0.33254075050354,
			["y"] = 124.000122070313,
		},
	},
}
