
AtlasLootCharDB = {
	["MiniMapButton"] = {
		["point"] = {
			"CENTER", -- [1]
			nil, -- [2]
			"CENTER", -- [3]
			-65.3499984741211, -- [4]
			-38.7999992370606, -- [5]
		},
		["shown"] = false,
	},
	["GUI"] = {
		["selected"] = {
			[4] = 1,
			[5] = 0,
		},
	},
	["__addonrevision"] = 4325,
	["QuickLootFrame"] = {
		["point"] = {
			[3] = "CENTER",
			[4] = -6.565175135619940e-005,
			[5] = 3.282587567809970e-005,
		},
	},
	["minimap"] = {
		["shown"] = false,
		["hide"] = true,
	},
}
