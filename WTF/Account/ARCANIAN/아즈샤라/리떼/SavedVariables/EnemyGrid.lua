
EnemyGridDBChr = {
	["KeyBinds"] = {
		[64] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
		},
		[63] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
		},
		[62] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
		},
	},
	["specEnabled"] = {
		[64] = false,
		[63] = false,
		[62] = false,
	},
	["debuffsBanned"] = {
	},
	["spellRangeCheck"] = {
		[64] = "얼음 화살",
		[63] = "화염구",
		[62] = "비전 작렬",
	},
}
