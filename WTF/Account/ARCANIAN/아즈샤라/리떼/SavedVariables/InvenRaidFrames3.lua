
InvenRaidFrames3CharDB = {
	["clickCasting"] = {
		{
			["shift-1"] = "spell__저속 낙하",
		}, -- [1]
		{
			["shift-2"] = "spell__저속 낙하",
		}, -- [2]
		{
			["shift-2"] = "spell__저속 낙하",
			["shift-1"] = "spell__저속 낙하",
		}, -- [3]
		{
		}, -- [4]
	},
	["class"] = "MAGE",
	["spellTimer"] = {
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "BOTTOMLEFT",
		}, -- [1]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "BOTTOM",
		}, -- [2]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "BOTTOMRIGHT",
		}, -- [3]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "LEFT",
		}, -- [4]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "RIGHT",
		}, -- [5]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOPLEFT",
		}, -- [6]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOP",
		}, -- [7]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOPRIGHT",
		}, -- [8]
	},
	["classBuff2"] = {
	},
}
