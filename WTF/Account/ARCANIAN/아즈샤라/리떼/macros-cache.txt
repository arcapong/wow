VER 3 0100000000000008 "물분출" "INV_MISC_QUESTIONMARK"
#showtooltip
/시전 물 분출
END
VER 3 010000000000000D "물빵" "INV_MISC_QUESTIONMARK"
#showtooltip
/사용 창조된 마나 찐빵
/사용 창조된 마나 케이크
END
VER 3 0100000000000009 "비작" "INV_MISC_QUESTIONMARK"
#showtooltip 비전 작렬
/시전 [nochanneling:신비한 화살] 비전 작렬
END
VER 3 0100000000000004 "양변" "INV_MISC_QUESTIONMARK"
#showtooltip 변이
/clearfocus [target=focus,dead]
/focus [mod:alt,harm][target=focus,noexists]
/clearfocus [target=focus,noharm]
/시전 [target=focus,harm] 변이; 변이
END
VER 3 0100000000000005 "얼구" "INV_MISC_QUESTIONMARK"
#showtooltip 얼어붙은 구슬
/사용 10
/시전 얼어붙은 구슬
END
VER 3 0100000000000002 "얼리기" "INV_MISC_QUESTIONMARK"
#showtooltip
/시전 얼리기
END
VER 3 0100000000000006 "얼방" "INV_MISC_QUESTIONMARK"
#showtooltip
/cancelaura 얼음 방패
/시전 얼음 방패
END
VER 3 010000000000000B "일방" "INV_MISC_QUESTIONMARK"
#showtooltip 얼음 방패
/시전 일렁임
/시전 얼음 방패
END
VER 3 010000000000000C "재귀차단" "INV_MISC_QUESTIONMARK"
/stopcasting
/대상 재귀의 정령
/시전 마법 차단
/외침 재귀 차단했습니다.
/targetlasttarget
END
VER 3 0100000000000003 "환복" "INV_MISC_QUESTIONMARK"
#showtooltip 환영 복제
/사용 14
/시전 환영 복제
END
