
InvenRaidFrames3CharDB = {
	["clickCasting"] = {
		{
		}, -- [1]
		{
		}, -- [2]
		{
			["1"] = "spell__해제",
			["shift-2"] = "spell__환생",
			["2"] = "spell__해제",
		}, -- [3]
		{
			["shift-2"] = "spell__환생",
			["2"] = "spell__자연의 치유력",
			["shift-1"] = "spell__정신 자극",
		}, -- [4]
	},
	["classBuff2"] = {
	},
	["class"] = "DRUID",
	["spellTimer"] = {
		{
			["name"] = "피어나는 생명",
			["scale"] = 1,
			["use"] = 1,
			["display"] = 1,
			["pos"] = "BOTTOMLEFT",
		}, -- [1]
		{
			["name"] = "회복",
			["scale"] = 1,
			["use"] = 1,
			["display"] = 1,
			["pos"] = "BOTTOM",
		}, -- [2]
		{
			["name"] = "재생",
			["scale"] = 1,
			["use"] = 1,
			["display"] = 1,
			["pos"] = "BOTTOMRIGHT",
		}, -- [3]
		{
			["name"] = "급속 성장",
			["scale"] = 1,
			["use"] = 1,
			["display"] = 1,
			["pos"] = "LEFT",
		}, -- [4]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "RIGHT",
		}, -- [5]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOPLEFT",
		}, -- [6]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOP",
		}, -- [7]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOPRIGHT",
		}, -- [8]
	},
}
