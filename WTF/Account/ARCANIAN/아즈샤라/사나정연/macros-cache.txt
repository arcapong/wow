VER 3 010000000000001E "광휘" "INV_MISC_QUESTIONMARK"
#showtooltip 신성한 광휘
/시전 [@mouseover,help][help][@player] 신성한 광휘
END
VER 3 0100000000000014 "기도" "INV_Misc_QuestionMark"
#showtooltip 신성한 기도
/시전 신성한 기도
END
VER 3 0100000000000026 "기물" "INV_MISC_QUESTIONMARK"
#showtooltip
/시전 [@mouseover,help][help][@player] 기의 물결
END
VER 3 0100000000000019 "무적" "INV_Misc_QuestionMark"
#showtooltip
/cancelaura 천상의 보호막
/시전 천상의 보호막
END
VER 3 0100000000000007 "발산" "INV_Misc_QuestionMark"
#showtooltip
/cast [@mouseover,help][help][@player] 발산
END
VER 3 0100000000000018 "서약" "INV_Misc_QuestionMark"
#showtooltip
/cast [@mouseover,help][help][@player] 영광의 서약
END
VER 3 0100000000000027 "신념" "INV_MISC_QUESTIONMARK"
#showtooltip
/시전 [@mouseover,help][help][@player] 신념 수여
END
VER 3 010000000000000C "신축" "INV_Misc_QuestionMark"
#showtooltip
/cast [target=mouseover,help] 신의 축복; [help] 신의 축복; [target=player] 신의 축복
END
VER 3 010000000000000E "신충" "INV_Misc_QuestionMark"
#showtooltip 신성 충격
/cast [@mouseover,exists][@target,exists][@player] 신성 충격
END
VER 3 0100000000000025 "자보" "INV_MISC_QUESTIONMARK"
#showtooltip
/cancelaura 보호의 축복
/시전 [@player] 보호의 축복 
END
VER 3 0100000000000024 "자수" "INV_MISC_QUESTIONMARK"
#showtooltip
/시전 [@player] 수호자의 손길
END
VER 3 0100000000000028 "자자" "INV_MISC_QUESTIONMARK"
#showtooltip
/시전 [@player] 자유의 축복
END
VER 3 010000000000001D "주보" "INV_Misc_QuestionMark"
#showtooltip
/시전 [@focustarget] 보호의 축복
END
VER 3 010000000000001B "차단" "INV_Misc_QuestionMark"
#showtooltip
/시전 [modifier] 심판의 망치; 비난
END
VER 3 0100000000000008 "포용" "INV_Misc_QuestionMark"
#showtooltip
/시전 [@mouseover,help][help][@player] 포용의 안개
END
VER 3 0100000000000017 "희축" "INV_MISC_QUESTIONMARK"
#showtooltip
/cast [@mouseover,help][help][@player] 희생의 축복
END
