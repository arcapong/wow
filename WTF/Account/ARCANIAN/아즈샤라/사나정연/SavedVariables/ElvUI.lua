
ElvCharacterDB = {
	["ChatEditHistory"] = {
		"/elvui", -- [1]
		"/tmw", -- [2]
		"/매크로", -- [3]
		"/g 호드 선택했음 ㅋㅋ", -- [4]
		"/g 수도사 이거 순례 한 다음에", -- [5]
		"/g 뭐 하면 버프 주는 거 없어졌나", -- [6]
		"/g 30부터인가보군", -- [7]
		"/g ㄷㄷ", -- [8]
		"/g 쪼렙 무작도 탱이 짱짱맨이구만", -- [9]
	},
	["ChatLog"] = {
		["1513569288.175"] = {
			"오늘 오후8시출 안토신화 4넴5넴(하사벨,이오나) 트라이팟 (3개잡힌팟)  3.5탐 딜2 모십니다", -- [1]
			"헬로스타-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"헬로스타", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			104, -- [11]
			"Player-205-058AF05A", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569269.907"] = {
			"안토 신화 월,화 2일일정 8시출발 4탐 2만올분 신기 모셔봅니다.", -- [1]
			"단스톰후프", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"단스톰후프", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			45, -- [11]
			"Player-205-06E91C94", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569285.843"] = {
			"안토 신화 부캐팟 3+@ 수요일 1시출발  최대5탐 힐러 (복술 운무) 스펙경험귓 ", -- [1]
			"게개", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"게개", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			99, -- [11]
			"Player-205-06709625", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569277.714"] = {
			"해피와우 =  레이드(밤요,살게,안토) // 레벨업 // 쐐기돌 // 신화던전 // 투기장정예룩 // 싸움꾼조합 // 마법사탑 // 영예의업적탈것/ 드군1위업체", -- [1]
			"해피해와우", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"해피해와우", -- [5]
			"DND", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			74, -- [11]
			"Player-205-06739C73", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569268.594"] = {
			":: 안토신화 :: 반고정모집, \"매주 목or월 오전10시\", 5탐, \"원딜(근딜풀), 힐러(복술)\", 착유경험귓이나우편주세요", -- [1]
			"징이", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"징이", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			39, -- [11]
			"Player-205-06407B08", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569305.448"] = {
			"안토 신화 정공 토 9-12///일 1-7 총 9탐  죽기/  풍/운무(힐스왑딜) or정복술 or 조회드 구합니다 우편 부탁드립니다", -- [1]
			"보엉이-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"보엉이", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			157, -- [11]
			"Player-205-064215F0", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569306.325"] = {
			"[안토 영웅 업손팟(깡손X)] 1:00 출발(N3/T20, 3탐이하) 선수(힐스왑딜1, 힐러1(신기제외)) 모십니다. [28/30]", -- [1]
			"마술사탕-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"마술사탕", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			160, -- [11]
			"Player-205-064FB101", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569315.358"] = {
			"안토 신화 부캐팟 3+@ 수요일 1시출발  최대5탐 힐러 (복술 운무) 스펙경험귓 ", -- [1]
			"게개-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"게개", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			187, -- [11]
			"Player-205-06709625", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569262.333"] = {
			"<<<<와우부주>>>>  마탑도전모드(전특성가능)/명성/싸움꾼조합/100-110렙업8시간걸림/전역퀘/수라마르이야기/군단길잡이,안전 고속 카톡 bubuxi친절상담", -- [1]
			"파티앱솔루트", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"파티앱솔루트", -- [5]
			"DND", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			28, -- [11]
			"Player-205-06F82317", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569281.021"] = {
			"안토 영웅 저녁6시반 가실  딜 힐 모십니다 노손팟 3/20 최대 3.5탐", -- [1]
			"딜같은소리하넹-아즈샤라", -- [2]
			"", -- [3]
			"5. 파티", -- [4]
			"딜같은소리하넹", -- [5]
			"", -- [6]
			0, -- [7]
			5, -- [8]
			"파티", -- [9]
			0, -- [10]
			81, -- [11]
			"Player-205-0557110C", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569272.161"] = {
			"[군단막공] [모이는대로~]  [살게신화5+사스즈인까지!] [2탐] [2만올분] [신기,회드,복술] 모십니당", -- [1]
			"정력왕군단-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"정력왕군단", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			48, -- [11]
			"Player-205-06F1A814", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569277.007"] = {
			"안토영웅올킬 업손팟 선수(딜전/고흑) 업손(복술or운무) 모셔요 27/30", -- [1]
			"Epic-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"Epic", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			66, -- [11]
			"Player-205-05FB2573", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569283.266"] = {
			"아..감솨합니당", -- [1]
			"김딩", -- [2]
			"", -- [3]
			"2. 거래 - 도시", -- [4]
			"김딩", -- [5]
			"", -- [6]
			2, -- [7]
			2, -- [8]
			"거래 - 도시", -- [9]
			0, -- [10]
			90, -- [11]
			"Player-205-06F7E0FC", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			true, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569304.945"] = {
			"안토신화 고정 일요일 6시 5탐 죽딜 도적 징기 악딜 법사 조드 냥꾼 정술 암사 힐스왑딜 신기 모셔요 템렙 유물 편지주세요 로그확인", -- [1]
			"남자곰임-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"남자곰임", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			153, -- [11]
			"Player-205-06BA83BF", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569275.007"] = {
			"안토 신화 신생고정 일/월 오후 9시-12시 (총6탐+킬각@) 함께하실 원딜 모십니다. 자세한 사항 귓/우편 주세요 오늘 첫신화예정", -- [1]
			"신비화", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"신비화", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			56, -- [11]
			"Player-205-06EDF961", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569274.564"] = {
			"안토영웅 화요일 오전11시(2만.20만-최대4탐) 가실 힐(신기),원딜3 모셔요.. 942+72+,영웅업적필 달초드림 문의주셔요 답변 쩌메 느림", -- [1]
			"고움", -- [2]
			"", -- [3]
			"5. 파티", -- [4]
			"고움", -- [5]
			"", -- [6]
			0, -- [7]
			5, -- [8]
			"파티", -- [9]
			0, -- [10]
			53, -- [11]
			"Player-205-063158D1", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569287.462"] = {
			"안토 일반 손님팟 가실 손님(판금,사슬,가죽)/선수전클 모십니다/all2만/1탐반", -- [1]
			"Berrek-아즈샤라", -- [2]
			"", -- [3]
			"5. 파티", -- [4]
			"Berrek", -- [5]
			"", -- [6]
			0, -- [7]
			5, -- [8]
			"파티", -- [9]
			0, -- [10]
			101, -- [11]
			"Player-205-028964A3", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569298.283"] = {
			":: 안토신화 :: 반고정모집, \"매주 목or월 오전10시\", 5탐, \"원딜(근딜풀), 힐러(복술)\", 착유경험귓이나우편주세요", -- [1]
			"징이-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"징이", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			135, -- [11]
			"Player-205-06407B08", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569311.961"] = {
			"[안토 영웅] 월요일 저녁8시 최대5탐 n2/t10 rms딜(분전//죽딜)//원딜(조드/정술/암사/흑마/냥꾼))// 힐솹딜1// 모십니다. 본캐업적인정 20/30", -- [1]
			"난푹기사-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"난푹기사", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			174, -- [11]
			"Player-205-05796F43", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569277.251"] = {
			"20일(수)저녁8시 4탐 살게7신화 헤딩팟 구인합니다. 공대장로그o최정예다수 3만올분/940,72+/탱1,힐1(복x),원딜// 최소5킬업적자 로그봅니다. 14/20", -- [1]
			"Feanaro", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"Feanaro", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			68, -- [11]
			"Player-205-06B23ECF", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569274.782"] = {
			"안토신화 고정 일요일 6시 5탐 죽딜 도적 징기 악딜 법사 조드 냥꾼 정술 암사 힐스왑딜 신기 모셔요 템렙 유물 편지주세요 로그확인", -- [1]
			"남자곰임-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"남자곰임", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			55, -- [11]
			"Player-205-06BA83BF", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569277.466"] = {
			"안토일반올킬 [4:30출발-2탐컷] 손님(판금,가죽), 업손(딜,힐) 모셔요. // 문의 귓 주세요 / n2t10시작 / 레이드중이라 답변늦어요 // 선수귓x", -- [1]
			"키오-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"키오", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			72, -- [11]
			"Player-205-061F4677", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569279.664"] = {
			"안토 영웅 (수) 오전10시 시작 최대 4탐 N3T10 골팟 가실// 근딜(전클) // 원딜 (전클) //힐러 전클)  모십니다. 템렙경험 귓주세여", -- [1]
			"알썽", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"알썽", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			78, -- [11]
			"Player-205-05C77A8F", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569324.966"] = {
			"안토신화 고정 일요일 6시 5탐 죽딜 도적 징기 악딜 법사 조드 냥꾼 정술 암사 힐스왑딜 신기 모셔요 템렙 유물 편지주세요 로그확인", -- [1]
			"남자곰임-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"남자곰임", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			216, -- [11]
			"Player-205-06BA83BF", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569265.131"] = {
			"안토신화 고정 일요일 6시 5탐 죽딜 도적 징기 악딜 법사 조드 냥꾼 정술 암사 힐스왑딜 신기 모셔요 템렙 유물 편지주세요 로그확인", -- [1]
			"남자곰임", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"남자곰임", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			32, -- [11]
			"Player-205-06BA83BF", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569321.129"] = {
			"안토신화3+@ 1시출발  4~5탐 가실 정술/복술 구해요.  18/20", -- [1]
			"스티븐훔바-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"스티븐훔바", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			203, -- [11]
			"Player-205-05ADC4A3", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569296.023"] = {
			"안토 영웅 저녁6시반 가실  딜 힐 모십니다 노손팟 3/20 최대 3.5탐", -- [1]
			"딜같은소리하넹-아즈샤라", -- [2]
			"", -- [3]
			"5. 파티", -- [4]
			"딜같은소리하넹", -- [5]
			"", -- [6]
			0, -- [7]
			5, -- [8]
			"파티", -- [9]
			0, -- [10]
			124, -- [11]
			"Player-205-0557110C", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569329.745"] = {
			"안토 신화 월,화 2일일정 8시출발 4탐 2만올분 신기 모셔봅니다.", -- [1]
			"단스톰후프-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"단스톰후프", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			230, -- [11]
			"Player-205-06E91C94", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569302.249"] = {
			"<<<<와우부주>>>>  마탑도전모드(전특성가능)/명성/싸움꾼조합/100-110렙업8시간걸림/전역퀘/수라마르이야기/군단길잡이,안전 고속 카톡 bubuxi친절상담", -- [1]
			"파티앱솔루트-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"파티앱솔루트", -- [5]
			"DND", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			148, -- [11]
			"Player-205-06F82317", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569321.936"] = {
			"[살게7신화확고팟]  오후2시    복술or회드    /   템렙, 유물, 해당캐릭터업적 귓주세요  /  N2T5올분, 2탐쫑", -- [1]
			"고요한드루-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"고요한드루", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			207, -- [11]
			"Player-205-06DD1793", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569261.288"] = {
			"안토신화3+@ 1시출발  4~5탐 가실 정술/복술 구해요.  18/20", -- [1]
			"스티븐훔바", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"스티븐훔바", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			19, -- [11]
			"Player-205-05ADC4A3", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569284.693"] = {
			"안토 5신화 1시출발 3/10올분 탱커1 구합니다. 귓말주세요 로그참조 착유경험귓", -- [1]
			"간격-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"간격", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			92, -- [11]
			"Player-205-026C5C76", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569268.814"] = {
			"안토 영웅 올킬 업손팟 N3/T15  월요일 저녁8시  최대 4탐 /  업손 원딜러구인중 선수풀(귓x) / 귓말주세요. 레이드중이라 답변 느립니다. ", -- [1]
			"돈먹", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"돈먹", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			41, -- [11]
			"Player-205-049EE770", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569282.201"] = {
			"[군단막공] [모이는대로~]  [살게신화5+사스즈인까지!] [2탐] [2만올분] [신기,회드,복술] 모십니당", -- [1]
			"정력왕군단-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"정력왕군단", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			87, -- [11]
			"Player-205-06F1A814", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569314.965"] = {
			"안토신화 고정 일요일 6시 5탐 죽딜 도적 징기 악딜 법사 조드 냥꾼 정술 암사 힐스왑딜 신기 모셔요 템렙 유물 편지주세요 로그확인", -- [1]
			"남자곰임-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"남자곰임", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			185, -- [11]
			"Player-205-06BA83BF", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569272.385"] = {
			"Rt://안토영웅[30인.노손]_오늘2시_[정술]_[29/30]_[N3만/T10만][4탐][템렙/경험/유물귓필][답변느림]", -- [1]
			"파흑아스", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"파흑아스", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			51, -- [11]
			"Player-205-04BF37AC", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569258.371"] = {
			"오늘 오후8시출 안토신화 4넴5넴(하사벨,이오나) 트라이팟 (3개잡힌팟)  3.5탐 딜2 모십니다", -- [1]
			"헬로스타", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"헬로스타", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			16, -- [11]
			"Player-205-058AF05A", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569328.281"] = {
			":: 안토신화 :: 반고정모집, \"매주 목or월 오전10시\", 5탐, \"원딜(근딜풀), 힐러(복술)\", 착유경험귓이나우편주세요", -- [1]
			"징이-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"징이", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			223, -- [11]
			"Player-205-06407B08", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569257.485"] = {
			"[★안토 영웅 업손팟☆] 오늘 낮 2시 업손1(고술,정술,냥꾼) 구합니다. 2.5~4탐이내 올클 N3/T20시작 문의귓주세요. 선수귓x 29/30", -- [1]
			"알레쿠퍼", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"알레쿠퍼", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			12, -- [11]
			"Player-205-059E7497", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569334.892"] = {
			"안토신화 고정 일요일 6시 5탐 죽딜 도적 징기 악딜 법사 조드 냥꾼 정술 암사 힐스왑딜 신기 모셔요 템렙 유물 편지주세요 로그확인", -- [1]
			"남자곰임-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"남자곰임", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			245, -- [11]
			"Player-205-06BA83BF", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569332.274"] = {
			"Rt://안토영웅[30인.노손]_오늘2시_[정술]_[29/30]_[N3만/T10만][4탐][템렙/경험/유물귓필][답변느림]", -- [1]
			"파흑아스-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"파흑아스", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			239, -- [11]
			"Player-205-04BF37AC", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569275.413"] = {
			"안토 영웅올킬 출발 3~4탐  업손 ( 원딜 힐 ) 모셔요  28/30 ", -- [1]
			"Emfnglfvl-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"Emfnglfvl", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			59, -- [11]
			"Player-205-05D6D3CA", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569326.911"] = {
			"안토 신화 이틀 일정 금(오후8시~12시) 토(오후8시~1시) // 원딜 (고흑), 힐(신사or수사or운무)", -- [1]
			"쌈장왕청정원-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"쌈장왕청정원", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			220, -- [11]
			"Player-205-05776144", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569276.533"] = {
			"[안토 영웅 업손팟(깡손X)] 1:00 출발(N3/T20, 3탐이하) 선수(힐스왑딜1, 힐러1(신기제외)) 모십니다. [28/30]", -- [1]
			"마술사탕", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"마술사탕", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			64, -- [11]
			"Player-205-064FB101", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569266.265"] = {
			"안토 영웅 저녁6시반 가실  딜 힐 모십니다 노손팟 3/20 최대 3.5탐", -- [1]
			"딜같은소리하넹", -- [2]
			"", -- [3]
			"5. 파티", -- [4]
			"딜같은소리하넹", -- [5]
			"", -- [6]
			0, -- [7]
			5, -- [8]
			"파티", -- [9]
			0, -- [10]
			33, -- [11]
			"Player-205-0557110C", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569334.777"] = {
			"안토 신화 신생고정 일/월 오후 9시-12시 (총6탐+킬각@) 함께하실 원딜 모십니다. 자세한 사항 귓/우편 주세요 오늘 첫신화예정", -- [1]
			"신비화-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"신비화", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			243, -- [11]
			"Player-205-06EDF961", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569331.051"] = {
			"안토 영웅 저녁6시반 가실  딜 힐 모십니다 노손팟 3/20 최대 3.5탐", -- [1]
			"딜같은소리하넹-아즈샤라", -- [2]
			"", -- [3]
			"5. 파티", -- [4]
			"딜같은소리하넹", -- [5]
			"", -- [6]
			0, -- [7]
			5, -- [8]
			"파티", -- [9]
			0, -- [10]
			234, -- [11]
			"Player-205-0557110C", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569330.777"] = {
			"안토영웅갠룻4탐확고팟 딜 힐 딜스왑힐 아그라확고딜죽1구합니다. 영웅로그봅니다.", -- [1]
			"배트킹", -- [2]
			"", -- [3]
			"5. 파티", -- [4]
			"배트킹", -- [5]
			"", -- [6]
			0, -- [7]
			5, -- [8]
			"파티", -- [9]
			0, -- [10]
			232, -- [11]
			"Player-205-06EBB0EA", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569309.273"] = {
			"안토 영웅 (수) 오전10시 시작 최대 4탐 N3T10 골팟 가실// 근딜(전클) // 원딜 (전클) //힐러 전클)  모십니다. 템렙경험 귓주세여", -- [1]
			"알썽-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"알썽", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			169, -- [11]
			"Player-205-05C77A8F", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569297.468"] = {
			"안토일반올킬 [4:30출발-2탐컷] 손님(판금,가죽), 업손(딜,힐) 모셔요. // 문의 귓 주세요 / n2t10시작 / 레이드중이라 답변늦어요 // 선수귓x", -- [1]
			"키오-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"키오", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			131, -- [11]
			"Player-205-061F4677", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569328.68"] = {
			"안토 영웅 올킬 업손팟 N3/T15  월요일 저녁8시  최대 4탐 /  업손 원딜러구인중 선수풀(귓x) / 귓말주세요. 레이드중이라 답변 느립니다. ", -- [1]
			"돈먹-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"돈먹", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			226, -- [11]
			"Player-205-049EE770", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569322.158"] = {
			"[군단막공] [모이는대로~]  [살게신화5+사스즈인까지!] [2탐] [2만올분] [신기,회드,복술] 모십니당", -- [1]
			"정력왕군단-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"정력왕군단", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			211, -- [11]
			"Player-205-06F1A814", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569291.113"] = {
			"안토신화3+@ 1시출발  4~5탐 가실 정술/복술 구해요.  18/20", -- [1]
			"스티븐훔바-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"스티븐훔바", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			109, -- [11]
			"Player-205-05ADC4A3", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569314.719"] = {
			"안토 5신화 1시출발 3/10올분 탱커1 구합니다. 귓말주세요 로그참조 착유경험귓", -- [1]
			"간격-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"간격", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			182, -- [11]
			"Player-205-026C5C76", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569275.628"] = {
			"안토 신화 정공 토 9-12///일 1-7 총 9탐  죽기/  풍/운무(힐스왑딜) or정복술 or 조회드 구합니다 우편 부탁드립니다", -- [1]
			"보엉이", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"보엉이", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			62, -- [11]
			"Player-205-064215F0", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569292.168"] = {
			"Rt://안토영웅[30인.노손]_오늘2시_[정술]_[29/30]_[N3만/T10만][4탐][템렙/경험/유물귓필][답변느림]", -- [1]
			"파흑아스-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"파흑아스", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			116, -- [11]
			"Player-205-04BF37AC", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569257.702"] = {
			"안토일반올킬 [4:30출발-2탐컷] 손님(판금,가죽), 업손(딜,힐) 모셔요. // 문의 귓 주세요 / n2t10시작 / 레이드중이라 답변늦어요 // 선수귓x", -- [1]
			"키오", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"키오", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			15, -- [11]
			"Player-205-061F4677", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569319.467"] = {
			"안토영웅 화요일 오전11시(2만.20만-최대4탐) 가실 힐(신기),원딜3 모셔요.. 942+72+,영웅업적필 달초드림 문의주셔요 답변 쩌메 느림", -- [1]
			"고움-아즈샤라", -- [2]
			"", -- [3]
			"5. 파티", -- [4]
			"고움", -- [5]
			"", -- [6]
			0, -- [7]
			5, -- [8]
			"파티", -- [9]
			0, -- [10]
			198, -- [11]
			"Player-205-063158D1", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569318.175"] = {
			"오늘 오후8시출 안토신화 4넴5넴(하사벨,이오나) 트라이팟 (3개잡힌팟)  3.5탐 딜2 모십니다", -- [1]
			"헬로스타-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"헬로스타", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			196, -- [11]
			"Player-205-058AF05A", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569317.478"] = {
			"안토일반올킬 [4:30출발-2탐컷] 손님(판금,가죽), 업손(딜,힐) 모셔요. // 문의 귓 주세요 / n2t10시작 / 레이드중이라 답변늦어요 // 선수귓x", -- [1]
			"키오-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"키오", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			194, -- [11]
			"Player-205-061F4677", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569299.719"] = {
			"안토 신화 월,화 2일일정 8시출발 4탐 2만올분 신기 모셔봅니다.", -- [1]
			"단스톰후프-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"단스톰후프", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			139, -- [11]
			"Player-205-06E91C94", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569312.256"] = {
			"Rt://안토영웅[30인.노손]_오늘2시_[정술]_[29/30]_[N3만/T10만][4탐][템렙/경험/유물귓필][답변느림]", -- [1]
			"파흑아스-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"파흑아스", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			178, -- [11]
			"Player-205-04BF37AC", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569264.344"] = {
			"경매장 파리바게뜨 도안은npc가 팜", -- [1]
			"술사킹카", -- [2]
			"", -- [3]
			"2. 거래 - 도시", -- [4]
			"술사킹카", -- [5]
			"", -- [6]
			2, -- [7]
			2, -- [8]
			"거래 - 도시", -- [9]
			0, -- [10]
			30, -- [11]
			"Player-205-06573FE2", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			true, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569281.955"] = {
			"[살게7신화확고팟]  오후2시    복술or회드    /   템렙, 유물, 해당캐릭터업적 귓주세요  /  N2T5올분, 2탐쫑", -- [1]
			"고요한드루-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"고요한드루", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			83, -- [11]
			"Player-205-06DD1793", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569307.037"] = {
			"안토영웅올킬 업손팟 선수(딜전/고흑) 업손(복술or운무) 모셔요 27/30", -- [1]
			"Epic-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"Epic", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			164, -- [11]
			"Player-205-05FB2573", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569296.884"] = {
			"안토 신화 이틀 일정 금(오후8시~12시) 토(오후8시~1시) // 원딜 (고흑), 힐(신사or수사or운무)", -- [1]
			"쌈장왕청정원-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"쌈장왕청정원", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			127, -- [11]
			"Player-205-05776144", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569284.942"] = {
			"안토신화 고정 일요일 6시 5탐 죽딜 도적 징기 악딜 법사 조드 냥꾼 정술 암사 힐스왑딜 신기 모셔요 템렙 유물 편지주세요 로그확인", -- [1]
			"남자곰임-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"남자곰임", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			94, -- [11]
			"Player-205-06BA83BF", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569301.949"] = {
			"[살게7신화확고팟]  오후2시    복술or회드    /   템렙, 유물, 해당캐릭터업적 귓주세요  /  N2T5올분, 2탐쫑", -- [1]
			"고요한드루-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"고요한드루", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			144, -- [11]
			"Player-205-06DD1793", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569295.061"] = {
			"어둠의해안 어케가는지 아시는분! ㅜㅜ", -- [1]
			"매혹적잉", -- [2]
			"", -- [3]
			"1. 공개 - 오그리마", -- [4]
			"매혹적잉", -- [5]
			"", -- [6]
			1, -- [7]
			1, -- [8]
			"공개 - 오그리마", -- [9]
			0, -- [10]
			122, -- [11]
			"Player-205-06F76587", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			true, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569266.897"] = {
			"안토 신화 이틀 일정 금(오후8시~12시) 토(오후8시~1시) // 원딜 (고흑), 힐(신사or수사or운무)", -- [1]
			"쌈장왕청정원-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"쌈장왕청정원", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			36, -- [11]
			"Player-205-05776144", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569294.845"] = {
			"안토신화 고정 일요일 6시 5탐 죽딜 도적 징기 악딜 법사 조드 냥꾼 정술 암사 힐스왑딜 신기 모셔요 템렙 유물 편지주세요 로그확인", -- [1]
			"남자곰임-아즈샤라", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"남자곰임", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			121, -- [11]
			"Player-205-06BA83BF", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
		["1513569262.126"] = {
			"[살게7신화확고팟]  오후2시    복술or회드    /   템렙, 유물, 해당캐릭터업적 귓주세요  /  N2T5올분, 2탐쫑", -- [1]
			"고요한드루", -- [2]
			"", -- [3]
			"4. 파티찾기", -- [4]
			"고요한드루", -- [5]
			"", -- [6]
			26, -- [7]
			4, -- [8]
			"파티찾기", -- [9]
			0, -- [10]
			21, -- [11]
			"Player-205-06DD1793", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			nil, -- [18]
			nil, -- [19]
			"CHAT_MSG_CHANNEL", -- [20]
		},
	},
}
