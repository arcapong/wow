
InvenRaidFrames3CharDB = {
	["clickCasting"] = {
		{
			["shift-2"] = "macro__전부",
			["2"] = "spell__눈속임",
		}, -- [1]
		{
			["2"] = "spell__눈속임",
		}, -- [2]
		{
		}, -- [3]
		{
		}, -- [4]
	},
	["class"] = "HUNTER",
	["spellTimer"] = {
		{
			["name"] = "눈속임",
			["scale"] = 1,
			["pos"] = "BOTTOMLEFT",
			["display"] = 1,
			["use"] = 1,
		}, -- [1]
		{
			["display"] = 1,
			["pos"] = "BOTTOM",
			["scale"] = 1,
			["use"] = 0,
		}, -- [2]
		{
			["display"] = 1,
			["pos"] = "BOTTOMRIGHT",
			["scale"] = 1,
			["use"] = 0,
		}, -- [3]
		{
			["display"] = 1,
			["pos"] = "LEFT",
			["scale"] = 1,
			["use"] = 0,
		}, -- [4]
		{
			["display"] = 1,
			["pos"] = "RIGHT",
			["scale"] = 1,
			["use"] = 0,
		}, -- [5]
		{
			["display"] = 1,
			["pos"] = "TOPLEFT",
			["scale"] = 1,
			["use"] = 0,
		}, -- [6]
		{
			["display"] = 1,
			["pos"] = "TOP",
			["scale"] = 1,
			["use"] = 0,
		}, -- [7]
		{
			["display"] = 1,
			["pos"] = "TOPRIGHT",
			["scale"] = 1,
			["use"] = 0,
		}, -- [8]
	},
	["classBuff2"] = {
	},
}
