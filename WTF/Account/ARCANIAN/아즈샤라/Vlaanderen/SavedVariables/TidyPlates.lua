
TidyPlatesOptions = {
	["PrimaryProfile"] = "Damage",
	["SecondSpecProfile"] = "Damage",
	["FriendlyAutomation"] = "No Automation",
	["EnemyAutomation"] = "No Automation",
	["FourthSpecProfile"] = "Damage",
	["ThirdSpecProfile"] = "Damage",
	["ForceBlizzardFont"] = false,
	["DisableCastBars"] = false,
	["CompatibilityMode"] = false,
	["ActiveProfile"] = "Damage",
	["ActiveTheme"] = "Neon",
	["WelcomeShown"] = true,
	["FirstSpecProfile"] = "Damage",
}
