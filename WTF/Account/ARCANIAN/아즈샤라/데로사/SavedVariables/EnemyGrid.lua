
EnemyGridDBChr = {
	["KeyBinds"] = {
		[577] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
			{
				["actiontext"] = "",
				["key"] = "type3",
				["action"] = "_taunt",
			}, -- [3]
		},
		[581] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
			{
				["actiontext"] = "",
				["key"] = "type3",
				["action"] = "_taunt",
			}, -- [3]
		},
	},
	["spellRangeCheck"] = {
		[577] = "안광",
		[581] = "고문",
	},
	["debuffsBanned"] = {
	},
	["specEnabled"] = {
		[577] = false,
		[581] = false,
	},
}
