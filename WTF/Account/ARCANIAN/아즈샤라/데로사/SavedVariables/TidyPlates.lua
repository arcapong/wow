
TidyPlatesOptions = {
	["PrimaryProfile"] = "Damage",
	["DisableCastBars"] = false,
	["SecondSpecProfile"] = "Tank",
	["FriendlyAutomation"] = "No Automation",
	["ForceBlizzardFont"] = false,
	["SecondaryTheme"] = "Neon",
	["CompatibilityMode"] = false,
	["ThirdSpecProfile"] = "Damage",
	["FourthSpecProfile"] = "Damage",
	["SecondaryProfile"] = "Damage",
	["PrimaryTheme"] = "Neon",
	["ActiveTheme"] = "Neon",
	["ActiveProfile"] = "Damage",
	["EnemyAutomation"] = "No Automation",
	["WelcomeShown"] = true,
	["FirstSpecProfile"] = "Damage",
}
