
InvenRaidFrames3CharDB = {
	["clickCasting"] = {
		{
			["shift-2"] = "spell__영원의 숨결",
			["2"] = "spell__영혼석",
		}, -- [1]
		{
			["2"] = "spell__영혼석",
		}, -- [2]
		{
			["shift-2"] = "spell__영원의 숨결",
			["2"] = "spell__영혼석",
		}, -- [3]
		{
		}, -- [4]
	},
	["class"] = "WARLOCK",
	["spellTimer"] = {
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "BOTTOMLEFT",
		}, -- [1]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "BOTTOM",
		}, -- [2]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "BOTTOMRIGHT",
		}, -- [3]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "LEFT",
		}, -- [4]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "RIGHT",
		}, -- [5]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOPLEFT",
		}, -- [6]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOP",
		}, -- [7]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOPRIGHT",
		}, -- [8]
	},
	["classBuff2"] = {
	},
}
