
EnemyGridDBChr = {
	["KeyBinds"] = {
		[266] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
		},
		[267] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
		},
		[265] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
		},
	},
	["spellRangeCheck"] = {
		[266] = "어둠의 화살",
		[267] = "혼돈의 화살",
		[265] = "고통",
	},
	["debuffsBanned"] = {
	},
	["specEnabled"] = {
		[266] = false,
		[267] = false,
		[265] = true,
	},
}
