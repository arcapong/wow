
InvenRaidFrames3CharDB = {
	["clickCasting"] = {
		{
			["3"] = "spell__빛의 봉화",
			["shift-1"] = "spell__자유의 축복",
			["shift-2"] = "spell__보호의 축복",
			["2"] = "spell__정화",
			["shift-3"] = "spell__신념의 봉화",
		}, -- [1]
		{
			["shift-2"] = "spell__보호의 축복",
			["shift-3"] = "spell__희생의 축복",
			["shift-1"] = "spell__자유의 축복",
			["2"] = "spell__독소 정화",
		}, -- [2]
		{
		}, -- [3]
		{
		}, -- [4]
	},
	["class"] = "PALADIN",
	["spellTimer"] = {
		{
			["name"] = "빛의 봉화,신념의 봉화",
			["scale"] = 1,
			["use"] = 3,
			["display"] = 1,
			["pos"] = "BOTTOMLEFT",
		}, -- [1]
		{
			["name"] = "보호의 축복,자유의 축복,희생의 축복,",
			["scale"] = 1,
			["use"] = 1,
			["display"] = 1,
			["pos"] = "BOTTOM",
		}, -- [2]
		{
			["name"] = "신념 수여",
			["scale"] = 1,
			["use"] = 1,
			["display"] = 1,
			["pos"] = "BOTTOMRIGHT",
		}, -- [3]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "LEFT",
		}, -- [4]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "RIGHT",
		}, -- [5]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOPLEFT",
		}, -- [6]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOP",
		}, -- [7]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOPRIGHT",
		}, -- [8]
	},
	["classBuff2"] = {
	},
}
