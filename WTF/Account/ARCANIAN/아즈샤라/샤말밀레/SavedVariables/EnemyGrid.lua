
EnemyGridDBChr = {
	["KeyBinds"] = {
		[70] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
			{
				["actiontext"] = "",
				["key"] = "type3",
				["action"] = "_taunt",
			}, -- [3]
		},
		[65] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_dispel",
			}, -- [2]
		},
		[66] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
			{
				["actiontext"] = "",
				["key"] = "type3",
				["action"] = "_taunt",
			}, -- [3]
		},
	},
	["spellRangeCheck"] = {
		[70] = "집행의 손길",
		[65] = "집행의 손길",
		[66] = "집행의 손길",
	},
	["debuffsBanned"] = {
	},
	["specEnabled"] = {
		[70] = false,
		[65] = false,
		[66] = false,
	},
}
