
TidyPlatesOptions = {
	["PrimaryProfile"] = "Healer",
	["SecondSpecProfile"] = "Tank",
	["FriendlyAutomation"] = "No Automation",
	["EnemyAutomation"] = "No Automation",
	["FourthSpecProfile"] = "Damage",
	["ThirdSpecProfile"] = "Damage",
	["DisableCastBars"] = false,
	["ActiveTheme"] = "Neon",
	["ForceBlizzardFont"] = false,
	["WelcomeShown"] = true,
	["FirstSpecProfile"] = "Healer",
}
