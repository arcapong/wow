
DBMEmeraldNightmare_SavedStats = {
	["EmeraldNightmareTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1726"] = {
		["heroicLastTime"] = 125.891999999993,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 125.891999999993,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 3,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 3,
		["challengePulls"] = 0,
	},
	["1703"] = {
		["heroicLastTime"] = 97.1169999998529,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 2,
		["heroic25Pulls"] = 0,
		["challengePulls"] = 0,
		["lfr25Pulls"] = 2,
		["normal25Pulls"] = 0,
		["heroicBestTime"] = 97.1169999998529,
		["normalKills"] = 0,
		["mythicPulls"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 3,
		["normal25Kills"] = 0,
		["heroicPulls"] = 3,
		["timewalkerKills"] = 0,
		["lfr25LastTime"] = 166.054000000004,
		["lfr25BestTime"] = 166.054000000004,
	},
	["1667"] = {
		["heroicLastTime"] = 74.0849999999628,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 74.0849999999628,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 3,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 3,
		["challengePulls"] = 0,
	},
	["1738"] = {
		["heroicLastTime"] = 129.300999999978,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 2,
		["heroic25Pulls"] = 0,
		["challengePulls"] = 0,
		["lfr25Pulls"] = 3,
		["normal25Pulls"] = 0,
		["heroicBestTime"] = 129.300999999978,
		["normalKills"] = 0,
		["mythicPulls"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 3,
		["normal25Kills"] = 0,
		["heroicPulls"] = 3,
		["timewalkerKills"] = 0,
		["lfr25LastTime"] = 142.261999999988,
		["lfr25BestTime"] = 142.261999999988,
	},
	["1704"] = {
		["heroicLastTime"] = 68.1959999999963,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 68.1959999999963,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 3,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 3,
		["challengePulls"] = 0,
	},
	["1750"] = {
		["heroicLastTime"] = 64.1119999999646,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroicBestTime"] = 64.1119999999646,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 3,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 3,
		["challengePulls"] = 0,
	},
	["1744"] = {
		["heroicLastTime"] = 94.5930000001099,
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 2,
		["heroic25Pulls"] = 0,
		["challengePulls"] = 0,
		["lfr25Pulls"] = 2,
		["normal25Pulls"] = 0,
		["heroicBestTime"] = 92.7190000000119,
		["normalKills"] = 0,
		["mythicPulls"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 3,
		["normal25Kills"] = 0,
		["heroicPulls"] = 3,
		["timewalkerKills"] = 0,
		["lfr25LastTime"] = 163.956999999995,
		["lfr25BestTime"] = 163.956999999995,
	},
}
