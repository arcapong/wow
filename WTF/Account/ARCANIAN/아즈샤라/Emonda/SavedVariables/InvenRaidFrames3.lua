
InvenRaidFrames3CharDB = {
	["clickCasting"] = {
		{
			["shift-2"] = "spell__수면 걷기",
			["2"] = "spell__영혼 정화",
		}, -- [1]
		{
			["shift-2"] = "spell__수면 걷기",
			["2"] = "spell__영혼 정화",
		}, -- [2]
		{
			["shift-2"] = "spell__수면 걷기",
			["2"] = "spell__영혼 정화",
		}, -- [3]
		{
		}, -- [4]
	},
	["class"] = "SHAMAN",
	["spellTimer"] = {
		{
			["name"] = "성난 해일",
			["scale"] = 1,
			["use"] = 1,
			["display"] = 1,
			["pos"] = "BOTTOMLEFT",
		}, -- [1]
		{
			["name"] = ",",
			["scale"] = 1,
			["use"] = 1,
			["display"] = 1,
			["pos"] = "BOTTOM",
		}, -- [2]
		{
			["name"] = "",
			["scale"] = 1,
			["use"] = 1,
			["display"] = 1,
			["pos"] = "BOTTOMRIGHT",
		}, -- [3]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "LEFT",
		}, -- [4]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "RIGHT",
		}, -- [5]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOPLEFT",
		}, -- [6]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOP",
		}, -- [7]
		{
			["display"] = 1,
			["use"] = 0,
			["scale"] = 1,
			["pos"] = "TOPRIGHT",
		}, -- [8]
	},
	["classBuff2"] = {
	},
}
