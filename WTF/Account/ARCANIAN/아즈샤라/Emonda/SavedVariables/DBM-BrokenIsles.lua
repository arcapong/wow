
DBMBrokenIsles_SavedStats = {
	["1885"] = {
		["normalPulls"] = 2,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 47.4539999999979,
		["normalKills"] = 2,
		["normalBestTime"] = 47.4539999999979,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1763"] = {
		["normalPulls"] = 3,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 3,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1795"] = {
		["normalPulls"] = 2,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 121.304999999935,
		["normalKills"] = 1,
		["normalBestTime"] = 121.304999999935,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1790"] = {
		["normalPulls"] = 2,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 73.4550000000745,
		["normalKills"] = 2,
		["normalBestTime"] = 73.4550000000745,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1884"] = {
		["normalPulls"] = 3,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 83.5679999999702,
		["normalKills"] = 3,
		["mythicPulls"] = 0,
		["normalBestTime"] = 83.5679999999702,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["normal25Kills"] = 0,
		["heroicPulls"] = 0,
		["timewalkerKills"] = 0,
		["heroic25Kills"] = 0,
		["challengePulls"] = 0,
	},
	["1883"] = {
		["normalPulls"] = 3,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 76.9969999999739,
		["normalKills"] = 3,
		["mythicPulls"] = 0,
		["heroic25Kills"] = 0,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["normal25Kills"] = 0,
		["heroicPulls"] = 0,
		["timewalkerKills"] = 0,
		["normalBestTime"] = 76.9969999999739,
		["challengePulls"] = 0,
	},
	["1783"] = {
		["normalPulls"] = 1,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 1,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1756"] = {
		["normalPulls"] = 3,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 2,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1769"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1749"] = {
		["normalPulls"] = 1,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 1,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1796"] = {
		["normalPulls"] = 6,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 1,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1774"] = {
		["normalPulls"] = 2,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 211.448000000004,
		["normalKills"] = 2,
		["heroic25Kills"] = 0,
		["normalBestTime"] = 211.448000000004,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1956"] = {
		["normalPulls"] = 4,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalLastTime"] = 51.9300000000512,
		["normalKills"] = 4,
		["heroic25Kills"] = 0,
		["normalBestTime"] = 51.9300000000512,
		["heroicKills"] = 0,
		["timewalkerPulls"] = 0,
		["mythicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["heroicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1789"] = {
		["normalPulls"] = 4,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 4,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1770"] = {
		["normalPulls"] = 2,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 1,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
}
