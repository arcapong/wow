
EnemyGridDBChr = {
	["KeyBinds"] = {
		[263] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
		},
		[264] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
		},
		[262] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
		},
	},
	["spellRangeCheck"] = {
		[263] = "사술",
		[264] = "번개 화살",
		[262] = "번개 화살",
	},
	["debuffsBanned"] = {
	},
	["specEnabled"] = {
		[263] = false,
		[264] = false,
		[262] = false,
	},
}
