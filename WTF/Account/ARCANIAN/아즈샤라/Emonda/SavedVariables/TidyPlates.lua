
TidyPlatesOptions = {
	["PrimaryProfile"] = "Damage",
	["DisableCastBars"] = false,
	["SecondSpecProfile"] = "Damage",
	["FriendlyAutomation"] = "No Automation",
	["ForceBlizzardFont"] = false,
	["SecondaryTheme"] = "Neon",
	["CompatibilityMode"] = false,
	["PrimaryTheme"] = "Neon",
	["FourthSpecProfile"] = "Damage",
	["SecondaryProfile"] = "Damage",
	["EnemyAutomation"] = "No Automation",
	["ActiveTheme"] = "Neon",
	["ActiveProfile"] = "Damage",
	["ThirdSpecProfile"] = "Healer",
	["WelcomeShown"] = true,
	["FirstSpecProfile"] = "Damage",
}
