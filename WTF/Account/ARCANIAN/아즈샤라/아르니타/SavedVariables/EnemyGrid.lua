
EnemyGridDBChr = {
	["KeyBinds"] = {
		[260] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
		},
		[261] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
		},
		[259] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
		},
	},
	["spellRangeCheck"] = {
		[260] = "권총 사격",
		[261] = "그림자 밟기",
		[259] = "독 칼",
	},
	["debuffsBanned"] = {
	},
	["specEnabled"] = {
		[260] = true,
		[261] = true,
		[259] = true,
	},
}
