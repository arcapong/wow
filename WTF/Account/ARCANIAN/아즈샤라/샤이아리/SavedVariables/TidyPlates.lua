
TidyPlatesOptions = {
	["PrimaryProfile"] = "Healer",
	["ForceBlizzardFont"] = false,
	["SecondSpecProfile"] = "Healer",
	["FriendlyAutomation"] = "No Automation",
	["DisableCastBars"] = false,
	["SecondaryTheme"] = "Neon",
	["CompatibilityMode"] = false,
	["PrimaryTheme"] = "Neon",
	["FourthSpecProfile"] = "Damage",
	["SecondaryProfile"] = "Damage",
	["ThirdSpecProfile"] = "Damage",
	["ActiveProfile"] = "Damage",
	["ActiveTheme"] = "Neon",
	["EnemyAutomation"] = "No Automation",
	["WelcomeShown"] = true,
	["FirstSpecProfile"] = "Healer",
}
