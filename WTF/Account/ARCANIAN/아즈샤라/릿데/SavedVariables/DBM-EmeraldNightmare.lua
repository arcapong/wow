
DBMEmeraldNightmare_SavedStats = {
	["EmeraldNightmareTrash"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 0,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 0,
		["normal25Pulls"] = 0,
		["normalKills"] = 0,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1726"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 1,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 1,
		["normal25Pulls"] = 0,
		["lfr25BestTime"] = 78.954000000027,
		["normalKills"] = 0,
		["lfr25LastTime"] = 78.954000000027,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1750"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 1,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 1,
		["normal25Pulls"] = 0,
		["lfr25BestTime"] = 55.5489999999991,
		["normalKills"] = 0,
		["lfr25LastTime"] = 55.5489999999991,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1667"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 1,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 1,
		["normal25Pulls"] = 0,
		["lfr25BestTime"] = 57.2210000000196,
		["normalKills"] = 0,
		["lfr25LastTime"] = 57.2210000000196,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1703"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 1,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 1,
		["normal25Pulls"] = 0,
		["lfr25BestTime"] = 76.405000000028,
		["normalKills"] = 0,
		["lfr25LastTime"] = 76.405000000028,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1704"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 1,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 1,
		["normal25Pulls"] = 0,
		["lfr25BestTime"] = 59.8620000000228,
		["normalKills"] = 0,
		["lfr25LastTime"] = 59.8620000000228,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1738"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 1,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 1,
		["normal25Pulls"] = 0,
		["lfr25BestTime"] = 119.201000000001,
		["normalKills"] = 0,
		["lfr25LastTime"] = 119.201000000001,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
	["1744"] = {
		["normalPulls"] = 0,
		["challengeKills"] = 0,
		["challengeBestRank"] = 0,
		["mythicKills"] = 0,
		["lfr25Kills"] = 1,
		["heroic25Pulls"] = 0,
		["lfr25Pulls"] = 1,
		["normal25Pulls"] = 0,
		["lfr25BestTime"] = 76.4490000000224,
		["normalKills"] = 0,
		["lfr25LastTime"] = 76.4490000000224,
		["heroic25Kills"] = 0,
		["timewalkerPulls"] = 0,
		["heroicKills"] = 0,
		["heroicPulls"] = 0,
		["normal25Kills"] = 0,
		["timewalkerKills"] = 0,
		["mythicPulls"] = 0,
		["challengePulls"] = 0,
	},
}
