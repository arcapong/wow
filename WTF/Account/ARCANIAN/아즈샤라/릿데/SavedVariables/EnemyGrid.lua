
EnemyGridDBChr = {
	["KeyBinds"] = {
		[73] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
			{
				["actiontext"] = "",
				["key"] = "type3",
				["action"] = "_taunt",
			}, -- [3]
		},
		[71] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
			{
				["actiontext"] = "",
				["key"] = "type3",
				["action"] = "_taunt",
			}, -- [3]
		},
		[72] = {
			{
				["actiontext"] = "",
				["key"] = "type1",
				["action"] = "_target",
			}, -- [1]
			{
				["actiontext"] = "",
				["key"] = "type2",
				["action"] = "_interrupt",
			}, -- [2]
			{
				["actiontext"] = "",
				["key"] = "type3",
				["action"] = "_taunt",
			}, -- [3]
		},
	},
	["specEnabled"] = {
		[73] = false,
		[71] = false,
		[72] = false,
	},
	["debuffsBanned"] = {
	},
	["spellRangeCheck"] = {
		[73] = "도발",
		[71] = "돌진",
		[72] = "돌진",
	},
}
